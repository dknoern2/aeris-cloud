package com.cars.platform.util;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class JavscriptHelperTest {

	@Test
	public void testToArray() {

		ArrayList<String> list = new ArrayList<String>();

		list.add("a");
		list.add("b");
		list.add("c");

		String s = JavascriptHelper.serialize(list);
		assertEquals("[\"a\",\"b\",\"c\"]", s);
	}
	
	
	@Test
	public void testToArray2d() {
				
		ArrayList<ArrayList<String>> twod = new ArrayList<ArrayList<String>>();	
		
		ArrayList<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		twod.add(list);
		list = new ArrayList<String>();
		list.add("4");
		list.add("5");
		list.add("6");
		twod.add(list);
		list = new ArrayList<String>();
		list.add("7");
		list.add("8");
		list.add("9");
		twod.add(list);
				
		String s = JavascriptHelper.serialize(twod);
		
		assertEquals("[[\"1\",\"2\",\"3\"],[\"4\",\"5\",\"6\"],[\"7\",\"8\",\"9\"]]", s);			
	}
	
	
	
	@Test
	public void testFrom2dArray() {
		
	String src="["+
	"['Lien Position','76%-90%','91%-100%','>100%','% of Total Portfolio'],"+
	"['Real Estate First Position','11.3%','0.7%','1.6%','78.5%'],"+
	"['Real Estate Subordinated','0.0%','0.0%','0.0%','1.8%'],"+
	"['NMTC Leverage Loans','7.9%','0.0%','0.0%','16.6%'],"+
	"['Other Collateral','3.1%','0.0%','0.0%','3.1%'],"+
	"['Totalish','22.3%','0.7%','1.6%','100%']"+
	"]";
	
	List<List<String>> lists = JavascriptHelper.deserializeListOfLists(src);
	
	
	assertEquals("0.7%",lists.get(1).get(2));
	
	}
	
	@Test
	public void testNan() {
	   String value = JavascriptHelper.serialize(Arrays.asList(Double.NaN));
	   assertEquals("[NaN]", value);
	}
	
	
}
