package com.cars.platform.util;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.junit.Test;

import com.cars.platform.service.SpreadService2;

public class POIHelperTest {

	@Test
	public void testFindRowIndex() throws IOException {
		String filename = "src/main/doc/CARS FinancialsTemplate full datapoints mapping (1).xls";
		InputStream is = new FileInputStream(filename);

		HSSFWorkbook wb = new HSSFWorkbook(is);

		HSSFSheet sheet = wb.getSheet("INPUT");
		
		POIHelper poiHelper = new POIHelper();
		
		int rowIndex = poiHelper.findRowIndex(sheet, "101010", 4);
		
		assertEquals(7,rowIndex);
		rowIndex = poiHelper.findRowIndex(sheet, "101011", 4);
		
		assertEquals(8,rowIndex);
		

	}

	
	
	@Test
	public void testGetCorrectFormatForAccountCode() throws IOException {
		String filename = "src/main/resources/history-template.xls";
		InputStream is = new FileInputStream(filename);

		HSSFWorkbook wb = new HSSFWorkbook(is);

		HSSFSheet sheet = wb.getSheet("INPUT");
		
		POIHelper poiHelper = new POIHelper();
		
		int rowIndex = poiHelper.findRowIndex(sheet, "101010", 4);
		
		HSSFCell cell = poiHelper.getCell(sheet, rowIndex, 4);
		
		FormulaEvaluator evaluator = wb.getCreationHelper()
				.createFormulaEvaluator();

		
		
		assertEquals("101010",poiHelper.evaluateCell(cell, evaluator));
		
		

	}
	

	
	@Test
	public void testDateString() throws IOException {
		String filename = "src/main/doc/test-quarterly.xls";
		InputStream is = new FileInputStream(filename);
		HSSFWorkbook wb = new HSSFWorkbook(is);
		HSSFSheet sheet = wb.getSheetAt(0);
		POIHelper poiHelper = new POIHelper();	
		HSSFCell cell = poiHelper.getCell(sheet, 3, 1);	
		
		FormulaEvaluator evaluator = wb.getCreationHelper()
				.createFormulaEvaluator();
		assertEquals("6/30/2012",poiHelper.evaluateCell(cell, evaluator));		
	}	
	
	@Test
	public void testDateStringFromIFF() throws IOException {
		//String filename = "src/test/doc/IFF financials 052412.xls";
		String filename = "src/test/doc/jon-iff.xls";
		InputStream is = new FileInputStream(filename);
		HSSFWorkbook wb = new HSSFWorkbook(is);
		HSSFSheet sheet = wb.getSheetAt(0);
		POIHelper poiHelper = new POIHelper();	
		HSSFCell cell = poiHelper.getCell(sheet, 4, 12);	
		
		FormulaEvaluator evaluator = wb.getCreationHelper()
				.createFormulaEvaluator();
		assertEquals("12/31/2008",poiHelper.evaluateCell(cell, evaluator));		
	}	
		
	
	
	
	
	@Test
	public void testNotDateString() throws IOException {
		String filename = "src/main/resources/history-template.xls";
		InputStream is = new FileInputStream(filename);
		HSSFWorkbook wb = new HSSFWorkbook(is);
		HSSFSheet sheet = wb.getSheetAt(1);
		POIHelper poiHelper = new POIHelper();	
		HSSFCell cell = poiHelper.getCell(sheet, 23, 5);	
		
		FormulaEvaluator evaluator = wb.getCreationHelper()
				.createFormulaEvaluator();
		assertEquals("0",poiHelper.evaluateCell(cell, evaluator));		
	}	
	
	
	
	@Test
	public void testGetWorkbookAsMappedSheets() throws IOException {
		String filename = "src/test/doc/jon-iff-q2.xls";
		InputStream is = new FileInputStream(filename);

		SpreadService2 spreadService = new SpreadService2();
		spreadService.getWorkbookAsMappedPages(filename, is);
	}	
	
	@Test
	public void testNegative() throws IOException {

		String filename = "src/test/doc/nef-2012q2-2.xls";
		InputStream is = new FileInputStream(filename);

		HSSFWorkbook wb = new HSSFWorkbook(is);
		HSSFSheet sheet = wb.getSheetAt(0);
		POIHelper poiHelper = new POIHelper();
		
		FormulaEvaluator evaluator = wb.getCreationHelper()
				.createFormulaEvaluator();

		HSSFCell cell = poiHelper.getCell(sheet, 14, 3);
		String evaluatedCell = poiHelper.evaluateCell(cell, evaluator); 
		assertEquals("-823,065", evaluatedCell);

		
		cell = poiHelper.getCell(sheet, 16, 3);
		evaluatedCell = poiHelper.evaluateCell(cell, evaluator); 
		assertEquals("-772,495", evaluatedCell);
		
		
		
	}
}
