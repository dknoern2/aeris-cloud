package com.cars.platform.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class ExcelHelperTest {

	
	@Test
	public void testGetEquivColumn(){
		
			
		assertEquals("A",ExcelHelper.getEquivColumn(0));
		assertEquals("Z",ExcelHelper.getEquivColumn(25));
		assertEquals("AA",ExcelHelper.getEquivColumn(26));
		assertEquals("AB",ExcelHelper.getEquivColumn(27));
		
	}
	
	@Test
	public void testRangeString(){				
		assertEquals("$A$1:$F$5",ExcelHelper.getRange(0, 0, 4, 5));
	}
	
}
