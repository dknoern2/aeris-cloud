package com.cars.platform.util;

import java.awt.Color;
import java.awt.geom.Area;

import org.junit.Test;

import com.smartxls.ChartFormat;
import com.smartxls.ChartShape;
import com.smartxls.WorkBook;

public class SmartXLSTest {

	@Test
	public void testCreateSheet() throws Exception {

		WorkBook wb = new WorkBook();

		wb.setSheet(0);

		wb.setText(1, 1, "Hi on sheet 1");

		wb.insertSheets(1, 1);
		wb.setSheet(1);
		wb.setText(1, 1, "Hi on sheet 2");

		wb.write("target/test-sheet.xls");
	}

	@Test
	public void testReadSheet() throws Exception {

		testCreateSheet();

		WorkBook wb = new WorkBook();
		wb.read("target/test-sheet.xls");
		wb.setSheet(0);
		if (!(wb.getText(1, 1).equals("Hi on sheet 1"))) {
			throw new Exception();
		}

		wb.setSheet(1);
		if (!(wb.getText(1, 1).equals("Hi on sheet 2"))) {
			throw new Exception();
		}

	}

	@Test
	public void testCreateXLSX() throws Exception {
		WorkBook wb = new WorkBook();
		wb.setSheet(0);
		wb.setText(1, 1, "Hi on sheet 1");
		wb.insertSheets(1, 1);
		wb.setSheet(1);
		wb.setText(1, 1, "Hi on sheet 2");

		wb.writeXLSX("target/test-sheet.xlsx");

	}

	@Test
	public void testReadXLSX() throws Exception {

		testCreateXLSX();
		WorkBook wb = new WorkBook();
		wb.readXLSX("target/test-sheet.xlsx");

		wb.setSheet(0);
		if (!(wb.getText(1, 1).equals("Hi on sheet 1"))) {
			throw new Exception();
		}

		wb.setSheet(1);
		if (!(wb.getText(1, 1).equals("Hi on sheet 2"))) {
			throw new Exception();
		}
	}

	@Test
	public void testCreateChart() {

		WorkBook workBook = new WorkBook();
		try {
			// set data
			workBook.setText(0, 1, "Jan");
			workBook.setText(0, 2, "Feb");
			workBook.setText(0, 3, "Mar");
			workBook.setText(0, 4, "Apr");
			workBook.setText(0, 5, "Jun");

			workBook.setText(1, 0, "Comfrey");
			workBook.setText(2, 0, "Bananas");
			workBook.setText(3, 0, "Papaya");
			workBook.setText(4, 0, "Mango");
			workBook.setText(5, 0, "Lilikoi");
			for (int col = 1; col <= 5; col++)
				for (int row = 1; row <= 5; row++)
					workBook.setFormula(row, col, "RAND()");
			workBook.setText(6, 0, "Total");
			workBook.setFormula(6, 1, "SUM(B2:B6)");
			workBook.setSelection("B7:F7");
			// auto fill the range with the first cell's formula or data
			workBook.editCopyRight();

			int left = 1;
			int top = 7;
			int right = 11;
			int bottom = 28;

			// create chart with it's location
			ChartShape chart = workBook.addChart(left, top, right, bottom);
			chart.setPlotStacked(true);

			// link data source, link each series to columns(true to rows).
			// chart.setLinkRange("Sheet1!$a$1:$F$6", false);
			chart.setLinkRange("$a$1:$F$6", false);
			// set axis title
			chart.setAxisTitle(ChartShape.XAxis, 0, "X-axis data");
			chart.setAxisTitle(ChartShape.YAxis, 0, "Y-axis data");
			// set series name
			// chart.setSeriesName(0, "My Series number 1");
			// chart.setSeriesName(1, "My Series number 2");
			// chart.setSeriesName(2, "My Series boo number 3");
			// chart.setSeriesName(3, "My Series number 4");
			// chart.setSeriesName(4, "My Series number 5");
			chart.setTitle("My Chart");
			// set plot area's color to darkgray
			ChartFormat chartFormat = chart.getPlotFormat();
			chartFormat.setSolid();
			// chartFormat.setForeColor(Color.red.getRGB());
			chart.setPlotFormat(chartFormat);

			// set series 0's color to blue
			ChartFormat seriesformat = chart.getSeriesFormat(0);
			seriesformat.setSolid();
			// seriesformat.setForeColor(Color.BLUE.getRGB());
			chart.setSeriesFormat(0, seriesformat);

			// set series 1's color to red
			seriesformat = chart.getSeriesFormat(1);
			seriesformat.setSolid();
			// seriesformat.setForeColor(Color.RED.getRGB());
			chart.setSeriesFormat(1, seriesformat);

			// set series 1's color to red
			seriesformat = chart.getSeriesFormat(2);
			seriesformat.setSolid();
			seriesformat.setForeColor(Color.RED.getRGB());
			chart.setSeriesFormat(2, seriesformat);

			seriesformat = chart.getSeriesFormat(3);
			seriesformat.setSolid();
			//seriesformat.setForeColor(Color.BLUE.getRGB());
			chart.setSeriesFormat(3, seriesformat);

			seriesformat = chart.getSeriesFormat(4);
			seriesformat.setSolid();
			//seriesformat.setForeColor(Color.WHITE.getRGB());
			chart.setSeriesFormat(4, seriesformat);

			// set chart title's font property
			ChartFormat titleformat = chart.getTitleFormat();
			titleformat.setFontSize(14 * 20);
			titleformat.setFontUnderline(true);
			titleformat.setTextRotation(90);
			chart.setTitleFormat(titleformat);




			workBook.writeXLSX("target/chart-test.xlsx");
			workBook.write("target/chart-test.xls");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
