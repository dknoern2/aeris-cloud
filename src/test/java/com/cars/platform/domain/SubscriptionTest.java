package com.cars.platform.domain;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SubscriptionTest {
	
    private final DocumentTypeCategory dtcRating= new DocumentTypeCategory();
    private final DocumentTypeCategory dtcOther= new DocumentTypeCategory();
    
    private DocumentType docTypeRating = new DocumentType();
    private DocumentType docTypeOpinion = new DocumentType();
    private DocumentType docTypeOther = new DocumentType();
    
    private Document docRating;
    private Document docOpinion;
    private Document docOther;
    
   
    private Company sub1 = new Company();
    private Company sub2 = new Company();
    private Company cdfi1 = new Company();
    private Company cdfi2 = new Company();
    private Company cdfi3 = new Company();
    


    @Before
    public void init() {
    	dtcRating.setName(DocumentTypeCategory.CARS_REPORT);
    	dtcOther.setName("other");
    	
    	docTypeRating.setDocumentTypeCategory(dtcRating);
    	docTypeRating.setReportType(ReportType.FULL);
    	docTypeOpinion.setDocumentTypeCategory(dtcRating);
    	docTypeOpinion.setReportType(ReportType.OPINION);
   	    docTypeOther.setDocumentTypeCategory(dtcOther);
    	docTypeOther.setReportType(ReportType.NONE);
   	
        docRating = new Document();
        docOpinion = new Document();
        docOther = new Document();
    	docRating.setDocumentType(docTypeRating);
    	docOpinion.setDocumentType(docTypeOpinion);
    	docOther.setDocumentType(docTypeOther);
    	
    	cdfi1.setId(1L);
    	cdfi2.setId(2L);
    	cdfi3.setId(3L);
    }
    
    Set<Access> createAccess(List<Company> companies) {
		Set<Access> accesses = new HashSet<Access>();
		for (Company company : companies) {
			Access access = new Access();
			access.setCompany(company);
			accesses.add(access);
		}
		return accesses;
    }

	@Test
	public void testGetSubscribersForDocument() {
		Subscription subscription = new Subscription();
		subscription.setSubscriptionType(SubscriptionType.ALL);
		
		subscription.setOwner(sub1);
		
		subscription.setSubscribers(createAccess(Arrays.asList(sub1)));
		subscription.setCdfis(createAccess(Arrays.asList(cdfi1, cdfi2)));
		
		docRating.setCompany(cdfi1);
		docOpinion.setCompany(cdfi1);
		docOther.setCompany(cdfi1);
		
		subscription.setRatings(true);
		
		List<Company> subscribers = subscription.getSubscribersForDocument(docRating);
		Assert.assertTrue(subscribers.contains(sub1));
		
		subscribers = subscription.getSubscribersForDocument(docOpinion);
		Assert.assertTrue(null == subscribers);
		
		subscription.setOpinions(true);
		subscribers = subscription.getSubscribersForDocument(docOpinion);
		Assert.assertTrue(subscribers.contains(sub1));
		
		subscribers = subscription.getSubscribersForDocument(docOther);
		Assert.assertTrue(null == subscribers);
		
		docOpinion.setCompany(cdfi3);
		subscribers = subscription.getSubscribersForDocument(docOpinion);
		Assert.assertTrue(null == subscribers);
		
		subscription.setSubscriptionType(SubscriptionType.SELECT);
		subscription.setSubscribers(createAccess(Arrays.asList(sub1, sub2)));
		subscription.setCdfis(createAccess(Arrays.asList(cdfi1)));
		subscribers = subscription.getSubscribersForDocument(docRating);
		Assert.assertTrue(subscribers.contains(sub1));
		Assert.assertTrue(subscribers.contains(sub2));
	}

}
