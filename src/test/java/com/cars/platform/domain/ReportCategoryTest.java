package com.cars.platform.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ReportCategoryTest {
	
	private ReportCategory rc = new ReportCategory();
	private Company c1 = new Company();
	private Company c2 = new Company();
	private Company c3 = new Company();
	
	Equation[] globalEq = new Equation[10];
	Equation[] c1Eq = new Equation[10];
	Equation[] c2Eq = new Equation[10];
	Equation[] c3Eq = new Equation[10];

	@Before
	public void init() {
		
		c1.setId(0L);
		c2.setId(1L);
		c3.setId(2L);
		c1.setName("Company 1");
		c2.setName("Company 2");
		c3.setName("Company 3");
		
		for (int i=0; i<10; i++) {
			Equation eGlobal = new Equation();
			eGlobal.setId((long)i);
			eGlobal.setName("Global:" + i);
			globalEq[i] = eGlobal;
			
			Equation eC1 = new Equation();
			eC1.setId((long)1000 + i);
			eC1.setName("Company1:" + i);
			eC1.setCompany(c1);
			c1Eq[i] = eC1;
			
			Equation eC2= new Equation();
			eC2.setId((long)2000 + i);
			eC2.setName("Company2:" + i);
			eC2.setCompany(c2);
			c2Eq[i] = eC2;
			
			Equation eC3= new Equation();
			eC3.setId((long)3000 + i);
			eC3.setName("Company3:" + i);
			eC3.setCompany(c3);
			c3Eq[i] = eC3;

		}
	}
	
	

	@Test
	public void test() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 1));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 2));       // c1 no change
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 3));       //  c2
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 4));     // c1 delete
		rces.add(new ReportCategoryEquation(rc, globalEq[2], 5)); 
		rces.add(new ReportCategoryEquation(rc, c1Eq[5], 6));       // c1 move earlier
		rces.add(new ReportCategoryEquation(rc, c2Eq[5], 7));       //  c2
		rces.add(new ReportCategoryEquation(rc, c1Eq[9], 8));    // c1 no change
		rces.add(new ReportCategoryEquation(rc, c2Eq[9], 9));    // trailing c2
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[5]);    
		newEquations.add(globalEq[1]);    
		newEquations.add(c1Eq[0]);
		newEquations.add(globalEq[2]);
		newEquations.add(c1Eq[9]);
		
		rc.updateCategoryEquations(newEquations, c1);
		
		logRces(rces);

		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[5],
				globalEq[1],
				c1Eq[0],
				globalEq[2],
				c1Eq[9]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				globalEq[1],
				c2Eq[0],
				globalEq[2],
				c2Eq[5],
				c2Eq[9]
		};
	
		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		
		validateRanks(rces);
	}
	
	@Test
	public void testVerifyCrossCompany() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);    
		newEquations.add(c2Eq[0]);    // this should cause failure
		newEquations.add(globalEq[1]);
		newEquations.add(c1Eq[1]);    
		
		boolean thrown = false;
		try {
		    rc.updateCategoryEquations(newEquations, c1);
		}
		catch (IllegalArgumentException e) {
			thrown = true;
		}
		
		Assert.assertTrue(thrown);
	}
	
	@Test
	public void testVerifyGlobalMissingCDFI() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);
		newEquations.add(c2Eq[0]);
		newEquations.add(c3Eq[0]);
		newEquations.add(globalEq[1]);
		newEquations.add(c1Eq[1]);
		//newEquations.add(c2Eq[1]); this will cause failure
		newEquations.add(c3Eq[1]);
		
		boolean thrown = false;
		try {
		    rc.updateCategoryEquations(newEquations, null);
		}
		catch (IllegalArgumentException e) {
			thrown = true;
		}
		
		Assert.assertTrue(thrown);
	}
	
	@Test
	public void testVerifyGlobalCDFIReorder() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);
		newEquations.add(c2Eq[0]);
		newEquations.add(c3Eq[0]);
		newEquations.add(globalEq[1]);
		newEquations.add(c1Eq[1]);
		newEquations.add(c3Eq[1]); // transposed
		newEquations.add(c2Eq[1]); // transposed
		
		boolean thrown = false;
		try {
		    rc.updateCategoryEquations(newEquations, null);
		}
		catch (IllegalArgumentException e) {
			thrown = true;
		}
		
		Assert.assertTrue(thrown);
	}

	
	
	@Test
	public void testVerifyGlobalAdd() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);    
		newEquations.add(globalEq[1]); 
		newEquations.add(c1Eq[1]); 
		newEquations.add(globalEq[2]); // this should cause failure
		
		boolean thrown = false;
		try {
		    rc.updateCategoryEquations(newEquations, c1);
		}
		catch (IllegalArgumentException e) {
			thrown = true;
		}
		
		Assert.assertTrue(thrown);
	}

	
	@Test
	public void testVerifyGlobalChange() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[1]); // this should cause failure
		newEquations.add(c1Eq[0]);    
		newEquations.add(globalEq[0]); // this should cause failure
		newEquations.add(c1Eq[1]); 
		
		boolean thrown = false;
		try {
		    rc.updateCategoryEquations(newEquations, c1);
		}
		catch (IllegalArgumentException e) {
			thrown = true;
		}
		
		Assert.assertTrue(thrown);
	}

	@Test
	public void testVerifyGlobalSmaller() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);    
		//newEquations.add(globalEq[1]); // this should cause failure
		newEquations.add(c1Eq[1]); 
		
		boolean thrown = false;
		try {
		    rc.updateCategoryEquations(newEquations, c1);
		}
		catch (IllegalArgumentException e) {
			thrown = true;
		}
		
		Assert.assertTrue(thrown);
	}



	
	@Test
	public void testDelete() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));     // removed
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);    
		newEquations.add(globalEq[1]);
		
		rc.updateCategoryEquations(newEquations, c1);
		
		logRces(rces);

		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[0],
				globalEq[1]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				globalEq[1],
				c2Eq[1]
		};
		Equation[] c3Result = new Equation[] {
				globalEq[0],
				c3Eq[0],
				globalEq[1],
				c3Eq[1]
		};
		
		Assert.assertTrue(rces.size() == 7);
		
		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		validateCompanyResult(c3Result, rces, c3);
		
		validateRanks(rces);
	}



	@Test
	public void testMoveEarlier() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[1]);    // moved earlier
		newEquations.add(c1Eq[0]);    
		newEquations.add(globalEq[1]);

		
		rc.updateCategoryEquations(newEquations, c1);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[1],
				c1Eq[0],
				globalEq[1]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				globalEq[1],
				c2Eq[1]
		};
		Equation[] c3Result = new Equation[] {
				globalEq[0],
				c3Eq[0],
				globalEq[1],
				c3Eq[1]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		validateCompanyResult(c3Result, rces, c3);

		validateRanks(rces);
	}
	
	
	@Test
	public void testDeleteGlobal() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));  // deleted 
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		rces.add(new ReportCategoryEquation(rc, globalEq[2], 8));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[2], 9));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[2], 10));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[2], 11));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c2Eq[0]);
		newEquations.add(c3Eq[0]);
		newEquations.add(c1Eq[0]);
		newEquations.add(c1Eq[1]);
		newEquations.add(c2Eq[1]);
		newEquations.add(c3Eq[1]);
		newEquations.add(globalEq[2]);
		newEquations.add(c1Eq[2]);
		newEquations.add(c2Eq[2]);
		newEquations.add(c3Eq[2]);

		
		rc.updateCategoryEquations(newEquations, null);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[0],
				c1Eq[1],
				globalEq[2],
				c1Eq[2]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				c2Eq[1],
				globalEq[2],
				c2Eq[2]
		};
		Equation[] c3Result = new Equation[] {
				globalEq[0],
				c3Eq[0],
				c3Eq[1],
				globalEq[2],
				c3Eq[2]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		validateCompanyResult(c3Result, rces, c3);

		validateRanks(rces);
	}

	@Test
	public void testAddGlobal() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		rces.add(new ReportCategoryEquation(rc, globalEq[2], 8));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[2], 9));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[2], 10));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[2], 11));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c2Eq[0]);
		newEquations.add(c3Eq[0]);
		newEquations.add(c1Eq[0]);
		newEquations.add(globalEq[4]);
		newEquations.add(globalEq[1]);
		newEquations.add(c1Eq[1]);
		newEquations.add(c2Eq[1]);
		newEquations.add(c3Eq[1]);
		newEquations.add(globalEq[2]);
		newEquations.add(c1Eq[2]);
		newEquations.add(c2Eq[2]);
		newEquations.add(c3Eq[2]);
		
		rc.updateCategoryEquations(newEquations, null);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[0],
				globalEq[4],
				globalEq[1],
				c1Eq[1],
				globalEq[2],
				c1Eq[2]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				globalEq[4],
				globalEq[1],
				c2Eq[1],
				globalEq[2],
				c2Eq[2]
		};
		Equation[] c3Result = new Equation[] {
				globalEq[0],
				c3Eq[0],
				globalEq[4],
				globalEq[1],
				c3Eq[1],
				globalEq[2],
				c3Eq[2]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		validateCompanyResult(c3Result, rces, c3);

		validateRanks(rces);
	}

	@Test
	public void testMoveGlobalEarlier() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		rces.add(new ReportCategoryEquation(rc, globalEq[2], 8));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[2], 9));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[2], 10));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[2], 11));   
		rces.add(new ReportCategoryEquation(rc, globalEq[3], 12));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c2Eq[0]);
		newEquations.add(c3Eq[0]);
		newEquations.add(c1Eq[0]);
		newEquations.add(globalEq[1]);
		newEquations.add(globalEq[3]);
		newEquations.add(c1Eq[1]);
		newEquations.add(c2Eq[1]);
		newEquations.add(c3Eq[1]);
		newEquations.add(globalEq[2]);
		newEquations.add(c1Eq[2]);
		newEquations.add(c2Eq[2]);
		newEquations.add(c3Eq[2]);

		
		rc.updateCategoryEquations(newEquations, null);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[0],
				globalEq[1],
				globalEq[3],
				c1Eq[1],
				globalEq[2],
				c1Eq[2]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				globalEq[1],
				globalEq[3],
				c2Eq[1],
				globalEq[2],
				c2Eq[2]
		};
		Equation[] c3Result = new Equation[] {
				globalEq[0],
				c3Eq[0],
				globalEq[1],
				globalEq[3],
				c3Eq[1],
				globalEq[2],
				c3Eq[2]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		validateCompanyResult(c3Result, rces, c3);

		validateRanks(rces);
	}

	@Test
	public void testMoveGlobalLater() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		rces.add(new ReportCategoryEquation(rc, globalEq[2], 8));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[2], 9));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[2], 10));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[2], 11));   
		rces.add(new ReportCategoryEquation(rc, globalEq[3], 12));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c2Eq[0]);
		newEquations.add(c3Eq[0]);
		newEquations.add(c1Eq[0]);
		newEquations.add(c1Eq[1]);
		newEquations.add(c2Eq[1]);
		newEquations.add(c3Eq[1]);
		newEquations.add(globalEq[2]);
		newEquations.add(c1Eq[2]);
		newEquations.add(c2Eq[2]);
		newEquations.add(c3Eq[2]);
		newEquations.add(globalEq[3]);
		newEquations.add(globalEq[1]);
		
		rc.updateCategoryEquations(newEquations, null);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[0],
				c1Eq[1],
				globalEq[2],
				c1Eq[2],
				globalEq[3],
				globalEq[1]

		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				c2Eq[1],
				globalEq[2],
				c2Eq[2],
				globalEq[3],
				globalEq[1]
		};
		Equation[] c3Result = new Equation[] {
				globalEq[0],
				c3Eq[0],
				c3Eq[1],
				globalEq[2],
				c3Eq[2],
				globalEq[3],
				globalEq[1]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		validateCompanyResult(c3Result, rces, c3);

		validateRanks(rces);
	}


	

	@Test
	public void testAdd() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		rces.add(new ReportCategoryEquation(rc, globalEq[2], 8));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[2], 9));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[2], 10));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[2], 11));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);    
		newEquations.add(globalEq[1]);
		newEquations.add(c1Eq[4]); // add
		newEquations.add(c1Eq[1]);    
		newEquations.add(globalEq[2]);
		newEquations.add(c1Eq[2]);   

		
		rc.updateCategoryEquations(newEquations, c1);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[0],
				globalEq[1],
				c1Eq[4],
				c1Eq[1],
				globalEq[2],
				c1Eq[2]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				globalEq[1],
				c2Eq[1],
				globalEq[2],
				c2Eq[2]
		};
		Equation[] c3Result = new Equation[] {
				globalEq[0],
				c3Eq[0],
				globalEq[1],
				c3Eq[1],
				globalEq[2],
				c3Eq[2]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		validateCompanyResult(c3Result, rces, c3);

		validateRanks(rces);
	}


// TODO This acutally re-orders other companies as it moves the global2 up right after global1
// We may need to disallow moving and allow only add and delete	
	@Test
	public void testMoveLaterSimple() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 3));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 4));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 5));     
		rces.add(new ReportCategoryEquation(rc, globalEq[2], 6));   

		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);    
		newEquations.add(globalEq[1]);
		newEquations.add(globalEq[2]);
		newEquations.add(c1Eq[1]);    // moved later

		
		rc.updateCategoryEquations(newEquations, c1);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[0],
				globalEq[1],
				globalEq[2],
				c1Eq[1]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				globalEq[1],
				c2Eq[1],
				globalEq[2]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);

		validateRanks(rces);
	}


	@Test
	public void testMoveLater() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 1));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[0], 2));     
		rces.add(new ReportCategoryEquation(rc, c1Eq[0], 3));     
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 4));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[1], 5));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[1], 6));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[1], 7));   
		rces.add(new ReportCategoryEquation(rc, globalEq[2], 8));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[2], 9));     
		rces.add(new ReportCategoryEquation(rc, c2Eq[2], 10));     
		rces.add(new ReportCategoryEquation(rc, c3Eq[2], 11));   
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[0]);    
		newEquations.add(globalEq[1]);
		newEquations.add(globalEq[2]);
		newEquations.add(c1Eq[1]);    // moved later
		newEquations.add(c1Eq[2]);   

		
		rc.updateCategoryEquations(newEquations, c1);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[0],
				globalEq[1],
				globalEq[2],
				c1Eq[1],
				c1Eq[2]
		};
		Equation[] c2Result = new Equation[] {
				globalEq[0],
				c2Eq[0],
				globalEq[1],
				c2Eq[1],
				globalEq[2],
				c2Eq[2]
		};
		Equation[] c3Result = new Equation[] {
				globalEq[0],
				c3Eq[0],
				globalEq[1],
				c3Eq[1],
				globalEq[2],
				c3Eq[2]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		validateCompanyResult(c3Result, rces, c3);

		validateRanks(rces);
	}

	
	
	
	@Test
	public void testLeadingTrailing() {
		
		List<ReportCategoryEquation> rces = new ArrayList<ReportCategoryEquation>();
		rces.add(new ReportCategoryEquation(rc, c2Eq[0], 0));   
		rces.add(new ReportCategoryEquation(rc, globalEq[0], 1));   
		rces.add(new ReportCategoryEquation(rc, globalEq[1], 2));   
		rces.add(new ReportCategoryEquation(rc, c1Eq[5], 3));       
		rces.add(new ReportCategoryEquation(rc, c2Eq[9], 4));    
		
		rc.setCategoryEquations(rces);
		
		List<Equation> newEquations = new ArrayList<Equation>();
		newEquations.add(globalEq[0]);
		newEquations.add(c1Eq[1]);   // add 
		newEquations.add(globalEq[1]);
		newEquations.add(c1Eq[5]);

		
		rc.updateCategoryEquations(newEquations, c1);
		
		logRces(rces);
		
		Equation[] c1Result = new Equation[] {
				globalEq[0],
				c1Eq[1],
				globalEq[1],
				c1Eq[5]
		};
		Equation[] c2Result = new Equation[] {
				c2Eq[0],
				globalEq[0],
				globalEq[1],
				c2Eq[9]
		};

		validateCompanyResult(c1Result, rces, c1);
		validateCompanyResult(c2Result, rces, c2);
		
		validateRanks(rces);
	}

	// ranks should start at 0 and increase by 1 
	private void validateRanks(List<ReportCategoryEquation> rces) {
		ReportCategoryEquation previous = null;
		for (ReportCategoryEquation rce : rces) {
			if (null != previous) {
                int rankDiff = rce.getRank() - previous.getRank();
				Assert.assertTrue(rankDiff == 1);
			}
			previous = rce;
		}
	}
	
	private void logRces(List<ReportCategoryEquation> rces) {
		for (int i=0; i< rces.size(); i++) {
			ReportCategoryEquation rce = rces.get(i);
			System.out.println(i + ": rank: " + rce.getRank() + " " + rce.getEquation().getName());
		}
	}
	
	private void validateCompanyResult(Equation[] result, List<ReportCategoryEquation> rces, Company company) {
		int i = 0;
		for (ReportCategoryEquation rce : rces) {
			Equation e = rce.getEquation();
			if (e.getCompany() == null || company.equals(e.getCompany())) {
				Assert.assertEquals(e, result[i]);
				i++;
			}
		}
		if (i<result.length) {
			Assert.fail("Expected " + result.length + " for company " + company.getName());
		}
	}



}
