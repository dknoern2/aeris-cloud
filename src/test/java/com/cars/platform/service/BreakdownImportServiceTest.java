package com.cars.platform.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.mock.staticmock.AnnotationDrivenStaticEntityMockingControl;
import org.springframework.mock.staticmock.MockStaticEntityMethods;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.service.BreakdownImportService.BreakdownImportException;
import com.smartxls.WorkBook;

@RunWith(JUnit4.class)
@MockStaticEntityMethods
public class BreakdownImportServiceTest {

    private static final double DELTA = 0.001;

    private final Company company = new Company();
    private final int year = 2007;
    private final int quarter = 4;
    private final Period period = new Period();
    
    MetricBreakdown mb;
    List<Metric> allMetrics;




    //private final String filename = "src/main/doc/Phase 2Progress Fund CARS Financials 092512.xls";
    
    private final String successFilename = "src/main/doc/phase 2/BreakdownInputTest.xlsx";
    private final String failFilename = "src/main/doc/phase 2/BreakdownInputFailTest.xlsx";
    
    //"C:\Dev\CARS\garage\src\main\doc\phase 2\TableDataInputTemplate.xlsx"
    private InputStream is;

    @Before
    public void init() throws FileNotFoundException {
	
		company.setId(1L);
	
		period.setPeriodType(PeriodType.AUDIT);
		period.setYear(year);
		period.setQuarter(quarter);
		period.setCompany(company);
		
		mb = new MetricBreakdown();
		mb.setName("Type of Investor");
		MetricBreakdownItem mbi1 = new MetricBreakdownItem();
		mbi1.setActive(true);
		mbi1.setName("Federal Gov");
		mbi1.setMetricBreakdown(mb);
		MetricBreakdownItem mbi2 = new MetricBreakdownItem();
		mbi2.setActive(true);
		mbi2.setName("State or Local Gov");
		mbi2.setMetricBreakdown(mb);
		mb.setItems(Arrays.asList(mbi1, mbi2));
		
		Metric m1 = new Metric();
		m1.setBreakdown(mb);
		m1.setType(MetricType.NUMERIC);
		m1.setName("Number of Investors");
		Metric m2 = new Metric();
		m2.setBreakdown(mb);
		m2.setType(MetricType.NUMERIC);
		m2.setName("Debt Outstanding");
		Metric m3 = new Metric();
		m3.setBreakdown(mb);
		m3.setType(MetricType.NUMERIC);
		m3.setName("Interest Paid");
		allMetrics = Arrays.asList(m1, m2, m3);
    }

    @Test
    public void testSuccess() throws IOException {

    	
    	MetricBreakdown.findAllMetricBreakdowns();
    	List<MetricBreakdown> allBreakdowns = Arrays.asList(mb);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(allBreakdowns);
    	
    	Metric.findBreakdownMetrics();
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(allMetrics);
    	
    	Period.findPeriod(company, year, quarter);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(period);
    	
    	AnnotationDrivenStaticEntityMockingControl.playback();
    	
		is = new FileInputStream(successFilename);

    	
        BreakdownImportService bis = new BreakdownImportService();
    	List<MetricValue> newMetrics = null;
		try {
			newMetrics = bis.createMetrics(successFilename, is, company, year, quarter);
		} catch (BreakdownImportException e) {
			Assert.fail("Caught BreakdownImportService exception");
		}
    	
    	Assert.assertTrue(newMetrics.size() == 6);

    }
    
    @Test
    public void testFail() throws IOException {

    	
    	MetricBreakdown.findAllMetricBreakdowns();
    	List<MetricBreakdown> allBreakdowns = Arrays.asList(mb);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(allBreakdowns);
    	
    	Metric.findBreakdownMetrics();
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(allMetrics);
    	
    	Period.findPeriod(company, year, quarter);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(period);
    	
    	AnnotationDrivenStaticEntityMockingControl.playback();
    	
		is = new FileInputStream(failFilename);

    	
        BreakdownImportService bis = new BreakdownImportService();
    	List<MetricValue> newMetrics = null;
		try {
			newMetrics = bis.createMetrics(failFilename, is, company, year, quarter);
		} catch (BreakdownImportException e) {

		}
    	
    	Assert.assertTrue(null == newMetrics);

    }


    
    @Test
    public void testTemplate() throws Exception {

    	MetricBreakdown.findAllMetricBreakdowns();
    	List<MetricBreakdown> allBreakdowns = Arrays.asList(mb);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(allBreakdowns);
    	
    	Metric.findMetricsForBreakdown(mb);
    	AnnotationDrivenStaticEntityMockingControl.expectReturn(allMetrics);
    	
    	AnnotationDrivenStaticEntityMockingControl.playback();
    	
		is = new FileInputStream(successFilename);

    	
        BreakdownTemplateService bts = new BreakdownTemplateService();
		WorkBook wb = bts.createTemplate();

    	Assert.assertTrue(wb.getNumSheets() == 1);
    	Assert.assertTrue(wb.getText(0, 1, 0).equals("Federal Gov"));
    	Assert.assertTrue(wb.getText(0, 2, 0).equals("State or Local Gov"));
    }



}
