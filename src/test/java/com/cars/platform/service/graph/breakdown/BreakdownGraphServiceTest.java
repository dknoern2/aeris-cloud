package com.cars.platform.service.graph.breakdown;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.persistence.TypedQuery;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.staticmock.AnnotationDrivenStaticEntityMockingControl;
import org.springframework.mock.staticmock.MockStaticEntityMethods;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.BreakdownHistory;
import com.cars.platform.domain.BreakdownPie;
import com.cars.platform.domain.BreakdownTable;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.GraphType;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricCategory;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.UnitType;
import com.cars.platform.service.graph.breakdown.BreakdownGraphService;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.graph.BarChart;
import com.cars.platform.viewmodels.graph.Graph;
import com.cars.platform.viewmodels.graph.PieChart;
import com.cars.platform.viewmodels.graph.Table;
import com.cars.platform.viewmodels.graph.Value;

@MockStaticEntityMethods
public class BreakdownGraphServiceTest {

	private BreakdownTable bt;
	private BreakdownPie bp;
	private BreakdownHistory bh;
	private List<MetricValue> allMetricValues;
	private List<Period> allPeriods;
	private Company company;
	private Person person;
	
	
	@Before
	public void init() {
		
        // add principal object to SecurityContextHolder
		UserDetails user = new User("davidk", "whatever", new ArrayList<GrantedAuthority>());
        Authentication auth = new UsernamePasswordAuthenticationToken(user,null);
        SecurityContextHolder.getContext().setAuthentication(auth);
		
		company = new Company();
		company.setName("Foo");
		company.setId(1L);
		
		person = new Person();
		person.setCompany(company);
		
		// MetricCategories
		MetricCategory mc0 = new MetricCategory();
		mc0.setAccountCode("50");
		MetricCategory mc1 = new MetricCategory();
		mc1.setAccountCode("10");
		mc1.setParent(mc0);
		
		// Metric Breakdown
		MetricBreakdown mb = new MetricBreakdown();
		mb.setName("Type of Investor");
		MetricBreakdownItem mbi0 = new MetricBreakdownItem();
		mbi0.setName("Federal Gov");
		mbi0.setRank(0);
		mbi0.setActive(true);
		mbi0.setMetricBreakdown(mb);
		MetricBreakdownItem mbi1 = new MetricBreakdownItem();
		mbi1.setName("State and Local Gov");
		mbi1.setRank(1);
		mbi1.setActive(true);
		mbi1.setMetricBreakdown(mb);
		mb.setItems(Arrays.asList(mbi0, mbi1));
		
		//Metrics
		Metric m0 = new Metric();
		m0.setName("Number of Investors");
		m0.setAccountCode("1");
		m0.setType(MetricType.NUMERIC);
		m0.setBreakdown(mb);
		m0.setParent(mc1);
		Metric m1 = new Metric();
		m1.setName("Debt Outstanding");
		m1.setAccountCode("2");
		m1.setType(MetricType.NUMERIC);
		m1.setBreakdown(mb);
		m1.setParent(mc1);	
		Metric m2 = new Metric();
		m2.setName("Interest Paid");
		m2.setAccountCode("3");
		m2.setType(MetricType.NUMERIC);
		m2.setBreakdown(mb);
		m2.setParent(mc1);
		
		// MetricValues
		MetricValue mv0 = new MetricValue();
		mv0.setAmount(1.0);
		mv0.setQuarter(4);
		mv0.setYear(2012);
		//mv0.setCompany();
		mv0.setBreakdownItem(mbi0);
		mv0.setMetric(m0);
		MetricValue mv1 = new MetricValue();
		mv1.setAmount(16.0);
		mv1.setQuarter(4);
		mv1.setYear(2012);
		//mv1.setCompany();
		mv1.setBreakdownItem(mbi1);
		mv1.setMetric(m0);		
		MetricValue mv2 = new MetricValue();
		mv2.setAmount(392732.81);
		mv2.setQuarter(4);
		mv2.setYear(2012);
		//mv2.setCompany();
		mv2.setBreakdownItem(mbi0);
		mv2.setMetric(m1);		
		MetricValue mv3 = new MetricValue();
		mv3.setAmount(2187565.93);
		mv3.setQuarter(4);
		mv3.setYear(2012);
		//mv3.setCompany();
		mv3.setBreakdownItem(mbi1);
		mv3.setMetric(m1);		
		MetricValue mv4 = new MetricValue();
		mv4.setAmount(20.38);
		mv4.setQuarter(4);
		mv4.setYear(2012);
		//mv4.setCompany();
		mv4.setBreakdownItem(mbi0);
		mv4.setMetric(m2);		
		MetricValue mv5 = new MetricValue();
		mv5.setAmount(96308.22);
		mv5.setQuarter(4);
		mv5.setYear(2012);
		//mv5.setCompany();
		mv5.setBreakdownItem(mbi1);
		mv5.setMetric(m2);
		
		
		// metric value history for "Debt Outstanding"
		MetricValue mv2_2011 = new MetricValue();
		mv2_2011.setAmount(1000.0);
		mv2_2011.setQuarter(4);
		mv2_2011.setYear(2011);
		//mv2_2011.setCompany();
		mv2_2011.setBreakdownItem(mbi0);
		mv2_2011.setMetric(m1);		
		MetricValue mv3_2011 = new MetricValue();
		mv3_2011.setAmount(2000.0);
		mv3_2011.setQuarter(4);
		mv3_2011.setYear(2011);
		//mv3_2011.setCompany();
		mv3_2011.setBreakdownItem(mbi1);
		mv3_2011.setMetric(m1);		

		MetricValue mv2_2010 = new MetricValue();
		mv2_2010.setAmount(10000.0);
		mv2_2010.setQuarter(4);
		mv2_2010.setYear(2010);
		//mv2_2010.setCompany();
		mv2_2010.setBreakdownItem(mbi0);
		mv2_2010.setMetric(m1);		
		MetricValue mv3_2010 = new MetricValue();
		mv3_2010.setAmount(20000.0);
		mv3_2010.setQuarter(4);
		mv3_2010.setYear(2010);
		//mv3_2010.setCompany();
		mv3_2010.setBreakdownItem(mbi1);
		mv3_2010.setMetric(m1);	

		
		// BreakdownColumns
		BreakdownColumn bc0 = new BreakdownColumn();
		bc0.setName("Number of Investors");
		bc0.setEquation("501001!");
		bc0.setRank(0);
		bc0.setBreakdown(mb);
		bc0.setShowTotal(true);
		bc0.setUnitType(UnitType.NUMBER);
		bc0.setDecimalPlaces(0);
		BreakdownColumn bc1 = new BreakdownColumn();
		bc1.setName("Total Loans Payable");
		bc1.setEquation("501002!");
		bc1.setRank(1);
		bc1.setBreakdown(mb);
		bc1.setShowTotal(true);
		bc1.setUnitType(UnitType.DOLLAR);
		bc1.setDecimalPlaces(0);
		BreakdownColumn bc2 = new BreakdownColumn();
		bc2.setName("Interest Paid");
		bc2.setEquation("501003!");
		bc2.setRank(2);
		bc2.setBreakdown(mb);
		bc2.setShowTotal(true);
		bc2.setUnitType(UnitType.DOLLAR);
		bc2.setDecimalPlaces(0);
		BreakdownColumn bc3 = new BreakdownColumn();
		bc3.setName("% of Total Loans Payable");
		bc3.setEquation("501002! / SUM 501002!");
		bc3.setTotalEquation("1");
		bc3.setRank(3);
		bc3.setBreakdown(mb);
		bc3.setShowTotal(true);
		bc3.setUnitType(UnitType.PERCENTAGE);
		bc3.setDecimalPlaces(1);
		BreakdownColumn bc4 = new BreakdownColumn();
		bc4.setName("Average per Investor");
		bc4.setEquation("501002! / 501001!");
		bc4.setTotalEquation("SUM 501002! / SUM 501001!");
		bc4.setRank(4);
		bc4.setBreakdown(mb);
		bc4.setShowTotal(true);
		bc4.setUnitType(UnitType.DOLLAR);
		bc4.setDecimalPlaces(0);
		BreakdownColumn bc5 = new BreakdownColumn();
		bc5.setName("Weighted Average Rate");
		bc5.setEquation("501003! / 501002!");
		bc5.setTotalEquation("SUM 501003! / SUM 501002!");
		bc5.setRank(5);
		bc5.setBreakdown(mb);
		bc5.setShowTotal(true);
		bc5.setUnitType(UnitType.PERCENTAGE);
		bc5.setDecimalPlaces(1);

		
		// BreakdownTable
		bt = new BreakdownTable();
		bt.setTitle("Sources of Debt");
		bt.setBreakdown(mb);
		bt.setColumns(Arrays.asList(bc0, bc1, bc3, bc4, bc5));
		
		// BreakdownPie
		bp = new BreakdownPie();
		bp.setTitle("Debt Pie");
		bp.setBreakdown(mb);
		bp.setEquation(bc1);
		
		// BreakdownHistory
		bh = new BreakdownHistory();
		bh.setTitle("Debt History");
		bh.setShowYears(5);
		bh.setShowInterim(false);
		bh.setBreakdown(mb);
		bh.setEquation(bc1);

		
		Period p0 = new Period();
		p0.setPeriodType(PeriodType.AUDIT);
		p0.setQuarter(4);
		p0.setYear(2012);
		p0.setEndDate("12/31/2012");
		Period p1 = new Period();
		p1.setPeriodType(PeriodType.AUDIT);
		p1.setQuarter(4);
		p1.setYear(2011);
		p1.setEndDate("12/31/2011");
		Period p2 = new Period();
		p2.setPeriodType(PeriodType.AUDIT);
		p2.setQuarter(4);
		p2.setYear(2010);
		p2.setEndDate("12/31/2010");
		
		allMetricValues = Arrays.asList(mv0, mv1, mv2, mv3, mv4, mv5, mv2_2011, mv3_2011, mv2_2010, mv3_2010);
		allPeriods = Arrays.asList(p0, p1, p2);
	}

	@Test
	public void testTable() {
		Person.findPeopleByUsernameEquals("davidk");
		AnnotationDrivenStaticEntityMockingControl.expectReturn(person);
    	AnnotationDrivenStaticEntityMockingControl.playback();

    	
		BreakdownGraphService gs = new BreakdownGraphService(company, allMetricValues, allPeriods);
		Graph graph = gs.getGraph(bt, false);
		
		Assert.assertTrue(graph instanceof Table);
		Assert.assertTrue(graph.getTitle().contains(bt.getTitle()));
		Assert.assertTrue(graph.getDate().equals("FYE 2012"));

		Table table = (Table)graph;
		List<Value> totals = table.getTotals().getValues();
		Assert.assertTrue(totals.get(0).getAmount().equals(17.0));
		String value = totals.get(3).getValue();
		Assert.assertTrue(value.equals("151,782"));
		Assert.assertTrue(table.getDatas().size() == 2);
	}
	
	
	@Test
	public void testPie() {
		Person.findPeopleByUsernameEquals("davidk");
		AnnotationDrivenStaticEntityMockingControl.expectReturn(person);
    	AnnotationDrivenStaticEntityMockingControl.playback();
    	
		BreakdownGraphService gs = new BreakdownGraphService(company, allMetricValues, allPeriods);
		Graph graph = gs.getGraph(bp, false);
		
		Assert.assertTrue(graph instanceof PieChart);
		Assert.assertTrue(graph.getTitle().contains(bp.getTitle()));
		
		PieChart pie = (PieChart)graph;
		Assert.assertTrue(pie.getDatas().get(0).getLabel().equals("Federal Gov"));
		Assert.assertTrue(pie.getDatas().get(0).getValues().size() == 1);
		Assert.assertTrue(pie.getDatas().get(0).getValues().get(0).getAmount() == 392732.81);
		Assert.assertTrue(pie.getDatas().size() == 2);
	}
	
	@Test
	public void testHistoryChart() {
		Person.findPeopleByUsernameEquals("davidk");
		AnnotationDrivenStaticEntityMockingControl.expectReturn(person);
    	AnnotationDrivenStaticEntityMockingControl.playback();

		BreakdownGraphService gs = new BreakdownGraphService(company, allMetricValues, allPeriods);
		bh.setGraphType(GraphType.COLUMN);
		Graph graph = gs.getGraph(bh, false);
		
		Assert.assertTrue(graph instanceof BarChart);
		Assert.assertTrue(graph.getTitle().equals(bh.getTitle()));
		
		BarChart chart = (BarChart)graph;
		Assert.assertTrue(chart.getDatas().size() == 2);
		Assert.assertTrue(chart.getUnitType() == UnitType.DOLLAR);
		Assert.assertTrue(chart.getDatas().get(1).getLabel().equals("State and Local Gov"));
	}
	
	@Test
	public void testHistoryTable() {
		Person.findPeopleByUsernameEquals("davidk");
		AnnotationDrivenStaticEntityMockingControl.expectReturn(person);
    	AnnotationDrivenStaticEntityMockingControl.playback();

		BreakdownGraphService gs = new BreakdownGraphService(company, allMetricValues, allPeriods);
		bh.setGraphType(GraphType.TABLE);
		Graph graph = gs.getGraph(bh, false);
		
		Assert.assertTrue(graph instanceof Table);
		Assert.assertTrue(graph.getTitle().equals(bh.getTitle()));
		
		Table table = (Table)graph;
		Assert.assertTrue(table.getDatas().size() == 2);
		Assert.assertTrue(table.getDatas().get(1).getLabel().equals("State and Local Gov"));
		
		List<Value> totals = table.getTotals().getValues();
		Assert.assertTrue(totals.size() == 3);
		Assert.assertTrue(totals.get(1).getAmount().equals(3000.0));
		Assert.assertTrue(totals.get(0).getAmount().equals(30000.0));
	}

	
	

}
