package com.cars.platform.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.service.ReportService;
import com.cars.platform.service.ValueFactService;
import com.cars.platform.util.JavascriptHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.TentativeChange;

public class ReportDataCache {

    private static final Logger logger = LoggerFactory.getLogger(ReportDataCache.class);

    private static Map<Long, ReportData> map = new ConcurrentHashMap<Long, ReportData>();

    /**
     * Get report data. Pull from cache is possible
     * 
     * @param company
     * @return
     */
    public static ReportData getReportData(Company company) {
	return getReportData(company, null);

    }

    /**
     * Get report data.
     * 
     * @param company
     * @param tentativeChanges
     *            if specified, tentative changes will be applied all values
     *            calculated from scratch
     * @return
     */
    public static ReportData getReportData(Company company, HashMap<String, TentativeChange> tentativeChanges) {

	ReportData reportData = null;

	if (tentativeChanges == null || tentativeChanges.size() == 0) {
	    logger.info("NO tentative changes, NOT regenerating spreads");
	    reportData = map.get(company.getId());
	} else {
	    logger.info("tentative changes, regenerating spreads");
	}

	if ((reportData == null)
		|| (reportData.isShowDraftColumns() != Period.isDraftColumnsVisible(UserHelper.getCurrentUser()))) {

	    reportData = new ReportData(Period.isDraftColumnsVisible(UserHelper.getCurrentUser()));
	    ReportService reportService = new ReportService(company, tentativeChanges, false, true);

	    ArrayList<ArrayList<String>> inputReport = reportService.getReport(ReportPage.INPUT);
	    ArrayList<ArrayList<String>> positionReport = reportService.getReport(ReportPage.POSITION);
	    ArrayList<ArrayList<String>> activityReport = reportService.getReport(ReportPage.ACTIVITY);
// handled by ReportService2 now	    
//	    ArrayList<ArrayList<String>> summaryReport = reportService.getReport(ReportPage.SUMMARY);
//	    ArrayList<ArrayList<String>> additionalReport = reportService.getReport(ReportPage.ADDITIONAL);

	    reportData.put("columns", reportService.getColumns());
	    reportData.put("fields", reportService.getFields());
	    reportData.put("footnotes", JavascriptHelper.serialize(reportService.getFootnotes()));
	    reportData.put("inputData", JavascriptHelper.serialize(inputReport));
	    reportData.put("positionData", JavascriptHelper.serialize(positionReport));
	    reportData.put("activityData", JavascriptHelper.serialize(activityReport));
//	    reportData.put("summaryData", JavascriptHelper.serialize(summaryReport));
//	    reportData.put("additionalData", JavascriptHelper.serialize(additionalReport));

/*
	    List<Graph> dynGraphs = Graph.findAllGraphsByCompany(company);

	    LinkedHashMap<Long, String> graphs = new LinkedHashMap<Long, String>();
	    LinkedHashMap<Long, String> graphTitles = new LinkedHashMap<Long, String>();

	    for (Graph dynGraph : dynGraphs) {
		String graph = reportService.getGraph(dynGraph, dynGraph.getId());
		graphs.put(dynGraph.getId(), graph);
		graphTitles.put(dynGraph.getId(), dynGraph.getTitle());
	    }

	    reportData.put("graphs", graphs);
	    reportData.put("graphTitles", graphTitles);

	    // find total assets (asset size)
	    try {
		for (ArrayList<String> line : positionReport) {
		    if ("<b>TOTAL ASSETS</b>".equals(line.get(0)) && line.size() > 1) {
			String totalAssets = line.get(line.size() - 1);
			totalAssets = totalAssets.replace("<b>", "");
			totalAssets = totalAssets.replace("</b>", "");
			reportData.put(ReportData.TOTAL_ASSETS, line.get(line.size() - 3));
		    }
		}
	    } catch (Exception ignore) {
	    }
*/
	    if (tentativeChanges == null || tentativeChanges.size() == 0) {
		// only cache if no tentative changes are involved.
		map.put(company.getId(), reportData);
	    }
	}

	return reportData;
    }

    public static void removeAndUpdateValueFacts(Company company) {
    	map.remove(company.getId());
    	
    	ValueFactService valueFactService = new ValueFactService(company);
    	valueFactService.updateCompany();
    }
    
    public static void removeAndUpdateValueFacts(Company company, List<Equation> formulas) {
    	map.remove(company.getId());
    	
    	ValueFactService valueFactService = new ValueFactService(company);
    	valueFactService.updateCompany(formulas);
    }

    
    public static void remove(Company company) {
    	map.remove(company.getId());
    }

    
    

    // TODO need to calculate all ValueFacts here but this should be done asynch as it will take a long while
    public static void removeAll() {
	try {
	    map = new ConcurrentHashMap<Long, ReportData>();
	} catch (Exception e) {
	    logger.error("", e);
	}
    }
}
