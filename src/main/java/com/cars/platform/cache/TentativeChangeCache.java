package com.cars.platform.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

import com.cars.platform.viewmodels.TentativeChange;

public class TentativeChangeCache {

    public static final String CACHE = "TENTATIVE_CHANGE_CACHE";

    // each analyst can have a map in their current session for changes for each
    // CDFI
    // key is CDFI companyId, value is a map of metricValue ID to tentative
    // metric value
    private static Map<Long, HashMap<String, TentativeChange>> cdfiMap = new ConcurrentHashMap<Long, HashMap<String, TentativeChange>>();

    private void put(Long companyId, Long metricValueId, TentativeChange tentativeChange) {

	HashMap<String, TentativeChange> map = cdfiMap.get(companyId);
	if (map == null) {
	    map = new HashMap<String, TentativeChange>();
	    cdfiMap.put(companyId, map);
	}
	
	String key = TentativeChange.getTentativeChangeKey(tentativeChange);	
	map.put(key, tentativeChange);

    }

    private HashMap<String, TentativeChange> getTentativeChanges(Long companyId) {
	return cdfiMap.get(companyId);
    }

    public static HashMap<String, TentativeChange> getTentativeChanges(HttpSession session, Long companyId) {
	TentativeChangeCache tentativeChangeCache = (TentativeChangeCache) session
		.getAttribute(TentativeChangeCache.CACHE);

	if (tentativeChangeCache != null) {
	    return tentativeChangeCache.getTentativeChanges(companyId);
	}
	return null;
    }

    public static void put(HttpSession session, Long companyId, Long metricValueId, TentativeChange tentativeChange) {

	TentativeChangeCache tentativeChangeCache = (TentativeChangeCache) session
		.getAttribute(TentativeChangeCache.CACHE);

	if (tentativeChangeCache == null) {
	    tentativeChangeCache = new TentativeChangeCache();
	    session.setAttribute(TentativeChangeCache.CACHE, tentativeChangeCache);
	}

	tentativeChangeCache.put(companyId, metricValueId, tentativeChange);
    }

    public static void clear(Long companyId) {
	cdfiMap.remove(companyId);
    }

}
