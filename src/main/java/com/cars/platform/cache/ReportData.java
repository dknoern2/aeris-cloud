package com.cars.platform.cache;

import java.util.HashMap;
import java.util.Map;

public class ReportData {

    public static final String TOTAL_ASSETS = "totalAssets";

    public static final String NO_ASSETS = "No financial data is currently available (no period set).";

    private final boolean showDraftColumns;

    private Map<String, Object> map = new HashMap<String, Object>();

    public ReportData(boolean showDraftColumns) {
	this.showDraftColumns = showDraftColumns;
    }

    public Object get(String key) {
	return map.get(key);
    }

    public void put(String key, Object value) {
	map.put(key, value);
    }

    public boolean isShowDraftColumns() {
	return showDraftColumns;
    }

}
