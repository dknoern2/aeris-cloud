package com.cars.platform.web;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cars.platform.domain.Activity;
import com.cars.platform.domain.ActivityType;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Person;
import com.cars.platform.service.BreakdownTemplateService;
import com.cars.platform.service.DocumentDataService;
import com.cars.platform.util.AuthorizationHelper;
import com.cars.platform.util.SpringApplicationContext;
import com.cars.platform.util.UserHelper;
import com.smartxls.WorkBook;
 
public class FileViewServlet extends HttpServlet {
    private static final long serialVersionUID = -3208409086358916855L;
 
    private DocumentDataService documentDataService = SpringApplicationContext.getDocumentDataService();
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	    	
    	long id = Long.parseLong(request.getParameter("id"));

    	Document doc = Document.findDocument(id);
        response.setHeader("Content-Disposition", "attachment; filename=\""+doc.getFileName()+"\"");
       
        Person currentUser = UserHelper.getCurrentUser();
        
        if (null != doc.getCompany()) {
			boolean authorized = AuthorizationHelper.isAuthorized(currentUser, doc.getCompany());
			if(!authorized){
				return;
			}
        }
        
        
        String filename = doc.getFileName().toLowerCase();
            
        if(filename.endsWith(".xls")){
        	response.setContentType("application/vnd.ms-excel");
        }else if(filename.endsWith(".xlsx")){
        	response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }else if(filename.endsWith(".doc")){
        	response.setContentType("application/vnd.ms-word");
        }else if(filename.endsWith(".docx")){
        	response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }else if(filename.endsWith(".pdf")){
        	response.setContentType("application/pdf");
        }
        
        
        // downloading a global (template)?
        if (doc.getCompany() == null) {
        	if (doc.getDocumentType().getId().equals(DocumentType.DOCUMENT_TYPE_BREAKDOWN_ID)) {
        		BreakdownTemplateService bts = new BreakdownTemplateService();
        		try {
	        		WorkBook wb = bts.createTemplate();
	        		wb.writeXLSX(response.getOutputStream());
        		}
        		catch (Exception e) {
        			// all smartxls methods throw exception
        			System.out.println("Download of breakdown template failed: " + e.getMessage());
        			e.printStackTrace();
        			throw new RuntimeException(e);
        		}
        	}
        	else {
        		throw new IllegalArgumentException("Unrecognized document type id: " + doc.getDocumentType().getId());
         	}
        }
        else {
        	InputStream in = documentDataService.getDocumentData(doc.getCompany().getId(), doc.getId());       	
       	       

	        byte[] buffer = new byte[1024];
	        int len;
	        while ((len = in.read(buffer)) != -1) {
	            response.getOutputStream().write(buffer, 0, len);
	        }
	    	    	
			Activity activity = new Activity();
			activity.setCompany(doc.getCompany());
			activity.setDate(new java.util.Date());
			activity.setPerson(currentUser);
			activity.setDescription(doc.getFileName());
			activity.setActivityType(ActivityType.DOCUMENT_DOWNLOAD);
			activity.setDocument(doc);
			activity.persist();  
        }


    }
}

