package com.cars.platform.web;
import com.cars.platform.domain.BreakdownTable;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/breakdowntables")
@Controller
@RooWebScaffold(path = "breakdowntables", formBackingObject = BreakdownTable.class)
public class BreakdownTableController {
}
