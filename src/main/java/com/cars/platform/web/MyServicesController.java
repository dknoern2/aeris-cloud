package com.cars.platform.web;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cars.platform.domain.Access;
import com.cars.platform.domain.ActionItem;
import com.cars.platform.domain.Activity;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.PersonRole;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;
import com.cars.platform.service.ActionItemService;
import com.cars.platform.util.UserHelper;

@RequestMapping("/myservices/**")
@Controller
public class MyServicesController {
	
	@RequestMapping(value = "/myservices")
	public String index(ModelMap modelMap) {
		return information(modelMap);
	}


	@RequestMapping(value = "/myservices/subscription")
	public String subscription(ModelMap modelMap) {

		Person person = UserHelper.getCurrentUser();
		modelMap.addAttribute("person", person);
		modelMap.addAttribute("company", person.getCompany());
	
		List<Person> employees = Person.findPeopleByPersonRole(PersonRole.ADMIN).getResultList();
		modelMap.addAttribute("employees", employees);
		//List<Subscription> subscriptions = new ArrayList<Subscription>();
		//Subscription subscriptions =  Subscription.findRatingsSubscriptionsByOwner(person.getCompany());
		List<Subscription> subscriptions =  Subscription.findRatingsSubscriptionsByOwner(person.getCompany());
		
		//if(subscription!=null){
		//	subscriptions.add(subscription);
		//}
		modelMap.addAttribute("subscriptions",subscriptions);

		return "myservices/subscription";
	}
	
	

	@RequestMapping(value = "/myservices/select")
	public String select(ModelMap modelMap) {
		return delegation(SubscriptionType.SELECT,modelMap);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/myservices/information")
	public String information(ModelMap modelMap) {
		return delegation(SubscriptionType.LIBRARY,modelMap);
	}
		
	private String delegation(SubscriptionType subscriptionType, ModelMap modelMap) {
		Person person = UserHelper.getCurrentUser();
		modelMap.addAttribute("person", person);
		Company company = person.getCompany();
		modelMap.addAttribute("company", company);
		modelMap.addAttribute("subscriptionType",subscriptionType);
		List<Subscription> delegatedSubscriptions = Subscription.findSubscriptionsByOwnerAndType(company,subscriptionType);
		modelMap.put("delegatedSubscriptions",delegatedSubscriptions);
		List<Person> employees = Person.findPeopleByPersonRole(PersonRole.ADMIN).getResultList();

		modelMap.addAttribute("employees", employees);

		List<Subscription> subscriptions = Subscription.findSubscriptionsByOwner(person.getCompany()).getResultList();
		modelMap.addAttribute("subscriptions",subscriptions);
		
		// create map of companyID to last access date
		
		HashMap<Long,Date> accessDates = new HashMap<Long,Date>();
		
		
		for(Subscription subscription:subscriptions){
			for(Access target:subscription.getSubscribers()){
				Date lastAccessTime = Activity.findLastAccessTimeByOwnerAndTarget(company,target.getCompany());
                if(lastAccessTime!=null){
                	accessDates.put(target.getId(), lastAccessTime);
                }
			}
		}
		
		modelMap.addAttribute("accessDates",accessDates);
		
		
		
		return "myservices/delegation";
	}


}
