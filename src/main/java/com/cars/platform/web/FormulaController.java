package com.cars.platform.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.domain.UnitType;
import com.cars.platform.service.ValueFactLoader;
import com.cars.platform.service.ValueFactService;
import com.cars.platform.util.SelectOptionHelper;

@RequestMapping("/formulas")
@Controller
public class FormulaController {

//	static Company nullCompany = new Company();
//	static {
//		nullCompany.setName("All CDFIs");
//		nullCompany.setId(0L);
//	}
//	
//	static Equation nullEquation = new Equation();
//	static {
//		nullEquation.setName("-- Select a Formula --");
//		nullEquation.setId(0L);
//	}
	
	@Autowired
	ValueFactLoader valueFactLoader;
	
    @RequestMapping(value = "/{cdfiEquationId}/override/{globalEquationId}", method = RequestMethod.PUT, produces = "text/html")
    public @ResponseBody String overrideEquation(
    		@PathVariable("cdfiEquationId") Long cdfiEquationId, 
    		@PathVariable("globalEquationId") Long globalEquationId) {
		Equation globalEquation = Equation.findEquation(globalEquationId);
		Equation cdfiEquation = Equation.findEquation(cdfiEquationId);
		if (null == globalEquation || null == cdfiEquation) {
			throw new IllegalArgumentException("There is no equation with ID: " + cdfiEquationId + " or " + globalEquationId);
		}
		cdfiEquation.setOverride(globalEquation);
		cdfiEquation.merge();
		return "Ok";
	}
    
    @RequestMapping(value = "/{id}/removeOverride/", method = RequestMethod.GET)
    public @ResponseBody com.cars.platform.viewmodels.report.Equation removeOverride(
    		@PathVariable("id") Long cdfiEquationId) {
		
		Equation cdfiEquation = Equation.findEquation(cdfiEquationId);
		if (null == cdfiEquation) {
			throw new IllegalArgumentException("There is no equation with ID: " + cdfiEquationId);
		}
		com.cars.platform.viewmodels.report.Equation globalEquation = new com.cars.platform.viewmodels.report.Equation(cdfiEquation.getOverride());
		cdfiEquation.setOverride(null);
		cdfiEquation.merge();
		
		return globalEquation;
	}
    
    @RequestMapping(value = "/createFormula/{companyId}", method = RequestMethod.POST, produces = "text/html")
	public @ResponseBody String createFormula(
			@RequestParam(value = "formula", required = true) String formula,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "decimals", required = true) Integer decimals,
			@RequestParam(value = "unitType", required = true) UnitType unitType,
	 	    @RequestParam(value = "smallerBetter", required = false) boolean smallerBetter, 
	 	    @RequestParam(value = "definition", required = false) String definition,
	 	    @PathVariable long companyId, Model model) {

		Company company = null;
		if (companyId != 0) {
			company = Company.findCompany(companyId);
		}
		Equation eq = new Equation();
		eq.setCompany(company);
		eq.setFormula(formula);
		eq.setDecimalPlaces(decimals);
		eq.setName(name);
		eq.setSmallerBetter(smallerBetter);
		eq.setUnitType(unitType);
		if (definition != null) {
			eq.setDefinition(definition);
		}
		
		eq.persist();	
		if (eq.getCompany() != null) {
			ReportDataCache.remove(eq.getCompany());
			// Since we are creating a brand new formula don't need to update update All the value facts
	    	ValueFactService valueFactService = new ValueFactService(company);
	    	valueFactService.createforEquation(eq);
		} else {
			ReportDataCache.removeAll();
			valueFactLoader.updateAllCompanies(); // done async
		}
		return eq.getId().toString();
	}
    
    @RequestMapping(value = "/{formulaId}/updateFormula/{companyId}", method = RequestMethod.POST, produces = "text/html")
	public @ResponseBody String updateFormula(@PathVariable Long formulaId,
			@RequestParam(value = "formula", required = true) String formula,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "decimals", required = true) Integer decimals,
			@RequestParam(value = "unitType", required = true) UnitType unitType,
	 	    @RequestParam(value = "smallerBetter", required = false) boolean smallerBetter, 
	 	    @RequestParam(value = "definition", required = false) String definition,
	 	    @PathVariable long companyId, Model model) {

		Company company = null;
		if (companyId != 0) {
			company = Company.findCompany(companyId);
		}
		Equation eq = Equation.findEquation(formulaId);
		boolean updateValueFact = true;
		if (eq.getFormula().equals(formula) && eq.getDecimalPlaces().equals(decimals) && eq.getName().equals(name) && eq.isSmallerBetter() == smallerBetter && eq.getUnitType() == unitType) {
			if (eq.getCompany() == null || (eq.getCompany() != null && eq.getCompany().equals(company))) {
				updateValueFact = false;
			}
		}
		
		eq.setCompany(company);
		eq.setFormula(formula);
		eq.setDecimalPlaces(decimals);
		eq.setName(name);
		eq.setSmallerBetter(smallerBetter);
		eq.setUnitType(unitType);
		if (definition != null) {
			eq.setDefinition(definition);
		}
		
		eq.merge();	
		if (updateValueFact) {
			if (eq.getCompany() != null) {
				ReportDataCache.removeAndUpdateValueFacts(eq.getCompany(), Arrays.asList(eq));			
			} else {
				//TODO: disable updating when only definition was changed
				ReportDataCache.removeAll();
				valueFactLoader.updateAllCompanies(); // done async
			}
		}
		return eq.getId().toString();
	}


	@RequestMapping(value = "/getUnitTypes", method = RequestMethod.GET)
	public @ResponseBody
	List<UnitType> getUnitTypes() {		
		return  Arrays.asList(UnitType.values());
	}
	
	@RequestMapping(value = "/getDecimalPlaces", method = RequestMethod.GET)
	public @ResponseBody
	List<Integer> getDecimalPlaces() {		
		return  SelectOptionHelper.getDecimalPlaces();
	}


	
	@RequestMapping(value = "/getPages", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<ReportPage> getPages() {
		ArrayList<ReportPage> pages = new ArrayList<ReportPage>();
		for (ReportPage page : ReportPage.values()) {
			pages.add(page);
		}
		return pages;
	}


	@RequestMapping(value = "/cdfi/{id}/getCDFIFormulas", method = RequestMethod.GET)
	public @ResponseBody
	List<com.cars.platform.viewmodels.report.Equation> getCDFIFormulas(@PathVariable("id") Long id) {
		Company company = Company.findCompany(id);
		List<com.cars.platform.viewmodels.report.Equation> equations = new ArrayList<com.cars.platform.viewmodels.report.Equation>();
		for (Equation equation : Equation.findEquationsByCompany(company).getResultList()) {
			equations.add(new com.cars.platform.viewmodels.report.Equation(equation));
		}
		return equations;
	}
	
	@RequestMapping(value = "/getGlobalFormulas", method = RequestMethod.GET)
	public @ResponseBody
	List<com.cars.platform.viewmodels.report.Equation> getGlobalFormulas() {
		List<com.cars.platform.viewmodels.report.Equation> equations = new ArrayList<com.cars.platform.viewmodels.report.Equation>();
		for (Equation equation : Equation.findGlobalEquations()) {
			equations.add(new com.cars.platform.viewmodels.report.Equation(equation));
		}
		return equations;
	}
	
	@RequestMapping(value = "/getGlobalCDFIFormulas/{id}", method = RequestMethod.GET)
	public @ResponseBody
	List<com.cars.platform.viewmodels.report.Equation> getGlobalCDFIFormulas(@PathVariable("id") Long id) {
		Company company = Company.findCompany(id);
		List<com.cars.platform.viewmodels.report.Equation> equations = new ArrayList<com.cars.platform.viewmodels.report.Equation>();
		if (company != null) {
			for (Equation equation : Equation.findAllEquationsByCompanySortedByName(company)) {
				equations.add(new com.cars.platform.viewmodels.report.Equation(equation));
			}
		}
		else {
			equations =  getGlobalFormulas();
		}
//		for (Equation equation : Equation.findGlobalEquations()) {
//			equations.add(new com.cars.platform.viewmodels.report.Equation(equation));
//		}
		return equations;
	}
	
	@RequestMapping(value = "/getLocalCDFIFormulas/{id}", method = RequestMethod.GET)
	public @ResponseBody
	List<com.cars.platform.viewmodels.report.Equation> getLocalCDFIFormulas(@PathVariable("id") Long id) {
		Company company = Company.findCompany(id);
		List<com.cars.platform.viewmodels.report.Equation> equations = new ArrayList<com.cars.platform.viewmodels.report.Equation>();
		if (company != null) {
			for (Equation equation : Equation.findLocalEquationsByCompanySortedByName(company)) {
				equations.add(new com.cars.platform.viewmodels.report.Equation(equation));
			}
		}
		return equations;
	}

	
	//-----------------------
	// jsp pages for listing and deleting formulas only. supports /views/formulas
	//-----------------------
	
	
	@RequestMapping(produces = "text/html")
	public String list(@RequestParam(value = "companyId", required = false) Long companyId, Model uiModel) {

		Company company = null;
		List<Equation> equations = null;

		if (companyId != null) {
			company = Company.findCompany(companyId);
		}

		if (company != null) {
			uiModel.addAttribute("company", Company.findCompany(companyId));
			equations = Equation.findEquationsByCompany(company).getResultList();
		} else {

			// EQNFIX
			equations = Equation.findGlobalEquations();
			// equations = Equation.findAllEquations();
		}

		uiModel.addAttribute("equations", equations);

		return "formulas/list";
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
	public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {

		Equation equation = Equation.findEquation(id);
		Company company = equation.getCompany();

		List<Equation> references = equation.findFormulaReferences();
		if (null != references && references.size() > 0) {
			String errorString = "Formula can not be deleted to due reference by other formula(s): ";
			for (Equation reference : references) {
				errorString = errorString + "</br>" + reference.getName();
			}
			uiModel.addAttribute("error", errorString);
		}
		else {
			try {

				equation.remove();
				uiModel.asMap().clear();
				// uiModel.addAttribute("page", (page == null) ? "1" :
				// page.toString());
				// uiModel.addAttribute("size", (size == null) ? "10" :
				// size.toString());
				if (company != null) {
					ReportDataCache.remove(equation.getCompany());
				} else {
					ReportDataCache.removeAll();
				}

			} catch (Exception e) {

				String errorString = "Unable to delete formula";

				if (e.getMessage().contains("ConstraintViolationException")) {
					errorString += " due to related data that requires this record";
				}

				uiModel.addAttribute("error", errorString);
			}
		}

		if (company != null) {
			return "redirect:/formulas?companyId=" + company.getId();
		} else {
			return "redirect:/formulas";
		}
	}
	


}
