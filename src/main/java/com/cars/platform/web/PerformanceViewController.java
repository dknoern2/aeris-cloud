package com.cars.platform.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.PerformanceView;

@RequestMapping("/performanceview/**")
@Controller
public class PerformanceViewController {

   
	@RequestMapping(value = "/performanceview", method = RequestMethod.POST)
	public String analysts(PerformanceView performanceView, Model model) {

		PerformanceView performanceViewSaved = null;

		try{
		    performanceViewSaved = PerformanceView.findPerformanceViewsByCompany(performanceView.getCompany());		
		}catch(Exception ignore){}
		
		if(performanceViewSaved==null){
			performanceView.persist();
		}else{
            performanceViewSaved.setGraphs(performanceView.getGraphs());
		    performanceViewSaved.merge();
		}
		
		
		return "redirect:/";
	}    
    
}
