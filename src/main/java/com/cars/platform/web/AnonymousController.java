package com.cars.platform.web;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Access;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.LendingType;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.SectoralFocus;
import com.cars.platform.domain.State;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;
import com.cars.platform.service.export.FactSheetService;
import com.cars.platform.service.export.TearSheetService;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.CdfiPOJO;

@RequestMapping("/anonymous")
@Controller
public class AnonymousController {
	
	private static final Logger logger = LoggerFactory.getLogger(AnonymousController.class);

	
	
    @RequestMapping(method = RequestMethod.GET)
    public String show(Model model, HttpServletRequest request, HttpServletResponse response) {
    	model.addAttribute("isAnonymous", true);
    	return "anonymous/index";
    }
    
    @RequestMapping(value = "/schedule", method = RequestMethod.GET)
    public String schedule(ModelMap model) {
    	final List<Review> reviews = Review.findAllReviewsPending();
    	
    	//Show only reviews for active companies and for which PeerGroupAllowed = true
    	final List<Review> activeReviews = new ArrayList<Review>();
    	for (Review review : reviews) {
    		if (review.getCompany().isActive() && review.getCompany().isPeerGroupAllowed()) {
    			activeReviews.add(review);
    		}
    	}

    	model.addAttribute("reviews", activeReviews);

    	return "anonymous/schedule";
    }
    
    
    /**
     * REST method for downloading a fact sheet
     */
    @RequestMapping(value = "/tearsheet/{id}", method = RequestMethod.GET)
    public void getFile(
        @PathVariable("id") Long companyId,
         HttpServletRequest request, HttpServletResponse response) {
    	Company company = Company.findCompany(companyId);
    	
   	
        response.setContentType("application/pdf");
    	
    	String filename = "Aeris Fact Sheet for "+company.getName()+".pdf";
    	filename = filename.replaceAll(" ","_");
    	filename = filename.replaceAll(",", "");
    	response.setHeader("Content-Disposition", "attachment; filename=" + filename);
    	
    	ServletContext servletContext = request.getSession().getServletContext();
    	try {
    		FactSheetService tearSheetService = new FactSheetService(company, servletContext);
			tearSheetService.export(response.getOutputStream());
	    	response.flushBuffer();
		} catch (Exception e) {
			e.printStackTrace();
			// rethrowing here gives the user an even worse experience than getting an empty file
			//throw new RuntimeException("Failed to generate tear sheet", e);
		}
    }

    
    @RequestMapping(value = "/getStates", method = RequestMethod.GET)
	public @ResponseBody
	List<String> getStates() {
    	List<String> ans = new ArrayList<String>();
    	for (State state : State.findAllStates()) {
    		ans.add(state.getStateCode());
    	}
		return ans;
	}
   
    @RequestMapping(value = "/getLenderTypes", method = RequestMethod.GET)
	public @ResponseBody
	List<String> getLenderTypes() {
		List<String> ans = new ArrayList<String>();
    	for (LendingType type : LendingType.findAllLendingTypes()) {
    		ans.add(type.getName());
    	}
		return ans;
	}

    @RequestMapping(value = "/getSectoralFocuses", method = RequestMethod.GET)
	public @ResponseBody
	List<String> getSectoralFocuses() {
		List<String> ans = new ArrayList<String>();
    	for (SectoralFocus focus : SectoralFocus.findAllSectoralFocuses()) {
    		ans.add(focus.getName());
    	}
		return ans;
	}
    
    
    @RequestMapping(value = "/getAllCompanies", method = RequestMethod.GET)
	public @ResponseBody
	List<CdfiPOJO> getAllCompanies() {
    	Person person = UserHelper.getCurrentUser();
    	List<Subscription> subs;
    	if (null == person) {
    		subs = new ArrayList<Subscription>();
    	}
    	else {
        	subs = Subscription.findAllActiveSubscriptionsForSubscriber(person.getCompany());
    	}
    	
    	List<CdfiPOJO> cdfis = new ArrayList<CdfiPOJO>();
		for (Company cdfi : Company.findAvailableCDFIs()) {
			CdfiPOJO pojo = null;
			try {
				pojo = new CdfiPOJO(cdfi);
				for (Subscription sub : subs) {
					if (sub.getSubscriptionType() == SubscriptionType.ALL) {
						for (Access access : sub.getCdfis()) {
							if (access.getCompany().getId() == cdfi.getId()) {
								pojo.setSubscriptionId(sub.getId());
							}
						}
					}
				}
				
			}
			// just in case there is a data issue with one CDFI don't want to fail the whole list
			catch(Exception e) {
				logger.error("Unexcpected error with CDFI: " + cdfi.getName(), e);
				continue;
			}
			cdfis.add(pojo);
		}
		return cdfis;
	}
    
    @RequestMapping(value = "/distribution")
	 public String distribution(ModelMap model) {

		TreeMap<String, Integer> impactMap = new TreeMap<String, Integer>();
		TreeMap<String, Integer> financialMap = new TreeMap<String, Integer>();

		List<Company> list = Company.findRatedCDFIs();

		for (Company cdfi : list) {
		    String rating = cdfi.getRating();
		    // rating will be "AA + 2" or "AA 2", need to parse

		    if (rating != null && rating.length() > 2) {
		    	

				String[] parts = rating.split(" ");
				String impact = parts[0];
				String financial = parts[parts.length - 1];

				String imp = "";
				if((impact.compareTo("★★★★") == 0) || (impact.compareTo("****") == 0))
					imp = "four_stars";
				else if (impact.compareTo("★★★") == 0 || (impact.compareTo("***") == 0))
					imp = "three_stars";
				else if (impact.compareTo("★★") == 0 || (impact.compareTo("**") == 0))
					imp = "two_stars";
				else if (impact.compareTo("★") == 0 || (impact.compareTo("*") == 0))
					imp = "one_stars";
				Integer impactTotal = impactMap.get(imp);
				if (impactTotal == null)
				    impactTotal = new Integer(0);
				impactTotal++;
				impactMap.put(imp, impactTotal);
				
				String f ="";
				if (financial.compareTo("AAA") == 0)
					f ="AAA";
				else if (financial.compareTo("AA+") == 0)
					f ="AA_plus";
				else if (financial.compareTo("AA") == 0)
					f ="AA";
				else if (financial.compareTo("AA-") == 0)
					f ="AA_minus";
				else if (financial.compareTo("A+") == 0)
					f ="A_plus";
				else if (financial.compareTo("A") == 0)
					f ="A";
				else if (financial.compareTo("A-") == 0)
					f ="A_minus";
				else if (financial.compareTo("BBB+") == 0)
					f ="BBB_plus";
				else if (financial.compareTo("BBB") == 0)
					f ="BBB";
				else if (financial.compareTo("BBB-") == 0)
					f ="BBB_minus";
				else if (financial.compareTo("BB+") == 0)
					f ="BB_plus";
				else if (financial.compareTo("BB") == 0)
					f ="BB";
				else if (financial.compareTo("BB-") == 0)
					f ="BB_minus";
				else if (financial.compareTo("B") == 0)
					f ="B";

				Integer financialTotal = financialMap.get(f);
				if (financialTotal == null)
				    financialTotal = new Integer(0);
				financialTotal++;
				
				financialMap.put(f, financialTotal);
		    	
		    	/*
		    	String[] parts = rating.split(" ");
		    	String impact = parts[0];
		    	String financial = parts[parts.length - 1];

		    	Integer impactTotal = impactMap.get(impact);
		    	if (impactTotal == null)
		    		impactTotal = new Integer(0);
		    	impactTotal++;
		    	impactMap.put(impact, impactTotal);
			
		    	String f ="";
		    	if (financial.compareTo("1") == 0)
		    		f ="one";
		    	else if (financial.compareTo("2") == 0)
		    		f ="two";
		    	else if (financial.compareTo("3") == 0)
		    		f ="three";
		    	else if (financial.compareTo("4") == 0)
		    		f ="four";

		    	Integer financialTotal = financialMap.get(f);
		    	if (financialTotal == null)
		    		financialTotal = new Integer(0);
		    	financialTotal++;
			
		    	financialMap.put(f, financialTotal);
		    	*/
		    }
		}
		
		model.addAttribute("impactGraph", impactMap);
		model.addAttribute("financialGraph", financialMap);
		
		return "anonymous/distribution";
	 }

}
