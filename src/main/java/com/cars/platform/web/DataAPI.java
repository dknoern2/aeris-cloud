package com.cars.platform.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricValue;

@RequestMapping("/dataapi/**")
@Controller
public class DataAPI {

    @RequestMapping(method = RequestMethod.POST, value = "{id}")
    public void post(@PathVariable Long id, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
    }

    @RequestMapping
    public String index() {
        return "dataapi/index";
    }
    
    @RequestMapping(value = "companymetrics/getRequired/{companyId}/{year}/{quarter}", method = RequestMethod.GET)
	public @ResponseBody
	List<MetricValue> requiredMetrics(@PathVariable long companyId,
			@PathVariable int year, @PathVariable int quarter) {

		System.out.println("Get Required Metrics Call... METRIC VALUE NOT POJO");

		Company company = Company.findCompany(companyId);

		List<MetricValue> metrics = MetricValue.findMetricValues(quarter, year,
				companyId);

		List<Metric> requiredMetrics = company.getRequiredMetrics();

		System.out.println("Required Metrics Count: " + requiredMetrics.size());

		// Loop through all required metrics.
		for (Metric metric : requiredMetrics) {

			// if there is not metric value for the metric then create a new
			// one.
			if (!metricExists(metric, metrics)) {

				MetricValue metricValue = new MetricValue();
				metricValue.setMetric(metric);
				metricValue.setYear(year);
				metricValue.setQuarter(quarter);
				metricValue.setCompany(company);
				metricValue.setId((long) -1);
				metrics.add(metricValue);
			}
		}
		return metrics;

	}
    
    private boolean metricExists(Metric metric, List<MetricValue> metrics) {
		boolean result = false;
		for (MetricValue metricValue : metrics) {

			if (metricValue.getMetric().getId() == metric.getId()) {
				result = true;
				break;
			}
		}
		return result;
	}
}
