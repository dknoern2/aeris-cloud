package com.cars.platform.web;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cars.platform.domain.ActionItem;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Person;
import com.cars.platform.service.ActionItemService;

@RequestMapping("/usermanagement/**")
@Controller
public class UserManagementController {

    @RequestMapping(method = RequestMethod.POST, value = "{id}")
    public void post(@PathVariable Long id, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
    }

    @RequestMapping
    public String index(Model model) {
    	
    	
    	LinkedHashMap<Company,List<ActionItem>> actionItemMap = new LinkedHashMap<Company,List<ActionItem>>();

    	List<Company> cdfis = new ArrayList<Company>();
    	List<Company> subscribers = new ArrayList<Company>();
    	
    	// get all companies then sort
    	
    	List<Company> allCompanies = Company.findAllCompanysAlphabetically();
    	
    	for(Company company:allCompanies){
    		if(CompanyType.CDFI.equals(company.getCompanyType())){
    			cdfis.add(company);
    	    	ActionItemService actionItemService = new ActionItemService(company);
       	    	List<ActionItem> actionItems = actionItemService.getActionItems(false);
                actionItemMap.put(company,actionItems);
    		} else if(CompanyType.INVESTOR.equals(company.getCompanyType())){
    			subscribers.add(company);
    		}
    	}

    	Company cars = Company.findCompanysByCompanyType(CompanyType.CARS).getSingleResult();
    	
    	List<Person> carsUsers = Person.findPeopleByCompany(cars).getResultList();

    	model.addAttribute("cdfis",cdfis);
    	model.addAttribute("subscribers",subscribers);
    	model.addAttribute("carsUsers",carsUsers);		    	
  	    
   	    model.addAttribute("actionItemMap",actionItemMap);
  	
        return "usermanagement/index";
    }
    
}
