package com.cars.platform.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.MetricMap;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.Person;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.PeriodPOJO;
import com.cars.platform.viewmodels.Response;

@RequestMapping("/period/**")
@Controller
public class PeriodController {

    @RequestMapping(value = "period/{companyId}", method = RequestMethod.GET)
    public String index(@PathVariable long companyId, Model model) {

	model.addAttribute("company", Company.findCompany(companyId));
	return "period/index";
    }

    @RequestMapping(value = "period/get/{companyId}", method = RequestMethod.GET)
    public @ResponseBody
    List<PeriodPOJO> getPeriods(@PathVariable long companyId) {

	List<Period> periods = Period.findPeriodsByCompanySorted(Company.findCompany(companyId));
	List<PeriodPOJO> result = new ArrayList<PeriodPOJO>();
	for (Period period : periods) {

	    PeriodPOJO p = new PeriodPOJO();
	    p.setCompanyId(period.getCompany().getId());
	    p.setCompanyName(period.getCompany().getName());
	    p.setEndDate(period.getEndDate());
	    p.setQuarter(period.getQuarter());
	    p.setYear(period.getYear());
	    p.setPeriodType(period.getPeriodType());
	    p.setId(period.getId());
	    result.add(p);

	    if (period.getEndDate() == null) {
		period.setEndDate(DateHelper.getQuarterEndDate(period.getYear(), period.getQuarter(), period
			.getCompany().getFiscalYearStart()));
		period.merge();
	    }
	}

	return result;

    }

    @RequestMapping(value = "/period/save", method = RequestMethod.POST)
    public @ResponseBody
    Response save(@RequestBody PeriodPOJO period) {

	Period foundPeriod = Period.findPeriod(period.getId());
	if (foundPeriod != null) {

	    foundPeriod.setPeriodType(period.getPeriodType());
	    foundPeriod.merge();

	}

	ReportDataCache.removeAndUpdateValueFacts(foundPeriod.getCompany());
	return new Response("");
    }

    @RequestMapping(value = "/period/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public @ResponseBody
    Response delete(@PathVariable long id, HttpServletResponse response) {

	Person person = UserHelper.getCurrentUser();

	String responseString = "";

	if (person.isAdmin()) {

	    Period period = Period.findPeriod(id);
	    if (period != null) {

		try {

		    final List<MetricValue> metricValues = MetricValue.findMetricValues(period.getQuarter(),
			    period.getYear(), period.getCompany().getId());
		    if (metricValues != null) {
			for (MetricValue metricValue : metricValues) {
			    metricValue.getComments().clear();
			    metricValue.merge();
			}
		    }

		    new MetricMap().deleteMetricMaps(period.getCompany(), period.getYear(), period.getQuarter());
		    MetricValue.deleteMetricValues(period.getQuarter(), period.getYear(),
		    		period.getCompany(), true, true);
		    period.remove();

		    ReportDataCache.removeAndUpdateValueFacts(period.getCompany());

		} catch (Exception e) {

		    responseString = "Unable to delete period";

		    if (e.getMessage().contains("ConstraintViolationException")) {
			responseString += " due to related data that requires this record";
		    }

		    try {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, responseString);
		    } catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    }
		}
	    }
	}
	return new Response(responseString);
    }
}
