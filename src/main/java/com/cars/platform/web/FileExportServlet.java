package com.cars.platform.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.PeerGroup;
import com.cars.platform.domain.Person;
import com.cars.platform.service.export.ExportService;
import com.cars.platform.util.AuthorizationHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.peergroup.ExportRequest;

import flexjson.JSONDeserializer;
 
public class FileExportServlet extends HttpServlet {
    private static final long serialVersionUID = -3208409086358916855L;
 
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	    	
    	
    	Long companyId = getId(request, "companyId");
    	Long peerGroupId = getId(request, "peerGroupId");
    	
    	Company company = null;
    	if (null != companyId) {
    		company = Company.findCompany(companyId);
    	}
    	PeerGroup peerGroup = null;
    	if (null != peerGroupId) {
    		peerGroup = PeerGroup.findPeerGroup(peerGroupId);
    	}

    	Person currentUser = UserHelper.getCurrentUser();

    	response.setContentType("application/vnd.ms-excel");
    	ExportService exportService = new ExportService();
   	
    	if (null == peerGroup) {
    		boolean authorized = AuthorizationHelper.isAuthorized(currentUser, company, AuthorizationHelper.ACCESS_TYPE_FINANCIALS);
    		if(!authorized){
    			return;
    		}
    		
	    	String filename = "Aeris Performance Map for "+company.getName()+".xlsx";
	    	filename = filename.replaceAll(" ","_");
	    		
        	// filename should be wrapped in double-quotes according to RFC2231, otherwise download won't work in Chrome.
            response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
	        
	        boolean showChange = Boolean.parseBoolean(request.getParameter("showChange"));
	        boolean showPctChange = Boolean.parseBoolean(request.getParameter("showPctChange"));
	        boolean showAllYears = Boolean.parseBoolean(request.getParameter("showAllYears"));
	
	    	try {
//				exportService.exportFinancials(company,response.getOutputStream(),currentUser.getCompany().isAllRows());
				exportService.exportFinancials(company,response.getOutputStream(),currentUser.getCompany().isAllRows(),
						showChange, showPctChange, showAllYears);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	else {
    		boolean authorized = AuthorizationHelper.isAuthorized(currentUser, company, AuthorizationHelper.ACCESS_TYPE_PEERGROUPS);
    		if(!authorized){
    			return;
    		}
//    		
//    		String requestBody = getBody(request);
//    		ExportRequest exportRequest = getExportRequest(requestBody);
    		
    		ExportRequest exportRequest = getExportRequest(request);
    		
    		String filename = peerGroup.getName() +  ".xls";
    		filename = filename.replaceAll("[^a-zA-Z0-9.-]", "_");
	    		
        	// filename should be wrapped in double-quotes according to RFC2231, otherwise download won't work in Chrome.
            response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
	
	    	try {
				exportService.exportPeerGroup(peerGroup, exportRequest, response.getOutputStream());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
   		
    	}
    	        	

    }
    
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
    	doGet(request, response);
    }


	private Long getId(HttpServletRequest request, String arg) {
		String idString = request.getParameter(arg);
		if (null == idString) {
			return null;
		}
		
		Long id = null;
		try {
		    id = Long.parseLong(idString);
		}
		catch (NumberFormatException e) {
			// ignore
		}
		
		return id;
	}




/* see comment in getExportRequest(HttpServletRequest request)
 	private String getBody(HttpServletRequest request) throws IOException {
	
	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;
	
	    try {
	        InputStream inputStream = request.getInputStream();
	        if (inputStream != null) {
	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	        } else {
	            stringBuilder.append("");
	        }
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        if (bufferedReader != null) {
	            try {
	                bufferedReader.close();
	            } catch (IOException ex) {
	                throw ex;
	            }
	        }
	    }
	
	    body = stringBuilder.toString();
	    return body;
	}

	private ExportRequest getExportRequest(String rawBody)
	{
		if (null == rawBody) {
			return null;
		}
		
		JSONDeserializer<ExportRequest> d = new JSONDeserializer<ExportRequest>();
		ExportRequest exportRequest = null;
		try {
			exportRequest = d.deserialize(rawBody, ExportRequest.class);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return exportRequest;
	}
*/		
	
	// UI was unable to send ExportRequest in the body as that would require an AJAX calls which aren't really allowed to download files
	// the UI 'decoding' the ExportRequest onto the URL results in the whacky 'equationIds[]' parameter value
	private ExportRequest getExportRequest(HttpServletRequest request)
	{
/*		Enumeration<?> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			System.out.println(object.getClass().getName() + ": " + object.toString());
		}
*/		
		ExportRequest exportRequest = new ExportRequest();
		
		exportRequest.setAllYears(Boolean.parseBoolean(request.getParameter("allYears")));
		exportRequest.setShowInterim(Boolean.parseBoolean(request.getParameter("showInterim")));
		exportRequest.setShowIncomplete(Boolean.parseBoolean(request.getParameter("showIncomplete")));
		String[] idStrings = request.getParameterValues("equationIds[]");
		if (null != idStrings) {
			Long[] equationIds = new Long[idStrings.length];
			for (int i=0; i<idStrings.length; i++) {
				equationIds[i] = Long.parseLong(idStrings[i]);
			}
			exportRequest.setEquationIds(equationIds);
		}
		return exportRequest;
	}

}

