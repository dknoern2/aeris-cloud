package com.cars.platform.web;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cars.platform.domain.Company;

public class LogoUploadServlet extends HttpServlet {
    private static final long serialVersionUID = -3208409086358916855L;
 
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        

        byte[] data = null;
        long companyId = 0;
        int logoTypeId;
    
        String filename = null;

        if (isMultipart) {
        	
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
 
            try {
                List items = upload.parseRequest(request);
                
                Company company = null;
                Iterator iterator = items.iterator();
                while (iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();
 
                    if (!item.isFormField()) {
                        filename = item.getName();

                        data = item.get();
                    }else{
                    	
                    	if("companyId".equals(item.getFieldName())){
                    		companyId = getLongOrZero(item.getString());
                    	}else if("logoTypeId".equals(item.getFieldName())){
                    		logoTypeId = getIntOrZero(item.getString());
                    	}                 	
                    }
                }
                
                company = Company.findCompany(companyId);
            
                /*
            	Logo existingLogo = Logo.findLogo(company,documentType,fiscalYear,fiscalQuarter);
            	
				if (existingDocument != null) {
					existingDocument.setFileName(filename);
					existingDocument.setData(data);
					existingDocument.setCompany(company);
					existingDocument.setVersion(existingDocument.getVersion() + 1);
					existingDocument.merge();
				} else {
					Document doc = new Document();
					doc.setFileName(filename);
					doc.setData(data);
					doc.setCompany(company);
	                doc.persist();
				}
                */                               
                response.sendRedirect("cdfi/"+company.getId()+"/library");
               
            } catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private static int getIntOrZero(String s){
    	int i = 0;
    	try{
    	i = Integer.parseInt(s);
    	}catch(Exception ignore){}
    	return i;
    }
    
    private static long getLongOrZero(String s){
    	long i = 0;
    	try{
    	i = Long.parseLong(s);
    	}catch(Exception ignore){}
    	return i;
    }
}