package com.cars.platform.web;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.UnitType;

@RequestMapping("/breakdowncolumns")
@Controller
//@RooWebScaffold(path = "breakdowncolumns", formBackingObject = BreakdownColumn.class)
public class BreakdownColumnController {

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
	public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
		BreakdownColumn breakdownColumn = BreakdownColumn.findBreakdownColumn(id);
		try {
			breakdownColumn.remove();
			uiModel.asMap().clear();
		}
		// TODO do this architecturally?
		catch(Throwable t) {
			uiModel.asMap().clear();
			uiModel.addAttribute("error", "Unable to delete the column. If there is a graph using it, the graph must be deleted first.");
		}
		return "redirect:/breakdowncolumns";
	}

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid BreakdownColumn breakdownColumn, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, breakdownColumn);
            return "breakdowncolumns/create";
        }
        uiModel.asMap().clear();
        breakdownColumn.persist();
        return "redirect:/breakdowncolumns/" + encodeUrlPathSegment(breakdownColumn.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new BreakdownColumn());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (MetricBreakdown.countMetricBreakdowns() == 0) {
            dependencies.add(new String[] { "metricbreakdown", "metricbreakdowns" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "breakdowncolumns/create";
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("breakdowncolumn", BreakdownColumn.findBreakdownColumn(id));
        uiModel.addAttribute("itemId", id);
        return "breakdowncolumns/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("breakdowncolumns", BreakdownColumn.findBreakdownColumnEntries(firstResult, sizeNo));
            float nrOfPages = (float) BreakdownColumn.countBreakdownColumns() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("breakdowncolumns", BreakdownColumn.findAllBreakdownColumns());
        }
        return "breakdowncolumns/list";
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid BreakdownColumn breakdownColumn, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, breakdownColumn);
            return "breakdowncolumns/update";
        }
        uiModel.asMap().clear();
        breakdownColumn.merge();
        return "redirect:/breakdowncolumns/" + encodeUrlPathSegment(breakdownColumn.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, BreakdownColumn.findBreakdownColumn(id));
        return "breakdowncolumns/update";
    }

	void populateEditForm(Model uiModel, BreakdownColumn breakdownColumn) {
        uiModel.addAttribute("breakdownColumn", breakdownColumn);
        uiModel.addAttribute("metricbreakdowns", MetricBreakdown.findAllMetricBreakdowns());
        uiModel.addAttribute("unittypes", Arrays.asList(UnitType.values()));
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
