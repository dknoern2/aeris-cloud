package com.cars.platform.web;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.domain.Access;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;
import com.cars.platform.util.DateHelper;

@RequestMapping("/subscriptions")
@Controller
public class SubscriptionController {

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "text/html")
	public String delete(@PathVariable("id") Long id, Model model) {

		Subscription subscription = Subscription.findSubscription(id);

		model.addAttribute("subscription", subscription);

		return "subscriptions/show";
	}

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
	public String create(@Valid Subscription subscription,
			BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest) {

		String subscriptionTypeError = this.checkForSubscriptionTypeErrors(subscription);
		if (subscriptionTypeError != null) {
			uiModel.addAttribute("error", subscriptionTypeError);
			populateEditForm(uiModel, subscription);
			return "subscriptions/create";
		}

		if (bindingResult.hasErrors()) {
			populateEditForm(uiModel, subscription);
			return "subscriptions/create";
		}

		uiModel.asMap().clear();

		// create access record for owner (CDFI or Subscriber)
		Access access = new Access();
		// access.setEffectiveDate(new Date());
		// access.setExpirationDate(DateUtils.addYears(access.getEffectiveDate(),
		// 1));
		access.setCompany(subscription.getOwner());
		
		SubscriptionType type = subscription.getSubscriptionType();

		if (type.equals(SubscriptionType.LIBRARY) || type.equals(SubscriptionType.SELECT)) {
			subscription.getCdfis().add(access);
		} else {
			subscription.getSubscribers().add(access);
		}
		
		if (type.equals(SubscriptionType.LIBRARY)) {
			subscription.setLibrary(true);
		}
		if (type.equals(SubscriptionType.SELECT)) {
			subscription.setRatings(true);
			subscription.setAnnualReviews(true);
		}

		subscription.persist();
		
		return "redirect:/subscriptions/?companyId="
		+ subscription.getOwner().getId();
	}

	@RequestMapping(value = "{subscriptionId}/access/{accessId}/delete")
	public String removeAccess(@PathVariable Long subscriptionId,
			@PathVariable Long accessId, Model model) {

		Subscription subscription = Subscription
				.findSubscription(subscriptionId);
		Access access = Access.findAccess(accessId);

		if (subscription.getCdfis().contains(access)) {
			subscription.getCdfis().remove(access);
			subscription.merge();
			access.remove();
		} else if (subscription.getSubscribers().contains(access)) {
			subscription.getSubscribers().remove(access);
			subscription.merge();
			access.remove();
		}

		return "redirect:/subscriptions/" + subscriptionId + "?form";

	}

	// Used only for SELECT subscriptions where  access for subscribers is handled individually
	@RequestMapping(value = "{id}/access", method = RequestMethod.POST)
	public String addAccess(
			@PathVariable Long id,
			Model model,
			@RequestParam(value = "accessType", required = true) String accessType,
			@RequestParam(value = "accessCompany", required = true) Long accessCompanyId,
			@RequestParam(value = "effectiveDate", required = true) String effectiveDate,
			@RequestParam(value = "expirationDate", required = false) String expirationDate) {

		Subscription subscription = Subscription.findSubscription(id);
		Company accessCompany = Company.findCompany(accessCompanyId);

//		System.out.println("company: " + accessCompany.getName() + " : "
//				+ accessCompany.getId());
//		System.out.println("accessType: " + accessType);
//		System.out.println("effectiveDate: " + effectiveDate);
//		System.out.println("expirationDate: " + expirationDate);

		Access access = new Access();
		access.setCompany(accessCompany);

		if (effectiveDate != null && effectiveDate.length() > 0) {
			try {
				access.setEffectiveDate(DateHelper
						.parseCompactFormattedDate(effectiveDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if (expirationDate != null && expirationDate.length() > 0) {
			try {
				access.setExpirationDate(DateHelper
						.parseCompactFormattedDate(expirationDate));
			} catch (ParseException e) {

				e.printStackTrace();
			}
		}

		if ("CDFI".equalsIgnoreCase(accessType)) {
			subscription.getCdfis().add(access);
		} else {

			subscription.getSubscribers().add(access);
		}

		subscription.merge();

		return "redirect:/subscriptions/" + id + "?form";

		// return "redirect:/subscriptions";
	}

	@RequestMapping(value = "create/{companyId}", produces = "text/html")
	public String createForm(@PathVariable("companyId") Long companyId,
			Model uiModel) {

		Subscription subscription = new Subscription();
		if (companyId != null) {
			Company owner = Company.findCompany(companyId);
			subscription.setOwner(owner);
		}
		Date today = new Date();
		subscription.setExpirationDate(DateUtils.addYears(today, 1));
		populateEditForm(uiModel, subscription);
		return "subscriptions/create";
	}

	@RequestMapping(produces = "text/html")
	public String list(
			@RequestParam(value = "companyId", required = false) Long companyId,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size,
			Model uiModel) {

		if (companyId != null) {
			Company owner = Company.findCompany(companyId);
			uiModel.addAttribute("owner", owner);
			uiModel.addAttribute("company", owner);
			uiModel.addAttribute("subscriptions", Subscription.findSubscriptionsByOwner(owner).getResultList());
			uiModel.addAttribute("delegatedSubscriptions", Subscription.findDelegatedSubscriptionsBySubscriber(owner));

		} else {

			uiModel.addAttribute("subscriptions", Subscription.findAllSubscriptions());

		}

		addDateTimeFormatPatterns(uiModel);
		return "subscriptions/list";
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
	public String update(@Valid Subscription subscription,
			BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest) {

		String subscriptionTypeError = this.checkForSubscriptionTypeErrors(subscription);
		if (subscriptionTypeError != null) {
			uiModel.addAttribute("error", subscriptionTypeError);
			populateEditForm(uiModel, subscription);
			return "subscriptions/update";
		}

		if (bindingResult.hasErrors()) {
			populateEditForm(uiModel, subscription);
			return "subscriptions/update";
		}
		uiModel.asMap().clear();
		
		if (subscription.getSubscriptionType().equals(SubscriptionType.LIBRARY)) {
			subscription.setLibrary(true);
			subscription.setRatings(false);
			subscription.setAnnualReviews(false);
		}
		if (subscription.getSubscriptionType().equals(SubscriptionType.SELECT)) {
			subscription.setRatings(true);
			subscription.setAnnualReviews(true);
			subscription.setLibrary(false);
		}
		
		subscription.merge();
		return "redirect:/subscriptions/?companyId="
		+ subscription.getOwner().getId();
	}

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		populateEditForm(uiModel, Subscription.findSubscription(id));
		return "subscriptions/update";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
	public String delete(@PathVariable("id") Long id,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size,
			Model uiModel) {

		Subscription subscription = Subscription.findSubscription(id);
		Company company = subscription.getOwner();

		subscription.remove();
		uiModel.asMap().clear();
		uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
		uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
		return "redirect:/subscriptions?companyId=" + company.getId();
	}

	void addDateTimeFormatPatterns(Model uiModel) {
		uiModel.addAttribute("subscription_expirationdate_date_format", DateTimeFormat.patternForStyle("M-",LocaleContextHolder.getLocale()));
	}

	void populateEditForm(Model uiModel, Subscription subscription) {
		uiModel.addAttribute("subscription", subscription);
		addDateTimeFormatPatterns(uiModel);
		Company owner = subscription.getOwner();

		uiModel.addAttribute("owners", Arrays.asList(owner));
		uiModel.addAttribute("owner", owner);

		List<SubscriptionType> subTypesFiltered = new ArrayList<SubscriptionType>();
		List<Company> allCompanies;
		Set<Access> existingCompanies;
		if (owner.isCDFI()) {
			subTypesFiltered.add(SubscriptionType.SELECT);
			subTypesFiltered.add(SubscriptionType.LIBRARY);
			allCompanies = Company.findCompanysByCompanyTypeAlphabetically(CompanyType.INVESTOR);
			existingCompanies = subscription.getSubscribers();
		}
		else {
			subTypesFiltered.add(SubscriptionType.ALL);
			subTypesFiltered.add(SubscriptionType.SINGLE);
			allCompanies = Company.findAvailableCDFIs();
			existingCompanies = subscription.getCdfis();
		}
		
		// Can't change subscription type on update
		if (subscription.getId() != null) {
			uiModel.addAttribute("subscriptiontypes", Arrays.asList(subscription.getSubscriptionType()));
		}
		else {
			uiModel.addAttribute("subscriptiontypes", subTypesFiltered);			
		}
		
		// exclude companies already on the subscription
		List<Company> companies = new ArrayList<Company>();
		for (Company company : allCompanies) {
			boolean found = false;
			for (Access access : existingCompanies) {
				if (access.getCompany().equals(company)) {
					found = true;
					break;
				}
			}
			if (!found) {
				companies.add(company);
			}
		}
		uiModel.addAttribute("companys", companies);
	}

	String encodeUrlPathSegment(String pathSegment,
			HttpServletRequest httpServletRequest) {
		String enc = httpServletRequest.getCharacterEncoding();
		if (enc == null) {
			enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
		}

		try {
			pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
		} catch (UnsupportedEncodingException uee) {
		}

		return pathSegment;
	}

	private String checkForSubscriptionTypeErrors(Subscription subscription) {
		if ((SubscriptionType.LIBRARY.equals(subscription.getSubscriptionType()) 
				|| SubscriptionType.SELECT.equals(subscription.getSubscriptionType()))
				&& subscription.getOwner() != null
				&& subscription.getOwner().isInvestor()) {

			return "Investors may only be assigned All or Single subscription types";
		}

//		if (SubscriptionType.REPORTING.equals(subscription.getSubscriptionType()) && (!subscription.isFinancials() && !subscription.isLibrary())) {
//			return "Reporting services must include Financials and/or Library access";
//		}
//
//		if (!SubscriptionType.REPORTING.equals(subscription.getSubscriptionType()) && (subscription.isLibrary())) {
//			return "Library access settings only applicable to Reporting services";
//		}

		if (subscription.getExpirationDate() == null) {
			return "Subscription expiration date is required";

		}
		
		if (subscription.getOwner() != null && subscription.getOwner().isInvestor()) {
			if (!subscription.isRatings() && !subscription.isFinancials() &&
					!subscription.isPeerGroups() && !subscription.isOpinions() && !subscription.isAnnualReviews()) {
				return "Subscription must have at least one option selected";
			}
			if (subscription.isShowPeerMetrics() && !subscription.isPeerGroups()) {
				return "Showing peer metrics requires access to Peer Groups";
			}
		}
		
		if (SubscriptionType.ALL.equals(subscription.getSubscriptionType())) {
			List<Subscription> activeSubscriptions = Subscription.findActiveOwnedSubscriptionsForSubscriber(subscription.getOwner());
			for (Subscription sub : activeSubscriptions) {
				if (sub.getId().equals(subscription.getId())) {
					continue; // updating an existing subscription
				}
				if (SubscriptionType.ALL.equals(sub.getSubscriptionType())) {
					return "There can only be 1 active subscription of type ALL";					
				}
			}
		}

		return null;
	}

}
