package com.cars.platform.web;

import static com.cars.platform.domain.DocumentTypeFolder.CARS_INTERNAL;
import static com.cars.platform.domain.DocumentTypeFolder.CARS_REPORT;
import static com.cars.platform.domain.DocumentTypeFolder.CARS_RESOURCES;
import static com.cars.platform.domain.DocumentTypeFolder.CARS_ARCHIVE;
import static com.cars.platform.domain.DocumentTypeFolder.CARS_INTERNAL_ARCHIVE;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cars.platform.domain.Activity;
import com.cars.platform.domain.ActivityType;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Person;
import com.cars.platform.service.ZipFileService;
import com.cars.platform.util.ArchivingFileHelper;
import com.cars.platform.util.AuthorizationHelper;
import com.cars.platform.util.UserHelper;

public class LibraryExportServlet extends HttpServlet {
    private static final long serialVersionUID = -3208409086358916855L;

    @Override
    protected void doGet(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {

	long companyId = Long.parseLong(request.getParameter("companyId"));
	int periodsCountCode = Integer.parseInt(request.getParameter("periodsCountCode"));

	Company company = Company.findCompany(companyId);
	Person person = UserHelper.getCurrentUser();

	boolean authorized = AuthorizationHelper.isAuthorized(
		person, company);
	if (!authorized) {
	    return;
	}

	response.setContentType("application/zip");

	List<Document> documents = Document.getDocuments(company, person);

	// ZipFileService zipfile = new ZipFileService();
	// String location = zipfile.createZipFile(documents, person);
	// String filename = "CARS Financials for "+company.getName()+".zip";
	// String filename = person.getUsername() + person.getId() +
	// "library.zip";
	// String filename = location;
	// System.out.println(filename);
	// filename = filename.replaceAll(" ","_");

	LinkedHashMap<DocumentType, ArrayList<Document>> documentMap = new LinkedHashMap<DocumentType, ArrayList<Document>>();

	List<DocumentType> documentTypes = null;

	if (company.isCARS()) {
	    documentTypes = DocumentType
		    .findAllDocumentTypesByParentCategoryName(CARS_RESOURCES);
	} else {
		documentTypes = DocumentType.findAllDocumentTypesSorted(company);
	}

	if (documentTypes != null && documentTypes.size() > 0) {

	    for (DocumentType documentType : documentTypes) {

		if ((!CARS_INTERNAL.equals(documentType
			.getDocumentTypeFolder().getName())
			&& !CARS_REPORT.equals(documentType
				.getDocumentTypeFolder().getName())
			&& !CARS_RESOURCES.equals(documentType
				.getDocumentTypeFolder().getName())
			&& !CARS_INTERNAL_ARCHIVE.equals(documentType
					.getDocumentTypeFolder().getName())
			&& !CARS_ARCHIVE.equals(documentType
					.getDocumentTypeFolder().getName())
			&& (DocumentType.DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID != documentType.getId() 
			|| company.getId() == person.getCompany().getId()) && !person.isAdmin())
			|| (person.isAdmin() && !CARS_RESOURCES
				.equals(documentType.getDocumentTypeFolder()
					.getName()))
			|| (person.getCompany().isCARS() && company.isCARS() && CARS_RESOURCES
				.equals(documentType.getDocumentTypeFolder()
					.getName()))
			|| (person.getCompany().isCARS() && !company.isCARS()

			&& (CARS_REPORT.equals(documentType
				.getDocumentTypeFolder().getName()) || CARS_INTERNAL
				.equals(documentType.getDocumentTypeFolder()
					.getName()) || CARS_INTERNAL_ARCHIVE
				.equals(documentType.getDocumentTypeFolder()
						.getName()))

			)) {
		    ArrayList<Document> documentList = new ArrayList<Document>();
		    documentMap.put(documentType, documentList);
		}
	    }

	    for (Document document : documents) {
		ArrayList<Document> documentList = documentMap.get(document
			.getDocumentType());

		if (documentList != null) {
		    documentList.add(document);
		}
	    }
	}

	// remove empty document types for subscriber

	LinkedHashMap<DocumentType, ArrayList<Document>> prunedDocumentMap = null;

	if (!person.getCompany().isCARS()
		&& person.getCompany().getId() != company.getId()) {
	    prunedDocumentMap = new LinkedHashMap<DocumentType, ArrayList<Document>>();
	    for (DocumentType key : documentMap.keySet()) {
		ArrayList<Document> value = documentMap.get(key);
		if (value != null && value.size() > 0) {
		    prunedDocumentMap.put(key, value);
		}
	    }
	} else {
	    prunedDocumentMap = documentMap;
	}
	
	int mapKeySize = prunedDocumentMap.keySet().size();
	int[] periodsCounts = new int[mapKeySize];
	Arrays.fill(periodsCounts, periodsCountCode);
	ArchivingFileHelper.filterDocumentsMap(prunedDocumentMap, periodsCounts);

	ZipFileService zipfile = new ZipFileService();

	File dir = new File(person.getUsername() + +company.getId());
	String filename = zipfile.createZipFile(prunedDocumentMap, dir);
	response.setHeader("Content-Disposition",
		"attachment; filename=library.zip");

	try {
	    OutputStream out = response.getOutputStream();
	    FileInputStream in = new FileInputStream(filename);
	    byte[] buffer = new byte[4096];
	    int length;
	    while ((length = in.read(buffer)) > 0) {
		out.write(buffer, 0, length);
	    }
	    out.flush();
	    in.close();
	    out.close();
	    File file = new File(filename);
	    file.delete();
	    if (dir.listFiles().length == 0) {
		dir.delete();
		new File(person.getUsername()).delete();
	    }
	    Activity activity = new Activity();
	    activity.setCompany(company);
	    activity.setDate(new java.util.Date());
	    activity.setPerson(person);
	    activity.setDescription("All " + company.getName()
		    + " authorized files for " + person.getUsername());
	    activity.setActivityType(ActivityType.DOCUMENT_DOWNLOAD);
	    activity.persist();

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}
