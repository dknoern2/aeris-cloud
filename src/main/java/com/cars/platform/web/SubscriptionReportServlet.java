package com.cars.platform.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cars.platform.domain.Access;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.Subscription;

public class SubscriptionReportServlet extends HttpServlet {
	private static final long serialVersionUID = 93485732498573485L;

	private String getEmployees(Company company) {
		List<Person> employees = Person.findPeopleByCompany(company).getResultList();
		StringBuilder sb = new StringBuilder();
		sb.append(" Employees: ");
		for (Person person : employees) {
			sb.append(person.getFirstName());
			sb.append(" ");
			sb.append(person.getLastName());
			sb.append(" (");
			sb.append(person.getEmail());
			if (person.getPhone() != null && person.getPhone().length() > 0) {
				sb.append(" phone: ");
				sb.append(person.getPhone());
			}
			sb.append("); ");
		}

		return sb.toString();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter writer = response.getWriter();

		writer.println("Subscription Report " + new Date());

		DateFormat sdf = SimpleDateFormat.getDateInstance();

		List<Subscription> subscriptions = Subscription.findAllSubscriptions();

		for (Subscription s : subscriptions) {

			String companyType = "CDFI";
			if (!s.getOwner().isCDFI())
				companyType = "SUB";
			writer.println("\n========================\n");
			writer.println("OWNER: " + s.getOwner().getName() + " (" + companyType + "), " + s.getSubscriptionType().getText() + ", expires: "
					+ formatDate(s.getExpirationDate()) + getEmployees(s.getOwner()));

			writer.println();
			writer.println("SUBSCRIBERS:");
			for (Access access : s.getSubscribers()) {
				writer.println(access.getCompany().getName() + ", effective " + formatDate(access.getEffectiveDate()) + " to " + formatDate(access.getExpirationDate()));
				writer.println(getEmployees(access.getCompany()));
				writer.println();
			}

			writer.println();
			writer.println("CDFIs:");
			for (Access access : s.getCdfis()) {
				writer.println(access.getCompany().getName() + ", effective " + formatDate(access.getEffectiveDate()) + " to " + formatDate(access.getExpirationDate()));
				writer.println(getEmployees(access.getCompany()));
				writer.println();
			}

		}

		writer.write("complete" + "\n");

	}

	private String formatDate(Date d) {
		DateFormat df = SimpleDateFormat.getDateInstance();
		String s = "";
		if (d != null) {
			s = df.format(d);
		}
		return s;
	}

}
