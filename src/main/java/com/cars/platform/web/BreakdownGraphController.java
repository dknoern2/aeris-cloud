package com.cars.platform.web;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.domain.BreakdownGraph;
import com.cars.platform.domain.BreakdownHistory;
import com.cars.platform.domain.BreakdownPie;
import com.cars.platform.domain.BreakdownTable;
import com.cars.platform.domain.MetricBreakdown;

@RequestMapping("/breakdowngraphs")
@Controller
//@RooWebScaffold(path = "breakdowngraphs", formBackingObject = BreakdownGraph.class)
public class BreakdownGraphController {
	
	@Autowired
	BreakdownHistoryController bhc;
	
	@Autowired
	BreakdownPieController bpc;
	
	@Autowired
	BreakdownTableController btc;

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		BreakdownGraph bg = BreakdownGraph.findBreakdownGraph(id);
		if (bg instanceof BreakdownHistory) {
			return bhc.updateForm(id, uiModel);
		}
		else if (bg instanceof BreakdownPie) {
			return bpc.updateForm(id, uiModel);
		}
		else if (bg instanceof BreakdownTable) {
			return btc.updateForm(id, uiModel);
		}
		else {
	        return "breakdowngraphs";
		}
    }
	
    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        return "breakdowngraphs/create";
    }

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid BreakdownGraph breakdownGraph, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, breakdownGraph);
            return "breakdowngraphs/create";
        }
        uiModel.asMap().clear();
        breakdownGraph.persist();
        return "redirect:/breakdowngraphs/" + encodeUrlPathSegment(breakdownGraph.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("breakdowngraph", BreakdownGraph.findBreakdownGraph(id));
        uiModel.addAttribute("itemId", id);
        return "breakdowngraphs/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("breakdowngraphs", BreakdownGraph.findBreakdownGraphEntries(firstResult, sizeNo));
            float nrOfPages = (float) BreakdownGraph.countBreakdownGraphs() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("breakdowngraphs", BreakdownGraph.findAllBreakdownGraphs());
        }
        return "breakdowngraphs/list";
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid BreakdownGraph breakdownGraph, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, breakdownGraph);
            return "breakdowngraphs/update";
        }
        uiModel.asMap().clear();
        breakdownGraph.merge();
        return "redirect:/breakdowngraphs/" + encodeUrlPathSegment(breakdownGraph.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        BreakdownGraph breakdownGraph = BreakdownGraph.findBreakdownGraph(id);
        breakdownGraph.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/breakdowngraphs";
    }

	void populateEditForm(Model uiModel, BreakdownGraph breakdownGraph) {
        uiModel.addAttribute("breakdownGraph", breakdownGraph);
        uiModel.addAttribute("metricbreakdowns", MetricBreakdown.findAllMetricBreakdowns());
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
