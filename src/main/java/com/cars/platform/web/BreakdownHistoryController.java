package com.cars.platform.web;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.BreakdownHistory;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.util.SelectOptionHelper;

@RequestMapping("/breakdownhistorys")
@Controller
//@RooWebScaffold(path = "breakdownhistorys", formBackingObject = BreakdownHistory.class)
public class BreakdownHistoryController {

	void populateEditForm(Model uiModel, BreakdownHistory breakdownHistory) {
        uiModel.addAttribute("breakdownHistory", breakdownHistory);
        uiModel.addAttribute("breakdowncolumns", BreakdownColumn.findAllBreakdownColumns());
        uiModel.addAttribute("graphtypes", BreakdownHistory.getAvailableGraphTypes());
        uiModel.addAttribute("metricbreakdowns", MetricBreakdown.findAllMetricBreakdowns());
        uiModel.addAttribute("showYears", SelectOptionHelper.getSelectShowYears());
    }

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid BreakdownHistory breakdownHistory, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, breakdownHistory);
            return "breakdownhistorys/create";
        }
        uiModel.asMap().clear();
        breakdownHistory.persist();
        return "redirect:/breakdownhistorys/" + encodeUrlPathSegment(breakdownHistory.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new BreakdownHistory());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (MetricBreakdown.countMetricBreakdowns() == 0) {
            dependencies.add(new String[] { "metricbreakdown", "metricbreakdowns" });
        }
        if (BreakdownColumn.countBreakdownColumns() == 0) {
            dependencies.add(new String[] { "breakdowncolumn", "breakdowncolumns" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "breakdownhistorys/create";
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("breakdownhistory", BreakdownHistory.findBreakdownHistory(id));
        uiModel.addAttribute("itemId", id);
        return "breakdownhistorys/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("breakdownhistorys", BreakdownHistory.findBreakdownHistoryEntries(firstResult, sizeNo));
            float nrOfPages = (float) BreakdownHistory.countBreakdownHistorys() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("breakdownhistorys", BreakdownHistory.findAllBreakdownHistorys());
        }
        return "breakdownhistorys/list";
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid BreakdownHistory breakdownHistory, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, breakdownHistory);
            return "breakdownhistorys/update";
        }
        uiModel.asMap().clear();
        breakdownHistory.merge();
        return "redirect:/breakdownhistorys/" + encodeUrlPathSegment(breakdownHistory.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, BreakdownHistory.findBreakdownHistory(id));
        return "breakdownhistorys/update";
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        BreakdownHistory breakdownHistory = BreakdownHistory.findBreakdownHistory(id);
        breakdownHistory.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/breakdownhistorys";
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
