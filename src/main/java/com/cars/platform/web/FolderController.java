package com.cars.platform.web;

import com.cars.platform.domain.Folder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/folders")
@Controller
@RooWebScaffold(path = "folders", formBackingObject = Folder.class)
public class FolderController {
}
