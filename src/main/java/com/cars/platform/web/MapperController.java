package com.cars.platform.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricMap;
import com.cars.platform.service.MapperService2;
import com.cars.platform.viewmodels.MappedCell;
import com.cars.platform.viewmodels.Response;

//import com.cars.platform.service.MapperService;

@RequestMapping("/mapper/**")
@Controller
public class MapperController {

    @RequestMapping(method = RequestMethod.POST, value = "{id}")
    public void post(@PathVariable Long id, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
    }

    @RequestMapping(value = "mapper/index/{companyId}/{year}/{quarter}")
    public String index(@PathVariable long companyId, @PathVariable int year, @PathVariable int quarter, Model model) {

	Company company = Company.findCompany(companyId);
	model.addAttribute("year", year);
	model.addAttribute("quarter", quarter);
	model.addAttribute("company", company);

	return "mapper/index";
    }

    @RequestMapping(value = "mapper/refresh/{companyId}/{year}/{quarter}")
    @ResponseBody
    public String refreshMetrics(@PathVariable long companyId, @PathVariable int year, @PathVariable int quarter,
	    Model model) {
	Company company = Company.findCompany(companyId);
	if (company == null) {
	    return "Company with such id doesn't exist";
	}
	List<MetricMap> metricMaps = MetricMap.findMetricMaps(company, year, quarter);
	if (metricMaps == null) {
	    return "No metric maps found";
	}
	MapperService2 mapperService = new MapperService2();
	boolean refresh = false;
	for (MetricMap metricMap : metricMaps) {
	    refresh |= mapperService.refreshMetricValue(company, metricMap.getMetric(), year, quarter);
	}
	
	if (refresh) {
	    ReportDataCache.removeAndUpdateValueFacts(company);
	}

	return "OK";
    }

    @RequestMapping(value = "mapper/getRequired/{companyId}/{year}/{quarter}", method = RequestMethod.GET)
    public @ResponseBody
    List<MappedCell> requiredMetrics(@PathVariable long companyId, @PathVariable int year, @PathVariable int quarter) {

		Company company = Company.findCompany(companyId);
		List<Metric> requiredMetrics = company.getRequiredMetrics();
		List<MetricMap> maps = MetricMap.findMetricMaps(company, year, quarter);
		List<MappedCell> mappedCells = new ArrayList<MappedCell>();
		List<Metric> allMetrics = Metric.findAllMetrics();
	
		for (Metric metric : allMetrics) {
		    List<MetricMap> existingMaps = mapExists(metric, maps);
		    // no mapping add a new one
		    if (existingMaps.size() < 1) {
				MappedCell mappedCell = new MappedCell();
				mappedCell.setMetricName(metric.getName());
				mappedCell.setYear(year);
				mappedCell.setQuarter(quarter);
				mappedCell.setCompanyId(company.getId());
				mappedCell.setMetricId(metric.getId());
				mappedCell.setCategoryId(metric.getParent().getId());
				mappedCell.setParentCategoryId(metric.getParent().getParent().getId());
		
				if (isMetricRequired(metric, requiredMetrics)) {
				    mappedCell.setRequired(true);
				} else {
				    mappedCell.setRequired(false);
				}
		
				mappedCells.add(mappedCell);
		    }
		    // mapping exists copy all the values
		    else {
				for (MetricMap existingMap : existingMaps) {
				    MappedCell mappedCell = new MappedCell();
				    mappedCell.setMetricName(existingMap.getMetric().getName());
				    mappedCell.setMetricId(existingMap.getMetric().getId());
				    mappedCell.setYear(year);
				    mappedCell.setQuarter(quarter);
				    mappedCell.setCompanyId(company.getId());
				    mappedCell.setCategoryId(existingMap.getMetric().getParent().getId());
				    mappedCell.setParentCategoryId(existingMap.getMetric().getParent().getParent().getId());
		
				    mappedCell.setLabelText(existingMap.getLabelText());
				    mappedCell.setLabelX(existingMap.getLabelX());
				    mappedCell.setLabelY(existingMap.getLabelY());
				    mappedCell.setLabelZ(existingMap.getLabelZ());
		
				    mappedCell.setValueText(existingMap.getValueText());
				    mappedCell.setValueX(existingMap.getValueX());
				    mappedCell.setValueY(existingMap.getValueY());
				    mappedCell.setValueZ(existingMap.getValueZ());
				    mappedCell.setNegate(existingMap.isNegate());
				    mappedCell.setRequired(true);
				    
				    mappedCell.setFoundAfter(existingMap.getFoundAfter());
				    mappedCell.setFoundBefore(existingMap.getFoundBefore());
				    mappedCell.setValuesRight(existingMap.isValuesRight());
		
				    mappedCells.add(mappedCell);
				}
		    }
		}
	
		return mappedCells;
    }

    @RequestMapping(value = "mapper/save", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> save(@RequestBody MappedCell[] cells) {
		MapperService2 mapperService = new MapperService2();
		boolean successMap = mapperService.remapMetric(cells);
		if (successMap) {
			return new ResponseEntity<String>(HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
    }

    @RequestMapping(value = "mapper/setRequired", method = RequestMethod.POST)
    public @ResponseBody
    Response setRequired(@RequestBody MappedCell cell) {
	Company company = Company.findCompany(cell.getCompanyId());
	Metric currentMetric = Metric.findMetric(cell.getMetricId());
	System.out.println("Current Metric id: " + currentMetric.getId());
	// create a list of metrics from the items
	List<Metric> requiredMetrics = company.getRequiredMetrics();

	if (cell.isRequired()) {
	    requiredMetrics.add(currentMetric);
	} else {
	    for (Metric metric : requiredMetrics) {
		System.out.println("Metric id: " + metric.getId());
		if (metric.getId() == currentMetric.getId()) {
		    requiredMetrics.remove(currentMetric);
		    List<MetricMap> metricMaps = MetricMap.findMetricMaps(company, metric, cell.getYear(),
			    cell.getQuarter());
		    for (MetricMap metricMap : metricMaps) {
			metricMap.remove();
		    }
		    break;
		}
	    }
	}

	company.setRequiredMetrics(requiredMetrics);
	company.merge();

	return new Response("");
    }

    private List<MetricMap> mapExists(Metric metric, List<MetricMap> metricMaps) {

	List<MetricMap> result = new ArrayList<MetricMap>();

	for (MetricMap metricMap : metricMaps) {
	    if (metricMap.getMetric().getId() == metric.getId()) {
		result.add(metricMap);
	    }
	}

	return result;
    }

    private boolean isMetricRequired(Metric metric, List<Metric> requiredMetrics) {

	boolean result = false;

	for (Metric requiredMetric : requiredMetrics) {
	    if (requiredMetric.getId() == metric.getId()) {
		result = true;
		break;
	    }
	}

	return result;
    }
}
