package com.cars.platform.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cars.platform.domain.Person;

@RequestMapping("/password/**")
@Controller
public class PasswordController {

	@RequestMapping(method = RequestMethod.POST)
	public String post(Long personId,HttpServletRequest request, HttpServletResponse response,Model model) {
		
		 String username = request.getParameter("username");
		 
		 String oldPassword = request.getParameter("oldPassword");
		 String newPassword = request.getParameter("newPassword");
		 String confirmPassword = request.getParameter("confirmPassword");
		 	 
		 Person person = Person.findPeopleByUsernameEquals(username);
		 		 		 
		String errorPage = "password/index";
		 if(!person.getPassword().equals(oldPassword)){
			 model.addAttribute("passwordChangeError", "wrong old password entered");
			 return errorPage; 
		 }
		
		 if(!newPassword.equals(confirmPassword)){
			 model.addAttribute("passwordChangeError", "new and confirmation passwords did not match");	 		 
			 return errorPage;
		 }
			
		 if(oldPassword.equals(newPassword)){
			 model.addAttribute("passwordChangeError", "your new password must be different from your old password");	 		 
			 return errorPage;
		 }		 
		 person.setPassword(newPassword);
		 person.merge();
 
		return "password/changed";
	}

	@RequestMapping
	public String index() {
		
		
		
		
		return "password/index";
	}
}
