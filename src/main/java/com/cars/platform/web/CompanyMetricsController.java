package com.cars.platform.web;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricCategory;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.viewmodels.MetricValuePOJO;
import com.cars.platform.viewmodels.Response;

@Controller
public class CompanyMetricsController {

	/*
	 * 
	 * @RequestMapping(method = RequestMethod.POST, value = "{id}") public void
	 * post(@PathVariable Long id, ModelMap modelMap, HttpServletRequest
	 * request, HttpServletResponse response) { }
	 */

	@RequestMapping(value = "companymetrics/{companyId}", method = RequestMethod.GET)
	public String getReportingPeriods(@PathVariable Long companyId, Model model) {

		// figure out how to get the correct year/quarters.
		String[] quarters = new String[4];
		quarters[0] = "1";
		quarters[1] = "2";
		quarters[2] = "3";
		quarters[3] = "4";

		model.addAttribute("quarters", quarters);
		return "companymetrics/index";
	}

	@RequestMapping(value = "companymetrics/{companyId}/{year}/{quarter}", method = RequestMethod.GET)
	public String getMetricValues(@PathVariable long companyId,
			@PathVariable int year, @PathVariable int quarter, Model model) {

		List<MetricValue> metricValues = MetricValue.findMetricValues(quarter,
				year, companyId);

		model.addAttribute("metricValues", metricValues);
		model.addAttribute("companyId", companyId);
		model.addAttribute("year", year);
		model.addAttribute("quarter", quarter);

		return "companymetrics/list";

	}

	@RequestMapping(value = "companymetrics/getRequired/{companyId}/{year}/{quarter}", method = RequestMethod.GET)
	public @ResponseBody
	List<MetricValue> requiredMetrics(@PathVariable long companyId,
			@PathVariable int year, @PathVariable int quarter) {

		System.out
				.println("Get Required Metrics Call... METRIC VALUE NOT POJO");

		Company company = Company.findCompany(companyId);

		List<MetricValue> metrics = MetricValue.findMetricValues(quarter, year,
				companyId);

		List<Metric> requiredMetrics = company.getRequiredMetrics();

		System.out.println("Required Metrics Count: " + requiredMetrics.size());

		// Loop through all required metrics.
		for (Metric metric : requiredMetrics) {

			// if there is not metric value for the metric then create a new
			// one.
			if (!metricExists(metric, metrics)) {

				MetricValue metricValue = new MetricValue();
				metricValue.setMetric(metric);
				metricValue.setYear(year);
				metricValue.setQuarter(quarter);
				metricValue.setCompany(company);
				metricValue.setId((long) -1);
				metrics.add(metricValue);
			}
		}
		return metrics;

	}

	@RequestMapping(value = "companymetrics/add/{companyId}/{year}/{quarter}")
	public String create(@PathVariable long companyId, @PathVariable int year,
			@PathVariable int quarter, Model model) {
		model.addAttribute("companyId", companyId);
		model.addAttribute("year", year);
		model.addAttribute("quarter", quarter);
		return "companymetrics/create";
	}

	/*@RequestMapping(value = "companymetrics/save", method = RequestMethod.POST)
	public @ResponseBody
	Response save(Model model, @RequestBody MetricValuePOJO[] metrics) {
		try {

			if (metrics == null) {
				throw new IllegalArgumentException(
						"There are no metric values submitted.");
			}

			for (MetricValuePOJO metricValuePOJO : metrics) {

				// check if the value exists in the database
				MetricValue metricValue;
				metricValue = MetricValue.findMetricValue(metricValuePOJO
						.getId());

				// new metrics
				if (metricValue == null && metricValuePOJO.getAmountValue() != null)

				{
					metricValue = new MetricValue();
					metricValue.setAmount(metricValuePOJO.getAmountValue());
					metricValue.setQuarter(metricValuePOJO.getQuarter());
					metricValue.setYear(metricValuePOJO.getYear());
					metricValue.setCompany(Company.findCompany(metricValuePOJO
							.getCompanyId()));
					metricValue.setMetric(Metric.findMetric(metricValuePOJO
							.getMetricId()));
					metricValue.setAmount(metricValuePOJO.getAmountValue());
					metricValue.merge();

				}

				// existing metrics
				else if (metricValue != null) {
					if (metricValuePOJO.getAmountValue() == null) {
						// Delete the row if the amount is set to null.
						metricValue.remove();

					} else {
						metricValue.setAmount(metricValuePOJO.getAmountValue());
						metricValue.merge();
					}
				}

			}

			model.addAttribute("companyId", metrics[0].getCompanyId());
			model.addAttribute("year", metrics[0].getYear());
			model.addAttribute("quarter", metrics[0].getQuarter());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Response("companymetrics/" + metrics[0].getCompanyId() + "/"
				+ metrics[0].getYear() + "/" + metrics[0].getQuarter());
	}*/

	private boolean metricExists(Metric metric, List<MetricValue> metrics) {
		boolean result = false;
		for (MetricValue metricValue : metrics) {

			if (metricValue.getMetric().getId() == metric.getId()) {
				result = true;
				break;
			}
		}
		return result;
	}

}
