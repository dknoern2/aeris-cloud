package com.cars.platform.web;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.ReviewType;
import com.cars.platform.util.SelectOptionHelper;
import com.cars.platform.util.UserHelper;

@RequestMapping("/reviews")
@Controller
//@RooWebScaffold(path = "reviews", formBackingObject = Review.class)
public class ReviewController {
	
	@RequestMapping(value = "createFull/{companyId}", produces = "text/html")
	public String createFull(@PathVariable("companyId") Long companyId, Model uiModel) {
		Company company = Company.findCompany(companyId);
		Review review = new Review();
		review.setCompany(company);
		review.setReviewType(ReviewType.ANNUAL_RATING);
		//review.setQuarter(4);
		populateEditForm(uiModel, review);
		return "reviews/create";
	}

	@RequestMapping(value = "createAnnual/{companyId}", produces = "text/html")
	public String createAnnual(@PathVariable("companyId") Long companyId, Model uiModel) {
		Company company = Company.findCompany(companyId);
		Review review = new Review();
		review.setCompany(company);
		review.setReviewType(ReviewType.ANNUAL);
		review.setQuarter(4);
		populateEditForm(uiModel, review);
		return "reviews/create";
	}

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Review review, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, review);
            return "reviews/create";
        }
        uiModel.asMap().clear();
        if (review.getImpactPerformance().equals(SelectOptionHelper.SELECT)) {
        	review.setImpactPerformance(null);
        }
        if (review.getFinancialStrength().equals(SelectOptionHelper.SELECT)) {
        	review.setFinancialStrength(null);
        }
        review.persist();
        return "redirect:/cdfi/" + review.getCompany().getId() + "/ratings";
    }
	
	
	@RequestMapping(value = "update/{id}", produces = "text/html")
    public String update(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Review.findReview(id));
        return "reviews/update";
    }
	
	
	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Review review, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        System.out.println("Impact performance 2:");
        System.out.println(review.getImpactPerformance());
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, review);
            return "reviews/update";
        }
        uiModel.asMap().clear();
        if (review.getImpactPerformance().equals(SelectOptionHelper.SELECT)) {
        	review.setImpactPerformance(null);
        }
        if (review.getFinancialStrength().equals(SelectOptionHelper.SELECT)) {
        	review.setFinancialStrength(null);
        }
        review.merge();
        return "redirect:/cdfi/" + review.getCompany().getId() + "/ratings";
    }


	@RequestMapping(value = "/getRating/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getRating(@PathVariable("id")Long id){
		Review r = Review.findReview(id);
        System.out.println("Impact performance 3:");
        System.out.println(r.getImpactPerformance());
		return r.getImpactPerformance();
	}


	private void populateEditForm(Model uiModel, Review review) {
		uiModel.addAttribute("person", UserHelper.getCurrentUser());
		uiModel.addAttribute("company", review.getCompany());
        uiModel.addAttribute("review", review);
        uiModel.addAttribute("years", SelectOptionHelper.getSelectYears(11, 1));
        uiModel.addAttribute("quarters", SelectOptionHelper.getSelectQuarters());
        if (review.isFull()) {
	        uiModel.addAttribute("impacts", getSelectionList(Review.GetAllowedFullImpactValues()));
	        uiModel.addAttribute("strengths", getSelectionList(Review.GetAllowedFullFinancialStrengthValues()));
        }
        else {
	        uiModel.addAttribute("impacts", getSelectionList(Review.ANNUAL_IMPACT_VALUES));
	        uiModel.addAttribute("strengths", getSelectionList(Review.ANNUAL_FINANCIAL_STRENGTH_VALUES));        	
        }
        addDateTimeFormatPatterns(uiModel);
        
        // values that are preset and can not be modified (but the damn UI has to show them or we don't get them back on POST/PUT)
        uiModel.addAttribute("companys", Arrays.asList(review.getCompany()));
        uiModel.addAttribute("reviewtypes", Arrays.asList(review.getReviewType()));
    }
	
	
	private List<String> getSelectionList(String[] options) {
		List<String> selectionList = new ArrayList<String>();
		selectionList.add(SelectOptionHelper.SELECT);
		selectionList.addAll(Arrays.asList(options));
		return selectionList;
	}
	
	void addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("review_dateofanalysis_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }
	
	
	// TODO remove unused ROO stuff below
/*	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Review());
        return "reviews/create";
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("review", Review.findReview(id));
        uiModel.addAttribute("itemId", id);
        return "reviews/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("reviews", Review.findReviewEntries(firstResult, sizeNo));
            float nrOfPages = (float) Review.countReviews() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("reviews", Review.findAllReviews());
        }
        addDateTimeFormatPatterns(uiModel);
        return "reviews/list";
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Review review = Review.findReview(id);
        review.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/reviews";
    }


	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Review.findReview(id));
        return "reviews/update";
    }*/
}
