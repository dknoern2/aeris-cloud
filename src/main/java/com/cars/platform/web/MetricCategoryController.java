package com.cars.platform.web;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.domain.MetricCategory;
import com.cars.platform.domain.ReportCategory;

@RequestMapping("/metriccategories")
@Controller
@RooWebScaffold(path = "metriccategories", formBackingObject = MetricCategory.class)
public class MetricCategoryController {

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
		
		
//        if (page != null || size != null) {
//            int sizeNo = size == null ? 10 : size.intValue();
//            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
//            uiModel.addAttribute("metriccategorys", MetricCategory.findMetricCategoryEntries(firstResult, sizeNo));
//            float nrOfPages = (float) MetricCategory.countMetricCategorys() / sizeNo;
//            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
//        } else {
//            uiModel.addAttribute("metriccategorys", MetricCategory.findAllMetricCategorys());
//        }
        
        uiModel.addAttribute("metriccategorys", MetricCategory.findMetricCategoriesHierarchy());

        return "metriccategories/list";
    }
	
	
	// work around roo generated view not supporting optional relationship
	void populateEditForm(Model uiModel, MetricCategory metricCategory) {
        uiModel.addAttribute("metricCategory", metricCategory);
        
    	List<MetricCategory> parents = MetricCategory.findParentCategories();
    	MetricCategory blank = new MetricCategory();
    	blank.setName("--- NONE ---");
    	blank.setId(0L);
    	parents.add(0, blank);
    	parents.remove(metricCategory); // can't set parent to yourself

        uiModel.addAttribute("metriccategorys", parents);
    }


	
	// stupid Aspect-J refactoring pushed the wrong thing in
	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }


}
