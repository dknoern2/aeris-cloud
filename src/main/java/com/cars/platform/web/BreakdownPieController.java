package com.cars.platform.web;
import com.cars.platform.domain.BreakdownPie;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/breakdownpies")
@Controller
@RooWebScaffold(path = "breakdownpies", formBackingObject = BreakdownPie.class)
public class BreakdownPieController {
}
