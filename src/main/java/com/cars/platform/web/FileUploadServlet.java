package com.cars.platform.web;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Activity;
import com.cars.platform.domain.ActivityType;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.DocumentTypeCategory;
import com.cars.platform.domain.DocumentTypeFolder;
import com.cars.platform.domain.Footnote;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.DocumentTypeFormat;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.Person;
import com.cars.platform.service.BreakdownImportService;
import com.cars.platform.service.BreakdownImportService.BreakdownImportException;
import com.cars.platform.service.DocumentDataService;
//import com.cars.platform.service.MapperService;
import com.cars.platform.service.MapperService2;
import com.cars.platform.util.ExcelHelper;
import com.cars.platform.util.SpringApplicationContext;
import com.cars.platform.util.UserHelper;
import com.smartxls.WorkBook;


public class FileUploadServlet extends HttpServlet {
    private static final long serialVersionUID = -3208409086358916855L;
    
    private static final String FIN_STATEMENT_EXISTS_ERROR = "Only+one+financial+statement+per+period+is+allowed." +
    		"%20If+you+want+to+upload+new+document,%20please+delete+existing+financial+statement+for+selected+period+first.";

    private DocumentDataService documentDataService = SpringApplicationContext.getDocumentDataService();
    
    private static final Logger logger = LoggerFactory.getLogger(FileUploadServlet.class);
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        
    	Person currentUser = UserHelper.getCurrentUser();
        Period.findPeriod(1L);
        long personId = 0;
        byte[] data = null;
        long companyId = 0;
        int fiscalYear = 0;
        int fiscalQuarter = 0;
        String filename = null;
        Long documentTypeId = null;
        DocumentType documentType = null;

        if (isMultipart) {
        	
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            Company company = null;
            try {
                List items = upload.parseRequest(request);
                
                Iterator iterator = items.iterator();
                while (iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();
 
                    if (!item.isFormField()) {
                        filename = item.getName();

                        data = item.get();
                    }else{
                    	
                    	if("companyId".equals(item.getFieldName())){
                    		companyId = getLongOrZero(item.getString());
                    	}else if("fiscalYear".equals(item.getFieldName())){
                    		fiscalYear = getIntOrZero(item.getString());
                    	}else if("fiscalQuarter".equals(item.getFieldName())){
                    		fiscalQuarter = getIntOrZero(item.getString());
                    	}else if("personId".equals(item.getFieldName())){
                    		personId = getLongOrZero(item.getString());
                    	}else if("documentType".equals(item.getFieldName())){
                    		
                    		// first, assume it's an ID
                    		try{
                    			documentTypeId = Long.parseLong(item.getString());
                    		}catch(Exception e){}
                    		
                    		// if not, try document type name
                    		if(documentTypeId==null){
                    			try{
                    			
                    			    DocumentType namedDocumentType = DocumentType.findDocumentTypesByNameEquals(item.getString()).getSingleResult();

                    			    if(namedDocumentType!=null){
                    			    	documentTypeId= namedDocumentType.getId();
                    			    }
                    			}catch(Exception ignore){}
                    		}
                    	}                 	
                    }
                }
                
                company = Company.findCompany(companyId);

                Person submittedBy = Person.findPerson(personId);
                documentType = DocumentType.findDocumentType(documentTypeId);
                
                
                // for certain docs ignore period
                
                if(DocumentType.DOCUMENT_TYPE_HISTORIC_FINANCIALS.equalsIgnoreCase(documentType.getName())){
                	fiscalYear=0;
                	fiscalQuarter=0;
                }else if(DocumentType.DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID == documentType.getId() 
                	|| DocumentType.DOCUMENT_TYPE_BREAKDOWN_ID == documentType.getId()
                	|| DocumentTypeFolder.CARS_REPORT.equalsIgnoreCase(documentType.getDocumentTypeFolder().getName())){      
                	if((fiscalQuarter < 1) || (fiscalYear < 1)){
                        response.sendRedirect("documentupload?companyId="+company.getId()+"&error=This+document+type+requires+fiscal+year+and+quarter.+Please+try+again.");
                        return;
                	}
                }
                
                Document doc = null;              
            	Document existingDocument = null;
            	
            	//Only one interim financial statement allowed for each period.
            	if (DocumentType.DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID == documentType.getId()) {
            		final List<Document> docs =  Document.findDocument(company,documentType,fiscalYear,fiscalQuarter);
            		if (!docs.isEmpty()) {
            			logger.error("Can't upload interim financial statement, it already exists for company " + company.getId() + " for period " + fiscalYear + " Q" + fiscalQuarter);
            			response.sendRedirect("documentupload/required/"+company.getId()+"/"+documentType.getId()+"/"+fiscalYear+"/"+fiscalQuarter+"?error=" +  FIN_STATEMENT_EXISTS_ERROR); 
            			return;
            		}
            	}
            	
            	try{
            		if(DocumentType.DOCUMENT_TYPE_CORPORATE_LOGO.equalsIgnoreCase(documentType.getName())){
            			List<Document> oldLogos = Document.findDocument(company, documentType);            			
            			for(Document oldLogo:oldLogos){
            				oldLogo.remove();
            			}
            		}else{           		
            		    existingDocument = Document.findDocument(company,documentType,fiscalYear,fiscalQuarter,filename);
            		}
            	}catch(Exception ignore){
            	}	
           	           	
			    if (existingDocument != null) {				
					// for corporate logo and historic financials there can only be one.
					if (DocumentType.DOCUMENT_TYPE_HISTORIC_FINANCIALS.equalsIgnoreCase(documentType.getName())
							|| DocumentType.DOCUMENT_TYPE_BREAKDOWN_ID == documentType.getId())
					{
						existingDocument.setFileName(filename);
						documentDataService.createDocumentData(existingDocument.getCompany().getId(),existingDocument.getId(),data);
						existingDocument.setSubmittedBy(submittedBy);
						existingDocument.setSubmittedDate(new Date());
						existingDocument.setFiscalYear(fiscalYear);
						existingDocument.setFiscalQuarter(fiscalQuarter);
						existingDocument.setCompany(company);
						existingDocument.setVersion(existingDocument.getVersion() + 1);
						existingDocument.merge();
						doc=existingDocument;		
					}else{		
				         response.sendRedirect("documentupload/required/"+company.getId()+"/"+documentType.getId()+"/"+fiscalYear+"/"+fiscalQuarter+"?error=Document+already+exists.+Please+rename+or+delete+existing+document+and+try+again.");
				     return;
					}
		
				} else {
					Boolean isExcel = filename!=null && (filename.endsWith(".xlsx") || filename.endsWith(".xls"));
					if (documentType.getDocumentTypeFormat() == DocumentTypeFormat.EXCEL){
						if (!isExcel) {
							response.sendRedirect("documentupload/required/"+company.getId()+"/"+documentType.getId()+"/"+fiscalYear+"/"+fiscalQuarter
									+"?error=For+this+document+type+you+can+upload+only+Excel+documents.");
							return;
						}
	                }
					
					if (isExcel)
					{
						Boolean isWrongExcel = false;
						try{
							WorkBook wb = new WorkBook();
							InputStream is = new ByteArrayInputStream(data);
							isWrongExcel = !ExcelHelper.readWorkBook(wb, is);
						}
						catch(Exception ex)
						{
							isWrongExcel = true;
						}
						if (isWrongExcel)
						{
							response.sendRedirect("documentupload/required/"+company.getId()+"/"+documentType.getId()+"/"+fiscalYear+"/"+fiscalQuarter
									+"?error=The+format+of+Excel+file+you're+uploading,+is+not+supported.+Please+save+it+as+Excel+2003+or+2007+format.");
							return;
						}
					}
					doc = new Document();
					doc.setFileName(filename);
					doc.setSubmittedBy(submittedBy);
					doc.setFiscalQuarter(fiscalQuarter);
					doc.setFiscalYear(fiscalYear);
					doc.setSubmittedDate(new Date());
					doc.setFiscalYear(fiscalYear);
					doc.setFiscalQuarter(fiscalQuarter);
					doc.setDocumentType(documentType);
					doc.setCompany(company);
	                doc.persist();
	                
                    documentDataService.createDocumentData(doc.getCompany().getId(),doc.getId(),data);
	                
	                if(doc.getDocumentType().getDocumentTypeFolder().name.equalsIgnoreCase(DocumentTypeFolder.CARS_REPORT) 
	                		&& (doc.getDocumentType().isFull() || doc.getDocumentType().isAnnual() || doc.getDocumentType().isAnnualIfAvailable())){
	                	if(company.isSuppress()){
	                		company.setSuppress(false);
	                		company.merge();
	                	}
	                }
				}
                 
                if(DocumentType.DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID == documentType.getId()){      
                	               	                	
            		ByteArrayInputStream bis = new ByteArrayInputStream(data);
            		//MapperService mapperService = new MapperService();
            		MapperService2 mapperService = new MapperService2();
            		
            		//int cellCount = mapperService.map(bis, company,fiscalYear,fiscalQuarter);
            		int cellCount = mapperService.map(doc.getFileName(), bis, company,fiscalYear,fiscalQuarter);
            		
            		System.out.println("Mapped "+ cellCount + " cells from quarterly financial document for "+ company.getName() +" " + fiscalYear + "Q"+fiscalQuarter);
           	
            		// end quarterly mapping
           		
                }else if(DocumentType.DOCUMENT_TYPE_HISTORIC_FINANCIALS.equalsIgnoreCase(documentType.getName())){
                	System.out.println("LOADING HISTORIC DATA");
                	                	                	               	
                	//MapperService mapperService = new MapperService();
                	MapperService2 mapperService = new MapperService2();
            		List<Metric> metrics = Metric.findAllMetrics();

            		if(metrics==null ||metrics.size()==0){
            			request.setAttribute("error", "No data point mappings specified yet.");
            			response.sendRedirect("documentupload?companyId="+company.getId()+"&error=No+data+point+mappings+defined");
            			return;
            		}
            		
            		ByteArrayInputStream bis = new ByteArrayInputStream(data);                   
            		//Collection<MetricValue> metricValues = mapperService.mapHistoric(bis, company, metrics);
            		Collection<MetricValue> metricValues = mapperService.mapHistoric(doc.getFileName(), bis, company, metrics);
 
            		if(metricValues==null ||metricValues.size()==0){
                        response.sendRedirect("cdfi/"+company.getId()+"/library?error=No+historic+data+could+be+parsed+from+document.");
                        return;
            		}else{
            		System.out.println("found " + metricValues.size() + " metric values");
            		
            		for(MetricValue metricValue:metricValues){
           			
                		MetricValue oldMetricValue = MetricValue.findMetricValue(metricValue.getQuarter(), metricValue.getYear(), metricValue.getCompany().getId(), metricValue.getMetric().getId());

                		if(oldMetricValue!=null){
                			oldMetricValue.setAmount(metricValue.getAmountValue());
                			oldMetricValue.merge();
                		}else{
            			    metricValue.persist();
                		}
            		}  
            		           		
            		bis = new ByteArrayInputStream(data);                   
            		//List<Footnote> footnotes = mapperService.mapHistoricFootnotes(bis, company);
            		List<Footnote> footnotes = mapperService.mapHistoricFootnotes(doc.getFileName(), bis, company);
 
            		System.out.println("FOUND " + footnotes.size() + " historic footnotes");
            		for(Footnote footnote:footnotes){
            			Footnote existingFootnote = Footnote.findFootnoteByCompanyAndLabel(company, footnote.getLabel());
            			if(existingFootnote!=null){
            				existingFootnote.remove();
            			}
            				
                		footnote.setPerson(submittedBy);
                		footnote.persist();               			
            				
            			}
            		}
            		
            		ReportDataCache.removeAndUpdateValueFacts(company);
            		
                }
                
                else if (DocumentType.DOCUMENT_TYPE_BREAKDOWN_ID == documentType.getId()) {
            		ByteArrayInputStream bis = new ByteArrayInputStream(data);
            		
            		BreakdownImportService breakdownService = new BreakdownImportService();
            		Collection<MetricValue> metricValues = null;
            		try {
            			metricValues = breakdownService.createMetrics(filename, bis, company, fiscalYear, fiscalQuarter);
            		}
            		catch (BreakdownImportException e) {
            			storeBreakdownValidationFile(e.getWorkBook(), doc);
            			String error = "The+file+you+uploaded+contains+validation+errors.+Plase+download+the+file+below,+correct+comments+and+try+again.";
            			request.setAttribute("error", "The file you uploaded contained validation errors"); // TODO does this actaully do anything
            			response.sendRedirect("cdfi/"+company.getId()+"/library?error="+error);
            			return;            			
            		}
            		if (null == metricValues || metricValues.size() == 0) {
            			request.setAttribute("error", "No metrics were found in this file.");
            			response.sendRedirect("documentupload?companyId="+company.getId()+"&error=No+metrics+were+found+in+file+uploaded.");
            			return;            			
            		}
            		else {
            			breakdownService.saveMetrics();
            		}
                }
                 
                // go back to dashboard if CDFI entered a new corporate logo
                if("Corporate Logo".equals(documentType.getName())){
                	

                	if(currentUser.getCompany().getId()==company.getId()){
                    	response.sendRedirect("");    
                    	return;
                	}else{
                       	response.sendRedirect("cdfi/"+company.getId()); 
                       	return;
                		
                	}
                }
                
                // go back to ratings page if rating logo uploaded
                if("Corporate Logo".equals(documentType.getName())
                		
                      	|| "Rated Logo PNG".equals(documentType.getName())
                      	|| "Rated Logo EPS".equals(documentType.getName())
                      	|| "Rated Since Logo PNG".equals(documentType.getName())
                      	|| "Rated Since Logo EPS".equals(documentType.getName())
                		){
                	response.sendRedirect("cdfi/"+company.getId()+"/ratings");
                	return;
                }                
                
        		Activity activity = new Activity();
        		activity.setCompany(company);
        		activity.setDate(new java.util.Date());
        		activity.setPerson(currentUser);
        		activity.setDescription(doc.getFileName());
        		activity.setActivityType(ActivityType.DOCUMENT_UPLOAD);
        		activity.persist();                                                 
                response.sendRedirect("cdfi/"+company.getId()+"/library?highlight=Document+uploaded");
                   
           //}catch(OfficeXmlFileException e){
            	
           //     response.sendRedirect("documentupload?companyId="+company.getId()+"&error=This+document+type+is+required+to+be+in+Excell+2003+format.+Please+convert+and+try+again.");
           //     return;
           
            } catch (FileUploadException e) {
                e.printStackTrace();
            }catch(IOException e){
               	            	
            	if(e.getMessage().startsWith("Invalid header signature")){
			        response.sendRedirect("documentupload/required/"+company.getId()+"/"+documentType.getId()+"/"+fiscalYear+"/"+fiscalQuarter+"?error=This+document+does+not+appear+to+be+in+Excel+2003+format.+Please+check+file+and+try+again.");             
            	}else{
			        response.sendRedirect("documentupload/required/"+company.getId()+"/"+documentType.getId()+"/"+fiscalYear+"/"+fiscalQuarter+"?error=This+document+format+could+not+be+handled.+Please+check+file+and+try+again.");       	
            	}
                 
            } catch (Exception e) {

            	if(e.getMessage()!=null && e.getMessage().startsWith("The supplied POIFSFileSystem does not contain a")){
			        response.sendRedirect("documentupload/required/"+company.getId()+"/"+documentType.getId()+"/"+fiscalYear+"/"+fiscalQuarter+"?error=This+document+does+not+appear+to+be+in+Excel+2003+format.+Please+check+file+and+try+again.");             
                    return;
            	}else{
            		e.printStackTrace();
			        response.sendRedirect("documentupload/required/"+company.getId()+"/"+documentType.getId()+"/"+fiscalYear+"/"+fiscalQuarter+"?error=This+document+format+could+not+be+handled.+Please+check+file+and+try+again.");       	

			        return;
            	
            	}

            }
        }
    }
    
    
    
    private void storeBreakdownValidationFile(WorkBook workBook, Document doc) throws Exception {
    	String fileName = doc.getFileName();
    	String needAnX = fileName.endsWith("xls") ? "x" : "";
		doc.setFileName("VALIDATION_ERRORS_" + fileName + needAnX);
		doc.merge();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		workBook.writeXLSX(baos);
		byte[] data = baos.toByteArray();
		
        documentDataService.createDocumentData(doc.getCompany().getId(),doc.getId(),data);
	}

	private static int getIntOrZero(String s){
    	int i = 0;
    	try{
    	i = Integer.parseInt(s);
    	}catch(Exception ignore){}
    	return i;
    }
    
    private static long getLongOrZero(String s){
    	long i = 0;
    	try{
    	i = Long.parseLong(s);
    	}catch(Exception ignore){}
    	return i;
    }
}
