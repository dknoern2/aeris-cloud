package com.cars.platform.web;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.cache.TentativeChangeCache;
import com.cars.platform.domain.Activity;
import com.cars.platform.domain.ActivityType;
import com.cars.platform.domain.Comment;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Footnote;
import com.cars.platform.domain.Message;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.TentativeChange;

@RequestMapping("/adjustment/**")
@Controller
public class AdjustmentController {
	
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy");
	SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
	

	@RequestMapping(method = RequestMethod.GET, value = "/adjustment/history")
	public @ResponseBody
	String history(
			Model model,
					
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "accountCode", required = false) String accountCode,
			@RequestParam(value = "period", required = false) String periodEndDate) {

		Company company = Company.findCompany(companyId);
		Metric metric = null;
		try{
		    metric = Metric.findMetricByFullAccountCode(accountCode, company);
		    if (metric == null) {
		    	return "error: unable to adjust/comment on metric " + accountCode + " for " + periodEndDate;
		    }
		}catch(Exception e){
			return "error: " +  e.getMessage() + " for " + accountCode;
		}

		
		Set<Comment> metricValueComments = null;
		
		Period period = getPeriod(company,periodEndDate);

		MetricValue metricValue = MetricValue.findMetricValue(period.getQuarter(),
					period.getYear(), companyId, metric.getId());

		if(metricValue!=null){
			metricValueComments = metricValue.getComments();
		}
			
		// build sorted map
		TreeMap<Date,Comment> commentMap = new TreeMap<Date,Comment>();
		
		if(metricValueComments==null||metricValueComments.size()<1){
			return "No history found";
		}
		for(Comment metricValueComment:metricValueComments){
			commentMap.put(metricValueComment.getCreated(),metricValueComment);
		}
			
		SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy h:mm a z");
	
		String history = "";
		for(Comment comment:commentMap.values()){
			
			String date = dateFormat.format(comment.getCreated());		
			history+="<p><b>"+date +", " +comment.getPerson().getFirstName() + " ";

			history += comment.getPerson().getLastName();
						
			if(comment.getPerson().getCompany().getCompanyType()==CompanyType.CARS){
				history += "(Aeris) ";
			}
			
			history += "</b>: " + comment.getComment() +"</p><br/>";		
		}		
		return history;
	}

	@RequestMapping(method = RequestMethod.POST,value = "/adjustment/add")
	public @ResponseBody String add(
			Model model,
						
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "accountCode", required = false) String accountCode,
			@RequestParam(value = "period", required = false) String periodEndDate,
			@RequestParam(value = "oldValue2", required = false) String oldValue,
			@RequestParam(value = "newValue", required = false) String newValue,
	 	    @RequestParam(value = "comment", required = false) String comment,
	 	    @RequestParam(value = "noteType", required = false) String noteType,
	 	    @RequestParam(value = "reportPage", required = false) String reportPage,
			HttpServletRequest request, HttpServletResponse response){		
		
		
		Company company = Company.findCompany(companyId);

		Person person = UserHelper.getCurrentUser();	
		
		if(oldValue==null||oldValue.length()==0){
			oldValue = "0";
		}
		
		if("titleFootnote".equals(noteType)){
			return addFootnote(noteType,accountCode,comment,company,person,reportPage);
		}
	
		if("periodFootnote".equals(noteType)){
			return addFootnote(noteType,periodEndDate,comment,company,person,reportPage);
		}	
		
		Metric metric = null;
		try{
			metric = Metric.findMetricByFullAccountCode(accountCode, company);
			if (metric == null) {
				return "Unable to find record of metric for account code: " + accountCode;
			}
		}catch(Exception e){
			e.printStackTrace();
			return "error: " + e.getMessage();
		}
		
		Period period = getPeriod(company,periodEndDate);

		try {
				
			MetricValue metricValue  = MetricValue.findMetricValue(period.getQuarter(),
					period.getYear(), companyId, metric.getId());
						
			if(metricValue==null){
				
				metricValue = new MetricValue();
				metricValue.setCompany(company);
				metricValue.setMetric(metric);
				metricValue.setYear(period.getYear());
				metricValue.setQuarter(period.getQuarter());			
			}
			
			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			formatter.setMaximumFractionDigits(0);
			
			Comment metricValueComment = new Comment();
			metricValueComment.setCreated(new Date());
			metricValueComment.setPerson(person);
			metricValueComment.setPending(false);

			if(newValue!=null && newValue.length()>0){
				newValue = newValue.replaceAll(",","");
				newValue = newValue.replaceAll("$","");
				newValue = newValue.replaceAll(" ","");
				Double.parseDouble(newValue);
				
				if(person.getCompany().getCompanyType().equals(CompanyType.CARS)){
							
					TentativeChange tentativeChange = new TentativeChange();
					tentativeChange.setComment("Changed from " +oldValue +" to " + newValue+", reason: " + comment);
					tentativeChange.setMetricName(metric.getName());
					tentativeChange.setPeriod(periodEndDate);
					tentativeChange.setMetricValueId(metricValue.getId());
					tentativeChange.setNewValue(newValue);
					tentativeChange.setMetricFullAccountCode(metric.getFullAccountCode());
					TentativeChangeCache.put(request.getSession(),company.getId(),metricValue.getId(),tentativeChange);
				    						    
				}else{
					return "Not authorized to change or request change for metric with account code: " + accountCode;
				}
														
			}else{
				// just set a comment
				metricValueComment.setComment(comment);
				metricValue.getComments().add(metricValueComment);	
								
				// if comment made by CDFI, set to pending and create activity
				
				if(person.getCompany().getId()==companyId){
					metricValueComment.setPending(true);
				
					List<Person> analysts = company.getAnalysts();
					for(Person analyst:analysts){

						Message message = new Message();
						message.setRecipient(analyst);
						message.setSender(person);
						message.setSent(new Date());
						message.setSubject("Change request for " + metricValue.getMetricName() + " " + periodEndDate);
						message.setText("A metric value was commented on by the CDFI for " + metricValue.getMetric().getName() + " " + periodEndDate +", comment: "+comment);						
						message.persist();
					}	
				
					// log activity
					Activity activity = new Activity();
					activity.setActivityType(ActivityType.METRIC_VALUE_CHANGE_REQUEST);
					activity.setCompany(metricValue.getCompany());
					activity.setDate(new Date());
				
					//description is "relational data" in text form to avoid orphan issue
					// 2007Q4 Cash and investments for lending from $2,000 to $2,025

					String description=metricValue.getYear()+"Q"+metricValue.getQuarter() + " "+ metricValue.getMetric().getName();		
				
					activity.setDescription(description);
					activity.setPerson(person);
					activity.setQuarter(metricValue.getQuarter());
					activity.setYear(metricValue.getYear());
					activity.persist();	
				}else{				
					// clear out pending comment (set to not pending)
					Set<Comment>comments = metricValue.getComments();
					for(Comment com:comments){
						com.setPending(false);
					}					
				}
				
				metricValue.merge();
				ReportDataCache.remove(company); // just a comment so no need to recalculate the value facts
			}
			
			
			// clear out pending comments if CARS user (approved or deny requests)
				


		} catch (Exception e) {
			e.printStackTrace();
			return "Unable to process adjustment: " + e.getMessage();
		}				
		
		Date date = new Date();
		
		String responseString = "<p>Thank you.</p>";
		responseString += "<p>The item <i>\""+ metric.getName()+"\"</i> ";	
			
		if(newValue!=null&&newValue.length()>0){
			if(!person.getCompany().getCompanyType().equals(CompanyType.CARS)){
				responseString += "has been proposed for change from "+oldValue + " to "+newValue+ " ";				
			}else{
				responseString += "has been tentatively changed from "+oldValue + " to "+newValue+ " ";
			}
		}else{
			responseString += "has been given the comment <i>\""+comment +"\"</i> ";		
		}
				
		responseString += "at "+ timeFormat.format(date);
		responseString += " on " + dateFormat.format(date)+".</p>";		
	
		return responseString;				
	}
	
	
			
	@RequestMapping(method = RequestMethod.POST,value = "/adjustment/apply")
	public @ResponseBody String apply(Model model,
			HttpServletRequest request, 	
			HttpServletResponse response,
			@RequestParam(value = "companyId", required = true) Long companyId
			){	
		
		
		try{
		
		Company company = Company.findCompany(companyId);
		Person person = UserHelper.getCurrentUser();	
		HashMap<String, TentativeChange> tentativeChanges = TentativeChangeCache.getTentativeChanges(request.getSession(), companyId);
		
		for(String key:tentativeChanges.keySet()){
			TentativeChange tentativeChange = tentativeChanges.get(key);
			
			MetricValue metricValue  = MetricValue.findMetricValue(tentativeChange.getMetricValueId());
			
			if(metricValue==null){
				metricValue = new MetricValue();
				metricValue.setCompany(company);
				Metric metric = Metric.findMetricByFullAccountCode(tentativeChange.getMetricFullAccountCode(), company);
			    Period period = Period.findPeriod(company, tentativeChange.getPeriod());
				metricValue.setQuarter(period.getQuarter());
				metricValue.setYear(period.getYear());
				metricValue.setMetric(metric);				
			}
			
			// clear out pending comment (set to not pending)
			Set<Comment>comments = metricValue.getComments();
			for(Comment comment:comments){
				comment.setPending(false);
			}
			
			
			Comment comment = new Comment();
			comment.setCreated(new Date());
			comment.setPerson(person);
			comment.setPending(false);	
			
			Metric metric = metricValue.getMetric();
			String oldValue = "";
					    
		    if(metric.getType() == MetricType.TEXT){
		    	oldValue = metricValue.getTextValue();
		    }else{
		    	try{
		    	oldValue = Double.toString(metricValue.getAmountValue());
		    	}catch(Exception ignore){}
		    }			
						
		    comment.setComment(tentativeChange.getComment());
		    // TODO: get correct comment here
		    
		    comments.add(comment);
		    
			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			formatter.setMaximumFractionDigits(0);
		    
		    if(metric.getType() == MetricType.TEXT){
		    	metricValue.setTextValue(tentativeChange.getNewValue());
		    }else{
		    	double doubleNewValue = Double.parseDouble(tentativeChange.getNewValue());
		        metricValue.setAmount(doubleNewValue);
		    }   
		    
		    if(metricValue.getId()==null){
		    	metricValue.persist();
		    }else{
		        metricValue.merge();
		    }
		}
	    
	    TentativeChangeCache.clear(companyId);
	    ReportDataCache.removeAndUpdateValueFacts(company);		
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "Adjustments applied";
	}
	
	
	@RequestMapping(method = RequestMethod.POST,value = "/adjustment/clear")
	public @ResponseBody String clear(Model model,
			HttpServletRequest request, HttpServletResponse response,@RequestParam(value = "companyId", required = true) Long companyId){		
	    TentativeChangeCache.clear(companyId);
	    ReportDataCache.remove(Company.findCompany(companyId));
		return "Adjustments cleard";
	}
	
	
	private String addFootnote(String noteType, String label, String comment, Company company, Person person,String reportPage) {
	
		String response = "";
		
		try{
		int supIndex = label.indexOf("<sup>");
		if(supIndex>0){
			label=label.substring(0,supIndex-1);
		}
		
		//Footnote footnote = Footnote.findFootnoteByCompanyAndLabel(company, label);
		
		ReportPage reportPageIndicator = null;
		
		
		if("position".equalsIgnoreCase(reportPage)){
			reportPageIndicator = ReportPage.POSITION;
		}else if("activity".equalsIgnoreCase(reportPage)){
			reportPageIndicator = ReportPage.ACTIVITY;
		}else if("summary".equalsIgnoreCase(reportPage)){
			reportPageIndicator = ReportPage.SUMMARY;
		}else if("additional".equalsIgnoreCase(reportPage)){
			reportPageIndicator = ReportPage.ADDITIONAL;
		}
		
		Footnote footnote = new Footnote();
		footnote = new Footnote();
		footnote.setCompany(company);
		footnote.setLabel(label);
		footnote.setComment(comment);
		footnote.setCreated(new Date());
		footnote.setPerson(person);
		footnote.setReportPage(reportPageIndicator);
		footnote.persist();
				
	    Date date = new Date();
		
		response = "<p>Thank you.</p>";
		response += "<p><i>\""+ label+"\"</i> ";				
		response += "has been given the footnote <i>\""+comment +"\"</i> ";							
	    response += "at "+ timeFormat.format(date);
		response += " on " + dateFormat.format(date)+".</p>";		
	
		ReportDataCache.remove(company);
		}catch(Exception e){
			e.printStackTrace();
		}
		return response;	
	}
	
	private Period getPeriod(Company company,String endDate){
		
		Period period = null;
		
		period = Period.findPeriod(company, endDate);
		
		if(period==null){
			int year=0;
			int quarter=0;
			try {
				year = DateHelper.getYear(company.getFiscalYearStart(), endDate);
				quarter = DateHelper.getQuarter(company.getFiscalYearStart(),
						endDate);
			}catch(Exception e ){
				e.printStackTrace();
			}

			period = new Period();
			period.setCompany(company);
			period.setEndDate(endDate);
			period.setYear(year);
			period.setQuarter(quarter);
		}
		
		return period;
	}

}
