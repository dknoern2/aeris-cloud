package com.cars.platform.web;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.roo.addon.web.mvc.controller.converter.RooConversionService;
import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricCategory;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.ReportCategory;

/**
 * A central place to register application converters and formatters. 
 */
@RooConversionService
public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {

	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
				
        registry.addConverter(getEquationToStringConverter());
        registry.addConverter(getIdToEquationConverter());
        registry.addConverter(getStringToEquationConverter());
        registry.addConverter(getPersonToStringConverter());
        registry.addConverter(getBreakdownColumnToStringConverter());
        registry.addConverter(getMetricBreakdownToStringConverter());
	}
	
	
    public Converter<Equation, String> getEquationToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.cars.platform.domain.Equation, java.lang.String>() {
            public String convert(Equation equation) {
            	// kurtl removed formula and code... who on earth wants to display something that big
            	// this is used by jsp pages which have table:column tags 
                //return new StringBuilder().append(equation.getName()).append(' ').append(equation.getFormula()).append(' ').append(equation.getCode()).toString();
            	Company company = equation.getCompany();
            	if (null == company) {
            		return equation.getName() + " [GLOBAL]";
            	}
            	else {
                    return equation.getName() + " [" + company.getName() + "]";          		
            	}
            }
        };
    }
    
    public Converter<Long, Equation> getIdToEquationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.cars.platform.domain.Equation>() {
            public com.cars.platform.domain.Equation convert(java.lang.Long id) {
                return Equation.findEquation(id);
            }
        };
    }
    
    public Converter<String, Equation> getStringToEquationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.cars.platform.domain.Equation>() {
            public com.cars.platform.domain.Equation convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Equation.class);
            }
        };
    }
    	
    
    public Converter<Person, String> getPersonToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<Person, java.lang.String>() {
            public String convert(Person person) {
                return person.getFullName();
            }
        };
    }


	
	// Instead of filtering the parent by report page on create and update we show the report page of the list of possible parents
	public Converter<ReportCategory, String> getReportCategoryToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.cars.platform.domain.ReportCategory, java.lang.String>() {
            public String convert(ReportCategory reportCategory) {
                // return new StringBuilder().append(reportCategory.getName()).toString();
            	return reportCategory.getReportPage().name() + ": " + reportCategory.getName();
            }
        };
    }

	public Converter<MetricCategory, String> getMetricCategoryToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.cars.platform.domain.MetricCategory, java.lang.String>() {
            public String convert(MetricCategory metricCategory) {
                //return new StringBuilder().append(metricCategory.getName()).append(' ').append(metricCategory.getAccountCode()).toString();
                return metricCategory.getName();
            }
        };
    }

	public Converter<BreakdownColumn, String> getBreakdownColumnToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.cars.platform.domain.BreakdownColumn, java.lang.String>() {
            public String convert(BreakdownColumn breakdownColumn) {
                //return new StringBuilder().append(breakdownColumn.getRank()).append(' ').append(breakdownColumn.getName()).append(' ').append(breakdownColumn.getEquation()).toString();
                return breakdownColumn.getName();
            }
        };
    }

	public Converter<MetricBreakdown, String> getMetricBreakdownToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.cars.platform.domain.MetricBreakdown, java.lang.String>() {
            public String convert(MetricBreakdown metricBreakdown) {
                return metricBreakdown.getName();
            }
        };
    }
}
