package com.cars.platform.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cars.platform.domain.Person;
import com.cars.platform.service.EmailService;
import com.cars.platform.util.RandomHelper;

@RequestMapping("/passwordreset/**")
@Controller
public class PasswordResetController {

	@Autowired
	EmailService emailService;

	@RequestMapping(value = "/passwordreset/generate", method = RequestMethod.POST)
	public String generate(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {

		String username = request.getParameter("username");

		Person person = null;
		try {
			person = Person.findPeopleByUsernameEquals(username);
		} catch (Exception e) {

		}
		if (person == null) {
			modelMap.addAttribute("error", "no account found for user \""
					+ username + "\"");
			return "passwordreset/index";
		}

		modelMap.addAttribute("person", person);

		String passwordResetCode = person.getPasswordResetCode();
				
		if(passwordResetCode==null||passwordResetCode.length()<1){
			passwordResetCode = RandomHelper.randomString(6);
			person.setPasswordResetCode(passwordResetCode);
			person.merge();
		}


		String message = "Dear " + person.getFirstName() + " "
				+ person.getLastName() + ",\n\n";
		message += "You have requested a password reset code.\n\n";
		message += "Password Reset Code: " + passwordResetCode + "\n\n";

		message += "IMPORTANT: If you have not requested a password reset code, or believe you have received this message in error, ";
		message += "please contact Aeris support (info@aerisinsight.com) immediately.\n";

		
		try{
		
		
		emailService.sendEmail(person.getEmail(),
				"Your Aeris Cloud Password Reset Code", message, false);

	}catch(Exception e){
		
		String errorString = "We're sorry, we are unable to send you a password reset code right now.  Please try again later.";

		modelMap.addAttribute("error", errorString);
		
		return "passwordreset/index";
	}
    
		
		
		return "passwordreset/validate";
	}

	@RequestMapping(value = "/passwordreset/validate", method = RequestMethod.POST)
	public String validate(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String resetCode = request.getParameter("resetCode");

		Person person = null;
		try {
			person = Person.findPeopleByUsernameEquals(username);
		} catch (Exception e) {

		}
		if (person == null) {
			modelMap.addAttribute("error", "no account found for user \""
					+ username + "\"");
			return "passwordreset/validate";
		}
				
		modelMap.addAttribute("person", person);

		if (!password.equals(passwordConfirm)) {
			modelMap.addAttribute("error",
					"the values you entered for password and password confirmation do not match");
			return "passwordreset/validate";
		}

		if (!resetCode.equals(person.getPasswordResetCode())) {
			modelMap.addAttribute("error",
					"the value you entered for password reset code does not match what was emailed to you");
			return "passwordreset/validate";
		}

		modelMap.addAttribute("person", person);
		
		person.setPassword(password);
		person.setPasswordResetCode(null);
		person.merge();
	
		return "passwordreset/done";
	}

	@RequestMapping
	public String index() {
		return "passwordreset/index";
	}
}
