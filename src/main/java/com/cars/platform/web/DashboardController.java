package com.cars.platform.web;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cars.platform.domain.ActionItem;
import com.cars.platform.domain.Activity;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Graph;
import com.cars.platform.domain.Message;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.PerformanceView;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.PersonRole;
import com.cars.platform.domain.Subscription;
import com.cars.platform.service.ActionItemService;
import com.cars.platform.service.DocumentDataService;
import com.cars.platform.service.EmailService;
import com.cars.platform.service.ReminderService;
import com.cars.platform.service.export.FactSheetService;
import com.cars.platform.util.LogoHelper;
import com.cars.platform.util.SpringApplicationContext;
import com.cars.platform.util.UserHelper;

@RequestMapping("/")
@Controller
public class DashboardController {

    @SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);
    
    @Autowired
    private ReminderService reminderService;
    
    @Autowired
    private InvestorController investorController;
    
    @Autowired
    private EmailService emailService;
    
    /**
     * Hidden method to enable sending of CDFI action item reminders replacing the to address with that of the logged in person
     */
    @RequestMapping(value = "/reminders/{companyId}", method = RequestMethod.GET)
    public String reminder(@PathVariable("companyId") Long companyId, Model model,  HttpServletRequest request, HttpServletResponse response) {
    	//reminderService.processActionItemReminders();
    	
    	Person person = UserHelper.getCurrentUser();
    	
    	Company company = Company.findCompany(companyId);
    	reminderService.sendActionItemReminders(company, person.getEmail());

	    return "redirect:/";
    }

    
    /**
     * Hidden method to test SMTP setup
     */
    @RequestMapping(value = "/emailtest", method = RequestMethod.GET)
    public String reminder(Model model,  HttpServletRequest request, HttpServletResponse response) {
    	
    	emailService.sendEmail("davidk@slalom.com", "GARAGE Test Email","Hi there, from GARAGE", false);
	    return "redirect:/";
    }  
    
    
    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request, HttpServletResponse response) {
    	
    	Person person = UserHelper.getCurrentUser();
    	if (null == person || null == person.getCompany()) {
    		// this should be unreachable via applicationContext-security.xml
    		// and you can't have a person without a company
    		throw new RuntimeException("Sorry but we can not properly identify you.");
    	}

		// Setting the person on the session has been 'properly' implemented via LoginListener class.
    	// original todo comment left for explanation
    	// ----------
		// This is realllly important as it ensures the jsp pages will find something when they test
		// for ${person.blah} which will take person from either the model or the session.
		// Many controller methods set the person in the model directly which wouldn't be necessary if
		// the below was done with some sort of Security filter. Doing this here is 'close' as after login you go to
		// the root (/). But, not in all cases for example if you type in /platform/manageplatform you will go to the 
		// login page and then redirected back to /platform/manageplatform in which case you don't go through this
		// controller method
		// request.getSession(true).setAttribute("person", person);
    	
		CompanyType companyType = person.getCompany().getCompanyType();
		
		switch(companyType) {
		case INVESTOR:
			// In Phase 2 the subscriber dashboard and MyReports was re-written into a new
			// InvestorController. We left all other user type dashboard functionality here
			Long companyId = person.getCompany().getId();
			return investorController.dashboard(companyId, model);
		case CARS:
			return carsDashboard(model, person);

		case CDFI:
			return cdfiDashboard(model, request, person);
		}
		
		throw new RuntimeException("Sorry but we can not properly identify you.");
    }

    
    private String cdfiDashboard(Model model, HttpServletRequest request, Person person) {

    	// siderail content:
    	request.getSession().setAttribute("cdfiLogo", LogoHelper.getCorporateLogo(person.getCompany()));

    	ActionItemService actionItemService = new ActionItemService(person.getCompany());
    	List<ActionItem> actionItems = actionItemService.getActionItems(false);
    	model.addAttribute("actionItems", actionItems);

    	PerformanceView performanceView = null;
    	try {
    		performanceView = PerformanceView.findPerformanceViewsByCompany(person.getCompany());
    	} catch (Exception ignore) {
    		System.out.println("No performance views found for  CDFI " + person.getCompany().getName());
    	}
    	if (performanceView == null) {
    		performanceView = new PerformanceView();
    		performanceView.setCompany(person.getCompany());
    	}
    	model.addAttribute("performanceView", performanceView);

    	List<Graph> availableGraphs = Graph.findAllGraphsByCompany(person.getCompany());
    	model.addAttribute("availableGraphs", availableGraphs);

    	model.addAttribute("activity", Activity.findRecentInvestorActivityForCDFI(person.getCompany()));

    	// This  was moved from the Investor || CDFI block below
    	List<Subscription> subscriptions = Subscription.findSubscriptionsByOwner(person.getCompany()).getResultList();
    	// TODO no idea why this has to be on the session as opposed to the model. Somewhere in mainnav.jspx?
    	request.getSession().setAttribute("subscriptions", subscriptions);
    	//model.addAttribute("subscriptions", subscriptions);
    	
		List<Message> messages = Message.findRecentMessagesByRecipient(person).getResultList();
		model.addAttribute("messages", messages);

    	return "dashboard/cdfi";
    }

    
    private String carsDashboard(Model model, Person person) {
    	addQuicklinksContent(model, person);

    	if (PersonRole.ADMIN.equals(person.getPersonRole())) {
    		model.addAttribute("activityCARS", Activity.findRecentActivityByCompanyType(CompanyType.CARS));
    		model.addAttribute("activityCDFI", Activity.findRecentActivityByCompanyType(CompanyType.CDFI));
    		model.addAttribute("activityInvestor", Activity.findRecentActivityByCompanyType(CompanyType.INVESTOR));
    	} else {
    		model.addAttribute("activityCARS", Activity.findRecentActivityByCompanyTypeForAnalyst(CompanyType.CARS, person));
    		model.addAttribute("activityCDFI", Activity.findRecentActivityByCompanyTypeForAnalyst(CompanyType.CDFI, person));
    		model.addAttribute("activityInvestor", Activity.findRecentActivityByCompanyTypeForAnalyst(CompanyType.INVESTOR, person));
    	}

    	// show pending change requests
    	try {
    		List<MetricValue> pendingComments = MetricValue.findMetricValuesWithPendingComments();
    		// make list of just one per company
    		HashSet<Company> companiesWithPendingComments = new HashSet<Company>();

    		for (MetricValue mv : pendingComments) {
    			if (person.isAdmin() || mv.getCompany().getAnalysts().contains(person)) {
    				companiesWithPendingComments.add(mv.getCompany());
    			}
    		}

    		model.addAttribute("changeRequests", companiesWithPendingComments);

    	} catch (Exception ignore) {
    	}
    	
		List<Message> messages = Message.findRecentMessagesByRecipient(person).getResultList();
		model.addAttribute("messages", messages);

    	return "dashboard/ops";
    }



    @RequestMapping(value = "/activity/delete/{id}")
    public String deleteActivity(@PathVariable Long id, Model model) {

    	Person person = UserHelper.getCurrentUser();
    	if (person.isAdmin()) {
    		Activity activity = Activity.findActivity(id);
    		activity.remove();
    	}
    	return "redirect:/";
    }

    /*
     * add data to populate quicklinks pulldowns
     */
    public static void addQuicklinksContent(Model model, Person person) {

    	if (person.getCompany().isCARS()) {
    		if (person.isAdmin()) {
    			List<Company> cdfis = new ArrayList<Company>();
    			List<Company> subscribers = new ArrayList<Company>();
    			List<Person> carsUsers = Person.findPeopleByCompany(person.getCompany()).getResultList();

    			// get all companies then sort

    			List<Company> allCompanies = Company.findAllCompanysAlphabetically();

    			for (Company company : allCompanies) {
    				if (CompanyType.CDFI.equals(company.getCompanyType())) {
    					cdfis.add(company);
    				} else if (CompanyType.INVESTOR.equals(company.getCompanyType())) {
    					subscribers.add(company);
    				}
    			}

    			model.addAttribute("cdfis", cdfis);
    			model.addAttribute("subscribers", subscribers);
    			model.addAttribute("carsUsers", carsUsers);
    		} else {
    			model.addAttribute("cdfis", Company.findCompanysByAnalyst(person));
    		}
    	}
    }
    
    /**
     * REST method for downloading a fact sheet
     */
    @RequestMapping(value = "/downloadcert", method = RequestMethod.GET)
    public void getFile(HttpServletRequest request, HttpServletResponse response) {
    	Person person = UserHelper.getCurrentUser();
    	Company company = person.getCompany();
    	DocumentType certDocumentType = DocumentType
				.findDocumentTypesByNameEquals("CDFI Certificates")
				.getSingleResult();
        List<Document> allCerts = Document.findDocument(company, certDocumentType);
        //Assume that CDFI can has only 1 certificate
        Document cert = allCerts.get(0);
    	try {
    		DocumentDataService documentDataService = SpringApplicationContext.getDocumentDataService();
    		InputStream in = documentDataService.getDocumentData(cert.getCompany().getId(), cert.getId());
    		
    		response.setContentType("application/pdf");
        	response.setHeader("Content-Disposition", "attachment; filename=" + cert.getFileName());
    		
    		byte[] buffer = new byte[1024];
	        int len;
	        while ((len = in.read(buffer)) != -1) {
	            response.getOutputStream().write(buffer, 0, len);
	        }
	    	response.flushBuffer();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

}
