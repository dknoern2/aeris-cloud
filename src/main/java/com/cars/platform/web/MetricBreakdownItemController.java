package com.cars.platform.web;
import com.cars.platform.domain.MetricBreakdownItem;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/metricbreakdownitems")
@Controller
@RooWebScaffold(path = "metricbreakdownitems", formBackingObject = MetricBreakdownItem.class)
public class MetricBreakdownItemController {
}
