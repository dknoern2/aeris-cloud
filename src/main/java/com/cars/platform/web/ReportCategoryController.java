package com.cars.platform.web;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.ReportCategory;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.service.ReportCategoryService;
import com.cars.platform.service.ReportService2;
import com.cars.platform.viewmodels.report.Category;
import com.cars.platform.viewmodels.report.Report;
import com.cars.platform.viewmodels.report.ReportCategories;

@RequestMapping("/reportcategorys")
@Controller
@RooWebScaffold(path = "reportcategorys", formBackingObject = ReportCategory.class)
public class ReportCategoryController {

	
	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
//        if (page != null || size != null) {
//            int sizeNo = size == null ? 10 : size.intValue();
//            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
//            uiModel.addAttribute("reportcategorys", ReportCategory.findReportCategoryEntries(firstResult, sizeNo));
//            float nrOfPages = (float) ReportCategory.countReportCategorys() / sizeNo;
//            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
//        } else {
//            uiModel.addAttribute("reportcategorys", ReportCategory.findAllReportCategorys());
//        }
        
        uiModel.addAttribute("reportcategorys", ReportCategory.findReportCategoriesHierarchy());
        return "reportcategorys/list";
    }
	
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("reportcategory", ReportCategory.findReportCategory(id));
        uiModel.addAttribute("itemId", id);
        return "reportcategorys/show";
    }

    
    /**
     * GET method for returning the hierarchy of ReportCategories, as Category viewmodel objects, which
     * contain the Equations within each category.
     * @param page 
     * @param companyId optional company for which to return the associated equations. If null only global equations are returned
     * @return list of Category viewModel objects
     */
    @RequestMapping(value = "/report/{page}")
    public @ResponseBody ReportCategories report(
    		@PathVariable ReportPage page,
       		@RequestParam(value = "companyId", required = false) Long companyId,
    		Model model, HttpServletRequest request, HttpServletResponse response)
    {
    	Company company = null;
    	if (null != companyId) {
    	    company = Company.findCompany(companyId);
    	}
    	
    	ReportCategoryService rcs = new ReportCategoryService(company);
    	
    	ReportCategories categories = rcs.getReportCategories(page);

 	    return categories;
 	}

    
    
    /**
     * PUT method for updating the equations associated to a report category
     * @param id the Id of the ReportCategory for which equation rank is changing
     * @param companyId the company for which the equations are being managed. Can be null
     * @param equationIds The IDs of all the equation in rank order. if company is not null this list must
     * still include the global equations in their existing rank order
     */
	@RequestMapping(value = "/{id}/equations", method = RequestMethod.PUT, produces = "text/html")
    public @ResponseBody String setEquations(
    		@PathVariable("id") Long id,
       		@RequestParam(value = "companyId", required = false) Long companyId,
       		@RequestBody List<Integer> equationIds, // making this List<Long> doesn't work... you actually get ints inside
    		Model uiModel, HttpServletRequest httpServletRequest)
	{
//        try { // TODO get the server to log exceptions thrown by controllers
		if (null == equationIds) {
			throw new IllegalArgumentException("The list of equations for this report category is required.");
		}
		
		Company company = null; 
		if (null != companyId) {
			company = Company.findCompany(companyId);
		}
		

		List<Equation> equations = new ArrayList<Equation>();
		for (Integer equationId : equationIds) {
			Long longId = Long.valueOf(equationId.longValue());
			Equation equation = Equation.findEquation(longId);
			if (null == equation) {
				throw new IllegalArgumentException("There is no equation with ID: " + longId);
			}
			// If the equation is a CDFI override we need to replace it with the global one it is overriding
			if (company != null && null != equation.getOverride()) {
				equation = equation.getOverride();
			}
			equations.add(equation);
		}

        ReportCategory reportCategory = ReportCategory.findReportCategory(id);
		reportCategory.updateCategoryEquations(equations, company);
        reportCategory.persist();

//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        
		return "OK";
	}

	
	// TODO  make sure new rank is not set to an existing value on a sibling
	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid ReportCategory reportCategory, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, reportCategory);
            return "reportcategorys/update";
        }
        uiModel.asMap().clear();
        
        // calling merge on the reportCategory passed in here causes the following error 
        //org.hibernate.HibernateException: A collection with cascade="all-delete-orphan" was no longer referenced by the owning entity instance
        // something to do with the OneToMany relationship to ReportCategoryEquation.
        // Fetching the existing entity and modifying it works around the issue

        ReportCategory reportCategory1 = ReportCategory.findReportCategory(reportCategory.getId());
        reportCategory1.setName(reportCategory.getName());
        reportCategory1.setParent(reportCategory.getParent());
        reportCategory1.setReportPage(reportCategory.getReportPage());
        
        // handle case where other report categories need their rank changed
        reportCategory1.updateRank(reportCategory.getRank());


        reportCategory1.merge();
        return "redirect:/reportcategorys/" + encodeUrlPathSegment(reportCategory.getId().toString(), httpServletRequest);
    }
	
	void populateEditForm(Model uiModel, ReportCategory reportCategory) {
        uiModel.addAttribute("reportCategory", reportCategory);
        
        // required='false' does not work on the on the select tag in the jsp pages (https://jira.springsource.org/browse/ROO-581)
    	ArrayList<ReportCategory> populate = new ArrayList<ReportCategory>();
    	ReportCategory blank = new ReportCategory();
    	blank.setReportPage(ReportPage.NONE);
    	blank.setName("");
    	blank.setId(0L);
    	populate.add(blank);
    	populate.addAll(ReportCategory.findAllReportCategorys());
        uiModel.addAttribute("reportcategorys", populate);
        
        //uiModel.addAttribute("reportcategoryequations", ReportCategoryEquation.findAllReportCategoryEquations());
        uiModel.addAttribute("reportpages", ReportPage.getManagedPages());
    }



	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, ReportCategory.findReportCategory(id));
        return "reportcategorys/update";
    }

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid ReportCategory reportCategory, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, reportCategory);
            return "reportcategorys/create";
        }
        uiModel.asMap().clear();
        
        // reset ranks of siblings if necessary
        reportCategory.updateRank(reportCategory.getRank());

        reportCategory.persist();
        return "redirect:/reportcategorys/" + encodeUrlPathSegment(reportCategory.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new ReportCategory());
        return "reportcategorys/create";
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        ReportCategory reportCategory = ReportCategory.findReportCategory(id);
        reportCategory.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/reportcategorys";
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
