package com.cars.platform.web;

import static com.cars.platform.domain.DocumentTypeFolder.CARS_INTERNAL;
import static com.cars.platform.domain.DocumentTypeFolder.CARS_REPORT;
import static com.cars.platform.domain.DocumentTypeFolder.CARS_RESOURCES;
import static com.cars.platform.domain.DocumentTypeFolder.CARS_ARCHIVE;
import static com.cars.platform.domain.DocumentTypeFolder.CARS_INTERNAL_ARCHIVE;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.cache.ReportData;
import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.cache.TentativeChangeCache;
import com.cars.platform.domain.Access;
import com.cars.platform.domain.ActionItem;
import com.cars.platform.domain.Activity;
import com.cars.platform.domain.ActivityType;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.DocumentType.DueType;
import com.cars.platform.domain.InitialsRecord;
import com.cars.platform.domain.LendingType;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.Note;
import com.cars.platform.domain.PeerGroupAccess;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.PersonRole;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.ReviewActionItem;
import com.cars.platform.domain.ReviewType;
import com.cars.platform.domain.SectoralFocus;
import com.cars.platform.domain.ShareFinancials;
import com.cars.platform.domain.State;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;
import com.cars.platform.domain.TargetBeneficiary;
import com.cars.platform.domain.TaxType;
import com.cars.platform.service.ActionItemService;
import com.cars.platform.service.ReminderService;
import com.cars.platform.service.ReportService2;
import com.cars.platform.util.ArchivingFileHelper;
import com.cars.platform.util.AuthorizationHelper;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.LogoHelper;
import com.cars.platform.util.SelectOptionHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.DocumentPOJO;
import com.cars.platform.viewmodels.InitialsRecordPOJO;
import com.cars.platform.viewmodels.NotePOJO;
import com.cars.platform.viewmodels.Response;
import com.cars.platform.viewmodels.ReviewPOJO;
import com.cars.platform.viewmodels.TentativeChange;
import com.cars.platform.viewmodels.report.Report;

@RequestMapping("/cdfi/**")
@Controller
public class CDFIController {
	private static final Logger logger = LoggerFactory.getLogger(CDFIController.class);
	
	@Autowired
	ReminderService reminderService;

	@RequestMapping(value = "/cdfi/{id}/overview")
	public String show2(@PathVariable Long id, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		return show(id, model, request, response);
	}


	// show all the peer groups for this cdfi, owned by the company associated to the logged in person
	@RequestMapping(value = "/cdfi/{id}/peergroups")
	public String peergroups(@PathVariable Long id, Model model,
			HttpServletRequest request, HttpServletResponse response)
	{
		boolean authorized = populateForPeerGroup(id, model);
		
		return "cdfi/peergroups";

	}
	
	// show  the peer group report for the given peer group Id
	@RequestMapping(value = "/cdfi/{id}/peergroupreport/{pgId}")
	public String peergroupreport(@PathVariable Long id, @PathVariable("pgId") Long pgId, Model model,
			HttpServletRequest request, HttpServletResponse response)
	{
    	model.addAttribute("pgId", pgId);
		boolean authorized = populateForPeerGroup(id, model);
		if  (authorized) {
			return "cdfi/peergroupreport";
		}
		else {
			return "cdfi/peergroups";
		}
	}
	
	// peer group selector without a peer group in context... that is Create new peer group
	@RequestMapping(value = "/cdfi/{id}/peergroupselector")
	public String peergroupselector(@PathVariable Long id, Model model,
			HttpServletRequest request, HttpServletResponse response)
	{
		// this method should be unreachable due to check in cdfi/peergroups but just in case we check anyway
		boolean authorized = populateForPeerGroup(id, model);
		if  (authorized) {
			return "cdfi/peergroupselector";
		}
		else {
			return "cdfi/peergroups";
		}
	}
	
	// peer group selector with a peer group in context. That is go into selector with an existing peer group	
	@RequestMapping(value = "/cdfi/{id}/peergroupselector/{pgId}")
	public String peergroupselector(@PathVariable("id") Long id, @PathVariable("pgId") Long pgId, Model model,
			HttpServletRequest request, HttpServletResponse response)
	{
	   	model.addAttribute("pgId", pgId);
		// this method should be unreachable due to check in cdfi/peergroups but just in case we check anyway
		boolean authorized = populateForPeerGroup(id, model);
		if  (authorized) {
			return "cdfi/peergroupselector";
		}
		else {
			return "cdfi/peergroups";
		}	}
	
	private Subscription getActiveAllSubscription(List<Subscription> activeSubs, Company company, Model model) {
		// The CDFI selector uses the fact that an active full subscription exists
				// in order to know whether or not to include 'add to garage' to the menu options
		Subscription activeSubscriptionOfTypeAll = null;
		for (Subscription sub : activeSubs) {
			if (sub.getSubscriptionType() == SubscriptionType.ALL) {
				activeSubscriptionOfTypeAll = sub;
				break;
			}
		}
		model.addAttribute("activeSubscriptionOfTypeAll", activeSubscriptionOfTypeAll == null ? null : activeSubscriptionOfTypeAll.getId());
		model.addAttribute("addToGarage", activeSubscriptionOfTypeAll != null );
		if (activeSubscriptionOfTypeAll != null) {
			for (Access cdfi : activeSubscriptionOfTypeAll.getCdfis()) {
				if (cdfi.getCompany().getId() == company.getId()) {
					model.addAttribute("addToGarage", false);
					break;
				}
			}
		}
		return activeSubscriptionOfTypeAll;
	}
	
	private boolean populateForPeerGroup(Long companyId, Model model) {
		Company company = Company.findCompany(companyId);
		Person person = UserHelper.getCurrentUser();
		
		model.addAttribute("company", company);
		model.addAttribute("person", person);
		
		List<Subscription> activeSubs = Subscription.findAllActiveSubscriptionsForSubscriber(person.getCompany());
		
		boolean authorized = AuthorizationHelper.isAuthorized(
				person, company, AuthorizationHelper.ACCESS_TYPE_PEERGROUPS, activeSubs);
		if (!authorized) {
			model.addAttribute("peerGroupAccess", PeerGroupAccess.NONE);
			return false;
		}
		
		Subscription activeSubscriptionOfTypeAll = getActiveAllSubscription(activeSubs, company, model);

		this.addCdfiSideBarItems(model, company);
		
		PeerGroupAccess peerGroupAccess;
		if (person.getCompany().isCARS()) {
			if (person.isAdmin()) {
				peerGroupAccess = PeerGroupAccess.FULL;
			}
			else {
				peerGroupAccess = PeerGroupAccess.VIEW;
			}
		}
		else if (person.getCompany().isCDFI()) {
			peerGroupAccess = PeerGroupAccess.VIEW;
		}
		else { // Investor
			if (null != activeSubscriptionOfTypeAll && activeSubscriptionOfTypeAll.isPeerGroups()) {
				peerGroupAccess = PeerGroupAccess.FULL;				
			}
			else { // since they are authorized via above check it must be a single subscription for which they only get VIEW
				peerGroupAccess = PeerGroupAccess.VIEW;								
			}
		}
		model.addAttribute("peerGroupAccess", peerGroupAccess);
		
		return true;
	}


	// REST service which provides data for a report page. Use the enumeration value such as ADDITIONAL for the page parameter
	@RequestMapping(value = "/cdfi/{id}/report/{page}")
	public @ResponseBody Report report(@PathVariable Long id, @PathVariable ReportPage page, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		Company company = Company.findCompany(id);

		ReportService2 rs = new ReportService2(company);

		Report report = rs.getReportData(page, false, true);

		return report;
	}



	@RequestMapping(value = "/cdfi/{id}")
	public String show(@PathVariable Long id, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		Company company = Company.findCompany(id);
		if (null == company || !company.isCDFI()) {
			return "redirect:/"; // someone typed in a bogus company id
		}
		model.addAttribute("company", company);

		Person person = UserHelper.getCurrentUser();
		model.addAttribute("person", person);
		
		boolean authorized = AuthorizationHelper.isAuthorized(
				person, company);
		if (!authorized) {
			return "redirect:/";
		}
		
		List<Subscription> activeSubs = Subscription.findAllActiveSubscriptionsForSubscriber(person.getCompany());
		getActiveAllSubscription(activeSubs, company, model);
		
		model.addAttribute("currentYear",
				DateHelper.getCurrentYear(company.getFiscalYearStart()));
		model.addAttribute("currentQuarter",
				DateHelper.getCurrentQuarter(company.getFiscalYearStart()));

		if (person.getCompany().isCARS()) {

			List<Person> analysts = Person.findAnalysts();
			model.addAttribute("analysts", analysts);

			// activity
			model.addAttribute("activityCARS",
					Activity.findRecentActivity(CompanyType.CARS, company));
			model.addAttribute("activityCDFI",
					Activity.findRecentActivity(CompanyType.CDFI, company));
			model.addAttribute("activityInvestor",
					Activity.findRecentActivity(CompanyType.INVESTOR, company));
		}

		List<Person> employees = Person.findPeopleByCompany(company)
				.getResultList();
		model.addAttribute("employees", employees);

		Document logo = LogoHelper.getCorporateLogo(company);
		if (logo != null) {
			model.addAttribute("logoId", logo.getId());
		}

		this.addCdfiSideBarItems(model, company);
		model.addAttribute("orgType",
				SelectOptionHelper.getOrgTypes().get(company.getOrgType()));
		model.addAttribute("lendingType", LendingType.findAllLendingTypes());
		model.addAttribute("totalAssets", company.getTotalAssetsString());
		return "cdfi/show";
	}

	@RequestMapping(value = "/cdfi/{id}/library")
	public String library(@PathVariable Long id, @RequestParam(value = "error", required = false) String error, @RequestParam(value="itemsCount[]", required = false) int[] itemsCount,
			@RequestParam(value="scrollTop", required = false) Integer scrollTop,
			Model model, HttpServletRequest request,
			HttpServletResponse response) {

		Company company = Company.findCompany(id);
		Person person = UserHelper.getCurrentUser();
		
		//TODO: fix it if you can
		model.addAttribute("today", new java.text.SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		
		model.addAttribute("company", company);
		model.addAttribute("person", person);
		
		List<Subscription> activeSubs = Subscription.findAllActiveSubscriptionsForSubscriber(person.getCompany());
		getActiveAllSubscription(activeSubs, company, model);

		boolean authorized = AuthorizationHelper.isAuthorized(
				person, company, AuthorizationHelper.ACCESS_TYPE_LIBRARY);
		if (!authorized) {
			model.addAttribute("authorized", false);
			return "cdfi/library";
		}
		model.addAttribute("authorized", true);
		
		HashSet<Company> subscriberCompanies = new HashSet<Company>();

		List<Subscription> subscriptions = Subscription
				.findSubscriptionsByOwnerAndType(company,
						SubscriptionType.LIBRARY);

		for (Subscription subscription : subscriptions) {
			for (Access targetCompany : subscription.getSubscribers()) {
				subscriberCompanies.add(targetCompany.getCompany());
			}
		}

		model.addAttribute("viewers", subscriberCompanies);

		// get current fiscal year and quarter
		int previousYear = DateHelper.getCurrentYear(company
				.getFiscalYearStart()) - 1;

		ReviewActionItem reviewActionItem = null;
		ActionItemService actionItemService = new ActionItemService(company);
		List<ActionItem> allActionItems = actionItemService.getActionItems(false);
		Map<DocumentType, Integer> taskByDocType = new HashMap<DocumentType, Integer>();
		for (ActionItem actionItem : allActionItems) {
			int taskType = actionItem.getTaskType();
			List<DocumentType> docTypes = actionItem.getDocumentTypes();
			if (null == docTypes)
				continue;
			for (DocumentType docType : docTypes) {
				Integer tt = taskByDocType.get(docType);
				// Full or Annual review trumps quarterly for icon tootip
				if (null == tt
						|| taskType == ActionItem.TASK_TYPE_ANNUAL_REVIEW_DOCUMENTS
					    || taskType == ActionItem.TASK_TYPE_FULL_ANALYSIS_DOCUMENTS) {
					taskByDocType.put(docType, taskType);
				}
			}
			
			if (taskType == ActionItem.TASK_TYPE_ANNUAL_REVIEW_DOCUMENTS
				    || taskType == ActionItem.TASK_TYPE_FULL_ANALYSIS_DOCUMENTS) {
				reviewActionItem = (ReviewActionItem)actionItem;
				model.addAttribute("reviewActionItem", reviewActionItem);
				previousYear = actionItem.getYear();
			}
			// UI only needs to understand that there is at least 1 quarterly action
			else if (actionItem.getTaskType() == ActionItem.TASK_TYPE_UPLOAD_QUARTERLY) {
				model.addAttribute("quarterlyDue", true);
			}
			
		}


		model.addAttribute("previousYear", previousYear);


		List<Document> documents = Document.getDocuments(company, person);
		// Add templates for download unless this is for a subscriber
		if (!person.isSubscriber()) {
			List<Document> templates = Document.findGlobalDocuments();
			documents.addAll(templates);
		}
		model.addAttribute("documents", documents);

		LinkedHashMap<DocumentType, ArrayList<Document>> documentMap = new LinkedHashMap<DocumentType, ArrayList<Document>>();

		List<DocumentType> documentTypes = new ArrayList<DocumentType>();

		if (company.isRated()) {
			model.addAttribute("rating", company.getRating());
		}
		
		if (company.isCARS()) {
			documentTypes = DocumentType
					.findAllDocumentTypesByParentCategoryName(CARS_RESOURCES);
		}
		else {
			documentTypes = DocumentType.findAllDocumentTypesSorted(company);			
		}

		List<Person> analysts = Person.findAnalysts();
		boolean isAnalyst = analysts.contains(person);

		if (documentTypes != null && documentTypes.size() > 0) {

			for (DocumentType documentType : documentTypes) {
				// TODO really.... 
				if ((!CARS_INTERNAL.equals(documentType.getDocumentTypeFolder().getName())
						&& !CARS_REPORT.equals(documentType.getDocumentTypeFolder().getName())
					    && !CARS_RESOURCES.equals(documentType.getDocumentTypeFolder().getName())
					    && !CARS_ARCHIVE.equals(documentType.getDocumentTypeFolder().getName())
					    && !CARS_INTERNAL_ARCHIVE.equals(documentType.getDocumentTypeFolder().getName())
						&& !person.isAdmin() && !isAnalyst)
										
					|| (person.isAdmin() && !CARS_RESOURCES.equals(documentType.getDocumentTypeFolder().getName()))
												
					|| (person.getCompany().isCARS() && company.isCARS()
						&& CARS_RESOURCES.equals(documentType.getDocumentTypeFolder().getName()))
																		
					|| (isAnalyst && !company.isCARS()
						&& !CARS_RESOURCES.equals(documentType.getDocumentTypeFolder().getName()))
						
					|| (person.getCompany().isCARS() && !company.isCARS()
							&& (CARS_REPORT.equals(documentType.getDocumentTypeFolder().getName())
							    || CARS_INTERNAL.equals(documentType.getDocumentTypeFolder().getName())
							    || CARS_INTERNAL_ARCHIVE.equals(documentType.getDocumentTypeFolder().getName())))
					)
				{
					ArrayList<Document> documentList = new ArrayList<Document>();
					documentMap.put(documentType, documentList);
				}
			}

			for (Document document : documents) {
				ArrayList<Document> documentList = documentMap.get(document
						.getDocumentType());

				if (documentList != null) {
					documentList.add(document);
				}
			}


			for (Map.Entry<DocumentType, ArrayList<Document>> entry : documentMap.entrySet()) {
				DocumentType documentType = entry.getKey();
				ArrayList<Document> typeDocuments = entry.getValue();
				documentType.setDueMessage(null);
				if (null == typeDocuments || typeDocuments.size() == 0) {
					documentType.setDueType(DueType.NONE);
				}
				else {
					documentType.setDueType(DueType.OK);
				}				

				Integer taskType = taskByDocType.get(documentType);
				if (null == taskType) {
					continue;
				}
				
				if (taskType == ActionItem.TASK_TYPE_FULL_ANALYSIS_DOCUMENTS) {
					documentType.setDueMessage("This document is required for pending full analysis");
				}
				else if (taskType == ActionItem.TASK_TYPE_ANNUAL_REVIEW_DOCUMENTS) {
					documentType.setDueMessage("This document is required for pending annual review");
				}
				else { // ActionItem.TASK_TYPE_UPLOAD_QUARTERLY
					documentType.setDueMessage("This document must be provided on a quarterly basis");
				}
				
				if (documentType.isAnnualIfAvailable() ){
					//there are the Document Types with the blue triangle

					if (reviewActionItem == null || !documentType.hasNotesForReview(reviewActionItem.getReview(), company)){
						documentType.setDueType(DueType.PAST_DUE_BLUE);
						documentType.setDueMessage("The most recent version of this document is required for pending Aeris analysis. Please confirm by initialing that documents are up to date or not applicable");
						documentType.setIsNoteRequired(true);
					} 
					
				}else {
					//there are the components with the red triangle:
					//documentType.isAnnualRequired() || documentType.isAnnualAsUpdated()
					
					documentType.setNeedToShowInitials(true);
					boolean showWarningMessage = true;
					
					if (reviewActionItem != null) {
						InitialsRecord initialsRecord = documentType.getInitialsRecord(reviewActionItem.getReview(), company);
						if (initialsRecord != null){
							showWarningMessage = false;
						}
					}
					
					if (showWarningMessage){
						documentType.setDueType(DueType.PAST_DUE);
						documentType.setDueMessage("Uploading this document is required for pending Aeris analysis");
						documentType.setIsInitialsRequired(true);
					}
				}
			}
		}

		// remove empty document types for subscriber

		LinkedHashMap<DocumentType, ArrayList<Document>> prunedDocumentMap = null;

		if (!person.getCompany().isCARS()
				&& !person.getCompany().getId().equals(company.getId())) {
			prunedDocumentMap = new LinkedHashMap<DocumentType, ArrayList<Document>>();
			for (DocumentType key : documentMap.keySet()) {
				ArrayList<Document> value = documentMap.get(key);
				if (value != null && value.size() > 0) {
					prunedDocumentMap.put(key, value);
				}
			}
		} else {
			prunedDocumentMap = documentMap;
		}
		
		if (itemsCount == null) {
			itemsCount = new int[prunedDocumentMap.keySet().size()];
			//for audits
			if (itemsCount.length > 0) {
				itemsCount[0] = 2;
				for (int i = 1; i < prunedDocumentMap.keySet().size(); i++) {
					itemsCount[i] = 0;
				}
			}
		}
		model.addAttribute("itemsCount", itemsCount);
		
		if (scrollTop == null) {
			scrollTop = 0;
		}
		model.addAttribute("scrollTop", scrollTop);
		
		ArchivingFileHelper.filterDocumentsMap(prunedDocumentMap, itemsCount);
		model.addAttribute("documentMap", prunedDocumentMap);

		if (null != error) {
			model.addAttribute("error", error);
		}
		
		HashMap<DocumentType, HashMap<Review, List<Note>>> notes = new HashMap<DocumentType, HashMap<Review, List<Note>>>();
		for (DocumentType iDocumentType: documentMap.keySet()){
			notes.put(iDocumentType, iDocumentType.getAllNotesByCompany(company));
		}
		model.addAttribute("allNotes", notes);

		return "cdfi/library";
	}

	@RequestMapping(value = "/cdfi/{id}/initialsRecord", method = RequestMethod.POST)
	public @ResponseBody InitialsRecordPOJO setInitials(
			@PathVariable Long id,
			Model model,
			@RequestParam(value = "reviewId", required = true) Long reviewId,
			@RequestParam(value = "documentTypeId", required = true) Long documentTypeId,
			@RequestParam(value = "initials", required = false) String initials){

		if (initials == null || initials.isEmpty() || initials.length() < 2 || initials.length() > 10 || !initials.matches("[a-zA-Z]+")){
			throw new IllegalArgumentException("The nitials must be between 2 and 10 lenght.");
		}
		
		Company company = Company.findCompany(id);
		model.addAttribute("company", company);
		
		Review review = Review.findReview(reviewId);
		DocumentType documentType = DocumentType.findDocumentType(documentTypeId);
		
		InitialsRecord initialsRecord = new InitialsRecord();
		initialsRecord.setDocumentType(documentType);
		initialsRecord.setReview(review);
		initialsRecord.setInitials(initials.toUpperCase());
		initialsRecord.setCompany(company);
		initialsRecord.persist();

		return new InitialsRecordPOJO( initialsRecord);
	}
	

	@RequestMapping(value = "/cdfi/{id}/note", method = RequestMethod.POST)
	public @ResponseBody NotePOJO putNote(
			@PathVariable Long id,
			Model model,
			@RequestParam(value = "reviewId", required = true) Long reviewId,
			@RequestParam(value = "documentTypeId", required = true) Long documentTypeId,
			@RequestParam(value = "text", required = false) String text){

		if (text == null || text.isEmpty() || text.length() > 140){
			throw new IllegalArgumentException("The text is not corret for a note.");
		}
		
		Company company = Company.findCompany(id);
		model.addAttribute("company", company);
		
		Review review = Review.findReview(reviewId);
		DocumentType documentType = DocumentType.findDocumentType(documentTypeId);
		
		Note note = new Note();
		note.setDocumentType(documentType);
		note.setReview(review);
		note.setText(text);
		note.setCompany(company);
		note.persist();
		note.setPerson(UserHelper.getCurrentUser());

		return new NotePOJO( note);
	}

	@RequestMapping(value = "/cdfi/{id}/note/{noteId}", method = RequestMethod.DELETE)
	public void putNote(
			@PathVariable Long id,
			@PathVariable Long noteId
			){
		Company company = Company.findCompany(id);
		Note note = Note.findById(noteId, company);
		note.remove();
	}
	
	@RequestMapping(value = "/cdfi/{id}/getReviews", method = RequestMethod.GET)
	public @ResponseBody
	List<ReviewPOJO> getReviews(@PathVariable Long id, Model model,
			HttpServletResponse response) {  
		Company company = Company.findCompany(id);
		List<Review> reviews = Review.findReviewsByCompanySorted(company);
		List<ReviewPOJO> data = new ArrayList<ReviewPOJO>();
		for(Review review : reviews)
		{
			ReviewPOJO item = new ReviewPOJO(review);
			data.add(item);
		}
		model.asMap().clear();
		return data;
	}

	@RequestMapping(value = "/cdfi/{id}/ratings", method = RequestMethod.GET)
	public String ratings(@PathVariable Long id, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		Company company = Company.findCompany(id);
		model.addAttribute("company", company);
		Person person = UserHelper.getCurrentUser();
		model.addAttribute("person", person);

		boolean authorized = AuthorizationHelper.isAuthorized(
				person, company);
		if (!authorized) {
			return "redirect:/";
		}
		
		List<Subscription> activeSubs = Subscription.findAllActiveSubscriptionsForSubscriber(person.getCompany());
		getActiveAllSubscription(activeSubs, company, model);

		if (person.getCompany().isCARS()) {
			List<Person> analysts = Person.findAnalysts();
			model.addAttribute("analysts", analysts);
		}

		List<Review> reviews = Review.findReviewsByCompanySorted(company);

		model.addAttribute("reviews", reviews);

		ServletContext context = request.getSession().getServletContext();

		this.addCdfiSideBarItems(model, company);

		if (!company.isRated() && !company.isShowRatingsTab()) { 
			return "redirect:/cdfi/" + id; 
		}

		// look for specific rating
		String rating = company.getRating();

		String ratingLogo = "AERIS_Rated_"
				+ rating
				.replaceAll("  ", " ")
				.replaceAll("\u2605\u2605\u2605\u2605", "4_stars")
				.replaceAll("\u2605\u2605\u2605", "3_stars")
				.replaceAll("\u2605\u2605", "2_stars")
				.replaceAll("\u2605", "1_star")
				.replaceAll("\\+", "_plus")
				.replaceAll("-", "_minus")
				.replaceAll(" ", "_");
		
		if (ratingLogo.contains("policy_plus")){
			ratingLogo = ratingLogo.replaceAll("_policy_plus", "");
			ratingLogo += "_policy_plus";
		}

		if (isImageAvailable(context, ratingLogo + ".jpg")) {
			model.addAttribute("ratingLogoJPG", ratingLogo + ".jpg");
		}
		if (isImageAvailable(context, ratingLogo + ".eps")) {
			model.addAttribute("ratingLogoEPS", ratingLogo + ".eps");
		}

		// look for ratedSince logos
		String ratedSinceLogo = "Aeris_RatedSince_"
				+ company.getRatedSince();

		if (isImageAvailable(context, ratedSinceLogo + ".jpg")) {
			model.addAttribute("ratedSinceLogoJPG", ratedSinceLogo + ".jpg");
		}
		if (isImageAvailable(context, ratedSinceLogo + ".eps")) {
			model.addAttribute("ratedSinceLogoEPS", ratedSinceLogo + ".eps");
		}
		String[] images = {"AERIS_Rated_1_star_A",
				 "AERIS_Rated_1_star_AA",
				 "AERIS_Rated_1_star_AAA",
				 "AERIS_Rated_1_star_AAA_policy_plus",
				 "AERIS_Rated_1_star_AA_minus",
				 "AERIS_Rated_1_star_AA_minus_policy_plus",
				 "AERIS_Rated_1_star_AA_plus",
				 "AERIS_Rated_1_star_AA_plus_policy_plus",
				 "AERIS_Rated_1_star_AA_policy_plus",
				 "AERIS_Rated_1_star_A_minus",
				 "AERIS_Rated_1_star_A_minus_policy_plus",
				 "AERIS_Rated_1_star_A_plus",
				 "AERIS_Rated_1_star_A_plus_policy_plus",
				 "AERIS_Rated_1_star_A_policy_plus",
				 "AERIS_Rated_1_star_B",
				 "AERIS_Rated_1_star_BB",
				 "AERIS_Rated_1_star_BBB",
				 "AERIS_Rated_1_star_BBB_minus",
				 "AERIS_Rated_1_star_BBB_minus_policy_plus",
				 "AERIS_Rated_1_star_BBB_plus",
				 "AERIS_Rated_1_star_BBB_plus_policy_plus",
				 "AERIS_Rated_1_star_BBB_policy_plus",
				 "AERIS_Rated_1_star_BB_minus",
				 "AERIS_Rated_1_star_BB_minus_policy_plus",
				 "AERIS_Rated_1_star_BB_plus",
				 "AERIS_Rated_1_star_BB_plus_policy_plus",
				 "AERIS_Rated_1_star_BB_policy_plus",
				 "AERIS_Rated_1_star_B_policy_plus",
				 "AERIS_Rated_2_stars_A",
				 "AERIS_Rated_2_stars_AA",
				 "AERIS_Rated_2_stars_AAA",
				 "AERIS_Rated_2_stars_AAA_policy_plus",
				 "AERIS_Rated_2_stars_AA_minus",
				 "AERIS_Rated_2_stars_AA_minus_policy_plus",
				 "AERIS_Rated_2_stars_AA_plus",
				 "AERIS_Rated_2_stars_AA_plus_policy_plus",
				 "AERIS_Rated_2_stars_AA_policy_plus",
				 "AERIS_Rated_2_stars_A_minus",
				 "AERIS_Rated_2_stars_A_minus_policy_plus",
				 "AERIS_Rated_2_stars_A_plus",
				 "AERIS_Rated_2_stars_A_plus_policy_plus",
				 "AERIS_Rated_2_stars_A_policy_plus",
				 "AERIS_Rated_2_stars_B",
				 "AERIS_Rated_2_stars_BB",
				 "AERIS_Rated_2_stars_BBB",
				 "AERIS_Rated_2_stars_BBB_minus",
				 "AERIS_Rated_2_stars_BBB_minus_policy_plus",
				 "AERIS_Rated_2_stars_BBB_plus",
				 "AERIS_Rated_2_stars_BBB_plus_policy_plus",
				 "AERIS_Rated_2_stars_BBB_policy_plus",
				 "AERIS_Rated_2_stars_BB_minus",
				 "AERIS_Rated_2_stars_BB_minus_policy_plus",
				 "AERIS_Rated_2_stars_BB_plus",
				 "AERIS_Rated_2_stars_BB_plus_policy_plus",
				 "AERIS_Rated_2_stars_BB_policy_plus",
				 "AERIS_Rated_2_stars_B_policy_plus",
				 "AERIS_Rated_3_stars_A",
				 "AERIS_Rated_3_stars_AA",
				 "AERIS_Rated_3_stars_AAA",
				 "AERIS_Rated_3_stars_AAA_policy_plus",
				 "AERIS_Rated_3_stars_AA_minus",
				 "AERIS_Rated_3_stars_AA_minus_policy_plus",
				 "AERIS_Rated_3_stars_AA_plus",
				 "AERIS_Rated_3_stars_AA_plus_policy_plus",
				 "AERIS_Rated_3_stars_AA_policy_plus",
				 "AERIS_Rated_3_stars_A_minus",
				 "AERIS_Rated_3_stars_A_minus_policy_plus",
				 "AERIS_Rated_3_stars_A_plus",
				 "AERIS_Rated_3_stars_A_plus_policy_plus",
				 "AERIS_Rated_3_stars_A_policy_plus",
				 "AERIS_Rated_3_stars_B",
				 "AERIS_Rated_3_stars_BB",
				 "AERIS_Rated_3_stars_BBB",
				 "AERIS_Rated_3_stars_BBB_minus",
				 "AERIS_Rated_3_stars_BBB_minus_policy_plus",
				 "AERIS_Rated_3_stars_BBB_plus",
				 "AERIS_Rated_3_stars_BBB_plus_policy_plus",
				 "AERIS_Rated_3_stars_BBB_policy_plus",
				 "AERIS_Rated_3_stars_BB_minus",
				 "AERIS_Rated_3_stars_BB_minus_policy_plus",
				 "AERIS_Rated_3_stars_BB_plus",
				 "AERIS_Rated_3_stars_BB_plus_policy_plus",
				 "AERIS_Rated_3_stars_BB_policy_plus",
				 "AERIS_Rated_3_stars_B_policy_plus",
				 "AERIS_Rated_4_stars_A",
				 "AERIS_Rated_4_stars_AA",
				 "AERIS_Rated_4_stars_AAA",
				 "AERIS_Rated_4_stars_AAA_policy_plus",
				 "AERIS_Rated_4_stars_AA_minus",
				 "AERIS_Rated_4_stars_AA_minus_policy_plus",
				 "AERIS_Rated_4_stars_AA_plus",
				 "AERIS_Rated_4_stars_AA_plus_policy_plus",
				 "AERIS_Rated_4_stars_AA_policy_plus",
				 "AERIS_Rated_4_stars_A_minus",
				 "AERIS_Rated_4_stars_A_minus_policy_plus",
				 "AERIS_Rated_4_stars_A_plus",
				 "AERIS_Rated_4_stars_A_plus_policy_plus",
				 "AERIS_Rated_4_stars_A_policy_plus",
				 "AERIS_Rated_4_stars_B",
				 "AERIS_Rated_4_stars_BB",
				 "AERIS_Rated_4_stars_BBB_minus",
				 "AERIS_Rated_4_stars_BBB_minus_policy_plus",
				 "AERIS_Rated_4_stars_BBB_plus",
				 "AERIS_Rated_4_stars_BBB_plus_policy_plus",
				 "AERIS_Rated_4_stars_BBB_policy_plus",
				 "AERIS_Rated_4_stars_BB_minus",
				 "AERIS_Rated_4_stars_BB_minus_policy_plus",
				 "AERIS_Rated_4_stars_BB_plus",
				 "AERIS_Rated_4_stars_BB_plus_policy_plus",
				 "AERIS_Rated_4_stars_BB_policy_plus",
				 "AERIS_Rated_4_stars_B_policy_plus",
				 "AERIS_Rated_4_star_BBB"};
		model.addAttribute("images", images);
		
		return "cdfi/ratings";
	}

	private boolean isImageAvailable(ServletContext context,
			String imageFileName) {

		boolean available = false;
		URL path = null;
		URL newPath = null;
		try {
			path = context.getResource("/images/logo/" + imageFileName);
			newPath = context.getResource("/images/logo/NewRatings/" + imageFileName);
		} catch (MalformedURLException ignore) {
		}

		available = path != null || newPath != null;

		return available;
	}

	@RequestMapping(value = "/cdfi/{id}/ratings", method = RequestMethod.POST)
	public String ratingsAdd(
			@PathVariable Long id,
			Model model,
			@RequestParam(value = "reviewType", required = true) String reviewTypeString,
			@RequestParam(value = "dateOfAnalysis", required = true) String dateOfAnalysis,
			@RequestParam(value = "impactPerformance", required = false) String impactPerformance,
			@RequestParam(value = "financialStrength", required = false) String financialStrength,
			@RequestParam(value = "policyPlus", required = false) boolean policyPlus,
			@RequestParam(value = "impactPerformanceTrend", required = false) String impactPerformanceTrend,
			@RequestParam(value = "financialStrengthTrend", required = false) String financialStrengthTrend,
			@RequestParam(value = "trend", required = false) String trend) {

		Company company = Company.findCompany(id);
		model.addAttribute("company", company);

		try {

			Review review = new Review();
			review.setCompany(company);
			review.setDateOfAnalysis(DateHelper
					.parseCompactFormattedDate(dateOfAnalysis));

			if ("fullAnalysis".equals(reviewTypeString)) {
				review.setReviewType(ReviewType.ANNUAL_RATING);
				review.setFinancialStrength(financialStrength);
				review.setImpactPerformance(impactPerformance);
				review.setPolicyPlus(policyPlus);

			} else {
				review.setReviewType(ReviewType.ANNUAL);
				review.setFinancialStrength(financialStrengthTrend);
				review.setImpactPerformance(impactPerformanceTrend);
			}

			review.persist();

		} catch (ParseException e) {

			// TODO: put error here
		}

		return "redirect:/cdfi/" + company.getId() + "/ratings";
	}

	@RequestMapping(value = "/cdfi/ratings/{id}/remove", method = RequestMethod.POST)
	public String ratingsDelete(@PathVariable("id") Long id, Model uiModel) {
		Review review = Review.findReview(id);
		Company company = review.getCompany();
		review.remove();
		uiModel.asMap().clear();
		return "redirect:/cdfi/" + company.getId() + "/ratings";
	}

	@RequestMapping(value = "/cdfi/ratings/{id}/save", method = RequestMethod.POST)
	public @ResponseBody
	Response ratingsSave(@PathVariable("id") Long id, @RequestBody Boolean skipNotification, 
			Model model, HttpServletResponse response) {
		Review review = Review.findReview(id);
		review.setSkipNotification(skipNotification);
		review.merge();
		model.asMap().clear();
		return new Response("");
	}

	@RequestMapping(value = "/cdfi/{id}/subscribers")
	public String subscribers(@PathVariable Long id, Model model) {

		Company company = Company.findCompany(id);
		model.addAttribute("company", company);
		Person person = UserHelper.getCurrentUser();
		model.addAttribute("person", person);

		if (person.getCompany().isCARS()) {
			List<Person> analysts = Person.findAnalysts();
			model.addAttribute("analysts", analysts);
		}

		HashMap<Company, Date> lastAccessed = new HashMap<Company, Date>();
		if (company != null && company.getCompanyType() == CompanyType.CDFI) {

			// find subscriptions
			// List<Subscription> cdfiSubscriptions =
			// Subscription.findSubscriptionsBySubscriberEntry(company);
			List<Subscription> cdfiSubscriptions = Subscription
					.findSubscriptionsByCdfiEntry(company);

			// find last accessed
			for (Subscription subs : cdfiSubscriptions) {
				Company owner = subs.getOwner();
				if (owner.isHideActivity() || (owner.getId().equals(id))
						// && !person.getCompany().isCARS()
						) {
					continue;
				}
				lastAccessed.put(owner, (Date) Activity
						.findLastAccessTimeByOwnerAndTarget(company, owner));
			}
			model.addAttribute("lastAccessed", lastAccessed);
			addCdfiSideBarItems(model, company);
		}

		return "cdfi/subscribers";
	}

	@RequestMapping(params = "form", method = RequestMethod.GET, produces = "text/html")
	public String createForm(Model model) {

		Company company = new Company();
		// company.setCompanyType(CompanyType.INVESTOR);
		model.addAttribute("company", company);

		Person contact = new Person();
		contact.setCompany(company);
		model.addAttribute("contact", contact);
		addReferenceDataToModel(model);
		return "cdfi/create";
	}

	@RequestMapping(value = "/cdfi/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) {

		Company company = Company.findCompany(id);
		Person contact = null;

		// should be just one contact, get her
		try {

			contact = Person.findPeopleByCompany(company).getResultList()
					.get(0);

		} catch (Exception ignore) {
		}

		if (contact == null) {
			contact = new Person();
			contact.setCompany(company);
		}

		Person person = UserHelper.getCurrentUser();
		model.addAttribute("person", person);
		model.addAttribute("contact", contact);
		model.addAttribute("company", company);

		addReferenceDataToModel(model);
		return "cdfi/create";
	}

	private void addReferenceDataToModel(Model model) {
		List<State> stateList = State.findAllStatesAlphabetically();
		model.addAttribute("states", SelectOptionHelper.getStates(stateList));
		model.addAttribute("areas", stateList);
		model.addAttribute("fiscalYearEnds",
				SelectOptionHelper.getFiscalYearEnds());
		model.addAttribute("orgTypes", SelectOptionHelper.getOrgTypes());
		model.addAttribute("lendingTypes", LendingType.findAllLendingTypes());
		model.addAttribute("sectoralFocuses", SectoralFocus.findAllSectoralFocuses());
		model.addAttribute("targetBeneficiaries", TargetBeneficiary.findAllTargetBeneficiaries());
		model.addAttribute("financialStrengths",
				SelectOptionHelper.getFinancialStrengths());
		model.addAttribute("impactStrengths",
				SelectOptionHelper.getImpactStrengths());
		model.addAttribute("shareFinancialsTypes", Arrays.asList(ShareFinancials.values()));
		model.addAttribute("taxTypes", Arrays.asList(TaxType.values()));
		model.addAttribute("years", SelectOptionHelper.getYears());
		model.addAttribute("quarters", SelectOptionHelper.getQuarters());	
	}

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		uiModel.addAttribute("company", Company.findCompany(id));
		return "cdfi/update";
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
	public String update(@Valid Company company, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest) {

		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("company", company);
			return "cdfi/update";
		}
		uiModel.asMap().clear();
		company.merge();
		return "redirect:/cdfi/" + company.getId();
	}

	@RequestMapping(value = "/cdfi/datapoints/{id}", method = RequestMethod.GET)
	public String showdatapoints(@PathVariable Long id, Model model) {

		Company company = Company.findCompany(id);
		model.addAttribute("company", company);
		List<Metric> datapoints = Metric.findAllCompanyMetrics(id);

		TreeMap<String, Metric> treeMap = new TreeMap<String, Metric>();

		for (Metric metric : datapoints) {
			treeMap.put(metric.getFullAccountCode(), metric);
		}

		model.addAttribute("datapoints", treeMap.values());
		return "cdfi/datapoints";
	}

	@RequestMapping(value = "/cdfi/datapoints/{id}", method = RequestMethod.POST)
	public String savedatapoints(Company company, Model model) {

		List<Metric> requiredMetrics = company.getRequiredMetrics();

		Company companySaved = Company.findCompany(company.getId());
		companySaved.setRequiredMetrics(requiredMetrics);
		companySaved.merge();
		return "redirect:/cdfi/" + company.getId();
	}

	@RequestMapping(method = RequestMethod.POST)
	public String update( @ModelAttribute Person contact, Model model) {

		String error = null;

		// update or create company
		Company company = null;
		if (contact.getCompany().getId() == null) {
			company = new Company();
		} else {
			company = Company.findCompany(contact.getCompany().getId());
		}

		company.setName(contact.getCompany().getName());
		company.setRated(contact.getCompany().isRated());
		company.setShowRatingsTab(contact.getCompany().isShowRatingsTab());
		company.setPeerGroupAllowed(contact.getCompany().isPeerGroupAllowed());
		company.setShareFinancials(contact.getCompany().getShareFinancials());
		company.setAddress(contact.getCompany().getAddress());
		company.setAddress2(contact.getCompany().getAddress2());
		company.setCity(contact.getCompany().getCity());
		company.setState(contact.getCompany().getState());
		company.setZip(contact.getCompany().getZip());
		company.setPhone(contact.getCompany().getPhone());
		company.setFiscalYearStart(contact.getCompany().getFiscalYearStart());
		company.setCompanyType(CompanyType.CDFI);
		company.setFax(contact.getCompany().getFax());
		company.setAreaServed(contact.getCompany().getAreaServed());
		company.setUrl(contact.getCompany().getUrl());
		company.setMission(contact.getCompany().getMission());
		company.setEIN(contact.getCompany().getEIN());
		company.setDescription(contact.getCompany().getDescription());
		company.setLendingType(contact.getCompany().getLendingType());
		company.setSectoralFocus(contact.getCompany().getSectoralFocus());
		company.setTargetBeneficiary(contact.getCompany().getTargetBeneficiary());
		company.setOrgType(contact.getCompany().getOrgType());
		company.setTaxType(contact.getCompany().getTaxType());
		company.setHideActivity(contact.getCompany().isHideActivity());
		company.setActive(contact.getCompany().isActive());
		company.setSuppress(contact.getCompany().isSuppress());

		int startRequestingYear = contact.getCompany().getStartRequestingYear();
		int startRequestingQuarter = contact.getCompany().getStartRequestingQuarter();

		if ((startRequestingYear ==0 && startRequestingQuarter == 0) 
				|| (startRequestingYear !=0 && startRequestingQuarter != 0))
		{
			company.setStartRequestingYear(startRequestingYear);
			company.setStartRequestingQuarter(startRequestingQuarter);
		}
		else{
			error = "Please set start requesting year and quarter correctly";
		}

		Person person = null;

		if (contact.getId() == null) {
			System.out.println("creating new contact");
			person = new Person();
		} else {
			person = Person.findPerson(contact.getId());
		}

		person.setUsername(contact.getUsername());
		person.setFirstName(contact.getFirstName());
		person.setLastName(contact.getLastName());
		person.setEmail(contact.getEmail());
		person.setTitle(contact.getTitle());
		person.setCompany(company);
		person.setPersonRole(PersonRole.BASIC);
		person.setEmailRemindersEnabled(contact.isEmailRemindersEnabled());

		if (contact.getId() == null) {

			Person existingPerson = null;

			try {
				existingPerson = Person.findPeopleByUsernameEquals(
						contact.getUsername());
			} catch (Exception ignore) {
			}

			if (existingPerson != null) {
				error = "user with name [" + contact.getUsername()
						+ "] already exists";
			}
		}

		if (error != null) {
			model.addAttribute("contact", contact);
			model.addAttribute("error", error);
			addReferenceDataToModel(model);
			return "cdfi/create";
		}

		// if all is well, persist data
		if (contact.getCompany().getId() != null) {
			company.merge();
		} else {
			company.persist();
		}
		// create user

		if (contact.getId() != null) {
			person.merge();
		} else {
			person.persist();
		}

		ReportDataCache.remove(company);
		return "redirect:/cdfi/" + company.getId();
	}

	private static long showElapsedTime(String activity, long millis) {
		long elapsed = System.currentTimeMillis() - millis;
		System.out.println(activity + " took " + elapsed + " ms");
		return System.currentTimeMillis();
	}

	@RequestMapping(value = "/cdfi/{id}/financial/{tab}")
	public String financial(@PathVariable Long id, @PathVariable String tab,
			Model model, HttpServletRequest request,
			HttpServletResponse response) {

		long elapsed = System.currentTimeMillis();

		Company company = Company.findCompany(id);
		model.addAttribute("company", company);

		Person person = UserHelper.getCurrentUser();
		model.addAttribute("person", person);
		
		List<Subscription> activeSubs = Subscription.findAllActiveSubscriptionsForSubscriber(person.getCompany());
		getActiveAllSubscription(activeSubs, company, model);

		boolean authorized = AuthorizationHelper.isAuthorized(
				person, company, AuthorizationHelper.ACCESS_TYPE_FINANCIALS);
		if (!authorized) {
			model.addAttribute("authorized", false);
			return "cdfi/financial";
		}
		model.addAttribute("authorized", true);
		
		

		HashMap<String, TentativeChange> tentativeChanges = TentativeChangeCache
				.getTentativeChanges(request.getSession(), company.getId());

		ReportData reportData = ReportDataCache.getReportData(company,
				tentativeChanges);

		String inputData = (String) reportData.get("inputData");

		if (inputData != null && inputData.length() > 0) {


			// TODO there is no data for these attributes. remove here as well as reference in financials, or add them in ReportService
			model.addAttribute("inputFootnotes",
					reportData.get("inputFootnotes"));
			model.addAttribute("positionFootnotes",
					reportData.get("positionFootnotes"));
			model.addAttribute("activityFootnotes",
					reportData.get("activityFootnotes"));
			model.addAttribute("summaryFootnotes",
					reportData.get("summaryFootnotes"));
			model.addAttribute("additionalFootnotes",
					reportData.get("additionalFootnotes"));


			model.addAttribute("periods", reportData.get("periods"));
			model.addAttribute("columns", reportData.get("columns"));
			model.addAttribute("fields", reportData.get("fields"));
			model.addAttribute("inputData", reportData.get("inputData"));
			model.addAttribute("positionData", reportData.get("positionData"));
			model.addAttribute("activityData", reportData.get("activityData"));
//			model.addAttribute("summaryData", reportData.get("summaryData"));
//			model.addAttribute("additionalData", reportData.get("additionalData"));
			model.addAttribute("footnotes", reportData.get("footnotes"));
//			model.addAttribute("graphs", reportData.get("graphs"));
//			model.addAttribute("graphTitles", reportData.get("graphTitles"));

			if (tentativeChanges != null && tentativeChanges.size() > 0) {
				model.addAttribute("tentativeChanges", tentativeChanges);
			}
		}

		// log activity if it's a subscriber or other CDFI viewing a different
		// CDFI's data
		if (!person.getCompany().getId().equals(company.getId())
				&& person.getCompany().getCompanyType() != CompanyType.CARS) {
			Activity activity = new Activity();
			activity.setCompany(company);
			activity.setDate(new java.util.Date());
			activity.setPerson(person);
			activity.setActivityType(ActivityType.FINANCIALS_VIEW);
			activity.persist();
		}
		elapsed = showElapsedTime("logging activity ", elapsed);
		return "cdfi/financial";
	}
	
	@RequestMapping(value = "/cdfi/{companyid}/getdocuments/{viewerid}")
	public @ResponseBody List<DocumentPOJO> getDocuments(@PathVariable Long companyid, @PathVariable("viewerid") Long viewerid, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		Company company = Company.findCompany(companyid);
	
		List<DocumentPOJO> docs = new ArrayList<DocumentPOJO>();
		for (Document doc : Document.findDocumentsByCompany(company).getResultList()) {
			DocumentPOJO pojo = new DocumentPOJO(doc);
			pojo.setCategoryId(doc.getDocumentType().getDocumentTypeCategory().getId());
			pojo.setFolderId(doc.getDocumentType().getDocumentTypeFolder().getId());
			if (viewerid.longValue() == -1 && doc.isAllAnalysts()) { //if CARS
				pojo.setHasAccess(true);
				
			} else {
				if (doc.isAllSubscribers()) {
					pojo.setHasAccess(true);
					if (viewerid.longValue() != -1 && viewerid.longValue() != -2) //if the viewer is not CARS or All Viewer
						pojo.setCanToggleAccess(false);
				} else {
					for (Company viewer : doc.getAuthorizedSubscribers()) {
						if (viewer.getId().longValue() == viewerid.longValue()) {
							pojo.setHasAccess(true);
							break;
						}
					}
				}
			}
			docs.add(pojo);
		}		
		return docs;
	}
	
	@RequestMapping(value = "/cdfi/savedocumentsaccess/{viewerid}", method = RequestMethod.POST)
    public @ResponseBody Response saveDocumentAccess(@RequestBody DocumentPOJO[] documents, @PathVariable("viewerid") Long viewerid)
	{
		if (documents != null) {
			boolean allCARS = viewerid.longValue() == -1; //CARS
			boolean allSubscribers = viewerid.longValue() == -2; 
			
			Company viewer = null;
			if (!allCARS && !allSubscribers)
				viewer = Company.findCompany(viewerid);			

			for (DocumentPOJO pojo : documents) {
				
				Document document = Document.findDocument(pojo.getId());
				if (document == null) continue;
				// Reset document
								
				if (allCARS)
					document.setAllAnalysts(pojo.isHasAccess());
				
				if (allSubscribers)
					document.setAllSubscribers(pojo.isHasAccess());
				
				if (viewer != null) {
					if (pojo.isHasAccess())
						document.getAuthorizedSubscribers().add(viewer);
					else {
						if (document.getAuthorizedSubscribers().contains(viewer))
							document.getAuthorizedSubscribers().remove(viewer);
					}
				}
				document.merge();
			}
		}	
		return new Response("");
	}
	
	@RequestMapping(value = "/cdfi/sendnotification", method = RequestMethod.PUT, produces = "text/html")
    public @ResponseBody String sendNotification(
       		@RequestBody List<Integer> viewerIds,
    		Model uiModel, HttpServletRequest httpServletRequest) {
		String ans = "OK";
		if (viewerIds != null && viewerIds.size() > 0) {
			try {
				List<Company> viewers = new ArrayList<Company>();
				for (Integer id : viewerIds) {
					Company viewer = Company.findCompany(id.longValue());
					if (viewer != null) {
						viewers.add(viewer);
					}
				}
				
				ans = reminderService.processLibraryNotification(viewers, UserHelper.getCurrentUser().getCompany()) ? "OK" : "NOTOK";
			} catch (Exception ex) {
				throw ex;
			}
		}
		return ans;
	}

	@RequestMapping(value = "/cdfi/{id}/analysts", method = RequestMethod.POST)
	public String analysts(Company company, Model model) {

		List<Person> analysts = company.getAnalysts();
		Company companySaved = Company.findCompany(company.getId());
		companySaved.setAnalysts(analysts);
		companySaved.merge();
		return "redirect:/cdfi/" + company.getId();
	}

	@RequestMapping(value = "/activity/delete/{id}")
	public String deleteActivity(@PathVariable Long id, Model model) {

		Person person = UserHelper.getCurrentUser();

		Activity activity = Activity.findActivity(id);

		if (person.isAdmin()) {
			activity.remove();
		}
		return "redirect:/cdfi/" + activity.getCompany().getId();
	}

	@RequestMapping(value = "/cdfi/delete/{id}")
	public String delete(@PathVariable Long id, Model model) {
		Company company = null;
		try {

			company = Company.findCompany(id);

			company.remove();

		} catch (Exception e) {

			String errorString = "Unable to delete subscriber";

			if (e.getMessage().contains("ConstraintViolationException")) {
				errorString += " due to related data that requires this record";
			}

			model.addAttribute("error", errorString);
			return "redirect:/cdfi/" + id;
		}

		return "redirect:/";
	}

	private void addCdfiSideBarItems(Model model, Company company) {
		if (company.isRated()) {
			model.addAttribute("rating", company.getRating());
		}

		ActionItemService actionItemService = new ActionItemService(company);
		List<ActionItem> actionItems = actionItemService
				.getActionItems(false);
		model.addAttribute("actionItems", actionItems);
	}

}

