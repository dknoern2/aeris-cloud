package com.cars.platform.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricMap;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.Person;
import com.cars.platform.util.UserHelper;

public class AccountCodeFixServlet extends HttpServlet {
	private static final long serialVersionUID = 93485732498573485L;

	private HashMap<String, String> getMap() {

		HashMap<String, String> map = new HashMap<String, String>();

		map.put("101012", "101008");
		map.put("101015", "101033");
		map.put("101020", "101034");
		map.put("101021", "101033");
		map.put("101030", "101019");
		map.put("101031", "101032");
		map.put("101035", "101011");
		map.put("101036", "101010");
		//map.put("101038", "103037");
		map.put("101038", "101037");
		map.put("101039", "101033");
		map.put("101042", "101019");
		map.put("102013", "102014");
		map.put("102020", "102046");
		map.put("102027", "102014");
		map.put("102042", "102014");
		map.put("102044", "102045");
		map.put("103015", "103010");
		map.put("103017", "103010");
		map.put("103026", "103013");
		map.put("103027", "103016");
		map.put("103028", "103010");
		map.put("103029", "103010");
		map.put("103031", "103022");
		map.put("103032", "103033");
		map.put("104011", "104012");
		map.put("104018", "104010");
		map.put("104019", "104012");
		map.put("104022", "104012");
		map.put("104023", "104010");
		map.put("104024", "104015");
		map.put("104026", "104025");
		map.put("105012", "105017");
		map.put("105013", "105017");
		map.put("105015", "105017");
		map.put("105016", "105017");
		map.put("106017", "106012");
		map.put("106019", "106016");
		map.put("106020", "106016");
		map.put("201016", "201014");
		map.put("201017", "201014");
		map.put("201018", "201014");
		map.put("201019", "201014");
		map.put("201020", "201010");
		map.put("202022", "202021");
		map.put("202025", "206033");
		map.put("202032", "202016");
		map.put("202034", "202012");
		map.put("204025", "2040146");
		map.put("204026", "204020");
		map.put("204032", "204014");

		return map;

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Map<String, String> map = getMap();
		PrintWriter writer = response.getWriter();

		Logger logger = Logger.getLogger("Account Code Fix");
		System.out.println("Account Code Fix");


		
        Person currentUser = UserHelper.getCurrentUser();
        
        if(currentUser==null||!currentUser.isAdmin()){
        	writer.write("unauthorized\n");
        	return;
        }else{
        	
        	writer.write("authorized\n");
        }

		
        writer.write("Account code fix running");
		logger.info("Account code fix running");

		int mappingCount = 0;
		int metricCount = 0;
		int metricValueCount = 0;

		for (Company company : Company.findCompanysByCompanyType(CompanyType.CDFI).getResultList()) {


			
			logger.info("Fixing " + company.getName());
			writer.write("Fixing " + company.getName() + "\n");

			for (Period period : Period.findPeriodsByCompany(company).getResultList()) {
				writer.write("  period: " + period.getEndDate() + "\n");

				for (String code1 : map.keySet()) {

					Metric metric1 = null;
					
					try{
						metric1=Metric.findMetricByFullAccountCode(code1).getSingleResult();
					}catch(Exception e){}

					
					if(metric1!=null){
					String code2 = map.get(code1);
					Metric metric2 = null;

					try {
						metric2 = Metric.findMetricByFullAccountCode(code2).getSingleResult();
					} catch (Exception e) {
						writer.write("    No metric found for account code " + code2 + "\n");

					}

					if (metric2 != null) {

						List<MetricMap> metricMaps = MetricMap.findMetricMaps(company, metric1, period.getYear(),
								period.getQuarter());

						if (metricMaps.size() > 0) {
							 for(MetricMap metricMap:metricMaps){
							     metricMap.setMetric(metric2);
							     metricMap.merge();
						     }

							writer.write("    found " + metricMaps.size() + " mapings for " + code1 + "\n");
							mappingCount += metricMaps.size();
						}

						MetricValue metricValue1 = MetricValue.findMetricValue(period.getQuarter(), period.getYear(),
								company.getId(), metric1.getId());

						if (metricValue1 != null) {
							writer.write("    found metric value for " + code1 + ": " + metricValue1.getAmountValue() + "\n");

							MetricValue metricValue2 = MetricValue.findMetricValue(period.getQuarter(), period.getYear(),
									company.getId(), metric2.getId());
							
							if(metricValue2==null){
								writer.write("      no metric value for " + code2 + ", will adjust original\n");
								
								metricValue1.setMetric(metric2);
								metricValue1.merge();
								
							}else{
								writer.write("      metric value found for " + code2 + ", will add\n");
								double amount2 = 0;
								if(metricValue2.getAmountValue()!=null){
									amount2+=metricValue2.getAmountValue();
								}
								if(metricValue1.getAmountValue()!=null){
									amount2+=metricValue1.getAmountValue();
								}
								
								metricValue2.setAmount(amount2);
								metricValue2.merge();
								
								//metricValue1.deleteMetricValue(metricValue1.getQuarter(), metricValue1.getYear(), metricValue1.getCompany().getId(), metricValue1.getMetric().getId());
								metricValue1.remove();
								
								
							}
							
							
							
							metricValueCount++;

						}
					}
				}
				}

			}
		}
		
		for (String code1 : map.keySet()) {

			Metric metric1 = null;
			try{
			metric1 = Metric.findMetricByFullAccountCode(code1).getSingleResult();
			
			
			
			for (Company company : Company.findCompanysByCompanyType(CompanyType.CDFI).getResultList()) {
				List<Metric> requiredMetrics = company.getRequiredMetrics();
				boolean contains = false;
				if(requiredMetrics.contains(metric1)){
					contains = true;
					requiredMetrics.remove(metric1);
				}
				if(contains){
					company.merge();
				}
			}

			Metric.removeMetric(metric1);
			metricCount++;
			writer.write("removing obsolete metric " + code1+"\n");
			}catch(Exception e){}
			

		}
		

		writer.write("\nfixed " + mappingCount + " mappings" + "\n");
		writer.write("\nfixed " + metricValueCount + " metric values" + "\n");
		writer.write("\nremoved " + metricCount + " obsolete metrics" + "\n");
		writer.write("Account code fix complete" + "\n");


	}
}
