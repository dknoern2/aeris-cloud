package com.cars.platform.web;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Footnote;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.util.UserHelper;
@RequestMapping("/footnotes")
@Controller
public class FootnoteController {
	
	

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Footnote footnote, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, footnote);
            return "footnotes/create";
        }
        uiModel.asMap().clear();
        
        footnote.setCreated(new Date());
        footnote.setPerson(UserHelper.getCurrentUser());
        footnote.persist();
        
    
        ReportDataCache.remove(footnote.getCompany());
        return "redirect:/footnotes?companyId="+ footnote.getCompany().getId();
       
      
    }   
    
    @RequestMapping(value = "cdfi/{id}",params = "form", produces = "text/html")
    public String createForCDFI(@PathVariable("id") Long id, Model uiModel) {
    	Company company = Company.findCompany(id);
    	Footnote footnote = new Footnote();
    	footnote.setCompany(company);
        populateEditForm(uiModel,footnote);
        uiModel.addAttribute("company", company);
        return "footnotes/create";
    }	  
    

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "companyId", required = false) Long companyId, Model uiModel) {
       
    	Company company = null; 
    	List<Footnote> footnotes = null;
    	
    	
    	if(companyId!=null){
    		company = Company.findCompany(companyId);
    	}
    	
    	uiModel.addAttribute("company",Company.findCompany(companyId));
    	footnotes = Footnote.findFootnotesByCompanySorted(company).getResultList();
    	
         uiModel.addAttribute("footnotes", footnotes);
        
        return "footnotes/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Footnote footnote, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, footnote);
            return "footnotes/update";
        }
        uiModel.asMap().clear();
        
        footnote.setCreated(new Date());
        footnote.setPerson(UserHelper.getCurrentUser());
        footnote.merge();
    
       
        ReportDataCache.remove(footnote.getCompany());
        
        return "redirect:/footnotes?companyId="+ footnote.getCompany().getId();
        
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
    	System.out.println("getting footnote to edit");
        populateEditForm(uiModel, Footnote.findFootnote(id));
    	System.out.println("forwarding to update");

        return "footnotes/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
    	Footnote footnote = Footnote.findFootnote(id);
    	Company company = footnote.getCompany();
    	  	
    	try{	
            footnote.remove();
            uiModel.asMap().clear();

            ReportDataCache.remove(company);
        
		}catch(Exception e){
			
			String errorString = "Unable to delete footnote";

			if (e.getMessage().contains("ConstraintViolationException")){
				errorString += " due to related data that requires this record";
			}
			
			uiModel.addAttribute("error", errorString);		
		}
        
       
        	ReportDataCache.remove(company);
        	return "redirect:/footnotes?companyId="+ company.getId();

    }
    
    void populateEditForm(Model uiModel, Footnote footnote) {
        uiModel.addAttribute("footnote", footnote);
        
        uiModel.addAttribute("reportPages", Arrays.asList(ReportPage.values()));

    	List<Company> companys = new ArrayList<Company>();
    	if(footnote!=null&&footnote.getCompany()!=null){
    		companys.add(footnote.getCompany());
    	}
        uiModel.addAttribute("companys", companys);        
        uiModel.addAttribute("employees", Person.findPeopleByCompany(UserHelper.getCurrentUser().getCompany()).getResultList());  
        uiModel.addAttribute("date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        
    }
    
    String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
