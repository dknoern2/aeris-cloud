package com.cars.platform.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Activity;
import com.cars.platform.domain.ActivityType;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricCategory;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Person;
import com.cars.platform.util.LogoHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.ChildCategory;
import com.cars.platform.viewmodels.MetricValuePOJO;
import com.cars.platform.viewmodels.ParentCategory;
import com.cars.platform.viewmodels.Response;

@RequestMapping("/webform/**")
@Controller
public class WebFormController {

	@RequestMapping(value = "/webform/{companyId}/{year}/{quarter}/{showAll}", method = RequestMethod.GET)
	public String list(@PathVariable Long companyId, @PathVariable int year,
			@PathVariable int quarter, @PathVariable boolean showAll, Model model) {

		model.addAttribute("companyId", companyId);
		model.addAttribute("year", year);
		model.addAttribute("quarter", quarter);
		model.addAttribute("showAll", showAll);
		
		// siderail content:
		model.addAttribute("cdfiLogo", LogoHelper.getCorporateLogo(Company.findCompany(companyId)));
		model.addAttribute("companyName", Company.findCompany(companyId).getName());
		return "webform/list";
	}

	@RequestMapping(value = "/webform/getMetricValues/{companyId}/{year}/{quarter}/{showAll}", method = RequestMethod.GET)
	public @ResponseBody
	List<ParentCategory> getMetricValues(@PathVariable Long companyId,
			@PathVariable int year, @PathVariable int quarter, @PathVariable boolean showAll) {
		List<ParentCategory> result = new ArrayList<ParentCategory>();
		try {

			Company company = Company.findCompany(companyId);
			List<MetricValue> metricValues = new ArrayList<MetricValue>();
			
			List<Metric> requiredMetrics = company.getRequiredMetrics();
			List<MetricValue> existingMetricValues = MetricValue
					.findMetricValues(quarter, year, company.getId());
			HashSet<Metric> metricsWithValues = new HashSet<Metric>();
			
			for (MetricValue metricValue : existingMetricValues) {
				metricsWithValues.add(metricValue.getMetric());
			}

			if(showAll){
				
				metricValues.addAll(existingMetricValues);
			}
			
			for (Metric requiredMetric : requiredMetrics) {
				if (!metricsWithValues.contains(requiredMetric)) {
					MetricValue newMetricValue = new MetricValue();
					newMetricValue.setCompany(company);
					newMetricValue.setMetric(requiredMetric);
					newMetricValue.setQuarter(quarter);
					newMetricValue.setYear(year);
					newMetricValue.setVersion(1);
					metricValues.add(newMetricValue);
				}
			}

			for (MetricValue metricValue : metricValues) {

				ParentCategory currentParent = null;
				ChildCategory currentChild = null;

				MetricCategory childCategory = metricValue.getMetric()
						.getParent();
				MetricCategory parentCategory = childCategory.getParent();

				currentParent = findParent(result, parentCategory);

				// check if we've seen this parent before, if we haven't then
				// add it to the list.
				if (currentParent == null) {

					ParentCategory pc = new ParentCategory();
					pc.setName(parentCategory.getName());
					result.add(pc);
					currentParent = pc;

				}

				// check if we've seen this child before, if we haven't then add
				// it to the list
				currentChild = findChild(currentParent.getChildren(),
						childCategory);
				if (currentChild == null) {
					ChildCategory cc = new ChildCategory();
					cc.setName(childCategory.getName());
					currentParent.getChildren().add(cc);
					currentChild = cc;
				}

				MetricValuePOJO mv = new MetricValuePOJO();
				mv.setMetricId(metricValue.getMetric().getId());
				mv.setName(metricValue.getMetric().getName());
				MetricType metricType = null;;
				if (metricValue.getMetric().getType() != null) {
					metricType = metricValue.getMetric().getType();
				}
				mv.setType(metricType);
				currentChild.getMetricValues().add(mv);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/webform/save/{companyId}/{year}/{quarter}", method = RequestMethod.POST)
	public @ResponseBody
	Response save(@RequestBody MetricValuePOJO[] metricValues,
			@PathVariable Long companyId, @PathVariable int year,
			@PathVariable int quarter) {


		Company company = Company.findCompany(companyId);
		// since this is only called from editing it will always be an update.
		for (MetricValuePOJO metricValue : metricValues) {
			MetricValue foundMetric = MetricValue.findMetricValue(quarter, year, companyId, metricValue.getMetricId());
			if (foundMetric != null) {

				foundMetric.setTextValue(metricValue.getTextValue());
				foundMetric.setAmount(metricValue.getAmount());

				foundMetric.merge();

			}
			else{
				MetricValue newMetricValue = new MetricValue();
				newMetricValue.setAmount(metricValue.getAmount());
				newMetricValue.setCompany(company);
				newMetricValue.setMetric(Metric.findMetric(metricValue.getMetricId()));
				newMetricValue.setQuarter(quarter);
				newMetricValue.setYear(year);
				newMetricValue.setTextValue(metricValue.getTextValue());
				newMetricValue.setVersion(1);
				newMetricValue.merge();

			}
		}
		
		if (metricValues.length > 0) {
            String description = year + "Q" + quarter;    
            Person person = UserHelper.getCurrentUser();
            
            Activity activity = new Activity();
            activity.setActivityType(ActivityType.ADDITIONAL_DATA_SUBMISSION);
            activity.setCompany(company);
            activity.setDate(new Date());
            activity.setDescription(description);
            activity.setPerson(person);
            activity.setQuarter(quarter);
            activity.setYear(year);
            activity.persist();    
		}
		
		ReportDataCache.removeAndUpdateValueFacts(company);
		return new Response("");

	}

	private ParentCategory findParent(List<ParentCategory> parents,
			MetricCategory value) {

		ParentCategory result = null;

		for (ParentCategory parentCategory : parents) {

			if (parentCategory.getName() == value.getName()) {
				result = parentCategory;
			}
		}

		return result;
	}

	private ChildCategory findChild(List<ChildCategory> children,
			MetricCategory value) {

		ChildCategory result = null;

		for (ChildCategory childCategory : children) {

			if (childCategory.getName() == value.getName()) {
				result = childCategory;
			}
		}

		return result;
	}

	private boolean metricValueExists(List<MetricValue> values, Metric metric) {

		boolean result = false;

		for (MetricValue metricValue : values) {
			if (metricValue.getMetric().getId() == metric.getId()) {
				result = true;
				break;
			}
		}

		return result;

	}
}
