package com.cars.platform.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Message;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.PersonRole;
import com.cars.platform.domain.PersonWrapper;
import com.cars.platform.domain.Subscription;
import com.cars.platform.service.EmailService;
import com.cars.platform.util.PropertiesHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.MessagePOJO;
import com.cars.platform.viewmodels.PersonPOJO;
import com.cars.platform.viewmodels.Response;

@RequestMapping("/message/**")
@Controller
public class MessageController {

	@Autowired
	EmailService emailService;
	
	@Autowired
	PropertiesHelper propertiesHelper;


    @RequestMapping
    public String index(Model model) {
    	
    	Person currentUser = UserHelper.getCurrentUser();
    	if(currentUser == null){
			return "redirect:/";
		}
    	model.addAttribute("recipientId", currentUser.getId());
    	model.addAttribute("recipientName", currentUser.getFullName());
        return "message/index";
    }
    
    @RequestMapping(value = "/message/{senderId}", method = RequestMethod.GET)
    public String index2(Model model, @PathVariable long senderId) {
    	Person currentUser = UserHelper.getCurrentUser();
    	if(currentUser == null){
    		return "redirect:/";
    	}
    	model.addAttribute("recipientId", currentUser.getId());
        model.addAttribute("recipientName", currentUser.getFullName());
    	model.addAttribute("senderId", senderId);    		
    	return "message/index";
    	
    }
    
    @RequestMapping(value = "/message/getConversationBySender/{recipientId}/{senderId}", method = RequestMethod.GET)
	public @ResponseBody
	List<MessagePOJO> getConversationBySender(@PathVariable long senderId, @PathVariable long recipientId) { 
    	Person sender = Person.findPerson(senderId);
    	Person recipient = Person.findPerson(recipientId);
    	//get all messages this person has sent or received.
    	List<MessagePOJO> result = new ArrayList<MessagePOJO>();
    	List<Message> messages = Message.findAllConversations(recipient, sender);
    	
    	for (Message message : messages) {
			MessagePOJO mp = new MessagePOJO();
			Person messageSender = message.getSender();
			Person messageRecipient = message.getRecipient();
			
			mp.setRecipientCompanyId(messageRecipient.getCompany().getId());
			mp.setRecipientCompanyName(messageRecipient.getCompany().getName());
			mp.setRecipientFullName(messageRecipient.getFirstName() + " " + messageRecipient.getLastName());
			
			mp.setSenderCompanyId(messageSender.getCompany().getId());
			mp.setSenderCompanyName(messageSender.getCompany().getName());
			mp.setSenderFullName(messageSender.getFirstName() + " " + messageSender.getLastName() + " (" + messageSender.getCompany().getName() + ")");
			
			mp.setText(message.getText());
			mp.setSubject(message.getSubject());
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy h:mm a");
			String sentDate = df.format(message.getSent()) + " EST";
			mp.setSentDate(sentDate);
			
			result.add(mp);
		}
    	return result;
    }
    
    @RequestMapping(value = {"/message/getSenders/{personId}","/message/message/getSenders/{personId}"}, method = RequestMethod.GET)
	public @ResponseBody
	List<PersonPOJO> getSenders(@PathVariable long personId) {
    	
    	List<PersonWrapper> senders = Message.findAllSenders(Person.findPerson(personId));    	
    	List<PersonPOJO> result = new ArrayList<PersonPOJO>();
    	Set<Person> ops = new HashSet();
    	
    	Person currentUser = UserHelper.getCurrentUser();
    	    	 
    	if(currentUser.getCompany().isCDFI()){    		
    		ops.addAll(currentUser.getCompany().getAnalysts());
    	}
    	
    	// add admins
    	ops.addAll(Person.findPeopleByPersonRole(PersonRole.ADMIN).getResultList());    	
    	
    	//remove duplicate persons in messageList from the ops list
    	for(PersonWrapper personWrapper: senders){
    		Person person = personWrapper.getPerson();    		
    			ops.remove(person);    		
    	}
    	
    	// find only ops that are visible    	
    	for(Iterator<Person> i = ops.iterator();i.hasNext();){
    		Person p = i.next();
    		if((p.isAdmin() && !p.isPublicContact())){
    			i.remove();
    	   	}
    	}    	
    
    	ops.remove(currentUser); //sanity check
    	
    	for(Person person:ops){
    		senders.add(new PersonWrapper(person, new Date()));
    	}
    	
    	for (PersonWrapper personWrapper : senders) {			
    		PersonPOJO pp = new PersonPOJO();
    		Person person = personWrapper.getPerson();
    		pp.setCompanyId(person.getCompany().getId());
    		pp.setCompanyName(person.getCompany().getName());
    		pp.setFirstName(person.getFirstName());
    		pp.setLastName(person.getLastName());
    		pp.setFullName(person.getFirstName() + " " + person.getLastName() + " (" + person.getCompany().getName() + ")");
    		pp.setId(person.getId());
    		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String sentDate = df.format(personWrapper.getSentDate());
    		pp.setLastSentDate(sentDate);
    		
    		result.add(pp);
		}    	    
    	return result;
    }
    
    @RequestMapping(value = "/message/save", method = RequestMethod.POST)
	public @ResponseBody
	Response save(@RequestBody MessagePOJO message) {
    	Person recipient = Person.findPerson(message.getRecipientId());
    	Message newMessage = new Message();
    	newMessage.setRecipient(Person.findPerson(message.getRecipientId()));
    	newMessage.setSender(Person.findPerson(message.getSenderId()));
    	newMessage.setSent(new Date());
    	newMessage.setSubject(message.getSubject());
    	newMessage.setText(message.getText());
    	newMessage.merge();
    	
    	// send recipient an email alerting of new message in message center
    	String subject="Aeris Cloud Message Center.";
    	String url = propertiesHelper.getPlatformUrl();
    	String body = "You have a new message from " + newMessage.getSender().getFullName() + ". Please visit the <a href=\"" + url + "/message/" + message.getSenderId() + "\"> Message Center </a> to view the message.";
    	    	
    	emailService.sendEmail(recipient.getEmail(), subject, body, true);
    	
    	return new Response("");
    
    }
}
