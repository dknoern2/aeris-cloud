package com.cars.platform.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.domain.constraint.CheckReportCategoryParent;


@RooJavaBean
@RooToString
@RooJpaActiveRecord
@CheckReportCategoryParent
public class ReportCategory {

    @ManyToOne
    private ReportCategory parent;
    
    @OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="reportCategory")
    @OrderBy("rank ASC")
    private List<ReportCategoryEquation> categoryEquations;
    
    @Enumerated
    private ReportPage reportPage;

    private String name;

    private int rank;
    
    /**
     * Finds the  instances of ReportCategory for the provided report page and parent
     * @param reportPage
     * @param parent may be null
     * @return A list of ReportCategory instances sorted by rank.
     */
    public static List<ReportCategory> findByReportPageAndParent(ReportPage reportPage, ReportCategory parent) {
        if (reportPage == null) throw new IllegalArgumentException("The reportPage argument is required");
        
        EntityManager em = ReportCategory.entityManager();
        TypedQuery<ReportCategory> q;
        
        if (null == parent) {
        	q = em.createQuery("SELECT o FROM ReportCategory AS o WHERE o.parent IS NULL AND o.reportPage = :reportPage order by o.rank", ReportCategory.class);
        }
        else {
            q = em.createQuery("SELECT o FROM ReportCategory AS o WHERE o.parent = :parent AND o.reportPage = :reportPage order by o.rank", ReportCategory.class);
            q.setParameter("parent", parent);
        }
        
        q.setParameter("reportPage", reportPage);
        return q.getResultList();
    }
    
    
    /**
     * Provides a list of Equations for this report category and company sorted by rank
     * @param company
     * @param allEquation
     * @return All equations, both global and company specific for this category. If "allEquation" is true then it will return
     * all global equations and all for every company. Note that if a company has overridden
     * a global equation only the company override will be included.
     */
    public List<Equation> getEquations(Company company, boolean allEquations) {
        List<Equation> rawEquations = ReportCategoryEquation.findEquationsByReportCategoryAndCompany(this, company, allEquations);
        
        // If we are getting only global equations there are no overrides
        if (null == company) {
        	return rawEquations;
        }
        
        // Replace global equations which have company specific overrides
        List<Equation> result = new ArrayList<Equation>();
        for (Equation equation : rawEquations) {
        	Equation override = equation.getCompanyOverride(company);
        	if (null == override) {
        		result.add(equation);
        	}
        	else {
        		result.add(override);
        	}
        }
        return result;
    }
    
    
    /**
     * Updates the relationship to equations and their ordering as per the passed in, ordered list of equations
     */
    public void updateCategoryEquations(List<Equation> equations, Company company) {
    	validateUpdateEquations(equations, company);
    	
    	if (null == company) {
    		updateGlobalCategoryEquations(equations);
    		return;
    	}
    	
    	//List<ReportCategoryEquation> newRces = new ArrayList<ReportCategoryEquation>(getCategoryEquations());
    	List<ReportCategoryEquation> rces = getCategoryEquations();
    	
    	// first remove equations that are not in the new list, leaving other company equations in tact
    	Iterator<ReportCategoryEquation> iterator = rces.iterator();
    	while (iterator.hasNext()) {
    		Equation equation = iterator.next().getEquation();
    		Company equationCompany = equation.getCompany();
    		if (null == equationCompany || equationCompany.equals(company)) {
	    	    if (!equations.contains(equation)) {
	    	    	//System.out.println("Removing: " + equation.getName());
	    	    	iterator.remove();
	    	    }
    		}
    	}
    	
    	// compare the new and existing list of global and company equations, adding and moving as necessary
    	boolean done = false;
    	while (!done) {
	    	List<EquationIndex> currentList = getCurrentIndexes(rces, company);
	    	int lastIndex = -1;
	    	int i;
	    	for (i=0; i<equations.size(); i++) {
	    		Equation equation = equations.get(i);
	    		//System.out.println("i: " + i + " " + equation.getName());
	    		int j;
	    		for (j=0; j<currentList.size(); j++) {
	    			if (equation.equals(currentList.get(j).equation)) {
	    	    		//System.out.println("j: " + j + " " + currentList.get(j).equation.getName());
	    				break;
	    			}
	    		}
    			// new equation, insert after previous one in the list lastIndex+1
	    		if (j == currentList.size()) {
	    			ReportCategoryEquation newRce = new ReportCategoryEquation(this, equation, -1);
	    			rces.add(lastIndex+1, newRce);
	    	    	//System.out.println("Adding: " + equation.getName());
	    			break;
	    		}
	    		// moved equation
	    		if (j>i) {
/* previous code
	    			ReportCategoryEquation movedRce = rces.remove(currentList.get(j).index);
	    			rces.add(lastIndex+1, movedRce);
	    	    	System.out.println("Moving up: " + equation.getName());
*/
	    			// company specific has moved up (lower rank value)
	    			// or global equation is moving
		    	    if (equation.getCompany() != null || company == null) {
		    			ReportCategoryEquation movedRce = rces.remove(currentList.get(j).index);
		    			rces.add(lastIndex+1, movedRce);
		    	    	//System.out.println("Moving up: " + equation.getName());
	    			}
	    			// company specific has moved down (higher rank value)
	    			else {
		    			ReportCategoryEquation movedRce = rces.remove(currentList.get(j-1).index);
		    			//rces.add(lastIndex+1, movedRce);
		    	    	//System.out.println("Moving down: " + equation.getName());	    				
	    			}
	    			break;
	    		}
	    		if (j<i) {
	    			//System.out.println("ugh: i: " + equation.getName() + " j: " + currentList.get(j).equation.getName());
	    			throw new IllegalArgumentException("Algorithim failure, This is only known to happen with duplicate equation IDs in the request list");
	    		}
	    		
	    		// i == j, save the index of the last matched equation and iterate
	    		lastIndex = currentList.get(j).index; 
	    	}
	    	if (i == equations.size()) {
	    		done = true;
	    	}
    	}
    	
    	// for all equations set the rank to the new values
    	for (int i=0; i<rces.size(); i++) {
    		ReportCategoryEquation rce = rces.get(i);
    		rce.setRank(i);
    	}
    }
    
    
    /**
     * Updates the global equations for this category.
     * @param equations contains the new global equations and all of the CDFI equations unchanged
     */
    private void updateGlobalCategoryEquations(List<Equation> equations) {
    	//List<ReportCategoryEquation> newRces = new ArrayList<ReportCategoryEquation>(getCategoryEquations());
    	List<ReportCategoryEquation> rces = getCategoryEquations();
    	rces.clear();
    	for (Equation equation : equations) {
			ReportCategoryEquation newRce = new ReportCategoryEquation(this, equation, -1);
			rces.add(newRce);
    	}
    	    	
    	// for all equations set the rank to the new values
    	for (int i=0; i<rces.size(); i++) {
    		ReportCategoryEquation rce = rces.get(i);
    		rce.setRank(i);
    	}
    }

    

    private List<EquationIndex> getCurrentIndexes(List<ReportCategoryEquation> rces, Company company) {
    	List<EquationIndex> current = new ArrayList<EquationIndex>();
    	for (int i=0; i<rces.size(); i++) {
    		Equation equation = rces.get(i).getEquation();
    		Company equationCompany = equation.getCompany();
    		if (null == equationCompany || equationCompany.equals(company)) {
	    		EquationIndex equationRank = new EquationIndex(equation, i);
	    		current.add(equationRank);
    		}
    	}
    	return current;
    }

    
    private void validateUpdateEquations(List<Equation> equations, Company company) {
    	List<Equation> globalEquations = new ArrayList<Equation>();
    	List<Equation> cdfiEquations = new ArrayList<Equation>();
    	for (Equation equation : equations) {
    		// get the list of global and cdfi equations for check at end of this method 
    		if (null == equation.getCompany()) {
    			globalEquations.add(equation);
    		}
    		else {
    			cdfiEquations.add(equation);
    		}
    		
    		Company company2 = equation.getCompany();
// changed approach to send in all equations when editing global
//    		if (company == null && company2 != null) {
//    			throw new IllegalArgumentException("Attempting to update report category equations for the global company passing in CDFI specific equations");
//    		}
    		if (company != null && !(company2 == null || company.equals(company2))) {
    			throw new IllegalArgumentException("Attempting to update report category equations for company A passing in equations from company B");	
    		}
    	}
    	
    	// capture the list of existing global and cdfi equations
    	List<ReportCategoryEquation> rces = getCategoryEquations();
    	List<Equation> existingGlobalEquations = new ArrayList<Equation>();
    	List<Equation> existingCdfiEquations = new ArrayList<Equation>();
    	for (ReportCategoryEquation rce : rces) {
    		Equation equation = rce.getEquation();
    		if (null == equation.getCompany()) {
    			existingGlobalEquations.add(equation);
     		}
    		else {
    			existingCdfiEquations.add(equation);
    		}
    	}
    	
    	// ensure that existing global or CDFI equations are send in unchanged
    	if (null != company) {
        	if (!equals(globalEquations, existingGlobalEquations, company)) {
    			throw new IllegalArgumentException("When modifying company equations all of the existing global equations must be passed in unchanged.");	       				        		
        	}
    	}
    	else {
        	if (!equals(cdfiEquations, existingCdfiEquations, company)) {
    			throw new IllegalArgumentException("When modifying global equations all of the existing CDFI equations must be passed in unchanged.");	       				        		
        	}   		
    	}

   }
    
    private boolean equals(List<Equation> list, List<Equation> existingList, Company company) {
    	if (list.size() != existingList.size()) {
    		return false;
    	}
    	
    	for (int i=0; i<list.size(); i++) {
    		Equation equation = list.get(i);
    		Equation existingEquation = existingList.get(i);
    		if (company != null && existingEquation.getOverride() != null) {
    			existingEquation = existingEquation.getOverride();
    		}
    		
    		if (!equation.equals(existingEquation)) {
    			return false;
    		}
    	}
    	
    	return true;
    }


	public static List<ReportCategory> findAllReportCategorys() {
        return entityManager().createQuery("SELECT o FROM ReportCategory o order by o.reportPage, o.rank", ReportCategory.class).getResultList();
    }
	
    public static List<ReportCategory> findReportCategoryEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM ReportCategory o order by o.reportPage, o.rank", ReportCategory.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    /**
     * Returns all ReportCategories structured by 1) ReportPage, 2) category with null parent sorted by rank, 2a) child categories sorted by rank
     * @return
     */
    public static List<ReportCategory> findReportCategoriesHierarchy() {
        List<ReportCategory> rootCategories = entityManager().createQuery("SELECT o FROM ReportCategory o where o.parent is null order by o.reportPage, o.rank", ReportCategory.class).getResultList();
        List<ReportCategory> allCategories = new ArrayList<ReportCategory>();
        for (ReportCategory rc : rootCategories) {
        	allCategories.add(rc);
        	List<ReportCategory> children = ReportCategory.findByReportPageAndParent(rc.getReportPage(), rc);
        	allCategories.addAll(children);
        }
        return allCategories;
    }



	public static ReportCategory findReportCategory(Long id) {
        if (id == null) return null;
        return entityManager().find(ReportCategory.class, id);
    }
	
	private static class EquationIndex {
		private Equation equation;
		private int index;
		
		public EquationIndex(Equation equation, int index) {
			this.equation = equation;
			this.index = index;
		}
	}

	public void setRank(int rank) {
        this.rank = rank;
    }
	
	// updates the rank of sibling ReportCategory if this has been inserted (set to same rank) as one of its siblings
	public void updateRank(int newRank) {
		// siblings are already sorted by rank
		List<ReportCategory> siblings = ReportCategory.findByReportPageAndParent(this.getReportPage(), this.getParent());
		
		this.setRank(newRank);
		
		boolean found = false;
		for (ReportCategory sibling : siblings) {
			if (sibling.getRank() == this.getRank() && sibling.getId() != this.getId()) {
				found = true;
			}
			if (found && sibling.getId() != this.getId()) {
				sibling.setRank(sibling.getRank()+1);
				sibling.merge();
			}
		}
	}

}