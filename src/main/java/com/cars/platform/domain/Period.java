package com.cars.platform.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.util.PeriodHelper;
import com.cars.platform.util.PropertiesHelper;
import com.cars.platform.util.UserHelper;
//import javax.persistence.Table;
//import javax.persistence.UniqueConstraint;

//@Table(name="period", uniqueConstraints={
//		@UniqueConstraint(name="uk_company_period", columnNames={"company", "endDate"}),
//		@UniqueConstraint(name="uk_company_period2", columnNames={"company", "year","quarter"})})

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findPeriodsByEndDateEquals", "findPeriodsByCompany" })
public class Period implements Comparable<Period>  {

    public static final int LAST_QUARTER = 4;
    public static final int MONTHS_IN_QUARTER = 3;
    public static final int QUARTERLY_DATA_COMPARABLE_YEAR = 2001;
    
    @Autowired
    @Transient
    private PropertiesHelper propertiesHelper;

    @ManyToOne
    private Company company;

    private int year;

    private int quarter;

    private String endDate;
    
    // The date the financials for this period were finalized
    private Date finalizeDate;
    
    private Date auditDate;


    @NotNull
    @Enumerated
    private PeriodType periodType = PeriodType.DRAFT;

    public static boolean isDraftColumnsVisible(Person person) {
    	if (null == person) {
    		return false;
    	}
	    return !person.isSubscriber();
    }
    
    public boolean isFiscalYearEnd() {
    	return quarter >= LAST_QUARTER;
    }
    
    public int quarterlyDataAvailableYear() {
    	return Integer.parseInt(propertiesHelper.getProperty("quarterlyDataComparableYear", "2014"));
    }
    
    /**
     * Returns whether or not this period is comparable within a peer group report. Quarterly data was not consistently
     * captured until until 2014
     * @return
     */
    public boolean isComparable(boolean showInterim) {
    	if (quarter > LAST_QUARTER) { // for cases where fiscal year end moved we only compare the first one (quarter == 4)
    		return false;
    	}
    	
    	if (quarter == LAST_QUARTER) {
    		return true;
    	}
    	
    	if (!showInterim) {
    		return false;
    	}
    	
    	if (year >= quarterlyDataAvailableYear()) {
    		return true;
    	}
    	
    	return false;
    }

    
	public void setPeriodType(PeriodType periodType) {
		if ((periodType == PeriodType.AUDIT || periodType == PeriodType.INTERIM) && this.periodType == PeriodType.DRAFT) {
			this.finalizeDate = new Date();
		}
		if ((periodType == PeriodType.AUDIT) && (this.periodType == PeriodType.INTERNAL || this.periodType == PeriodType.INTERIM)) {
			this.auditDate = new Date();
		}
        this.periodType = periodType;
    }
	
    // If this messes with your unit tests using AnnotationDrivenStaticEntityMockingControl use PeriodHelper instead
    // STS refactor-->move didn't work so left this method here
    public static Period getMostRecent(Collection<Period> periods) {
    	return PeriodHelper.getMostRecent(periods);
    }

    public static List<Period> findPeriodsFinalizedSinceDate(Date date) {
        if (date == null) throw new IllegalArgumentException("The date argument is required");
        EntityManager em = entityManager();
        TypedQuery<Period> q = em.createQuery("SELECT o FROM Period AS o WHERE o.finalizeDate > :date", Period.class);
        q.setParameter("date", date);
        return q.getResultList();
    }
    
    public static List<Period> findPeriodsAuditedSinceDate(Date date) {
        if (date == null) throw new IllegalArgumentException("The date argument is required");
        EntityManager em = entityManager();
        TypedQuery<Period> q = em.createQuery("SELECT o FROM Period AS o WHERE o.auditDate > :date", Period.class);
        q.setParameter("date", date);
        return q.getResultList();
    }
    
    public static Period findPeriod(Company company, String endDate) {
    	Period period = null;
    	if (company == null)
    		throw new IllegalArgumentException("The company argument is required");
    	if (endDate == null || endDate.length() == 0)
    		throw new IllegalArgumentException("The endDate argument is required");
    	EntityManager em = Period.entityManager();
    	TypedQuery<Period> q = em.createQuery(
    			"SELECT o FROM Period AS o WHERE o.company = :company and o.endDate = :endDate", Period.class);
    	q.setParameter("company", company);
    	q.setParameter("endDate", endDate);
    	try {
    		period = q.getSingleResult();
    	} catch (Exception ignore) {
    	}
    	return period;
    }

    public static Period findPeriod(Company company, int year, int quarter) {
    	Period period = null;
    	if (company == null)
    		throw new IllegalArgumentException("The company argument is required");
    	EntityManager em = Period.entityManager();
    	TypedQuery<Period> q = em.createQuery(
    			"SELECT o FROM Period AS o WHERE o.company = :company and o.year = :year and o.quarter = :quarter",
    			Period.class);
    	q.setParameter("company", company);
    	q.setParameter("year", year);
    	q.setParameter("quarter", quarter);
    	try {
    		period = q.getSingleResult();
    	} catch (Exception ignore) {
    	}
    	return period;
    }

    public static SortedMap<String, Period> findPeriodMapByCompany(Company company) {
    	if (company == null)
    		throw new IllegalArgumentException("The company argument is required");
    	EntityManager em = Period.entityManager();
    	TypedQuery<Period> q = em.createQuery("SELECT o FROM Period AS o WHERE o.company = :company", Period.class);
    	q.setParameter("company", company);
    	List<Period> list = q.getResultList();
    	SortedMap<String, Period> map = new TreeMap<String, Period>();
    	final Person currentUser = UserHelper.getCurrentUser();
    	for (Period period : list) {
    		// The only user that should not see draft financials is the
    		// Subscriber Users.
    		if (PeriodType.DRAFT.equals(period.getPeriodType()) && !isDraftColumnsVisible(currentUser)) {
    			continue;
    		}
    		String key = Integer.toString(period.getYear()) + "Q" + Integer.toString(period.getQuarter());
    		map.put(key, period);
    	}
    	return map;
    }

    public static List<Period> findAllPeriods() {
    	return entityManager().createQuery("SELECT o FROM Period o order by o.company.name, o.year, o.quarter",
    			Period.class).getResultList();
    }



    /**
     * 
     * @param company
     * @param year
     * @param quarter
     * @return list of periods that go after given year and quarter for the
     *         given company.
     */
    public static List<Period> findAfterPeriodsByCompany(Company company, int year, int quarter) {
    	if (company == null) {
    		throw new IllegalArgumentException("The company argument is required");
    	}
    	EntityManager em = entityManager();
    	TypedQuery<Period> q = em
    			.createQuery(
    					"SELECT o FROM Period AS o WHERE o.company = :company AND ((o.year > :year) OR (o.year = :year AND o.quarter >= :quarter))",
    					Period.class);
    	q.setParameter("company", company);
    	q.setParameter("year", year);
    	q.setParameter("quarter", quarter);

    	return q.getResultList();
    }

    /**
     * 
     * @param company
     * @param year
     * @param quarter
     * @return list of periods that go before given year and quarter for the
     *         given company.
     */
    public static List<Period> findOlderPeriodsByCompany(Company company, int year, int quarter) {
    	if (company == null) {
    		throw new IllegalArgumentException("The company argument is required");
    	}
    	EntityManager em = entityManager();
    	TypedQuery<Period> q = em
    			.createQuery(
    					"SELECT o FROM Period AS o WHERE o.company = :company AND ((o.year < :year) OR (o.year = :year AND o.quarter < :quarter)) ORDER BY o.year DESC, o.quarter DESC",
    					Period.class);
    	q.setParameter("company", company);
    	q.setParameter("year", year);
    	q.setParameter("quarter", quarter);

    	return q.getResultList();
    }


    /**
     * Returns all the periods for a company for which an Audit (final report) has been created
     * @param company
     * @return list of Periods sorted by year/quarter ascending
     */
    public static List<Period> findAuditOrInternalPeriodsForCompany(Company company) {
    	if (company == null) {
    		throw new IllegalArgumentException("The company argument is required");
    	}
    	EntityManager em = entityManager();
    	TypedQuery<Period> q = em
    			.createQuery(
    					"SELECT o FROM Period AS o WHERE o.company = :company AND (o.periodType = :periodType1 or o.periodType = :periodType2) ORDER BY o.year ASC, o.quarter ASC",
    					Period.class);
    	q.setParameter("company", company);
    	q.setParameter("periodType1", PeriodType.AUDIT);
    	q.setParameter("periodType2", PeriodType.INTERNAL);
    	return q.getResultList();
    }
    
    
    public static Period findMostRecentAuditOrInternalPeriod(Company company) {
    	List<Period> auditPeriods = findAuditOrInternalPeriodsForCompany(company);
    	if (null == auditPeriods || auditPeriods.size() == 0) {
    		return null;
    	}
    	return getMostRecent(auditPeriods);
    }

    
    public static List<Period> findPeriodsByCompanySorted(Company company) {
		if (company == null)
		    throw new IllegalArgumentException("The company argument is required");
		EntityManager em = entityManager();
		TypedQuery<Period> q = em.createQuery(
			"SELECT o FROM Period AS o WHERE o.company = :company order by year, quarter", Period.class);
		q.setParameter("company", company);
		return q.getResultList();
    }
    
    
   
    public static List<Period> findViewablePeriodsByCompanySorted(Company company) {
    	List<Period> allPeriods = findPeriodsByCompanySorted(company);
    	List<Period> viewablePeriods = new ArrayList<Period>();
		final Person currentUser = UserHelper.getCurrentUser();
    	for (Period period : allPeriods) {
		    // The only user that should not see draft financials is the
		    // Subscriber Users.
		    if (PeriodType.DRAFT.equals(period.getPeriodType()) && !isDraftColumnsVisible(currentUser)) {
			    continue;
		    }
		    viewablePeriods.add(period);    		
    	}
    	return viewablePeriods;
    }
    
    /**
     * Returns the newest of this and the passed in period
     * @param otherPeriod
     */
    public Period newest(Period otherPeriod) {
    	int compare = compareTo(otherPeriod);
    	
    	if (compare >= 0) {
    		return this;
    	}
    	else {
    		return otherPeriod;
    	}
     }

	@Override
	public int compareTo(Period o) {
		if (null == o) {
			return 1;
		}
		
    	if (this.getYear() > o.getYear()) {
    		return 1;
    	}
    	else if (this.getYear() < o.getYear()) {
    		return -1;
    	}
    	else if (this.getQuarter() > o.getQuarter()) {
    		return 1;
    	}
    	else if (this.getQuarter() < o.getQuarter()) {
    		return -1;
    	}
    	else {
    		return 0;
    	}
	}


}
