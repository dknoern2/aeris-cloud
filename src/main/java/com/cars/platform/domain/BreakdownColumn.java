package com.cars.platform.domain;
import java.util.Map;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.domain.constraint.CheckBreakdownColumn;
import com.cars.platform.service.graph.breakdown.CellValue;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@CheckBreakdownColumn
public class BreakdownColumn implements IEquationFormat, IGetValueString {
    private static final Logger logger = LoggerFactory.getLogger(BreakdownColumn.class);
	
	@NotNull
	private int rank;
	
	@NotNull
	private String name;
	
	@NotNull
	private String equation;
	
	private String totalEquation;
	
	private boolean showTotal;
	
	private UnitType unitType = UnitType.NUMBER;
	
	private Integer decimalPlaces = 0;

	
	@NotNull
	@ManyToOne
	private MetricBreakdown breakdown;
	
	// The value shown in the selection drop down for the graphs
	public String getSelectName() {
		return breakdown.getName() + ": " + name;
	}
	

	/**
	 * Calculates the value for a cell in a breakdown graph. Moved here from the Graph service to be leveraged by validation at time of BreakdownColumn creation
	 */
	public CellValue caclulateCellValue(MetricBreakdownItem mbi, MetricBreakdownItem summationItem,
			Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem,
			IGetValueString getValue, boolean getTotal)
	{
		Map<String, MetricValue> valuesByCode = valueByCodeByItem.get(mbi);
		Map<String, MetricValue> sumsByCode = valueByCodeByItem.get(summationItem);
		
		MetricBreakdown breakdown = mbi.getMetricBreakdown();
		
		// By default the equation for total is the sum 
		String equation;
		if (getTotal) {
			equation = StringUtils.isEmpty(this.getTotalEquation()) ? this.getEquation() : this.getTotalEquation();
		}
		else {
			equation = this.getEquation();
		}
		
		equation = equation.replaceAll(" ", "");
		equation = equation.toUpperCase();
		String[] x = equation.split("[\\+\\-\\*\\/\\(\\)]+");

		Double result = null;

	    try {
		    MetricValue metricValue = null;
			for (String token : x) {
				if (token.isEmpty()) {
					continue;
				}
				if (!token.endsWith("!")) {
					try {
						Double.parseDouble(token);
					}
					catch (NumberFormatException e) {
				    	throw new EvaluationException("Expected a numeric value for: " + token);
					}
					continue;
				}
			    double value = 0;
			    String code = null;
			    
			    if (token.startsWith("SUM")) {
			    	code = token.substring(3, token.length()-1);
			    	metricValue = sumsByCode.get(code);
				    if (null == metricValue) {
				    	throw new EvaluationException("There is no metric with code: " + code + " associated to the breakdown: " + breakdown.getName());
				    }
			    } else {
			    	code = token.substring(0, token.length()-1);
			    	metricValue = valuesByCode.get(code);
				    if (null == metricValue) {
				    	throw new EvaluationException("There is no metric with code: " + code + " associated to the breakdown: " + breakdown.getName());
				    }
			    }
			    
			    if (metricValue.getMetric().getType() == MetricType.TEXT) {
			    	if (!token.equals(equation)) {
			    		throw new EvaluationException("Metric with code [" + code + "] is of type text and can not be part of complex equation");
			    	}
			    	else {
			    		return new CellValue(null, metricValue.getTextValue());
			    	}
			    }
			    else {
			    	if (metricValue.getAmountValue() != null) {
			    		value = metricValue.getAmountValue();
			    	}
			    	equation = equation.replaceFirst(token, Double.toString(value));
			    }
			}
			
			Evaluator evaluator = new Evaluator();
	
			result = evaluator.getNumberResult(equation);
	    }
	    catch (EvaluationException e) {
		    logger.error("can't evaluate: " + equation);
		    return new CellValue(e);
		}
	    
	    String value = getValue.getValueString(result, this);
	    
	    return new CellValue(result, value);
	}


	@Override
	public String getValueString(double value, IEquationFormat equation) {
		return "1.0";
	}

	
}
