package com.cars.platform.domain;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.domain.constraint.CheckBreakdownSingle;

/**
 * A breakdown chart/graph which contains data for only a single equation (column) 
 * @author kurtl
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
@CheckBreakdownSingle
public abstract class BreakdownSingle extends BreakdownGraph {
	
	@OneToOne()
	@NotNull
	private BreakdownColumn equation;
}
