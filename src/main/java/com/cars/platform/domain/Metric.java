package com.cars.platform.domain;

import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findMetricsByAccountCodeEquals", "findMetricsByParent" })
public class Metric implements Comparable<Metric> {

    public static final Character HARD_STOP = '!';

    private String accountCode;

    @ManyToOne //(fetch = FetchType.LAZY)
    private MetricCategory parent;

    private String name;

    @ManyToOne //(fetch = FetchType.LAZY)
    private Company company;

    @NotNull
    @Enumerated
    private MetricType type = MetricType.NUMERIC;

    @NotNull
    private long rank;
    
    // TODO validate the breakdown only allowed for global metrics?
    // optional for cases where a metric is broken down... sub-categorized into MetricBreakdownItems
    @ManyToOne
    private MetricBreakdown breakdown;

    public String getFullAccountCode() {
	String fullAccountCode = "";
	if (parent != null && parent.getFullAccountCode() != null) {
	    fullAccountCode += parent.getFullAccountCode();
	}
	if (this.accountCode.length() == 1) {
	    fullAccountCode += "0";
	}
	fullAccountCode += this.accountCode;
	return fullAccountCode;
    }

    public static TypedQuery<com.cars.platform.domain.Metric> findMetric(String accountCode, Long parentId) {
	if (accountCode == null || accountCode.length() == 0)
	    throw new IllegalArgumentException("The accountCode argument is required");
	if (parentId == null)
	    throw new IllegalArgumentException("The parentId argument is required");
	EntityManager em = Metric.entityManager();
	TypedQuery<Metric> q = em.createQuery(
		"SELECT o FROM Metric AS o WHERE o.accountCode = :accountCode AND parent.id = :parentId", Metric.class);
	q.setParameter("accountCode", accountCode);
	q.setParameter("parentId", parentId);
	return q;
    }

    public static TypedQuery<com.cars.platform.domain.Metric> findMetricByFullAccountCode(String accountCode) {
	if (accountCode == null || accountCode.length() == 0)
	    throw new IllegalArgumentException("The accountCode argument is required");
	String grandparentAccountCode = accountCode.substring(0, 2);
	String parentAccountCode = accountCode.substring(2, 4);
	char last = accountCode.charAt(accountCode.length() - 1);
	String metricAccountCode;
	if (last == 'P' || last == '#' || last == HARD_STOP) {
	    metricAccountCode = accountCode.substring(4, accountCode.length() - 1);
	} else {
	    metricAccountCode = accountCode.substring(4, accountCode.length());
	}
	if (metricAccountCode.startsWith("0")) {
	    metricAccountCode = metricAccountCode.substring(1);
	}
	EntityManager em = Metric.entityManager();
	TypedQuery<Metric> q = em
		.createQuery(
			"SELECT o FROM Metric AS o, MetricCategory AS mc WHERE o.accountCode = :metricAccountCode AND o.parent.id = mc.id AND mc.accountCode =:parentAccountCode AND mc.parent.accountCode =:grandparentAccountCode",
			Metric.class);
	q.setParameter("metricAccountCode", metricAccountCode);
	q.setParameter("parentAccountCode", parentAccountCode);
	q.setParameter("grandparentAccountCode", grandparentAccountCode);
	return q;
    }
    
    public static Metric findMetricByFullAccountCode(String accountCode, Company company) {
	if (accountCode == null || accountCode.length() == 0)
	    throw new IllegalArgumentException("The accountCode argument is required");
	String grandparentAccountCode = accountCode.substring(0, 2);
	String parentAccountCode = accountCode.substring(2, 4);
	char last = accountCode.charAt(accountCode.length() - 1);
	String metricAccountCode;
	if (last == 'P' || last == '#' || last == HARD_STOP) {
	    metricAccountCode = accountCode.substring(4, accountCode.length() - 1);
	} else {
	    metricAccountCode = accountCode.substring(4, accountCode.length());
	}
	if (metricAccountCode.startsWith("0")) {
	    metricAccountCode = metricAccountCode.substring(1);
	}
	EntityManager em = Metric.entityManager();
	TypedQuery<Metric> q = em
		.createQuery(
			"SELECT o FROM Metric AS o, MetricCategory AS mc WHERE o.accountCode = :metricAccountCode AND o.parent.id = mc.id AND mc.accountCode =:parentAccountCode AND mc.parent.accountCode =:grandparentAccountCode AND (o.company =:company OR o.company.id = NULL)",
			Metric.class);
	q.setParameter("metricAccountCode", metricAccountCode);
	q.setParameter("parentAccountCode", parentAccountCode);
	q.setParameter("grandparentAccountCode", grandparentAccountCode);
	q.setParameter("company", company);
	
	
	final List<Metric> results = q.getResultList();
	if ((results == null) || (results.isEmpty())) {
		return null;
	}
	
	//prefer metric with company != null
	for (Metric metric : results) {
		if (metric.getCompany() != null) {
			return metric;
		}
	}
	
	return results.get(0);
    }

    public static int findMaxAccountCode(Long categoryId) {
	int result = 0;
	try {
	    if (categoryId == null)
		throw new IllegalArgumentException("The categoryId argument is required");
	    EntityManager em = Metric.entityManager();
	    Query q = em
		    .createQuery("select max(cast(m.accountCode, integer)) FROM Metric m WHERE m.parent.id = :categoryId");
	    q.setParameter("categoryId", categoryId);
	    Object maxCode = q.getSingleResult();
	    result = Integer.parseInt(maxCode.toString());
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return result;
    }

    public static List<com.cars.platform.domain.Metric> findAllCompanyMetrics(long companyId) {
	EntityManager em = Metric.entityManager();
	TypedQuery<Metric> q = em.createQuery("SELECT o FROM Metric o WHERE o.company.id = :companyId OR o.company.id = NULL",
		Metric.class);
	q.setParameter("companyId", companyId);
	return q.getResultList();
    }
    
    @Transactional
    public static void removeMetric(Metric metric){
    	//remove metric values
    	MetricValue.removeMetricValuesFromMetric(metric);
    	//remove company required metrics
    	Company company = metric.getCompany();
    	if (null != company) {
	    	List<Metric> requiredMetrics = company.getRequiredMetrics();
	    	for(Metric requiredMetric : requiredMetrics)
	    	{
	    		if (requiredMetric.getId() == metric.getId())
	    		{
	    			List<Metric> newRequiredMetrics = requiredMetrics;
	    			newRequiredMetrics.remove(requiredMetric);
	    			company.setRequiredMetrics(newRequiredMetrics);
	    			break;
	    		}
	    	}
    	}
    	//remove metric maps
    	MetricMap.removeMetricMapsFromMetric(metric);
    	//remove metric
    	metric.remove();
    }

    public static List<com.cars.platform.domain.Metric> findCompanyOnlyMetrics(long companyId) {
	List<Metric> metrics = null;
	EntityManager em = Metric.entityManager();
	try {
		TypedQuery<Metric> q = em
		    .createQuery(
			    "SELECT o FROM Metric o WHERE o.company.id = :companyId order by o.parent, cast(o.accountCode, integer)",
			    Metric.class);
	    q.setParameter("companyId", companyId);
	    metrics = q.getResultList();
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return metrics;
    }

    public static List<com.cars.platform.domain.Metric> findCommonMetrics() {
	EntityManager em = Metric.entityManager();
	TypedQuery<Metric> q = em.createQuery("SELECT o FROM Metric o WHERE o.company.id = NULL", Metric.class);
	return q.getResultList();
    }

    public static List<com.cars.platform.domain.Metric> findAllMetrics() {
	return entityManager().createQuery("SELECT o FROM Metric o order by o.parent, cast(o.accountCode, integer)",
		Metric.class).getResultList();
    }

    public static List<com.cars.platform.domain.Metric> findMetricsByRank(List<Long> rank) {
	EntityManager em = Metric.entityManager();
	TypedQuery<Metric> q = em.createQuery("SELECT o FROM Metric o WHERE o.rank IN (:rank) ORDER BY o.rank", Metric.class);
	q.setParameter("rank", rank);

	return q.getResultList();
    }
    
    /**
     * finds all metrics with an association to a MetricBreakdown
     * @return a list of metrics
     *      
     **/
    public static List<Metric> findBreakdownMetrics() {
		EntityManager em = Metric.entityManager();
		TypedQuery<Metric> q = em.createQuery("SELECT o FROM Metric o WHERE o.breakdown is not null", Metric.class);
		return q.getResultList();
	}
    
    public static List<Metric> findMetricsForBreakdown(MetricBreakdown breakdown) {
		EntityManager em = Metric.entityManager();
		TypedQuery<Metric> q = em.createQuery("SELECT o FROM Metric o WHERE o.breakdown = :breakdown", Metric.class);
	    q.setParameter("breakdown", breakdown);
		return q.getResultList();
	}


    public static Comparator<Metric> sortByRank = new Comparator<Metric>() {
	public int compare(Metric metric1, Metric metric2) {
	    if (metric1.getRank() == metric2.getRank()) {
		return 0;
	    } else if (metric1.getRank() <= metric2.getRank()) {
		return -1;
	    }
	    return 1;
	}
    };

	@Override
	public int compareTo(Metric o) {
		int fullCode = Integer.parseInt(this.getFullAccountCode());
		int otherFullCode = Integer.parseInt(o.getFullAccountCode());
		if (fullCode > otherFullCode) {
			return 1;
		}
		else if (fullCode < otherFullCode) {
			return -1;
		}
		else {
			return 0;
		}
	}
}
