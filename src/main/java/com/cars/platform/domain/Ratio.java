package com.cars.platform.domain;

public class Ratio {

	private String title;
	private String formula;
	private boolean currency;
	
	public String getTitle() {
		return title;
	}
	public boolean isCurrency() {
		return currency;
	}
	public void setCurrency(boolean currency) {
		this.currency = currency;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFormula() {
		return formula;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
	
	public Ratio(String title,String formula){
		this.title = title;
		this.formula = formula;
		this.currency = false;
	}
	public Ratio(String title,String formula,boolean currency){
		this.title = title;
		this.formula = formula;
		this.currency = currency;
	}	
	
	
}
