package com.cars.platform.domain;

public enum ShareFinancials {
	NONE("None"), ALL("All"), SPECIFIC("Specific");
	
	private final String value;
    
    private ShareFinancials(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    public String toString() {
    	return value;
    }
}
