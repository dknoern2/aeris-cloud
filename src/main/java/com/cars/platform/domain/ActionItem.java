package com.cars.platform.domain;

import java.util.List;

public class ActionItem {
	
	public static final int TASK_TYPE_UPLOAD_QUARTERLY = 1;
	public static final int TASK_TYPE_MAP_QUARTERLY_FINANCIALS = 3;
	public static final int TASK_TYPE_ANNUAL_REVIEW_DOCUMENTS = 4;
	public static final int TASK_TYPE_UPLOAD_HISTORIC_FINANCIALS = 5; // TODO unused so remove?
	public static final int TASK_TYPE_ENTER_WEBFORM = 6;
	public static final int TASK_TYPE_FULL_ANALYSIS_DOCUMENTS = 7;
	
	private String title;
	private String description;
	private int taskType;
	private int percentComplete;
	private int daysUntilDue;
	private int year;
	private int quarter;
	private List<DocumentType> documentTypes;
	
	public List<DocumentType> getDocumentTypes() {
		return documentTypes;
	}
	public void setDocumentTypes(List<DocumentType> documentTypes) {
		this.documentTypes = documentTypes;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPercentComplete() {
		return percentComplete;
	}
	public void setPercentComplete(int percentComplete) {
		this.percentComplete = percentComplete;
	}
	public int getDaysUntilDue() {
		return daysUntilDue;
	}
	public void setDaysUntilDue(int daysUntilDue) {
		this.daysUntilDue = daysUntilDue;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getQuarter() {
		return quarter;
	}
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}
	public int getTaskType() {
		return taskType;
	}
	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

}
