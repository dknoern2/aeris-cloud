package com.cars.platform.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findMetricCategorysByParent" })
public class MetricCategory {

    @ManyToOne
    private MetricCategory parent;

    private String name;

    private String accountCode;

    public String getName() {
        return name;
    }
    
    public static List<MetricCategory> findParentCategories() {
        
        EntityManager em = MetricCategory.entityManager();
        TypedQuery<MetricCategory> q = em.createQuery("SELECT o FROM MetricCategory AS o WHERE o.parent = null order by accountCode", MetricCategory.class);
        return q.getResultList();
    }
   
    
    public static List<MetricCategory> findChildCategories() {
        
        EntityManager em = MetricCategory.entityManager();
        TypedQuery<MetricCategory> q = em.createQuery("SELECT o FROM MetricCategory AS o WHERE o.parent != null", MetricCategory.class);
        return q.getResultList();
    }
    
    public static List<MetricCategory> findCategoriesByParentAccountCode(String accountCode) {
        
        if (accountCode == null || accountCode.length() == 0) throw new IllegalArgumentException("The accountCode argument is required");

        EntityManager em = MetricCategory.entityManager();
        TypedQuery<MetricCategory> q = em.createQuery("SELECT o FROM MetricCategory AS o WHERE o.parent.accountCode = :accountCode", MetricCategory.class);
        q.setParameter("accountCode", accountCode);        
        return q.getResultList();
    }    
    
    
    
    public String getFullAccountCode()
    {
    	String result = "";
    	
    	if(parent != null)
    		result = parent.getAccountCode() + this.accountCode;
    	else
    		result = this.accountCode;
    	
    	return result;
    }
    
    public List<MetricCategory> fetchChildCategories() {
        EntityManager em = MetricCategory.entityManager();
        TypedQuery<MetricCategory> q = em.createQuery("SELECT o FROM MetricCategory AS o WHERE o.parent = :parent order by accountCode", MetricCategory.class);
        q.setParameter("parent", this); 
        return q.getResultList();
   }
    

	public static MetricCategory findMetricCategoryByFullAccountCode(String accountCode) {
		if (accountCode == null || accountCode.length() == 0) throw new IllegalArgumentException("The accountCode argument is required");
		String parentAccountCode = accountCode.substring(0,2);
		String childAccountCode = accountCode.substring(2,4);
	    EntityManager em = MetricCategory.entityManager();
	    TypedQuery<MetricCategory> q = em.createQuery("SELECT o FROM MetricCategory AS o WHERE o.parent.accountCode = :parentAccountCode and o.accountCode = :childAccountCode", MetricCategory.class);
	    q.setParameter("parentAccountCode", parentAccountCode);        
	    q.setParameter("childAccountCode", childAccountCode);        
	    return q.getSingleResult();
	}
	
	
    /**
     * Returns all MetricCategories structured by 1) parent, 2) account code
     * @return
     */
    public static List<MetricCategory> findMetricCategoriesHierarchy() {
        List<MetricCategory> rootCategories = MetricCategory.findParentCategories();
        List<MetricCategory> allCategories = new ArrayList<MetricCategory>();
        for (MetricCategory mc : rootCategories) {
        	allCategories.add(mc);
        	List<MetricCategory> children = mc.fetchChildCategories();
        	allCategories.addAll(children);
        }
        return allCategories;
    }
}
