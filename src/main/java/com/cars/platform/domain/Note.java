package com.cars.platform.domain;

import java.util.Date;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

//@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findNotesByReviewAndDocumentType" })
public class Note {
	@ManyToOne
	@NotNull
	private DocumentType documentType;
	@ManyToOne
	@NotNull
	private Review review;
	@ManyToOne
	@NotNull
	private Person person;
	@ManyToOne
	@NotNull
	private Company company;
	@Size(max=140)
	private String text;
	private Date dateStamp = new Date();

	public DocumentType getDocumentType() {
		return documentType;
	}
	public void setDocumentType(DocumentType value) {
		documentType = value;
	}
	public Review getReview() {
		return review;
	}
	public void setReview(Review value) {
		review = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getDateStamp() {
		return dateStamp;
	}
	public void setDateStamp(Date dateStamp) {
		this.dateStamp = dateStamp;
	}
	public Company getCompany(){
		return company;
	}
	public void setCompany(Company company){
		this.company = company;
	}
	public Person getPerson(){
		return person;
	}
	public void setPerson(Person person){
		this.person = person;
	}
}
