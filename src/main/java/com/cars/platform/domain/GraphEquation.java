package com.cars.platform.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;


@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class GraphEquation {

    @ManyToOne
    private Graph graph;

    @ManyToOne
    private Equation equation;

    private int rank;
    
    
    public GraphEquation(GraphEquation ge) {
    	this.graph = ge.graph;
    	this.equation = ge.equation;
    	this.rank = ge.rank;
    }
    
    public GraphEquation(Graph graph, Equation equation, int rank) {
    	this.graph = graph;
    	this.equation = equation;
    	this.rank = rank;
    }

    
    
    public static List<Equation> findEquationsByGraph(Graph graph) {
        if (graph == null) throw new IllegalArgumentException("The graph argument is required");
        EntityManager em = GraphEquation.entityManager();
        TypedQuery<Equation> q;
        q = em.createQuery("SELECT e FROM GraphEquation AS o JOIN o.equation AS e WHERE o.graph = :graph order by o.rank", Equation.class);
        q.setParameter("graph", graph);
        return q.getResultList();
    }
    
    public GraphEquation clone(Graph graph) {
    	GraphEquation clone = new GraphEquation();
    	clone.graph = graph;
    	clone.equation = this.equation;
    	clone.rank = this.rank;
    	return clone;
    }
}
