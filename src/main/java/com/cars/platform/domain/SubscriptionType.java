package com.cars.platform.domain;

public enum SubscriptionType {

	ALL, 	    // has access to specified resources for all CDFIs for a period of 12 months 
	THREEPAK_DEPRECATED, 	// has three years of ratings and reports for any three CDFIs select over 12 month period.
	SINGLE,  	// has access to specified resources for a single CDFI for a period of 12 months
	LIBRARY, 	// CDFI provide subscriber library access for 12 months.
	SELECT;		// For a period of 1 year CDFI provide 60 days limited view of rating reports to subscribers of their choice

	//***************************
	//********* WARNING *********
	//***************************
	// These literal strings are used in the jspx pages, be very careful if you need to change them
	public String getText() {
		if(this.equals(SubscriptionType.ALL))
		    return "All";
		else if(this.equals(SubscriptionType.THREEPAK_DEPRECATED))
		    return "3Pak";
		else if(this.equals(SubscriptionType.SINGLE))
		    return "Single";
		else if(this.equals(SubscriptionType.LIBRARY))
		    return "Library";
		else if(this.equals(SubscriptionType.SELECT))
		    return "Aeris Select";
		else
			return "Other";
	}
}
