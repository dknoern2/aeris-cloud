package com.cars.platform.domain.constraint;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.service.graph.breakdown.CellValue;

/**
 * Validates that the equation on the BreakdownColumn is valid
 * @author kurtl
 *
 */
public class CheckBreakdownColumnValidator implements ConstraintValidator<CheckBreakdownColumn, BreakdownColumn> {
	
	MetricBreakdownItem mbi;
	MetricBreakdownItem summationItem;
	Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem;
	
	@Override
	public boolean isValid(BreakdownColumn breakdownColumn, ConstraintValidatorContext context) {
		String failure = init(breakdownColumn);
		if (null != failure) {
			setMessage(context, failure);
			return false;
		}
		failure = validateEquation(breakdownColumn, breakdownColumn.getEquation(), false);
		if (null != failure) {
			setMessage(context, "Malformed Equation: " + failure);
			return false;
		}
		failure = validateEquation(breakdownColumn, breakdownColumn.getTotalEquation(), true);
		if (null != failure) {
			setMessage(context, "Malformed Total Equation: " + failure);
			return false;
		}
		return true;
	}

	private String validateEquation(BreakdownColumn breakdownColumn, String equation, boolean total) {
		if (null == equation) {
			return null;
		}
		CellValue cellValue = breakdownColumn.caclulateCellValue(mbi, summationItem, valueByCodeByItem, breakdownColumn, total);
		if (null != cellValue.e) {
			return cellValue.e.getMessage();
		}
		return null;
	}

	@Override
	public void initialize(CheckBreakdownColumn arg0) {
	}
	
	private void setMessage(ConstraintValidatorContext context, String message) {
        context.disableDefaultConstraintViolation();
        context
            .buildConstraintViolationWithTemplate(message)
            .addConstraintViolation();
	}
	
	private String init(BreakdownColumn bd) {
		MetricBreakdown mb = bd.getBreakdown();
		List<MetricBreakdownItem> mbis = mb.getItems();
		if (null == mbis || mbis.size() == 0) {
			return "There are no MetricBreakdownItems defined for the MetricBreakdown " + mb.getName();
		}
		
		// randomly choose the first breakdown item
		mbi = mbis.get(0);
		
		summationItem = new MetricBreakdownItem();
		summationItem.setName("SUM");
		summationItem.setMetricBreakdown(mb);

		
		List<Metric> metrics = Metric.findMetricsForBreakdown(mb);
		if (null == metrics || metrics.size() == 0) {
			return "There are no Metrics defined for the MetricBreakdown " + mb.getName();
		}
		
		Map<String, MetricValue> valueByCode = new HashMap<String, MetricValue>();
		for (Metric metric : metrics) {
			MetricValue mv = new MetricValue();
			mv.setMetric(metric);
			mv.setAmount(1.0);
			mv.setTextValue("OK");
			mv.setBreakdownItem(mbi);
			valueByCode.put(metric.getFullAccountCode(), mv);
		}
		
		valueByCodeByItem = new HashMap<MetricBreakdownItem, Map<String, MetricValue>>();
		valueByCodeByItem.put(mbi, valueByCode);
		valueByCodeByItem.put(summationItem, valueByCode);
		
		return null;
	}

}