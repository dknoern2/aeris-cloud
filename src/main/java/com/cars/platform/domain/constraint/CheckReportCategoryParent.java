package com.cars.platform.domain.constraint;


import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target( { TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = ReportCategoryParentValidator.class)
public @interface CheckReportCategoryParent {
    String message() default "The parent must be on the same report page";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}