package com.cars.platform.domain.constraint;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars.platform.domain.BreakdownSingle;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.ReportCategory;

/**
 * Validates that the MetricBreakdown associated to this graph is the same as the equation's (column's) breakdown
 * @author kurtl
 *
 */
public class CheckBreakdownSingleValidator implements ConstraintValidator<CheckBreakdownSingle, BreakdownSingle> {

	@Override
	public boolean isValid(BreakdownSingle breakdownSingle, ConstraintValidatorContext arg1) {
		MetricBreakdown columnBreakdown = breakdownSingle.getEquation().getBreakdown();
		return breakdownSingle.getBreakdown().equals(columnBreakdown);
	}

	@Override
	public void initialize(CheckBreakdownSingle arg0) {
		// TODO Auto-generated method stub
		
	}

}