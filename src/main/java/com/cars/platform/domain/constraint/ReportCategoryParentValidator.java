package com.cars.platform.domain.constraint;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars.platform.domain.ReportCategory;

public class ReportCategoryParentValidator implements ConstraintValidator<CheckReportCategoryParent, ReportCategory> {

	@Override
	public void initialize(CheckReportCategoryParent arg0) {
	}

	@Override
	public boolean isValid(ReportCategory reportCategory, ConstraintValidatorContext arg1) {
		if (reportCategory.getParent() == null) {
			return true;
		}
		if (reportCategory.getParent().getReportPage() != reportCategory.getReportPage()) {
		    return false;
		}
		return true;
	}

}