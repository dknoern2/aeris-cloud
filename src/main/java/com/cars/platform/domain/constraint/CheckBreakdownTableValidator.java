package com.cars.platform.domain.constraint;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.BreakdownSingle;
import com.cars.platform.domain.BreakdownTable;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.ReportCategory;

/**
 * Validates that the MetricBreakdown associated to this graph is the same as the equation's (column's) breakdown
 * @author kurtl
 *
 */
public class CheckBreakdownTableValidator implements ConstraintValidator<CheckBreakdownTable, BreakdownTable> {

	@Override
	public boolean isValid(BreakdownTable breakdownTable, ConstraintValidatorContext arg1) {
		MetricBreakdown tableBreakdown = breakdownTable.getBreakdown();
		for (BreakdownColumn column : breakdownTable.getColumns()) {
			if (! column.getBreakdown().equals(tableBreakdown)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void initialize(CheckBreakdownTable arg0) {
		// TODO Auto-generated method stub
		
	}

}