package com.cars.platform.domain;

public enum LogoType {

	CORPORATE, RATED_PNG, RATED_EPS, RATED_SINCE_PNG, RATED_SINCE_EPS
	// 0,      1,         2,         3,               4
}
