package com.cars.platform.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.util.DateHelper;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findReviewsByCompany" })
public class Review {
	
	public static String[] FULL_IMPACT_VALUES = {"AAA", "AA", "A", "B", "****", "***", "**", "*", "\u2605\u2605\u2605\u2605", "\u2605\u2605\u2605", "\u2605\u2605", "\u2605"};
	public static String[] IGNORE_FULL_IMPACT_VALUES = {"AAA", "AA", "A", "B", "****", "***", "**", "*"};
	public static String[] GetAllowedFullImpactValues(){
		return differences(FULL_IMPACT_VALUES, IGNORE_FULL_IMPACT_VALUES);
	}
	
	public static String[] FULL_FINANCIAL_STRENGTH_VALUES = {"1", "2", "3", "4", "5", "AAA", "AA+", "AA", "AA-", "A+", "A", "A-", "BBB+", "BBB", "BBB-", "BB+", "BB", "BB-", "B"};
	public static String[] IGNORE_FULL_FINANCIAL_STRENGTH_VALUES = {"1", "2", "3", "4", "5"};
	public static String[] GetAllowedFullFinancialStrengthValues(){
		return differences(FULL_FINANCIAL_STRENGTH_VALUES, IGNORE_FULL_FINANCIAL_STRENGTH_VALUES);
	}
	
	public static String[] ANNUAL_IMPACT_VALUES = {
		"Improvement",
		"Stable with Improving Trends",
		"Stable",
		"Stable with Declining Trends",
		"Decline"
	};
	public static String[] ANNUAL_FINANCIAL_STRENGTH_VALUES = {
		"Improvement",
		"Stable with Improving Trends",
		"Stable",
		"Stable with Declining Trends",
		"Decline"
	};
	private static String[] differences(String[] first, String[] second) {
		
        LinkedList<String> diffs = new LinkedList<String>();  
        
        for (int i = 0; i < first.length; i++){
        	Boolean flag = true;
        	for (int j = 0; j < second.length; j++){
        		if (second[j] == first[i]){
        			flag = false;
        			break;
        		}
        	}
        	if (flag){
        		diffs.add(first[i]);
        	}
        }

        String[] strDups = new String[diffs.size()];

        return diffs.toArray(strDups);
    }

    private static void append(LinkedList<String> diffs, String[] sortedArray, int index) {
        while(index < sortedArray.length) {
            diffs.add(sortedArray[index]);
            index++;
        }
    }
	

    @ManyToOne
    private Company company;

    @Enumerated
    private ReviewType reviewType;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @NotNull
    private Date dateOfAnalysis;

    @Column(columnDefinition="VARCHAR(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci")
    private String impactPerformance;
    
    private boolean policyPlus;

    @Column(columnDefinition="VARCHAR(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci")
    private String financialStrength;
    
    private boolean skipNotification;
    
    @Min(2000)
    private int year;
    
	@Min(1)
	@Max(4)
    private int quarter;
    
    public int getYear() {
		return year;
	}

	public int getQuarter() {
		return quarter;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	public String getPeriod() {
    	return year + " Q" + quarter;
    }

	public static List<Review> findReviewsByCompanySorted(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
		TypedQuery<Review> q = entityManager().createQuery("SELECT o FROM Review o where o.company = :company order by o.dateOfAnalysis", Review.class);
        q.setParameter("company", company);		
		return q.getResultList();	
	}
	
	public static Review findMostRecentReviewByType(Company company, ReviewType type) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
		TypedQuery<Review> q = entityManager().createQuery("SELECT o FROM Review o where o.company = :company and o.reviewType =:reviewType and o.impactPerformance is not null and o.impactPerformance!='' order by o.dateOfAnalysis desc", Review.class);	
        q.setParameter("company", company);	
        q.setParameter("reviewType", type);
        q.setMaxResults(1);
		
        List<Review> list = q.getResultList();
        if(list!=null&&list.size()>0) return list.get(0);
        return null;	
	}
	
	public static Review findMostRecentCompletedReview(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
		TypedQuery<Review> q = entityManager().createQuery("SELECT o FROM Review o where o.company = :company  and o.impactPerformance is not null and o.impactPerformance!='' order by o.dateOfAnalysis desc", Review.class);	
		q.setParameter("company", company);		
		q.setMaxResults(1);
		
		List<Review> list = q.getResultList();
        if(list!=null&&list.size()>0) return list.get(0);
        return null;
	}

	public static List<Review> findCompletedFullAnalysisReviews(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
		TypedQuery<Review> q = entityManager().createQuery("SELECT o FROM Review o where o.company = :company and o.reviewType =:reviewType and o.impactPerformance is not null and o.impactPerformance!='' order by o.dateOfAnalysis desc", Review.class);	
		q.setParameter("company", company);		
		q.setParameter("reviewType", ReviewType.ANNUAL_RATING);		
		
		List<Review> list = q.getResultList();
        if(list!=null&&list.size()>0) {
        	return list;
        }
        return null;
	}	
	
	

	
	public static Review findMostRecentCompletedFullAnalysisReview(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
		TypedQuery<Review> q = entityManager().createQuery("SELECT o FROM Review o where o.company = :company and o.reviewType =:reviewType and o.impactPerformance is not null and o.impactPerformance!='' order by o.dateOfAnalysis desc", Review.class);	
		q.setParameter("company", company);		
		q.setParameter("reviewType", ReviewType.ANNUAL_RATING);		
		q.setMaxResults(1);
		
		List<Review> list = q.getResultList();
        if(list!=null&&list.size()>0) return list.get(0);
        return null;
	}	
	
	
	public static Review findEarliestCompletedFullAnalysisReview(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
		TypedQuery<Review> q = entityManager().createQuery("SELECT o FROM Review o where o.company = :company and o.reviewType =:reviewType and o.impactPerformance is not null and o.impactPerformance!='' order by o.dateOfAnalysis asc", Review.class);	
		q.setParameter("company", company);		
		q.setParameter("reviewType", ReviewType.ANNUAL_RATING);		
		q.setMaxResults(1);
		
		List<Review> list = q.getResultList();
        if(list!=null&&list.size()>0) return list.get(0);
        return null;
	}	
		
	
	
	public boolean isFull() {
		return ReviewType.ANNUAL_RATING.equals(reviewType);	
	}
	
	public boolean isAnnual() {
		return ReviewType.ANNUAL.equals(reviewType);	
	}

	public static List<Review> findAllReviewsPending() {
		TypedQuery<Review> q = entityManager().createQuery("SELECT o FROM Review o where (o.impactPerformance is null or o.impactPerformance='') and (o.financialStrength is null or o.financialStrength='') order by dateOfAnalysis", Review.class);	
		List<Review> reviews = q.getResultList();	
		
		// add 3 months to the start of date of analysis to get expected date
		// the analysis is available
		Date today = new Date();
		for (int i = reviews.size() - 1; i > -1; --i) {
		    reviews.get(i).setDateOfAnalysis(DateUtils.addMonths(reviews.get(i).getDateOfAnalysis(), 3));
		    if (!reviews.get(i).isFull() || today.after(reviews.get(i).getDateOfAnalysis())) {
			reviews.remove(i);
		    }
		}
		
		return reviews;
	}
	
	
	/**
	 * A review is "pending" if both financialStrength and impactPerformance are not set
	 * @return
	 */
	public boolean isPending(){
		
		return (financialStrength==null||financialStrength.length()==0)
				&& (impactPerformance==null||impactPerformance.length()==0)
				&& dateOfAnalysis!=null;
	}

	public boolean isSkipNotification() {
		return skipNotification;
	}

	public void setSkipNotification(boolean skipNotification) {
		this.skipNotification = skipNotification;
	}
	
	

}
