package com.cars.platform.domain;

public enum TaxType {

    NON_PROFIT("Non-Profit"), FOR_PROFIT("For-Profit"), GOVERNMENT("Government");
    
    private final String value;
    
    private TaxType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    public String toString() {
    	return value;
    }
}
