package com.cars.platform.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findMessagesByRecipient",
		"findMessagesBySender" })
public class Message {

	@ManyToOne
	private Person sender;

	@ManyToOne
	private Person recipient;

	@ManyToOne
	private com.cars.platform.domain.Message parent;

	private String text;

	private String subject;

	@Temporal(TemporalType.TIMESTAMP)
	private Date sent;

	private boolean archived;

	public static TypedQuery<com.cars.platform.domain.Message> findRecentMessagesByRecipient(
			Person recipient) {
		if (recipient == null)
			throw new IllegalArgumentException(
					"The recipient argument is required");
		EntityManager em = Message.entityManager();
		TypedQuery<Message> q = em.createQuery("SELECT o FROM Message AS o WHERE o.recipient = :recipient order by o.sent desc", Message.class);
		q.setParameter("recipient", recipient);
		q.setMaxResults(5);
		return q;
	}

	public static TypedQuery<com.cars.platform.domain.Message> findRecentMessagesByRecipientAndSenderCompany(
			Person recipient, Company senderCompany) {
		if (recipient == null)
			throw new IllegalArgumentException(
					"The recipient argument is required");
		if (senderCompany == null)
			throw new IllegalArgumentException(
					"The senderCompany argument is required");
		EntityManager em = Message.entityManager();
		TypedQuery<Message> q = em.createQuery("SELECT o FROM Message AS o WHERE o.recipient = :recipient and o.sender.company = :senderCompany order by o.sent desc",Message.class);
		q.setParameter("recipient", recipient);
		q.setParameter("senderCompany", senderCompany);
		q.setMaxResults(5);
		return q;
	}

	public static List<Message> findAllConversations(Person recipient,Person sender) {
		if (recipient == null) throw new IllegalArgumentException("The recipient argument is required");
		if (sender == null)throw new IllegalArgumentException("The sender argument is required");
		EntityManager em = Message.entityManager();
		TypedQuery<Message> q = em.createQuery("SELECT o FROM Message AS o WHERE (o.recipient = :recipient AND o.sender = :sender) OR (o.recipient = :sender AND o.sender = :recipient) order by o.sent", Message.class);
		q.setParameter("recipient", recipient);
		q.setParameter("sender", sender);

		return q.getResultList();
	}

	public static List<PersonWrapper> findAllSenders(Person recipient) {

		List<PersonWrapper> result = null;
		EntityManager em = Message.entityManager();
		try {

			TypedQuery<PersonWrapper> q = em.createQuery("SELECT DISTINCT new com.cars.platform.domain.PersonWrapper (o.sender, max(o.sent)) FROM Message AS o WHERE o.recipient = :recipient group by o.sender order by o.sent desc", PersonWrapper.class);
			q.setParameter("recipient", recipient);
			 result = q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static List<PersonWrapper> findAllRecipients(Person sender) {

		List<PersonWrapper> result = null;
		EntityManager em = Message.entityManager();
		try {

			TypedQuery<PersonWrapper> q = em.createQuery("SELECT DISTINCT new com.cars.platform.domain.PersonWrapper (o.recipient, max(o.sent)) FROM Message AS o WHERE o.sender = :sender group by o.recipient order by o.sent desc", PersonWrapper.class);
			q.setParameter("sender", sender);
			 result = q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}


}
