// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.State;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect State_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager State.entityManager;
    
    public static final EntityManager State.entityManager() {
        EntityManager em = new State().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long State.countStates() {
        return entityManager().createQuery("SELECT COUNT(o) FROM State o", Long.class).getSingleResult();
    }
    
    public static List<State> State.findAllStates() {
        return entityManager().createQuery("SELECT o FROM State o", State.class).getResultList();
    }
    
    public static State State.findState(Long id) {
        if (id == null) return null;
        return entityManager().find(State.class, id);
    }
    
    public static List<State> State.findStateEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM State o", State.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void State.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void State.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            State attached = State.findState(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void State.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void State.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public State State.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        State merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
