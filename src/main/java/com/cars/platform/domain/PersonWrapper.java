package com.cars.platform.domain;

import java.util.Date;

public class PersonWrapper {

	public PersonWrapper(Person person, Date sentDate){
		this.person = person;
		this.sentDate = sentDate;
	}
	
	private Person person;
	private Date sentDate;
	
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	
}
