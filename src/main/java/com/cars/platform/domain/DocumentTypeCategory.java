package com.cars.platform.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class DocumentTypeCategory {

    public static final String CARS_REPORT = "AERIS - Report";
    public static final String CARS_RESOURCES = "AERIS - Resources";
    public static final String CARS_INTERNAL = "AERIS - Internal";

    @NotNull
    @Size(min = 2)
    public String name;
    
    public boolean cdfiResponsible() {
    	return !(name.equals(CARS_REPORT) || name.equals(CARS_RESOURCES) || name.equals(CARS_INTERNAL));
    }
    
    public static DocumentTypeCategory getCategoryTypeForLibraryUpdatesMigration() {
        EntityManager em = entityManager();
        TypedQuery<DocumentTypeCategory> q = em.createQuery("SELECT o FROM DocumentTypeCategory AS o ORDER BY o.id DESC", DocumentTypeCategory.class);
        List<DocumentTypeCategory> documentTypeCategories = q.getResultList();
        return documentTypeCategories.get(0);
    }
}
