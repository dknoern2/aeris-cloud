package com.cars.platform.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.StringUtils;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

@RooJavaBean
@RooJpaActiveRecord
public class Equation implements Comparable<Equation>, IEquationFormat {
	

    @ManyToOne
    private Company company;
    
    private String name;

    @Size(max = 500)
    private String formula;

    // deprecated by ReportCategoryEquation
    private String code;
   
	private UnitType unitType;

	private Integer decimalPlaces;
	
    @OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="equation")
    private Set<ReportCategoryEquation> categoryEquations;
    
    // this relationship exists only so that the related ValueFacts are deleted when this equation is deleted
    @OneToMany(fetch = FetchType.LAZY, orphanRemoval=true, mappedBy="equation")
    private List<ValueFact> valueFacts;
    
    // this relationship exists only so that the related GraphEquations are deleted when this equation is deleted
    @OneToMany(fetch = FetchType.LAZY, orphanRemoval=true, mappedBy="equation")
    private List<GraphEquation> graphEquation;

  
    /**
     * This is set if a CDFI equation overrides a global one
     * The target of this relationship must have a null company (be global)
     */
    @ManyToOne
    private Equation override;
    
    /**
     * When calculating top/bottom quartile the definition is reversed if smaller values are better 
     */
    private boolean smallerBetter; 
    
    @Column(columnDefinition="VARCHAR(1000) default ''")
    private String definition;

    
    /**
     * Returns the override of this equation for the provided company, or null if one does not exist.
     * Returns null if this is already company specific, That is only global equations can be overridden.
     */
    public Equation getCompanyOverride(Company company) {
    	if (null != this.getCompany()) {
    		return null;
    	}
    	
    	return Equation.findCompanyOverride(this, company);
    }
    
    /**
     * Updates this equation with values from the passed in equation
     */
    public void update(Equation update) {
    	this.setCompany(update.getCompany());
    	this.setName(update.getName());
    	this.setUnitType(update.getUnitType());
    	this.setDecimalPlaces(update.getDecimalPlaces());
    	this.setOverride(update.getOverride());
    	this.setSmallerBetter(update.isSmallerBetter());
    	this.setFormula(update.getFormula());
    	this.setDefinition(update.definition);
    	// categoryEquations is managed by the destination entity
    	
    	this.merge();
    }
    
    @Override
    public int compareTo(Equation other){
        int last = this.name.compareTo(other.name);
        return last == 0 ? this.name.compareTo(other.name) : last;
    }
    

    // out of box included categoryEquations which ended up realllly slow due to circular references
    // then adding the relationship to valueFacts did it again
	public String toString() {
//		ReflectionToStringBuilder rsb = new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
//		rsb.setExcludeFieldNames("categoryEquations", "valueFacts");
//        return rsb.toString();
		return getCompany() + ": " + getName();
    }
	
	public List<Equation> findFormulaReferences() {
		EntityManager em = Equation.entityManager();
		// The ID of this equation must be at the start of the line or not preceded by another digit
		// Ugh... hibernate does not support rlike
        //TypedQuery<Equation> q = em.createQuery("SELECT Equation FROM Equation AS equation WHERE equation.formula rlike '(^|[^0-9])" + this.getId() + "[fF]'", Equation.class);
        TypedQuery<Equation> q = em.createQuery("SELECT Equation FROM Equation AS equation WHERE equation.formula like '%" + this.getId() + "%'", Equation.class);
        List<Equation> possibles = q.getResultList();
        if (null == possibles) {
        	return null;
        }
        
        // since there is no regex in hibernate make sure we don't match on '3339F' when looking for '9F' as an example
        Pattern formulaRef = Pattern.compile("(?<!\\d)" + getId() + "[fF]");
        List<Equation> references = new ArrayList<Equation>();
        for (Equation e : possibles) {
            Matcher matcher = formulaRef.matcher(e.getFormula());
        	if (matcher.find()) {
        		references.add(e);
        	}
        }
        
        return references;
	}


    /**
     * returns the equation corresponding to Total Assets... this has magic ID of 1
     * @return
     */
    public static Equation findTotalAssets() {
    	return findEquation(1L);
    }
    
    /**
     * Find all global (non-cdfi specific) equations
     * @param company
     * @return
     */
    public static List<com.cars.platform.domain.Equation> findGlobalEquations() {
    	
        EntityManager em = Equation.entityManager();
        TypedQuery<Equation> q = em.createQuery("SELECT Equation FROM Equation AS equation WHERE equation.company is null order by equation.code", Equation.class);
        return q.getResultList();
    }    
        
    
    
    /**
     * Find all equations relavent to a company, both global and specific to that company
     * @param company
     * @return
     */
    public static List<com.cars.platform.domain.Equation> findAllEquationsByCompany(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = Equation.entityManager();
        TypedQuery<Equation> q = em.createQuery("SELECT Equation FROM Equation AS equation WHERE equation.company is null or equation.company = :company order by equation.code,equation.company", Equation.class);
        q.setParameter("company", company);
        return q.getResultList();
    } 
    
    public static List<com.cars.platform.domain.Equation> findAllEquationsByCompanySortedByName(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = Equation.entityManager();
        TypedQuery<Equation> q = em.createQuery("SELECT Equation FROM Equation AS equation WHERE equation.company is null or equation.company = :company order by equation.name", Equation.class);
        q.setParameter("company", company);
        return q.getResultList();
    }    

    public static List<com.cars.platform.domain.Equation> findLocalEquationsByCompanySortedByName(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = Equation.entityManager();
        TypedQuery<Equation> q = em.createQuery("SELECT Equation FROM Equation AS equation WHERE equation.company = :company order by equation.name", Equation.class);
        q.setParameter("company", company);
        return q.getResultList();
    }    
    
    public static TypedQuery<Equation> findEquationsByCompany(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = Equation.entityManager();
        TypedQuery<Equation> q = em.createQuery("SELECT o FROM Equation AS o WHERE o.company = :company order by o.code", Equation.class);
        q.setParameter("company", company);
        return q;
    }
    
    
    /**
     * Find the equation that overrides the provided equation for the provided company
     * @param company
     * @return
     */
    public static Equation findCompanyOverride(Equation override, Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (override == null) throw new IllegalArgumentException("The override argument is required");
        if (override.company != null) throw new IllegalArgumentException("The override argument must be a global equation (company == null)");
        

        EntityManager em = MetricValue.entityManager();
        TypedQuery<Equation> q = em.createQuery("SELECT Equation FROM Equation AS equation WHERE equation.company = :company and equation.override = :override", Equation.class);
        q.setParameter("company", company);
        q.setParameter("override", override);
        
        List<Equation> result = q.getResultList();
        if (null == result || result.size() == 0) {
        	return null;
        }
        return result.get(0);
    } 
 
    public static List<Equation> findOverridesForCompany(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        
        EntityManager em = MetricValue.entityManager();
        TypedQuery<Equation> q = em.createQuery("SELECT Equation FROM Equation AS equation WHERE equation.company = :company and equation.override != null", Equation.class);
        q.setParameter("company", company);
    
        return q.getResultList();
    } 

    


	public Company getCompany() {
        return this.company;
    }

	public void setCompany(Company company) {
        this.company = company;
    }

	public String getCode() {
        return this.code;
    }

	public void setCode(String code) {
        this.code = code;
    }
}
