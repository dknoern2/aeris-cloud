package com.cars.platform.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.util.DateHelper;

@RooJavaBean
@RooToString(excludeFields={"cdfis", "subscribers", "owner"})
@RooJpaActiveRecord(finders = { "findSubscriptionsByExpirationDateBetween", "findSubscriptionsByExpirationDateGreaterThan" })
public class Subscription implements Serializable{

    private static final long serialVersionUID = 1L;

    @Enumerated
    private SubscriptionType subscriptionType;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date expirationDate = DateUtils.addYears(new Date(), 1);

    @ManyToOne
    private Company owner;

    // should equal owner for CDFI based subscriptions
    @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER) // EAGER order to avoid no session error in ReminderService)
    private Set<Access> cdfis = new HashSet<Access>();
    
    // should equal owner for Subscriber based subscriptions
    @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER) // EAGER order to avoid no session error in ReminderService)
    private Set<Access> subscribers = new HashSet<Access>();
    
    // access to full analysis reports, allowed for subscriber and cdfi based subscriptions
    boolean ratings;
    
    // access to annual reviews, allowed for subscriber and cdfi based subscriptions
    boolean annualReviews;
    
    // access to opinion reports, subscriber based subscriptions only
    boolean opinions;
    
    // access to financials, subscriber based subscriptions only
    boolean financials;
    
    // access to peer group reports, subscriber based subscriptions only
    boolean peerGroups;
    
    // If there is access to peer group reports this controls whether or not peer metrics are shown
    boolean showPeerMetrics;
   
    // access to CDFI's library, CDFI based subscriptions only
    boolean library;
    
    // unused column
    boolean checked = false;
    

	public boolean isRatings() {
        return this.ratings;
    }

	public void setRatings(boolean ratings) {
        this.ratings = ratings;
    }
	
    public boolean isAnnualReviews() {
		return annualReviews;
	}

	public void setAnnualReviews(boolean annualReviews) {
		this.annualReviews = annualReviews;
	}

	public boolean isOpinions() {
		return opinions;
	}

	public void setOpinions(boolean opinions) {
		this.opinions = opinions;
	}

	public boolean isPeerGroups() {
		return peerGroups;
	}

	public void setPeerGroups(boolean peerGroups) {
		this.peerGroups = peerGroups;
	}

	public boolean isShowPeerMetrics() {
		return showPeerMetrics;
	}

	public void setShowPeerMetrics(boolean showPeerMetrics) {
		this.showPeerMetrics = showPeerMetrics;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/** 
     * Returns true if subscription is expiring in one month or less.
     * @return
     */
    public boolean isExpiringSoon(){
    	
    	if(expirationDate==null) return false;
    	return expirationDate.getTime() <DateUtils.addMonths(new Date(),1).getTime();  
    	
    }
    
    /**
     * Returns a list of all active subscriptions for the subscriber. Does not include delegated subscriptions.
     * Use this when determining what CDFIs are 'in your garage'
     */
    public static List<Subscription> findActiveOwnedSubscriptionsForSubscriber(Company subscriber) {
    	List<Subscription> activeSubscriptions = new ArrayList<Subscription>();
    	List<Subscription> ownedSubscriptions = findSubscriptionsBySubscriberEntry(subscriber);
		Date today = new Date();
    	for (Subscription subscription : ownedSubscriptions) {
    		if (today.before(subscription.getExpirationDate())) {
    			activeSubscriptions.add(subscription);
    		}
    	}
    	return activeSubscriptions;
    }
    
    /**
     * Returns a list of all active subscriptions for the subscriber, both owned and delegated
     */
    public static List<Subscription> findAllActiveSubscriptionsForSubscriber(Company subscriber) {
    	List<Subscription> activeSubscriptions = new ArrayList<Subscription>();
    	List<Subscription> ownedSubscriptions = findSubscriptionsBySubscriberEntry(subscriber);
		Date today = new Date();
    	for (Subscription subscription : ownedSubscriptions) {
    		if (today.before(subscription.getExpirationDate())) {
    			activeSubscriptions.add(subscription);
    		}
    	}
    	List<Subscription> delegatedSubscriptions = findDelegatedSubscriptionsBySubscriber(subscriber);
    	for (Subscription subscription : delegatedSubscriptions) {
    		if (today.before(subscription.getExpirationDate())) {
    			activeSubscriptions.add(subscription);
    		}
    	}
    	
    	return activeSubscriptions;
    }

    
    
    public static List<com.cars.platform.domain.Subscription> findSubscriptionsBySubscriptionTypeAndExpirationDateGreaterThan(SubscriptionType subscriptionType, Date expirationDate) {
        if (subscriptionType == null) throw new IllegalArgumentException("The subscriptionType argument is required");
        if (expirationDate == null) throw new IllegalArgumentException("The expirationDate argument is required");
        EntityManager em = entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o WHERE o.expirationDate > :expirationDate", Subscription.class);
        q.setParameter("expirationDate", expirationDate);
        return q.getResultList();
    }
    
    /*public static List<com.cars.platform.domain.Subscription> findSubscriptionsByTargetEntry(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o JOIN o.target as c WHERE c = :company", Subscription.class);
        q.setParameter("company", company);
        return q.getResultList();
    }*/
    
    
    public static List<com.cars.platform.domain.Subscription> findSubscriptionsByCdfiEntry(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o JOIN o.cdfis as c WHERE c.company = :company", Subscription.class);
        q.setParameter("company", company);
        return q.getResultList();
    }    
    
    public static List<com.cars.platform.domain.Subscription> findSubscriptionsBySubscriberEntry(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = entityManager();
        //TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o JOIN o.cdfis AS c WHERE c.company = :company", Subscription.class);
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o JOIN o.subscribers WHERE o.owner = :company", Subscription.class);
        q.setParameter("company", company);
        return q.getResultList();
    }    
    
    
    /*public static List<com.cars.platform.domain.Subscription> findDelegatedSubscriptionsByTarget(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o JOIN o.target as c WHERE c = :company and(o.subscriptionType = :subscriptionType1 or o.subscriptionType = :subscriptionType2) ", Subscription.class);
        q.setParameter("company", company);
        q.setParameter("subscriptionType1", SubscriptionType.REPORTING);
        q.setParameter("subscriptionType2", SubscriptionType.SELECT);
        return q.getResultList();
    }*/
 
    public static List<com.cars.platform.domain.Subscription> findDelegatedSubscriptionsByCdfi(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o JOIN o.cdfis as c WHERE c.company = :company and(o.subscriptionType = :subscriptionType1 or o.subscriptionType = :subscriptionType2) ", Subscription.class);
        q.setParameter("company", company);
        q.setParameter("subscriptionType1", SubscriptionType.LIBRARY);
        q.setParameter("subscriptionType2", SubscriptionType.SELECT);
        return q.getResultList();
    }        
    public static List<com.cars.platform.domain.Subscription> findDelegatedSubscriptionsBySubscriber(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o JOIN o.subscribers as c WHERE c.company = :company and(o.subscriptionType = :subscriptionType1 or o.subscriptionType = :subscriptionType2) ", Subscription.class);
        q.setParameter("company", company);
        q.setParameter("subscriptionType1", SubscriptionType.LIBRARY);
        q.setParameter("subscriptionType2", SubscriptionType.SELECT);
        return q.getResultList();
    }        
    
    
    public static TypedQuery<Subscription> findSubscriptionsByOwner(Company owner) {
        if (owner == null) throw new IllegalArgumentException("The owner argument is required");
        EntityManager em = Subscription.entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o WHERE o.owner = :owner order by o.owner.name", Subscription.class);
        q.setParameter("owner", owner);
        return q;
    }    
    

	public static List<com.cars.platform.domain.Subscription>  findSubscriptionsByOwnerAndType(Company owner,
			SubscriptionType subscriptionType) {

        if (owner == null) throw new IllegalArgumentException("The owner argument is required");
        if (subscriptionType == null) throw new IllegalArgumentException("The subscriptionType argument is required");

        EntityManager em = Subscription.entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o WHERE o.owner = :owner and o.subscriptionType = :subscriptionType order by o.owner.name", Subscription.class);
        q.setParameter("owner", owner);
        q.setParameter("subscriptionType", subscriptionType);

        return q.getResultList();
		
	}

	public static List<Subscription> findRatingsSubscriptionsByOwner(Company owner) {
		Subscription subscription = null;
		
        if (owner == null) throw new IllegalArgumentException("The owner argument is required");

        EntityManager em = Subscription.entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o WHERE o.owner = :owner and (o.subscriptionType = :subscriptionType1 or o.subscriptionType = :subscriptionType2) order by o.expirationDate desc", Subscription.class);
        q.setParameter("owner", owner);
        q.setParameter("subscriptionType1", SubscriptionType.ALL);
        q.setParameter("subscriptionType2", SubscriptionType.SINGLE);

        // should just be one!
        //try{
            //subscription = q.getSingleResult();
            
        //}catch(Exception ignore){}
            	
            	
        //return subscription;
        return q.getResultList();
	}   
	
	
	//public static List<com.cars.platform.domain.Subscription>  findSubscriptionsByTargetAndType(Company owner,
	//		SubscriptionType subscriptionType) {
		
		
		
    public static List<com.cars.platform.domain.Subscription> findSubscriptionsBySubscriberAndType(Company company,SubscriptionType subscriptionType) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (subscriptionType == null) throw new IllegalArgumentException("The subscriptionType argument is required");

        EntityManager em = entityManager();
        TypedQuery<Subscription> q = em.createQuery("SELECT o FROM Subscription AS o JOIN o.subscribers as c WHERE c.company = :company and o.subscriptionType = :subscriptionType order by o.owner.name", Subscription.class);
        q.setParameter("company", company);
        q.setParameter("subscriptionType", subscriptionType);
        return q.getResultList();
    }	
    
    
    /**
     * Convenience routine to determine number of targets set
     * @return
     */
    public int getTargetCount(){
    	int targetCount =0;
    	if(cdfis!=null){
    		targetCount = cdfis.size();
    	}
    	return targetCount;
    }
	    
    /**
     * Convenience routine to determine number of targets that may yet be set
     * @return
     */
    public int getAvailableTargetCount(){
    	int availableTargetCount = 1001; // unlimited
    	int targetCount  = getTargetCount();
    	
    	if(SubscriptionType.SINGLE.equals(subscriptionType)){
    		availableTargetCount = 1 - targetCount;
    	}
    	return availableTargetCount;
    }
	    
	public int getDaysUntilExpiration() {
		return DateHelper.getDaysUntilDue(expirationDate);
		
	}
	
	
	/**
	 * Returns the subscribers that have access to the provided document, null if there is no access via this subscription.
	 * Should only be called for active subscriptions.
	 */
	public List<Company> getSubscribersForDocument(Document document) {
		if (document.isCarsFull()) {
			if (!this.isRatings()) {
				return null;
			}
		}
		else if (document.isCarsOpinion()) {
			if (!this.isOpinions()) {
				return null;
			}
		}
		else if (document.isCarsAnnual()) {
			if (!this.isAnnualReviews()) {
				return null;
			}
		}
		else {
			return null;
		}
		
		Company docCompany = document.getCompany();
		boolean found = false;
		for (Access access : this.cdfis) {
			// YIKES: this being called from a scheduler (ReminderService) comparing the company objects does not work
			if (access.getCompany().getId().equals(docCompany.getId())) {
				found = true;
				break;
			}
		}
		if (found) {
			List<Company> subscribers = new ArrayList<Company>();
			for (Access access : this.subscribers) {
				if (access.isActive()) { // need the active check only for Select subscription types
					subscribers.add(access.getCompany());
				}
			}
			if (subscribers.size() > 0) {
				return subscribers;
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}


}
