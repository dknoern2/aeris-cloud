// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.DocumentType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.transaction.annotation.Transactional;

privileged aspect DocumentType_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager DocumentType.entityManager;
    
    public static final EntityManager DocumentType.entityManager() {
        EntityManager em = new DocumentType().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long DocumentType.countDocumentTypes() {
        return entityManager().createQuery("SELECT COUNT(o) FROM DocumentType o", Long.class).getSingleResult();
    }
    
    public static List<DocumentType> DocumentType.findAllDocumentTypes() {
        return entityManager().createQuery("SELECT o FROM DocumentType o", DocumentType.class).getResultList();
    }
    
    public static DocumentType DocumentType.findDocumentType(Long id) {
        if (id == null) return null;
        return entityManager().find(DocumentType.class, id);
    }
    
    public static List<DocumentType> DocumentType.findDocumentTypeEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM DocumentType o", DocumentType.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public InitialsRecord DocumentType.getInitialsRecord(Review review, Company company){
    	if (review == null || company == null){
    		return null;
    	}
    	
    	List<InitialsRecord> result = InitialsRecord.findInitialsRecordByReviewAndDocumentType(review, this, company).getResultList();
    	//the list must have only one element
    	if (result.isEmpty()){
    		return null;
    	} else {
    		return result.get(0);
    	}
    }
    
    public List<Note> DocumentType.getNotesByReview(Review review, Company company){
    	return Note.findNotesByReviewAndDocumentType(review, this, company).getResultList();
    }
    
    public HashMap<Review, List<Note> > DocumentType.getAllNotesByCompany(Company company){
    	HashMap<Review, List<Note>> result = new HashMap<Review, List<Note>>();
    	List<Note> notes = Note.findAllNotesByCompany(this, company).getResultList();
    	for(Note iNote : notes){
    		if (!result.containsKey(iNote.getReview())){
    			result.put(iNote.getReview(), new ArrayList<Note>());
    		}
    		result.get(iNote.getReview()).add(iNote);
    	}
    	return result;
    }
    
    public Boolean DocumentType.hasNotesForReview(Review review, Company company){
    	Long count = Note.getCountOfNoties(review, this, company);
    	return count > 0;
    }
    
    @Transactional
    public void DocumentType.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void DocumentType.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            DocumentType attached = DocumentType.findDocumentType(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void DocumentType.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void DocumentType.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public DocumentType DocumentType.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        DocumentType merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
