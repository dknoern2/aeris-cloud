package com.cars.platform.domain;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findDocumentTypesByNameEquals" })
public class DocumentType {

    public final static String DOCUMENT_TYPE_HISTORIC_FINANCIALS = "Historic Financial Data";
    public final static String DOCUMENT_TYPE_CORPORATE_LOGO = "Corporate Logo";
    public final static String DOCUMENT_TYPE_FULL_ANALYSIS = "Full Analysis";

    // TODO put these IDs in a properties file
    public final static long DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID = 46;
    public final static long DOCUMENT_TYPE_BREAKDOWN_ID = 1000; // see docs/phase2/Migration.sql

    @ManyToOne
    private DocumentTypeCategory documentTypeCategory;
    
    @ManyToOne
    private DocumentTypeFolder documentTypeFolder;
    
    @ManyToOne
    private DocumentTypeSubFolder documentTypeSubFolder;

    private boolean quarterly;
    private boolean annual; //"Annual: As Updated"
    private boolean full; //Annual: Required
    private boolean annualIfAvailable; //annualIfAvailable
    
    // does this document only need to be uploaded once (Mission statement for example)
    private boolean oneTime;
    
    private String description;
    
    private DocumentTypeFormat documentTypeFormat = DocumentTypeFormat.ANY;
    
    @Enumerated
    private ReportType reportType = ReportType.NONE;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
	public boolean isOneTime() {
		return oneTime;
	}

	public void setOneTime(boolean oneTime) {
		this.oneTime = oneTime;
	}

	public boolean isQuarterly() {
		return (null == dtc) ? quarterly : dtc.isQuarterly();
	}

	public void setQuarterly(boolean quarterly) {
		this.quarterly = quarterly;
	}

	@Deprecated
	public boolean isAnnual() {
		return (null == dtc) ? annual : dtc.isAnnual();
	}
	@Deprecated
	public void setAnnual(boolean annual) {
		this.annual = annual;
	}
	
	public boolean isAnnualIfAvailable() {
		return (null == dtc) ? annualIfAvailable : dtc.isAnnualIfAvailable();
	}
	
	public void setAnnualIfAvailable(boolean annualIfAvailable) {
		this.annualIfAvailable = annualIfAvailable;
	}
	@Deprecated
	public boolean isFull() {
		return (null == dtc) ? full : dtc.isFull();
	}
	@Deprecated
	public void setFull(boolean full) {
		this.full = full;
	}

	public boolean isAnnualRequired(){
		return isFull();
	}
	public void setAnnualRequired(boolean annualRequired){
		setFull(annualRequired);
	}
	
	public boolean isAnnualAsUpdated(){
		return isAnnual();
	}
	public void setAnnualAsUpdated(boolean annualAsUpdated){
		setAnnual(annualAsUpdated);
	}
	
    @NotNull
    @Size(min = 2)
    public String name;
    

	public static Boolean isExcelFormatDocumentType(DocumentType documentType){
    	return documentType.getDocumentTypeFormat() == DocumentTypeFormat.EXCEL;
    }
  
    // ---------
    // Transient members used for sending derived fields to the jsp pages
    // ---------
    @Transient
    private DueType dueType;
    
    @Transient
    private String dueMessage;

    @Transient
    private Boolean isInitialsRequired = false;

    @Transient
    private Boolean isNoteRequired = false;

    @Transient
    private Boolean needToShowInitials = false;
    
    public DueType getDueType() {
		return dueType;
	}
	public String getDueMessage() {
		return dueMessage;
	}
	public Boolean getIsInitialsRequired(){
		return isInitialsRequired;
	}
	public Boolean getIsNoteRequired(){
		return isNoteRequired;
	}
	public Boolean getNeedToShowInitials(){
		return needToShowInitials;
	}
	public void setDueType(DueType dueType) {
		this.dueType = dueType;
	}
	public void setDueMessage(String dueMessage) {
		this.dueMessage = dueMessage;
	}
	public void setIsInitialsRequired(Boolean isInitialsRequired){
		this.isInitialsRequired = isInitialsRequired;
	}
	public void setIsNoteRequired(Boolean isNoteRequired){
		this.isNoteRequired = isNoteRequired;
	}
	public void setNeedToShowInitials(Boolean needToShowInitials){
		this.needToShowInitials = needToShowInitials;
	}
	
	
	// ------------
	// Most find methods pass in a Company which  is stored here in order
	// to derive the report period properties (isQuarterly, isFinal, isAnnual)
	// ------------
	@Transient
	private DocumentTypeCompany dtc = null;
	


	// on Fetches we need to put the Company in context
	private static List<DocumentType> setCompany(List<DocumentType> docTypes, Company company) {
		List<DocumentTypeCompany> dtcs = DocumentTypeCompany.findForCompany(company);
		if (null == dtcs) {
			return docTypes;
		}
	
		for (DocumentTypeCompany dtc : dtcs) {
			for (DocumentType docType : docTypes) {
				if (docType.equals(dtc.getDocumentType())) {
					docType.dtc = dtc;
					break;
				}
			}
		}
		return docTypes;
	}
	
	
	public static List<DocumentType> findAllDocumentTypesAlphabetically(Company company) {
		EntityManager em = Company.entityManager();
		List<DocumentType> docTypes = em.createQuery("SELECT o FROM DocumentType o order by o.name",
				DocumentType.class).getResultList();
		return setCompany(docTypes, company);
	}



	public static List<DocumentType> findAllDocumentTypesSorted(Company company) {
		EntityManager em = Company.entityManager();
		List<DocumentType> docTypes = em
				.createQuery(
						"SELECT o FROM DocumentType o order by o.documentTypeFolder.id, o.documentTypeSubFolder.id, o.name",
						DocumentType.class).getResultList();
		return setCompany(docTypes, company);
	}



	// this is not 'company aware' as the only caller is for CARS-Resources which are 'global'
	public static List<DocumentType> findAllDocumentTypesByParentCategoryName(
			String documentTypeCategoryName) {
		if (documentTypeCategoryName == null)
			throw new IllegalArgumentException(
					"The documentTypeCategoryName argument is required");

		EntityManager em = Company.entityManager();
		TypedQuery<DocumentType> q = em
				.createQuery(
						"SELECT o FROM DocumentType o where o.documentTypeCategory.name = :documentTypeCategoryName order by o.name",
						DocumentType.class);
		q.setParameter("documentTypeCategoryName", documentTypeCategoryName);
		return q.getResultList();

	}

    public static List<DocumentTypeFormat> findAllDocumentTypeFormats() {
		DocumentTypeFormat[] values = DocumentTypeFormat.values();
		List<DocumentTypeFormat> allDocumentTypeFormats = Arrays.asList(values);
	    return allDocumentTypeFormats;
	}
    
    public static List<ReportType> findAllReportTypes() {
    	ReportType[] values = ReportType.values();
		List<ReportType> allReportTypes = Arrays.asList(values);
	    return allReportTypes;
	}

    
    public enum DueType {
    	NONE,
    	PAST_DUE,
    	OK,
    	PAST_DUE_BLUE
    }

}
