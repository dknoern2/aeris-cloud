package com.cars.platform.domain;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findPerformanceViewsByCompany" })
public class PerformanceView {

    @ManyToOne
    private Company company;

    @ManyToMany
    private List<Graph> graphs;

	public static PerformanceView findPerformanceViewsByCompany(Company company) {
		PerformanceView performanceViews = null;
        if (company != null) {
        	EntityManager em = PerformanceView.entityManager();
        	TypedQuery<PerformanceView> q = em.createQuery("SELECT o FROM PerformanceView AS o WHERE o.company = :company", PerformanceView.class);
        	q.setParameter("company", company);
        	performanceViews =  q.getSingleResult();
        }
        return performanceViews;
    }
}
