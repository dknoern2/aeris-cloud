package com.cars.platform.domain;

public enum PeerGroupAccess {
	NONE,
	VIEW,
	FULL
}
