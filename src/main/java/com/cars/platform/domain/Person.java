package com.cars.platform.domain;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findPeopleByUsernameEquals", "findPeopleByCompany", "findPeopleByPersonRole" })
public class Person {

    private String firstName;
    private String lastName;
    private String email;
    private String title;
    private String password;
    @ManyToOne
    private Company company;
    @Enumerated
    private PersonRole personRole = PersonRole.BASIC;

    private String username;
    private String phone;
    private String passwordResetCode;
    private boolean enabled = true;

    private boolean publicContact;
    private boolean emailRemindersEnabled;
    private boolean emailUploadRemindersEnabled;
    private boolean newFinancialRemindersEnabled;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date agreementDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date agreementDateSelect;

    public boolean isAdmin() {
	return PersonRole.ADMIN.equals(personRole);
    }

    public boolean isAnalyst() {
	return ((company != null) && (CompanyType.CARS.equals(company.getCompanyType())));
    }

    public boolean isSubscriber() {
	return ((company != null) && (CompanyType.INVESTOR.equals(company.getCompanyType())));
    }

    public String getFullName() {
	String fullName = "";
	if (firstName != null) {
	    fullName += firstName;
	}
	fullName += " ";
	if (lastName != null) {
	    fullName += lastName;
	}
	return fullName;
    }

    public static List<Person> findAnalysts() {
	TypedQuery<Person> q = entityManager().createQuery("SELECT o FROM Person o where o.company.companyType = :companyType and o.personRole = :personRole", Person.class);
	q.setParameter("companyType", CompanyType.CARS);
	q.setParameter("personRole", PersonRole.BASIC);

	return q.getResultList();
    }

    public static boolean isUsernameInUse(String username) {
	boolean inUse = false;
	try {

	    Person person = Person.findPeopleByUsernameEquals(username);
	    if (null != person) {
		    inUse = true;
	    }
	} catch (Exception ignore) {
	}
	return inUse;
    }


	public static Person findPeopleByUsernameEquals(String username) {
        if (username == null || username.length() == 0) throw new IllegalArgumentException("The username argument is required");
        EntityManager em = Person.entityManager();
        TypedQuery<Person> q = em.createQuery("SELECT o FROM Person AS o WHERE o.username = :username", Person.class);
        q.setParameter("username", username);
        return q.getSingleResult();
    }
}
