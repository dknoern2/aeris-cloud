package com.cars.platform.domain;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * Metrics can be broken down by category. For example outstanding loan data by financial institution
 * @author kurtl
 *
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class MetricBreakdown {
	
	// This is used as the name of the sheet in the Additional Graphs template so need to conform to excel 
	@Size(min = 2, max = 31)
	@Pattern(regexp="[^\\\\\\/?*\\[\\]]*", message="Name can not contain \\ / ? * [ or ]")
	private String name;
	
	@Size(min = 2, max = 30)
	private String label;
	
	// TODO add MetricBreakdownType which would be something like LIST_OF_VALUE, FREE_FORM, YEAR_DELTA
	
    @OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="metricBreakdown")
    @OrderBy("rank ASC")
    private List<MetricBreakdownItem> items;
    
 
    public String getLabel() {
        return this.label;
    }
    
    public void setLabel(String label) {
        this.label = label;
    }

    public List<MetricBreakdownItem> fetchActiveItems() {
    	List<MetricBreakdownItem> activeItems = getItems();
    	Iterator<MetricBreakdownItem> it = activeItems.iterator();
    	while (it.hasNext()) {
    		if (!it.next().isActive()) {
    			it.remove();
    		}
    	}
    	
    	return activeItems;
    }

}
