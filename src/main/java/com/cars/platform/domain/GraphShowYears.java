package com.cars.platform.domain;

public class GraphShowYears {

	public GraphShowYears(int count, String label) {
		super();
		this.count = count;
		this.label = label;
	}
	private int count;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	private String label;
}
