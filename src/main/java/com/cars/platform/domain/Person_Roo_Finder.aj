// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.PersonRole;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect Person_Roo_Finder {
    
    public static TypedQuery<Person> Person.findPeopleByCompany(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = Person.entityManager();
        TypedQuery<Person> q = em.createQuery("SELECT o FROM Person AS o WHERE o.company = :company", Person.class);
        q.setParameter("company", company);
        return q;
    }
    
    public static TypedQuery<Person> Person.findPeopleByPersonRole(PersonRole personRole) {
        if (personRole == null) throw new IllegalArgumentException("The personRole argument is required");
        EntityManager em = Person.entityManager();
        TypedQuery<Person> q = em.createQuery("SELECT o FROM Person AS o WHERE o.personRole = :personRole", Person.class);
        q.setParameter("personRole", personRole);
        return q;
    }
    
        
}
