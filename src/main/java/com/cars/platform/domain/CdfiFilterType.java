package com.cars.platform.domain;

public enum CdfiFilterType {
	RATED("Rated"),
	IMPACT("Impact"),
	PERFORMANCE("Performance"),
	LENDING_TYPE("Lending Type"),
	SECTORAL_FOCUS("Impact Area"),
	TOTAL_ASSETS("Total Assets"),
	LOCATION("Location"),
	AREA_SERVED("Areas Served");
	
	private String displayName;
	
	private CdfiFilterType(String displayName) {
		this.displayName = displayName;
	}
	

	public String getDisplayName() {
		return displayName;
	}

}
