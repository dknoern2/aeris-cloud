package com.cars.platform.domain;

import java.util.Arrays;
import java.util.List;

public enum ReportPage {
	    POSITION,ACTIVITY,SUMMARY,ADDITIONAL,GRAPHS,INPUT,PEER_GROUP,NONE;
	    
	    private static List<ReportPage> managedPages = Arrays.asList(ReportPage.SUMMARY, ReportPage.ADDITIONAL, ReportPage.PEER_GROUP);
	    
	    public static List<ReportPage> getManagedPages() {
	    	return managedPages;
	    }
}
