package com.cars.platform.domain;

import javax.persistence.ManyToOne;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Folder {

    @ManyToOne
    private Company company;

    @ManyToOne
    private com.cars.platform.domain.Folder parent;
}
