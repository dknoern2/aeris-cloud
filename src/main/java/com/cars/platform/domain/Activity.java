package com.cars.platform.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.util.DateHelper;


@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Activity {
	
    @ManyToOne
	private Person person;
    
    @ManyToOne
	private Company company;
    
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;	
	private int year;
	private int quarter;
	private String description;
	
	@Enumerated
	private ActivityType activityType;
	
	@ManyToOne
	private Document document;
	
    public Document getDocument() {
		return document;
	}
	public void setDocument(Document document) {
		this.document = document;
	}



	public static List<Activity> findRecentActivity(Person person) {
        EntityManager em = Activity.entityManager();       
        List<Activity> allActivity = em.createQuery("SELECT o FROM Activity AS o order by o.date desc", Activity.class).getResultList(); 
        
        List<Activity> filteredActvity = new ArrayList<Activity>();
        
        for(Activity activity:allActivity){
        	if(person.getCompany().isCARS()||
        			(
        			activity.getActivityType().equals(ActivityType.DOCUMENT_DOWNLOAD)||
           			activity.getActivityType()==ActivityType.FINANCIALS_VIEW||
           			activity.getActivityType()==ActivityType.METRIC_VALUE_CHANGE_ACCEPT||
           			activity.getActivityType()==ActivityType.METRIC_VALUE_CHANGE_REJECT)
           			&&person.getCompany().equals(activity.getCompany())){
        		filteredActvity.add(activity); 		
        	}
        }          
        return filteredActvity;
    }
    
    
    
    public static List<Activity> findRecentActivityByCompanyType(CompanyType companyType) {

    	EntityManager em = Activity.entityManager();       
    	TypedQuery<Activity> q = em.createQuery("SELECT o FROM Activity AS o where o.person.company.companyType =:companyType order by o.date desc", Activity.class); 
        q.setParameter("companyType", companyType);
        q.setMaxResults(50);
        
        return q.getResultList();
     }
    
    
  
    public static List<Activity> findRecentInvestorActivityForCDFI(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");

    	EntityManager em = Activity.entityManager();       
    	TypedQuery<Activity> q = em.createQuery("SELECT o FROM Activity AS o where o.company =:company and o.person.company.companyType !=:carsCompanyType and o.person.company != company and o.person.company.hideActivity = false order by o.date desc", Activity.class); 
        q.setParameter("company", company);
        q.setParameter("carsCompanyType", CompanyType.CARS);
        q.setMaxResults(10);
        
        return q.getResultList();
     }
  
     
    public static List<Activity> findRecentActivityByCompanyEmployees(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");

    	EntityManager em = Activity.entityManager();       
    	TypedQuery<Activity> q = em.createQuery("SELECT o FROM Activity AS o where o.person.company =:company order by o.date desc", Activity.class); 
        q.setParameter("company", company);
        q.setMaxResults(10);        
        return q.getResultList();
     }
 
    
    public static List<Activity> findActivityByCompanyEmployeesAndType(Company company, ActivityType type) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (type == null) throw new IllegalArgumentException("The type argument is required");

    	EntityManager em = Activity.entityManager();       
    	TypedQuery<Activity> q = em.createQuery("SELECT o FROM Activity AS o where o.person.company =:company and o.activityType = :type order by o.date desc", Activity.class); 
        q.setParameter("company", company);
        q.setParameter("type", type);
        return q.getResultList();
     }

 
   public static List<Activity> findRecentActivityByCompanyTypeForAnalyst(CompanyType companyType,Person person) {

   	EntityManager em = Activity.entityManager();       
   	TypedQuery<Activity> q = em.createQuery("SELECT o FROM Activity AS o join o.company.analysts as a where o.person.company.companyType =:companyType and a = :person order by o.date desc", Activity.class); 
    q.setParameter("companyType", companyType);
    q.setParameter("person", person);
       q.setMaxResults(50);
       
       return q.getResultList();
    }
      
      
    
    public String getSummary(){
    	StringBuffer sb = new StringBuffer();
    	//sb.append("<b>"+person.getFirstName());
    	//sb.append(" ");
    	//sb.append(person.getLastName()+" ("+ person.getCompany().getName()+")</b>");
  	
    	if(activityType.equals(ActivityType.DOCUMENT_CHECKOUT)){
    		sb.append("checked out the document ");
    		sb.append(description);
    		
    	}else if(activityType.equals(ActivityType.DOCUMENT_DOWNLOAD)){  		
    		if(description!=null){
    		    sb.append("downloaded the document ");
    		    sb.append("<i>"+description+"</i>"); 
    		}else{
    		    sb.append("downloaded a document");
    		}
    	}else if(activityType.equals(ActivityType.DOCUMENT_UPLOAD)){	
    		if(description!=null){
    		sb.append("uploaded the document ");
    		sb.append("<i>"+description+"</i>");   
    		}else{
        		sb.append("uploaded a document");	
    		}
    	}else if(activityType.equals(ActivityType.FINANCIALS_VIEW)){
    		sb.append("viewed financial information for ");
    		sb.append(company.getName());        		    		
    	}else if(activityType.equals(ActivityType.MESSAGE)){
    		sb.append("sent a message ");
    		sb.append(company.getName());           		
    	}else if(activityType.equals(ActivityType.METRIC_VALUE_CHANGE)){
    		sb.append("changed the value for ");
    		sb.append(getMetricCommonBlurb());     		
    	}else if(activityType.equals(ActivityType.METRIC_VALUE_CHANGE_REJECT)){
    		sb.append("rejected value change for ");
    		sb.append(getMetricCommonBlurb());      		
    	}else if(activityType.equals(ActivityType.METRIC_VALUE_CHANGE_ACCEPT)){
    		sb.append("accepted a value change for ");
    		sb.append(getMetricCommonBlurb()); 
    	}else if(activityType.equals(ActivityType.METRIC_VALUE_CHANGE_REQUEST)){
    		sb.append("requested value change for ");
    		sb.append(getMetricCommonBlurb()); 
    	}else if(activityType.equals(ActivityType.METRIC_VALUE_FOOTNOTE)){
    		sb.append("made footnote for ");
    		sb.append(getMetricCommonBlurb()); 
    	}else if(activityType.equals(ActivityType.METRIC_VALUE_NOTE)){
    		sb.append("made note for ");
    		sb.append(getMetricCommonBlurb()); 
    	}else if(activityType.equals(ActivityType.ADDITIONAL_DATA_SUBMISSION)){
    		sb.append(description);
    		sb.append(" Additional Data Submission was saved for ");
    		sb.append(company.getName());
    	}
  	
    	sb.append(" ");
    	sb.append(DateHelper.getTimePassed(date));
    	sb.append(".");
    	return sb.toString();
    }
 
    
    private String getMetricCommonBlurb(){
    	StringBuffer sb = new StringBuffer();
		//sb.append(year);
		//sb.append("Q");
		//sb.append(quarter);
		//sb.append(" ");
		sb.append(description);
		sb.append(" for "); 		
		sb.append(company.getName());  
		
		return sb.toString();
    }



	public static Object findRecentActivity(CompanyType companyType, Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (companyType == null) throw new IllegalArgumentException("The companyType argument is required");
		
		EntityManager em = Activity.entityManager();       
    	TypedQuery<Activity> q = em.createQuery("SELECT o FROM Activity AS o where o.person.company.companyType =:companyType and company = :company order by o.date desc", Activity.class); 
        q.setParameter("company", company);
        q.setParameter("companyType", companyType);
        q.setMaxResults(10);
        
        return q.getResultList();
	}



	public static Date findLastAccessTimeByOwnerAndTarget(Company company,
			Company target) {
		
		Date lastAccessTime = null;
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (target == null) throw new IllegalArgumentException("The target argument is required");

    	EntityManager em = Activity.entityManager();       
    	TypedQuery<Activity> q = em.createQuery("SELECT o FROM Activity AS o where o.company =:company and o.person.company =:target order by o.date desc", Activity.class); 
        q.setParameter("company", company);
        q.setParameter("target", target);
        q.setMaxResults(1);
        List<Activity> list = q.getResultList();
        
        if(list!=null&&list.size()>0){
        	lastAccessTime = list.get(0).getDate();
        }
        return lastAccessTime;
	}
    
    
    
}
