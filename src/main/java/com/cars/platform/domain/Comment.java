package com.cars.platform.domain;

import java.util.Date;

import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Comment {

	/**
	 * Indicates whether action is required, as is the case with an adjustment request
	 */
	private boolean pending = false;
	
    private String comment;

    @ManyToOne
    private Person person;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
}
