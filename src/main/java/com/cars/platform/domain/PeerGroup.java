package com.cars.platform.domain;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class PeerGroup {
	
    @NotBlank
    @Size(min=2, max = 100)
    private String name;
    
    // company == null defines it as a global peer group
    // company != null is the subscriber that created the peer group
    @ManyToOne
    private Company company;

    @ManyToMany
    @NotEmpty
    private List<Company> peers;
    
    // If this exists then the PG is a comparison of this CDFI against a set of peers
    @ManyToOne
    private Company cdfi;
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, mappedBy="peerGroup")
    private List<PeerGroupFilter> filters;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date updated;
    
    // For peer groups that are not 'saved' by the user
    private boolean transitory;
    
    private Long copiedFrom = null;
    
    // when going back to the selector for a peer group the report generated
    // will be for a transitory instance. This is a relationship back to the original
    // peer group that will be deleted if the transitory one is saved (updated).
	@ManyToOne
    private PeerGroup original;

	// reverse relationship to remove 'dead' transitory references when an original is deleted
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="original")
    private List<PeerGroup> transitoryReferences;

    
    public boolean isTransitory() {
		return transitory;
	}

	public PeerGroup getOriginal() {
		return original;
	}

	public void setTransitory(boolean transitory) {
		this.transitory = transitory;
	}

	public void setOriginal(PeerGroup original) {
		this.original = original;
	}
  
    
    public Company getCdfi() {
		return cdfi;
	}

	public void setCdfi(Company cdfi) {
		this.cdfi = cdfi;
	}

	/**
     * Provides the list of PeerGroups owned by the company, or global (none) if company is null, and for the CDFI (if provided)   
     * @param company if null only returns global peer groups. If not null returns those owned by the company
     * @param cdfi if provided filters the result set by with the cdfi attribute set to this parameter
     */
    public static List<PeerGroup> findPeerGroupsByCompanyAndCdfi(Company company, Company cdfi) {
        EntityManager em = entityManager();
        TypedQuery<PeerGroup> q;
        if (null != company) {
        	if (null == cdfi) {
                q = em.createQuery("SELECT g FROM PeerGroup AS g WHERE g.company = :company and g.transitory = false and cdfi is null order by updated DESC", PeerGroup.class);
        	}
        	else {
        		q = em.createQuery("SELECT g FROM PeerGroup AS g  WHERE g.cdfi = :cdfi and g.company = :company and g.transitory = false order by updated DESC", PeerGroup.class);        		
                q.setParameter("cdfi", cdfi);
        	}
            q.setParameter("company", company);
        }
        else {
        	if (null == cdfi) {
                q = em.createQuery("SELECT g FROM PeerGroup AS g WHERE g.company is null and g.transitory = false and cdfi is null order by updated DESC", PeerGroup.class);
        	}
        	else {
        		q = em.createQuery("SELECT g FROM PeerGroup AS g WHERE g.cdfi = :cdfi and g.company is null and g.transitory = false order by updated DESC", PeerGroup.class);        		
                q.setParameter("cdfi", cdfi);
        	}
        }
        return q.getResultList();
    }
    
    public static PeerGroup findPeerGroupCopiedFrom(PeerGroup peerGroup, Company subscriber) {
    	EntityManager em = entityManager();
        TypedQuery<PeerGroup> q = em.createQuery("SELECT g FROM PeerGroup AS g WHERE g.company = :company and copiedFrom = :peerGroupId", PeerGroup.class);
        q.setParameter("company", subscriber);
        q.setParameter("peerGroupId", peerGroup.getId());
        return q.getSingleResult();
    }
    
    
    /**
     * Clones all of the global Peer Groups (with or without a CDFI) and assigns them to the provided subscriber
     * Used the first time a Subscriber views its peer group list
     */
    public static List<PeerGroup> cloneGlobal(Company subscriber, Company cdfi) {
    	List<PeerGroup> globals = PeerGroup.findPeerGroupsByCompanyAndCdfi(null, cdfi);
    	
    	List<PeerGroup> clones = new ArrayList<PeerGroup>();
    	for (PeerGroup global : globals) {
    		clones.add(global.cloneForCompany(subscriber));    		
    	}
    	return clones;
    }

	public PeerGroup cloneForCompany(Company subscriber) {
		PeerGroup clone = new PeerGroup();
		clone.name = name;
		clone.cdfi = cdfi;
		clone.created = new Date(); // not shown on the UI so it indicates when they first visited the PG page
		clone.updated = updated; 
		clone.transitory = false;
		clone.original = null;
		clone.copiedFrom = this.getId();
		
		clone.peers = new ArrayList<Company>();
		clone.peers.addAll(peers);
		
		clone.filters = new ArrayList<PeerGroupFilter>();
		for (PeerGroupFilter pgf : filters) {
			clone.filters.add(pgf.cloneToPeerGroup(clone));
		}
		
		clone.company = subscriber;
		
		clone.persist();
		
		return clone;
	}

}
