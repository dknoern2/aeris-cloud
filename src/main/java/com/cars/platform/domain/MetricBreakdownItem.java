package com.cars.platform.domain;
import java.util.List;

import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * The individual items that a metric is broken down into
 * For example if the breakdown is by financial institution type this lists those institution types
 * @author kurtl
 *
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class MetricBreakdownItem {
	
	@Size(min = 2, max = 30)
	private String name;

	// can used for display order
	private int rank;
	
	private boolean active = true;
	
    @ManyToOne
    private MetricBreakdown metricBreakdown;


	public static List<MetricBreakdownItem> findAllMetricBreakdownItems() {
        return entityManager().createQuery("SELECT o FROM MetricBreakdownItem o order by metricBreakdown, rank", MetricBreakdownItem.class).getResultList();
    }
}
