package com.cars.platform.domain;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public abstract class BreakdownGraph {
	
	@NotNull
	@Size(min=2)
	private String title;
	
	@ManyToOne
	@NotNull
	private MetricBreakdown breakdown;
	
	private boolean export = true;
	
	public abstract GraphType getGraphType();
	
	/**
	 * A string representation of the type of graph
	 * Note: can not make this abstract or the roo generated stuff fails to compile
	 */
	public String getType() {
		return null;
	}
}
