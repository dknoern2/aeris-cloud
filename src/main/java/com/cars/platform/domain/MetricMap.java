package com.cars.platform.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findMetricMapsByCompany", "findMetricMapsByMetric" })
public class MetricMap {

    @ManyToOne
    private Metric metric;

    private String labelText;

    private String valueText;
    
    private String foundAfter;
    
    private String foundBefore;
    
    private boolean valuesRight;

    private int labelX;

    private int labelY;

    private int labelZ;

    private int valueX;

    private int valueY;

    private int valueZ;

    @ManyToOne
    private Company company;

    int year;

    int quarter;

    private boolean negate;

    public static MetricMap findMetricMap(Metric metric, Company company, int year, int quarter, String labelText) {
	if (metric == null)
	    throw new IllegalArgumentException("The metric argument is required");
	if (company == null)
	    throw new IllegalArgumentException("The company argument is required");
	EntityManager em = MetricMap.entityManager();
	TypedQuery<MetricMap> q = em
		.createQuery(
			"SELECT o FROM MetricMap AS o WHERE o.metric = :metric AND o.company = :company AND o.year = :year AND o.quarter = :quarter AND o.labelText = :labelText",
			MetricMap.class);
	q.setParameter("metric", metric);
	q.setParameter("company", company);
	q.setParameter("year", year);
	q.setParameter("quarter", quarter);
	q.setParameter("labelText", labelText);
	return q.getSingleResult();
    }

    public static List<MetricMap> findMetricMaps(Company company, int year, int quarter) {
	if (company == null)
	    throw new IllegalArgumentException("The company argument is required");
	EntityManager em = MetricMap.entityManager();
	TypedQuery<MetricMap> q = em.createQuery(
		"SELECT o FROM MetricMap AS o WHERE o.company = :company AND o.year = :year and o.quarter = :quarter",
		MetricMap.class);
	q.setParameter("company", company);
	q.setParameter("year", year);
	q.setParameter("quarter", quarter);
	return q.getResultList();
    }

    public static List<MetricMap> findMetricMaps(Company company, Metric metric, int year, int quarter) {
	if (company == null)
	    throw new IllegalArgumentException("The company argument is required");
	if (metric == null)
	    throw new IllegalArgumentException("The metric argument is required");
	EntityManager em = MetricMap.entityManager();
	TypedQuery<MetricMap> q = em
		.createQuery(
			"SELECT o FROM MetricMap AS o WHERE o.company = :company AND o.metric = :metric AND o.year = :year and o.quarter = :quarter",
			MetricMap.class);
	q.setParameter("company", company);
	q.setParameter("metric", metric);
	q.setParameter("year", year);
	q.setParameter("quarter", quarter);
	return q.getResultList();
    }
    
    public static List<MetricMap> findMetricMapsBefore(Company company, Metric metric, int year, int quarter) {
    	if (company == null)
    	    throw new IllegalArgumentException("The company argument is required");
    	if (metric == null)
    	    throw new IllegalArgumentException("The metric argument is required");
    	EntityManager em = MetricMap.entityManager();
    	TypedQuery<MetricMap> q = em
    		.createQuery(
    			"SELECT o FROM MetricMap AS o WHERE o.company = :company AND o.metric = :metric AND (o.year < :year OR (o.year = :year and o.quarter < :quarter))",
    			MetricMap.class);
    	q.setParameter("company", company);
    	q.setParameter("metric", metric);
    	q.setParameter("year", year);
    	q.setParameter("quarter", quarter);
    	return q.getResultList();
    }

    @Transactional
    public void deleteOlderMetricMaps(Company company, Metric metric, int year, int quarter) {
	if (company == null)
	    throw new IllegalArgumentException("The company argument is required");
	EntityManager em = MetricMap.entityManager();
	Query q = em
		.createQuery("DELETE FROM MetricMap AS o WHERE o.company = :company AND o.metric = :metric AND ((o.year > :year) OR (o.year = :year AND o.quarter >= :quarter))");
	q.setParameter("company", company);
	q.setParameter("metric", metric);
	q.setParameter("year", year);
	q.setParameter("quarter", quarter);

	q.executeUpdate();
    }
    
    @Transactional
    public static void removeMetricMapsFromMetric(Metric metric) {
    	TypedQuery<MetricMap> q = MetricMap.findMetricMapsByMetric(metric);
		List<MetricMap> metricMaps = q.getResultList();
		for(MetricMap metricMap : metricMaps)
		{
			metricMap.remove();
		}
    }

    @Transactional
    public void deleteMetricMaps(Company company, int year, int quarter) {
	if (company == null)
	    throw new IllegalArgumentException("The company argument is required");
	EntityManager em = MetricMap.entityManager();
	Query q = em
		.createQuery("DELETE FROM MetricMap AS o WHERE o.company = :company AND o.year = :year AND o.quarter = :quarter");
	q.setParameter("company", company);
	q.setParameter("year", year);
	q.setParameter("quarter", quarter);

	q.executeUpdate();
    }
}
