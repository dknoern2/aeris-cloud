package com.cars.platform.domain;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class State {

	private String name;
	private String stateCode;

    public static List<State> findAllStatesAlphabetically() {
        EntityManager em = State.entityManager();
        return em.createQuery("SELECT o FROM State o order by o.stateCode", State.class).getResultList();
    }
}
