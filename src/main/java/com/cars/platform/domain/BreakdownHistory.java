package com.cars.platform.domain;
import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * Shows a history of data from a single equation. Can be represented in table, line, or bar chart format
 * @author kurtl
 *
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class BreakdownHistory extends BreakdownSingle {
	
	private boolean showInterim;
	
	@NotNull
	@Min(2)
	@Max(10)
	private int showYears = 5;
		
	@NotNull
	private GraphType graphType;

	@Override
	public GraphType getGraphType() {
		return graphType;
	}

	@Override
	public String getType() {
		switch (graphType) {
		case LINE: return "History Line Chart";
		case COLUMN: return "History Column Chart";
		case TABLE: return "History Table";
		default: return "UNKNOWN";
		}
	}
	
	public static List<GraphType> getAvailableGraphTypes() {
		return Arrays.asList(GraphType.COLUMN, GraphType.LINE, GraphType.TABLE);
	}
}
