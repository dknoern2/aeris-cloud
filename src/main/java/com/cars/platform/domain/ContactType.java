package com.cars.platform.domain;

public enum ContactType {

    RATINGS, FINANCIALS, LIBRARY;
}
