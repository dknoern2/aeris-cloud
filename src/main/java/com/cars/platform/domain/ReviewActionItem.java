package com.cars.platform.domain;

public class ReviewActionItem extends ActionItem {
	public ReviewActionItem(Review review){
		this.review = review;
	}
	
	private Review review;
	public Review getReview(){
		return review;
	}
}
