// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricMap;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect MetricMap_Roo_Finder {
    
    public static TypedQuery<MetricMap> MetricMap.findMetricMapsByCompany(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = MetricMap.entityManager();
        TypedQuery<MetricMap> q = em.createQuery("SELECT o FROM MetricMap AS o WHERE o.company = :company", MetricMap.class);
        q.setParameter("company", company);
        return q;
    }
    
    public static TypedQuery<MetricMap> MetricMap.findMetricMapsByMetric(Metric metric) {
        if (metric == null) throw new IllegalArgumentException("The metric argument is required");
        EntityManager em = MetricMap.entityManager();
        TypedQuery<MetricMap> q = em.createQuery("SELECT o FROM MetricMap AS o WHERE o.metric = :metric", MetricMap.class);
        q.setParameter("metric", metric);
        return q;
    }
    
}
