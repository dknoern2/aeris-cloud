package com.cars.platform.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class DocumentTypeFolder {

	public static final String CARS_REPORT = "AERIS - Report";
    public static final String CARS_RESOURCES = "AERIS - Resources";
    public static final String CARS_INTERNAL = "AERIS - Internal";
    public static final String CARS_INTERNAL_ARCHIVE = "AERIS - Internal Archive";
    public static final String CARS_ARCHIVE = "AERIS - Archive";
	public static final String CARS_PREFIX = "AERIS";
	
    @NotNull
    @Size(min = 2)
    public String name;
    
    public boolean cdfiResponsible() {
    	return !(name.startsWith(CARS_PREFIX));
    }
}
