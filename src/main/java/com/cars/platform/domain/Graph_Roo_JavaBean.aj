// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cars.platform.domain;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Graph;
import com.cars.platform.domain.GraphEquation;
import com.cars.platform.domain.GraphType;
import com.cars.platform.domain.UnitType;
import java.util.List;

privileged aspect Graph_Roo_JavaBean {
    
    public Company Graph.getCompany() {
        return this.company;
    }
    
    public void Graph.setCompany(Company company) {
        this.company = company;
    }
    
    public String Graph.getTitle() {
        return this.title;
    }
    
    public void Graph.setTitle(String title) {
        this.title = title;
    }
    
    public String Graph.getCode() {
        return this.code;
    }
    
    public void Graph.setCode(String code) {
        this.code = code;
    }
    
    public GraphType Graph.getGraphType() {
        return this.graphType;
    }
    
    public void Graph.setGraphType(GraphType graphType) {
        this.graphType = graphType;
    }
    
        
        
    public List<GraphEquation> Graph.getGraphEquations() {
        return this.graphEquations;
    }
    
    public void Graph.setGraphEquations(List<GraphEquation> graphEquations) {
        this.graphEquations = graphEquations;
    }
    
    public UnitType Graph.getUnitType() {
        return this.unitType;
    }
    
    public void Graph.setUnitType(UnitType unitType) {
        this.unitType = unitType;
    }
    
    public boolean Graph.isShowInterim() {
        return this.showInterim;
    }
    
    public void Graph.setShowInterim(boolean showInterim) {
        this.showInterim = showInterim;
    }
    
    public int Graph.getShowYears() {
        return this.showYears;
    }
    
    public void Graph.setShowYears(int showYears) {
        this.showYears = showYears;
    }    
}
