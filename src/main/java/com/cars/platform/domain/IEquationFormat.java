package com.cars.platform.domain;

/**
 * Interface containing the methods necessary for formatting the result of an equation evaluation
 * @author kurtl
 *
 */
public interface IEquationFormat {
	public UnitType getUnitType();
	public Integer getDecimalPlaces();
}
