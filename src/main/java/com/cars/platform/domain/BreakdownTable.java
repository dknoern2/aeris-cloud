package com.cars.platform.domain;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cars.platform.domain.constraint.CheckBreakdownTable;


/**
 * A breakdown table which contains data for multiple equations
 * @author kurtl
 *
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
@CheckBreakdownTable
public class BreakdownTable extends BreakdownGraph {
	
    @ManyToMany
    @OrderBy("rank ASC")
    @Size(min=1)
    private List<BreakdownColumn> columns;

	@Override
	public GraphType getGraphType() {
		return GraphType.TABLE;
	}

	@Override
	public String getType() {
		return "Table";
	}

}
