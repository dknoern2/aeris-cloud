package com.cars.platform.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findFootnotesByCompany" })
public class Footnote {

    @NotNull
    private String label;

    @ManyToOne
    private Person person;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    
    @NotNull
    @Size(max = 1000)
    private String comment;

    @NotNull
    @ManyToOne
    private Company company;

    @NotNull
    @Enumerated
    private ReportPage reportPage;
    
    
    
    public static Footnote findFootnoteByCompanyAndLabel(Company company,String label) {
    	
    	Footnote footnote = null;
    	
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        if (label == null) throw new IllegalArgumentException("The label argument is required");
        EntityManager em = Footnote.entityManager();
        TypedQuery<Footnote> q = em.createQuery("SELECT o FROM Footnote AS o WHERE o.company = :company AND o.label = :label", Footnote.class);
        q.setParameter("company", company);
        q.setParameter("label", label);
        
        List<Footnote> list = q.getResultList();
        if(list.size()>0){
        	footnote = list.get(0);
        }      
        return footnote;
    }
    
    public static TypedQuery<Footnote> findFootnotesByCompanySorted(Company company) {
        if (company == null) throw new IllegalArgumentException("The company argument is required");
        EntityManager em = Footnote.entityManager();
        TypedQuery<Footnote> q = em.createQuery("SELECT o FROM Footnote AS o WHERE o.company = :company order by o.reportPage, o.label", Footnote.class);
        q.setParameter("company", company);
        return q;
    }
   

    
    
}
