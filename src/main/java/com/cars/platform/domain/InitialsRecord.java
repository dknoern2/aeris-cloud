package com.cars.platform.domain;

import java.util.Date;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

//@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findInitialsRecordByReviewAndDocumentType" })
public class InitialsRecord {
	@ManyToOne
	@NotNull
	private DocumentType documentType;
	@ManyToOne
	@NotNull
	private Review review;
	@ManyToOne
	@NotNull
	private Company company;
	@Size(max=10, min=2)
	private String initials;
	private Date dateStamp = new Date();

	public DocumentType getDocumentType() {
		return documentType;
	}
	public void setDocumentType(DocumentType value) {
		documentType = value;
	}
	public Review getReview() {
		return review;
	}
	public void setReview(Review value) {
		review = value;
	}
	public String getInitials() {
		return initials;
	}
	public void setInitials(String value) {
		initials = value;
	}
	public Date getDateStamp() {
		return dateStamp;
	}
	public void setDateStamp(Date dateStamp) {
		this.dateStamp = dateStamp;
	}
	public Company getCompany(){
		return company;
	}
	public void setCompany(Company company){
		this.company = company;
	}
}
