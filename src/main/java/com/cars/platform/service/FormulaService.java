package com.cars.platform.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.script.ScriptException;

import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricCategory;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.util.DateHelper;
import com.cars.platform.viewmodels.EquationUnit;

public class FormulaService {
	

    private static final Logger logger = LoggerFactory.getLogger(FormulaService.class);

    private static final String MONTH = "MONTH";

    private static final String HARD_STOP = Metric.HARD_STOP.toString();
    
    private static Pattern formulaSplit = Pattern.compile("[\\+\\-\\*\\/\\(\\)]+");

   
    private static Pattern remove0Plus = Pattern.compile("(?<!\\d)0\\+"); // matches 0+
    private static Pattern remove0Minus = Pattern.compile("(?<!\\d)0\\-"); // matches 0-
    private static Pattern removePlusOrMinus0 = Pattern.compile("(\\+|\\-)0(?!\\d)"); // matches -0 or +0
    

    int fiscalYearStart = 1;
    
    Set<Equation> visitedEquations = null;
    
    public FormulaService(int fiscalYearStart) {
    	this.fiscalYearStart = fiscalYearStart;
    }
    
    
    public Double crunchFormula(String formula, String period, HashMap<String, MetricValue> metricValueMap,
    	    Collection<Period> periods, Company company) throws NumberFormatException, ScriptException, EvaluationException
    {
    	FormulaResult result = crunchFormulaWithSpread(formula, period, metricValueMap, periods, company);
    	return null == result ? null : result.result;
    }

    /**
     * replaces the text of any referenced formulas prior to final evaluation
     */
    public FormulaResult crunchFormulaWithSpread(String formula, String period, HashMap<String, MetricValue> metricValueMap,
    	    Collection<Period> periods, Company company) throws NumberFormatException, ScriptException, EvaluationException
    {
    	visitedEquations = new HashSet<Equation>();
    	
    	formula = formula.toUpperCase(); // allow for lower case F and P
    	formula = formula.replaceAll(" ", ""); // so replaceSubFormulas is called only once when there are no sub formulas
    	
    	while (true) {
        	String newFormula = replaceSubFormulas(formula, company);
        	if (newFormula.equals(formula)) {
        	    break; // no sub formulas were found so we are done
        	}
        	formula = newFormula;
    	}
    	
    	return crunchFinalFormulaWithSpread(formula, period, metricValueMap, periods);
    }
    
    private String replaceSubFormulas(String formula, Company company) throws NumberFormatException, EvaluationException
    {
    	formula = formula.replaceAll(" ", "");
    	String[] x = formulaSplit.split(formula);
    	for (String token : x) {
    		if (token.endsWith("F")) {

    		    String equationIdString = token.substring(0, token.indexOf("F"));
    		    Long equationId = Long.parseLong(equationIdString);
    		    Equation equation = Equation.findEquation(equationId);
    		    if (null == equation) {
    		    	throw new EvaluationException("There is no formula with ID: " + equationIdString);
    		    }
    		    Equation override = equation.getCompanyOverride(company);
    		    if (null != override) {
    		    	equation = override;
    		    }
    		    if (visitedEquations.contains(equation)) {
    		    	throw new EvaluationException("This formula contains a circular reference to Equation with ID: " + equation.getId());
    		    }
    		    visitedEquations.add(equation);
    		    String subFormula = equation.getFormula();
    		    formula = formula.replaceAll("(?<!\\d)" + token, "(" + subFormula + ")");
    		}
    	}
    	
    	return formula;
    }



    /**
     * Crunch the CARS formula Formula should be
     * 
     * @param formula
     *            of format (1010#+1020#)/1030# where # is wildcard
     * @param period
     *            if format 3/31/2012
     * @param metricValueMap
     *            a map of metric values where key is period|accountCode example
     *            3/31/2012|101012
     * @param periods
     *            - Should be sorted!
     * @return null if no matching metric values
     * @throws NumberFormatException
     * @throws ScriptException
     * @throws EvaluationException
     */
    private FormulaResult crunchFinalFormulaWithSpread(String formula, String period, HashMap<String, MetricValue> metricValueMap,
	    Collection<Period> periods) throws NumberFormatException, ScriptException, EvaluationException {
	formula = formula.replaceAll(" ", "");
	String spreadFormula = new String(formula);
	String[] x = formulaSplit.split(formula);

	boolean atLeastOneValueFound = false;

	for (String token : x) {
	    double value = 0;
	    String adjustedPeriod = period;

	    if (MONTH.equals(token)) {
		try {
		    value = getMonths(period, periods);
		    formula = formula.replace(token, "(" + value + ")");
		    spreadFormula = spreadFormula.replace(token, "(" + value + ")");
		    atLeastOneValueFound = true;
		} catch (ParseException e) {
		    logger.error("", e);
		}
	    } else if (token.length() > 3) {

		if (token.endsWith("P")) {
		    try {
		    Period lastPeriod = getPriorYearPeriod(period, periods);
		    adjustedPeriod = (lastPeriod == null) ? "" : lastPeriod.getEndDate();
		    } catch (ParseException e) {
			e.printStackTrace();
		    }
		}

		String target = adjustedPeriod + "|" + token;
		char spreadIndicator = '#';
		if (target.endsWith("#") || target.endsWith("P")) {
		    if (target.endsWith("#")) {
				target = target.substring(0, target.indexOf('#'));
		    }
		    if (target.endsWith("P")) {
				target = target.substring(0, target.indexOf("P"));
				spreadIndicator = 'P';
		    }

		    // key format is "3/31/2012|101011"
		    ArrayList<MetricValue> matches = new ArrayList<MetricValue>();
		    for (String key : metricValueMap.keySet()) {

				if (key.startsWith(target)) {
				    atLeastOneValueFound = true;
				    MetricValue metricValue = metricValueMap.get(key);
				    if (metricValue != null && metricValue.getAmountValue() != null) {
				    	double metricAmount = metricValueMap.get(key).getAmountValue();
				    	value += metricAmount;
				    	// TODO re-evaluate this if/when InputTab export is done in terms of double values as opposed to strings (ReportService deprecation)
				    	// For export we reference cells on the Input sheet. For small (typically zero) values these cells are set to '-' which we can not reference in a formula
				    	if (Math.abs(metricAmount) >= AbstractReportService.SMALLEST_DISPLAYED_VALUE) {
				    		matches.add(metricValue);
				    	}
				    	else {
				    		logger.debug("Ignoring metric value [" +  metricAmount + "] for metric [" + metricValue.getMetricName() + "]");
				    	}
				    }
				}
		    }

		    formula = formula.replaceAll("(?<!\\d)" + token, "(" + value + ")");
		    if (matches.size() > 0) {
		    	StringBuilder accountCodes = new StringBuilder(matches.get(0).getMetric().getFullAccountCode() + spreadIndicator);
		    	for (int i=1; i<matches.size(); i++) {
		    		accountCodes.append(","+ matches.get(i).getMetric().getFullAccountCode() + spreadIndicator);
		    	}
		    	if (matches.size() == 1) {
			    	spreadFormula = spreadFormula.replaceAll("(?<!\\d)" + token, accountCodes.toString());				    		
		    	}
		    	else {
			    	spreadFormula = spreadFormula.replaceAll("(?<!\\d)" + token, "SUM(" + accountCodes.toString() + ")");				    				    		
		    	}
		    }
		    else {
		    	spreadFormula = spreadFormula.replaceAll("(?<!\\d)" + token, "0");
		    }
		} else if (target.endsWith(HARD_STOP)) {

		    target = target.substring(0, target.indexOf(HARD_STOP));
		    String accountCode = "0";

		    // key format is "3/31/2012|101011"
		    for (String key : metricValueMap.keySet()) {
				if (key.equals(target)) {
				    atLeastOneValueFound = true;
				    MetricValue metricValue = metricValueMap.get(key);
				    if (metricValue != null && metricValue.getAmountValue() != null) {
				    	value += metricValueMap.get(key).getAmountValue();
				    	if (Math.abs(value) >= AbstractReportService.SMALLEST_DISPLAYED_VALUE) {
					    	accountCode = metricValue.getMetric().getFullAccountCode() + spreadIndicator;
				    	}
				    }
				    break; // hardstop should only find 1 since it is an exact match
				}
		    }

		    formula = formula.replaceAll("(?<!\\d)" + token, "(" + value + ")");
		    spreadFormula = spreadFormula.replaceAll("(?<!\\d)" + token, accountCode);
		}
	    }
	}

	// return null if no value found so we can leave it off the spread
	if (!atLeastOneValueFound) {
	    return null;
	}

	Evaluator evaluator = new Evaluator();
	Double result = null;

	try {
	    result = evaluator.getNumberResult(formula);

	} catch (EvaluationException e) {
	    logger.error("cant evaluate: " + formula);
	    throw e;

	}
	
	// For cases where there is no metric value (or the value is actually 0) 0 is used in the formula.
	// This results in cases that look something like 1010001#+0+0+0+1010002#
	// so, we want to remove the superfluous adding/subracting of zero, careful not to remove something like 10+ which
	// could occur if there was a literal number or MONTH included in the formula
	// also need to leave something like 0/(1010001#) or (ugh) divide by zero
	// Lastly can't remove the 0- part of the following (0-101001#) as it would change the 
	// A good regex testing site http://www.regexplanet.com/advanced/java/index.html
	spreadFormula = removePlusOrMinus0.matcher(spreadFormula).replaceAll(""); // removes all -0 or +0
	spreadFormula = remove0Plus.matcher(spreadFormula).replaceAll(""); // removes 0+
	spreadFormula = remove0Minus.matcher(spreadFormula).replaceAll("-"); // replace 0- with just -

	return new FormulaResult(result, spreadFormula);

    }


	// private String getPriorYearEndString(String period, int fiscalYearStart2)
    // throws ParseException {
    // return DateHelper.getPriorYearEndString(period, fiscalYearStart);
    // }

    public static Period getPriorYearPeriod(String periodEndDate, Collection<Period> periods) throws ParseException {

	Period priorYearPeriod = null;
	Period currentPeriod = null;

	for (Period p : periods) {
	    if (periodEndDate.equals(p.getEndDate())) {
		currentPeriod = p;
		break;
	    }
	}

	if (currentPeriod == null)
	    return null;

	// found current period, now find previous year end if possible

	if (currentPeriod.getQuarter() > Period.LAST_QUARTER) {
	    // "STUB year" special case
	    for (Period p : periods) {
		if (p.getYear() == currentPeriod.getYear() && p.getQuarter() == currentPeriod.getQuarter() - 1) {
		    priorYearPeriod = p;
		}
	    }

	} else {
	    // normal period (non-STUB)
	    for (Period p : periods) {
		if (p.getYear() == currentPeriod.getYear() - 1 && p.getQuarter() >= Period.LAST_QUARTER) {
		    priorYearPeriod = p;
		}
	    }
	}

	return priorYearPeriod;

    }

    public String explainFormula(String formula) {

	try {
	    formula = formula.replaceAll(" ", "");
	    String[] x = formulaSplit.split(formula);

	    for (String x1 : x) {
		if (x1.length() > 0) {

		    if (x1.endsWith("#") && x1.length() < 6) {
			MetricCategory metricCategory = MetricCategory.findMetricCategoryByFullAccountCode(x1
				.substring(0, 4));
			String expression = "sum(" + metricCategory.getName() + ")";
			formula = formula.replace(x1, expression);
		    } else {
			Metric metric = Metric.findMetricByFullAccountCode(x1).getSingleResult();
			String expression = metric.getName();
			formula = formula.replace(x1, expression);
		    }
		}
	    }
	} catch (Exception e) {
	    return "UNABLE TO PARSE";
	}

	return formula;
    }

    public List<EquationUnit> transformFormula(String formula) throws Exception {
    	List<EquationUnit> equationUnits = new ArrayList<EquationUnit>();
    	try {
    		formula = formula.replaceAll(" ", "");
    		formula = formula.replaceAll(Pattern.quote("("), "|(|");
    		formula = formula.replaceAll(Pattern.quote(")"), "|)|");
    		formula = formula.replaceAll(Pattern.quote("+"), "|+|");
    		formula = formula.replaceAll(Pattern.quote("-"), "|-|");
    		formula = formula.replaceAll(Pattern.quote("*"), "|*|");
    		formula = formula.replaceAll(Pattern.quote("\\"), "|\\|");
    		formula = formula.replaceAll(Pattern.quote("/"), "|/|");

    		// String[] x = formula.split("[\\+\\-\\*\\/\\(\\)]+");
    		String[] x = formula.split("[\\|]+");

    		for (String x1 : x) {
    			if (x1.length() > 0) {

    				if (x1.equals("+")) {

    					equationUnits.add(new EquationUnit("+", "+", "OPERATOR"));

    				} else if (x1.equals("-")) {
    					equationUnits.add(new EquationUnit("-", "-", "OPERATOR"));

    				} else if (x1.equals("*")) {
    					equationUnits.add(new EquationUnit("*", "*", "OPERATOR"));

    				} else if (x1.equals("\\")) {
    					equationUnits.add(new EquationUnit("\\", "\\", "OPERATOR"));

    				} else if (x1.equals("/")) {
    					equationUnits.add(new EquationUnit("/", "/", "OPERATOR"));

    				} else if (x1.equals("(")) {

    					equationUnits.add(new EquationUnit("(", "(", "START_GROUPING"));

    				} else if (x1.equals(")")) {

    					equationUnits.add(new EquationUnit(")", ")", "END_GROUPING"));

    				}

    				else if ((x1.endsWith("#") || x1.endsWith("P") || x1.endsWith(HARD_STOP)) && x1.length() < 6) {
    					String accountCode = x1.substring(0, 4);

    					MetricCategory metricCategory = MetricCategory.findMetricCategoryByFullAccountCode(accountCode);
    					String suffix = "#";
    					if (x1.endsWith("P")) {
    						suffix = "P";
    					}
    					if (x1.endsWith(HARD_STOP)) {
    						suffix = HARD_STOP;
    					}

    					equationUnits.add(new EquationUnit(metricCategory.getName(), accountCode + suffix, "CATEGORY"));

    				} else if ((x1.endsWith("#") || x1.endsWith("P") || x1.endsWith(HARD_STOP)) && x1.length() > 6) {
    					String suffix = "#";
    					if (x1.endsWith("P")) {
    						suffix = "P";
    						// x1 = x1.replace("P", "#");
    					}
    					if (x1.endsWith(HARD_STOP)) {
    						suffix = HARD_STOP;
    					}

    					try {
    						Metric metric = Metric.findMetricByFullAccountCode(x1).getSingleResult();
    						equationUnits.add(new EquationUnit(metric.getName(), metric.getFullAccountCode() + suffix,
    								"METRIC"));
    					} catch (Exception e) {
    						equationUnits.add(new EquationUnit(x1, x1, "VALUE"));
    					}
    				} else if (x1.endsWith("F")) {
    					String id = x1.substring(0, x1.length()-1);
    					Equation equation = Equation.findEquation(Long.valueOf(id));
    					equationUnits.add(new EquationUnit(equation.getName(), id + "F", "FORMULA"));
    				} else {
    					equationUnits.add(new EquationUnit(x1, x1, "VALUE"));
    				}
    			}
    		}
    	} catch (Exception e) {
    		throw e;
    	}

    	return equationUnits;
    }

    int getMonths(String endDate, Collection<Period> periods) throws ParseException {
    	Period prevAuditPeriod = null;
    	for (Period period : periods) {
    		if (endDate.equals(period.getEndDate())) {
    			break;
    		}
    		if (PeriodType.AUDIT.equals(period.getPeriodType()) || PeriodType.INTERNAL.equals(period.getPeriodType())) {
    			prevAuditPeriod = period;
    		}
    	}

    	if (prevAuditPeriod == null) {
    		return DateHelper.getQuarter(fiscalYearStart, endDate) * Period.MONTHS_IN_QUARTER;
    	} else {
    		return DateHelper.getMonthsBetween(endDate, prevAuditPeriod.getEndDate());
    	}
    }
    
    public class FormulaResult {
    	public Double result;
    	public String spreadFormula;
    	
    	public FormulaResult(Double result, String spreadFormula) {
    		this.result = result;
    		this.spreadFormula = spreadFormula;
    	}
    }

}
