package com.cars.platform.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;


import com.cars.platform.domain.Access;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.PeerGroup;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodDim;
import com.cars.platform.domain.ReportCategory;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;
import com.cars.platform.domain.UnitType;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.util.DateHelper;
import com.cars.platform.viewmodels.graph.Graph;
import com.cars.platform.viewmodels.graph.LineChart;
import com.cars.platform.viewmodels.graph.PieChart;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Table;
import com.cars.platform.viewmodels.graph.Value;
import com.cars.platform.viewmodels.peergroup.CDFI;
import com.cars.platform.viewmodels.peergroup.CdfiCategory;
import com.cars.platform.viewmodels.peergroup.CdfiMetric;
import com.cars.platform.viewmodels.peergroup.PeerMetricBase;
import com.cars.platform.viewmodels.peergroup.PeerMetrics;
import com.cars.platform.viewmodels.peergroup.Report;

public class PeerGroupReportService extends AbstractGraphReportService {
	
	public static final String TOP_QUARTILE = "Top Quartile";
	public static final String MEDIAN = "Median";
	public static final String BOTTOM_QUARTILE = "Bottom Quartile";
	
	private PeerGroup peerGroup;
	
	// An ordered Map of ReportCategories and their global equations
	private Map<ReportCategory, List<Equation>> categoryEquations = null;
	
	// all of the equations (global, no overrides)
	private List<Equation> allEquations = null;
	
	private TreeSet<PeriodDim> periods = null;
	
	// list is by date (PeriodDim)
	// The Equations in categoryEquations and allEquations are global.
	// The facts in this collection are for the CDFI overrides if they exist. This allows for the 
	// name of the equations to be the global value, necessary since multiple peers could
	// have overrides with different names.
	private List<Map<Equation, Map<Company, ValueFact>>> listOFactByCompanyByEquation = null;
	
	// TODO potential optimization not to do this for each company
	Map<Company, Map<Equation, Equation>> overridesByCompany = null;
	
	
	public PeerGroupReportService(PeerGroup peerGroup, boolean showIncomplete) {
		this(peerGroup, true, true, showIncomplete);
	}
	
	
	public PeerGroupReportService(PeerGroup peerGroup) {
		this(peerGroup, true, true, false);
	}
	
	
	public PeerGroupReportService(PeerGroup peerGroup, boolean allYears, boolean showInterim, boolean showIncomplete) {
		super(peerGroup.getCdfi());
		this.peerGroup = peerGroup;
		// Originally the CDFI if it existed had to exist as one of the peers but now it is a separate attribute.
		// rather than refactor a bunch of code we add it back here (not to be persisted)
		List<Company> peers = peerGroup.getPeers();
		if (null != getCdfi() && !peers.contains(getCdfi())) {
			peers.add(getCdfi());
		}
		getReportCategories();
		
		periods = determinePeriods(allYears, showInterim, showIncomplete);
		
    	overridesByCompany = preFetchOverrides();
	}
	
	private Company getCdfi() {
		return company;
	}
	
	public Report getReportData() {		
		
		//fetchValueFacts();
		
		Report report = new Report();
		
		report.setCdfi(getCdfiData());
		report.setFilters(peerGroup.getFilters());
		report.setDates(getDateLabels());
		report.setPeerNames(getPeerNames());
		report.setRatings(getRatings());
		report.setPeerMetrics(getPeerMetrics());

		return report;
	}
	
    
    
    private void getReportCategories() {
    	categoryEquations = new LinkedHashMap<ReportCategory, List<Equation>>();
    	allEquations = new ArrayList<Equation>();
    	
    	List<ReportCategory> categories = ReportCategory.findByReportPageAndParent(ReportPage.PEER_GROUP, null);
    	for (ReportCategory category : categories) {
    		List<Equation> equations = category.getEquations(null, false);
    		categoryEquations.put(category, equations);
    		allEquations.addAll(equations);
    	}    	
    }
	
    
    private TreeSet<PeriodDim> determinePeriods(boolean allYears, boolean showInterim, boolean showIncomplete) {
		TreeSet<PeriodDim> resultSet = new TreeSet<PeriodDim>();
		
    	if (null != getCdfi()) {
    		for (Period period : Period.findViewablePeriodsByCompanySorted(getCdfi())) {
    			if (period.isComparable(showInterim)) {
    				PeriodDim periodDim = PeriodDim.findByPeriod(period);
    				resultSet.add(periodDim);
    			}
    		}
    	}
    	else {
			for (Company peer : peerGroup.getPeers()) {
	    		for (Period period : Period.findViewablePeriodsByCompanySorted(peer)) {
	    			if (period.isComparable(showInterim)) {
	    				PeriodDim periodDim = PeriodDim.findByPeriod(period);
	    				resultSet.add(periodDim);
	    			}
	    		}
	    		
			}
    	}
    	
    	//By default, don't show results for not completed years
    	if (!showIncomplete) {
    		Iterator<PeriodDim> iterator = resultSet.descendingIterator();
    		while (iterator.hasNext()) {
    			PeriodDim periodDim = iterator.next();
    			if (periodDim.getYear() < DateHelper.getCurrentYear()) {
    				break;
    			}
    			
    			iterator.remove();
       		}
    	}
    	
    	// are we showing only the last 5 FYE?
    	if (!allYears) {
    		int numFYE = 0;
    		for (PeriodDim periodDim : resultSet) {
    			if (periodDim.getQuarter() >= Period.LAST_QUARTER) {
    				numFYE++;
    			}
    		}
    		
    		Iterator<PeriodDim> iterator = resultSet.iterator();
    		while (iterator.hasNext()) {
    			if (numFYE <= 5) {
    				break;
    			}
    			PeriodDim periodDim = iterator.next();
    			if (periodDim.getQuarter() >= Period.LAST_QUARTER) {
    				iterator.remove();
    				numFYE--;
    			}
    		}
    	}
		
		return resultSet;
    }

    
    /**
     * Fills the list of facts by company by equation collection
     * Note that the Equation key here is the global equation. The ValueFact can be for a company specific override
     */
    private void fetchValueFacts() {
    	//Map<Company, Map<Pair<PeriodDim, Equation>, ValueFact>> allValueFacts = preFetchValueFacts();
    	
		listOFactByCompanyByEquation = new ArrayList<Map<Equation, Map<Company, ValueFact>>>();
		TreeSet<PeriodDim> periodsWithData = new TreeSet<PeriodDim>();
		for (PeriodDim periodDim : periods) {
			Map<Equation, Map<Company, ValueFact>> factByCompanyByEquation = 
					new HashMap<Equation, Map<Company, ValueFact>>();
			boolean factFound = false;
    		for (Equation equation : allEquations) {
    			HashMap<Company, ValueFact> factByCompany = new HashMap<Company, ValueFact>();
    			factByCompanyByEquation.put(equation, factByCompany);
    			for (Company peer : peerGroup.getPeers()) {
        			//Equation override = equation.getCompanyOverride(peer);
    				Map<Equation, Equation> overrideMap = overridesByCompany.get(peer);
    				Equation override = overrideMap.get(equation);
        			if (null == override) {
        				override = equation;
        			}
        			// TODO: handle case where fact's PeriodType does not match that of selected periods to compare
// --- fetching these individually is bad but fetching all facts for a company is even worse
// --- as it simply creates too many ValueFact beans. There are way more formulas created
// --- than we need for the peer group report. Using an 'in' clause for the list of equations doesn't
// --- fly either as due to the override we don't know the list of equations to get for each peer.
//        			Map<Pair<PeriodDim, Equation>, ValueFact> factsByPE = allValueFacts.get(peer);
//        			if (null == factsByPE) {
//        				continue;
//        			}
//        			Pair<PeriodDim, Equation> pair= new ImmutablePair<PeriodDim, Equation>(periodDim, override);
//        			ValueFact valueFact = factsByPE.get(pair);
    				ValueFact valueFact = ValueFact.findValueFact(periodDim, override, peer);
    				if (null != valueFact) {
    					factFound = true;
    				}
    				factByCompany.put(peer, valueFact);
    			}
    		}
    		// There are cases where periods are created for company's but they do not have any data yet
    		if (factFound) {
    			periodsWithData.add(periodDim);
    			listOFactByCompanyByEquation.add(factByCompanyByEquation);
    		}
		}
		periods = periodsWithData;
    }
    
    
    // Originally we were fetching each ValueFact individually. Fetching them all for each peer for performance
    // ---- actaully see comment above this made it even worse as we were creating too many unused ValueFact beans
    private Map<Company, Map<Pair<PeriodDim, Equation>, ValueFact>> preFetchValueFacts() {
    	Map<Company, Map<Pair<PeriodDim, Equation>, ValueFact>>  result = new HashMap<Company, Map<Pair<PeriodDim, Equation>, ValueFact>>();
    	
    	for (Company peer : peerGroup.getPeers()) {
    		Map<Pair<PeriodDim, Equation>, ValueFact> factsByPE = new HashMap<Pair<PeriodDim, Equation>, ValueFact>();
    		result.put(peer, factsByPE);
    		for (PeriodDim periodDim : periods) {
	    		List<ValueFact> valueFacts = ValueFact.findForCompanyAndPeriod(peer, periodDim);
	    		if (null == valueFacts || valueFacts.size() == 0) {
	    			continue;
	    		}
	    		for (ValueFact valueFact : valueFacts) {
	    			Pair<PeriodDim, Equation> pair = new ImmutablePair<PeriodDim, Equation>(valueFact.getPeriodDim(), valueFact.getEquation());
	    			factsByPE.put(pair, valueFact);
    		    }
    		}
    	}	
    	return result;
    }
    
    
    
    /**
     * return a  list of facts by company for the provided equation. list is in the order of Periods we show
     * Note that the Equation key here is the global equation. The ValueFact can be for a company specific override
     */
    private List<Map<Company, ValueFact>>  fetchValueFacts(Equation equation) {
    	List<Map<Company, ValueFact>> listOfFactByCompany = new ArrayList<Map<Company, ValueFact>>();
		TreeSet<PeriodDim> periodsWithData = new TreeSet<PeriodDim>();
		for (PeriodDim periodDim : periods) {
			Map<Company, ValueFact> factByCompany = new HashMap<Company, ValueFact>();
			boolean factFound = false;
			for (Company peer : peerGroup.getPeers()) {
    			//Equation override = equation.getCompanyOverride(peer);
				Map<Equation, Equation> overrideMap = overridesByCompany.get(peer);
				Equation override = overrideMap.get(equation);
    			if (null == override) {
    				override = equation;
    			}
				ValueFact valueFact = ValueFact.findValueFact(periodDim, override, peer);
				if (null != valueFact) {
					factFound = true;
				}
				factByCompany.put(peer, valueFact);
			}

    		// There are cases where periods are created for company's but they do not have any data yet
    		if (factFound) {
    			periodsWithData.add(periodDim);
    		}
    		// TODO add this only if there is a factFound... needs UI change
    		listOfFactByCompany.add(factByCompany);
		}
		
		return listOfFactByCompany;
    }


    
    Map<Company, Map<Equation, Equation>> preFetchOverrides() {
    	 Map<Company, Map<Equation, Equation>> result = new HashMap<Company, Map<Equation, Equation>>();
     	 for (Company peer : peerGroup.getPeers()) {
     		 Map<Equation, Equation> overrideMap = new HashMap<Equation, Equation>();
     		 result.put(peer, overrideMap);
     		 List<Equation> overrides = Equation.findOverridesForCompany(peer);
     		 if (null != overrides && overrides.size() > 0) {
     			 for (Equation override : overrides) {
     				 overrideMap.put(override.getOverride(), override);
     			 }
     		 }
     	 }
     	 return result;
    }
    
    
    private CDFI getCdfiData() {
    	if (null == getCdfi()) {
    		return null;
    	}
    	
    	CDFI reportCdfi = new CDFI(getCdfi());
    	List<CdfiCategory> reportCategories = new ArrayList<CdfiCategory>();
    	
    	for (Map.Entry<ReportCategory, List<Equation>> entry : categoryEquations.entrySet()) {
    		ReportCategory reportCategory = entry.getKey();
    		List<Equation> equations = entry.getValue();
    		CdfiCategory cdfiCategory = new CdfiCategory();
    		cdfiCategory.setName(reportCategory.getName());
    		cdfiCategory.setMetrics(getCdfiMetricList(equations));
    		reportCategories.add(cdfiCategory);
    	}
    	
    	reportCdfi.setCategories(reportCategories);
    	return reportCdfi;
    }

    
    private List<CdfiMetric> getCdfiMetricList(List<Equation> equations) {
    	List<CdfiMetric> metrics = new ArrayList<CdfiMetric>();
    	
    	// Create the CdfiMetrics which will have their metric values filled as we iterate through the value collection 
    	Map<Equation, CdfiMetric> metricMap = new HashMap<Equation, CdfiMetric>();
    	for (Equation equation : equations) {
    		String equationName = equation.getName(); // This will be the 'global' name as equation is not the override
    		CdfiMetric cdfiMetric = new CdfiMetric(equationName);
    		metricMap.put(equation, cdfiMetric);
    		metrics.add(cdfiMetric);
    	}
    	
		Map<Equation, Equation> overrideMap = overridesByCompany.get(getCdfi());

		// TODO don't include periods that don't have data
		for (PeriodDim periodDim : periods) {
    		for (Equation equation : metricMap.keySet()) {
				Equation override = overrideMap.get(equation);
    			if (null == override) {
    				override = equation;
    			}
				ValueFact valueFact = ValueFact.findValueFact(periodDim, override, getCdfi());
    			String valueString;
    			if (null == valueFact) {
    			    valueString = "-";
    			}
    			else {
        			valueString = getValueString(valueFact.getAmount(), equation); // todo get override here?	
    			}
    			
    			List<String> metricValues = metricMap.get(equation).getValues();
    			metricValues.add(valueString);	 
    		}
    	}
		return metrics;
    }

    
    private List<CdfiMetric> getCdfiMetricListOld(List<Equation> equations) {
    	List<CdfiMetric> metrics = new ArrayList<CdfiMetric>();
    	
    	// Create the CdfiMetrics which will have their metric values filled as we iterate through the value collection 
    	Map<Equation, CdfiMetric> metricMap = new HashMap<Equation, CdfiMetric>();
    	for (Equation equation : equations) {
    		String equationName = equation.getName(); // This will be the 'global' name as equation is not the override
    		CdfiMetric cdfiMetric = new CdfiMetric(equationName);
    		metricMap.put(equation, cdfiMetric);
    		metrics.add(cdfiMetric);
    	}
    	
    	for (Map<Equation, Map<Company, ValueFact>> factByCompanyByEquation : listOFactByCompanyByEquation) {
    		for (Equation equation : metricMap.keySet()) {
    			Map<Company, ValueFact> factByCompany = factByCompanyByEquation.get(equation);
    			ValueFact valueFact = factByCompany.get(getCdfi());
    			String valueString;
    			if (null == valueFact) {
    			    valueString = "-";
    			}
    			else {
        			valueString = getValueString(valueFact.getAmount(), equation); // todo get override here?	
    			}
    			
    			List<String> metricValues = metricMap.get(equation).getValues();
    			metricValues.add(valueString);	 
    		}
    	}
		return metrics;
    }
    


    // TODO !!! periodType could be different for each CDFI!!!
    private List<String> getDateLabels() {
    	List<String> labels = new ArrayList<String>();
    	for (PeriodDim period : periods) {
    		String label = getDateString(period.getYear(), period.getQuarter());
    		labels.add(label);
    	}
    	return labels;
    }
    
    
    private List<String> getPeerNames() {
    	List<String> names = new ArrayList<String>();
    	for (Company peer : peerGroup.getPeers()) {
    		if (peer.equals(getCdfi())) {
    			continue;
    		}
    		names.add(peer.getName());
    	}
    	return names;
    }
    
    
	private List<PieChart> getRatings() {
		Map<String, Integer> impactMap = new LinkedHashMap<String, Integer>();
		for (String impact : Review.GetAllowedFullImpactValues()) {
			impactMap.put(impact, 0);
		}
		Map<String, Integer> strengthMap = new LinkedHashMap<String, Integer>();
		for (String strength : Review.GetAllowedFullFinancialStrengthValues()) {
			strengthMap.put(strength, 0);
		}
		for (Company peer : peerGroup.getPeers()) {
			Review review = peer.getMostRecentCompletedFullAnalysisReview();
			if (null != review) {
				Date date = review.getDateOfAnalysis();
				String impact = review.getImpactPerformance();
				Integer impactCount = impactMap.get(impact);
				if (null != impactCount) {
					impactMap.put(impact, impactCount+1);
				}
				String strength = review.getFinancialStrength();
				Integer strengthCount = strengthMap.get(strength);
				if (null != strengthCount) {
					strengthMap.put(strength, strengthCount+1);
				}
			}
		}
		PieChart impactPie = getPieChart(impactMap, "Impact Performance");
		impactPie.setTitle("Distribution of Current Impact Performance Ratings");
		//impactPie.setDate("CHANGEME"); // No good single value for date here

		PieChart strengthPie = getPieChart(strengthMap, "Financial Strength");
		strengthPie.setTitle("Distribution of Current Financial Strength Ratings");
		//strengthPie.setDate("CHANGEME"); // No good single value for date here

		
		List<PieChart> ratings = new ArrayList<PieChart>();
		ratings.add(impactPie);
		ratings.add(strengthPie);
		return ratings;
	}
	
	
	private PieChart getPieChart(Map<String, Integer> ratingMap, String ratingName) {
		PieChart ratingPie = new PieChart();
		ratingPie.setUnitType(UnitType.NUMBER);
		ratingPie.setDecimalPlaces(0);
		
		Row columnHeader = new Row();
		columnHeader.setLabel(ratingName);
		Value headerValue = new Value("Count");
		List<Value> headerValues = new ArrayList<Value>();
		headerValues.add(headerValue);
		columnHeader.setValues(headerValues);
		ratingPie.setColumnHeaders(columnHeader);
		
		List<Row> datas = new ArrayList<Row>();
		for (String rating : ratingMap.keySet()) {
			Row row = new Row();
			row.setLabel(rating);
			List<Value> values = new ArrayList<Value>();
			Integer count = ratingMap.get(rating);
			if (count == 0) continue;
			Value value = new Value((double)count, ratingPie, Integer.toString(count));
			values.add(value);
			row.setValues(values);
			datas.add(row);
		}
		ratingPie.setDatas(datas);
		
		return ratingPie;
	}
	
	
	
	private List<PeerMetricBase> getPeerMetrics() {
		List<PeerMetricBase> peerMetrics = new ArrayList<PeerMetricBase>();
		for (Equation equation : allEquations) {
			peerMetrics.add(new PeerMetricBase(equation));
		}
		return peerMetrics;
	}
	

    public PeerMetrics getPeerMetric(Equation equation) {
		PeerMetrics peerMetrics = new PeerMetrics(equation); // This is the global name as equation is not an override
		
		List<Map<Company, ValueFact>> listOfFactByCompany = fetchValueFacts(equation);

		LineChart lineChart = new LineChart();
		lineChart.setUnitType(equation.getUnitType());
		lineChart.setDecimalPlaces(equation.getDecimalPlaces());
		fillGraph(equation, lineChart, false, listOfFactByCompany);
		peerMetrics.setLineChart(lineChart);
		
		// TODO fillGraph is kinda expensive... we could create a deep constructor on Graph
		Table table = new Table();
		fillGraph(equation, table, showPeersInTable(), listOfFactByCompany);
		peerMetrics.setTable(table);			
		return peerMetrics;
	}
    
    
    
    private boolean showPeersInTable() {
    	if (null == person) {
    		return false;
    	}
    	
    	if (person.isAdmin()) {
    		return true;
    	}
    	
    	if (person.isSubscriber()) {
    		return canSubscriberSeePeerMetrics(person.getCompany());	
    	}
    	
    	return false; // CDFI or Analyst
    }
    
    
    private boolean canSubscriberSeePeerMetrics(Company subscriber) {
		List<Subscription> activeOwnedSubscriptions = Subscription.findActiveOwnedSubscriptionsForSubscriber(person.getCompany());
		for (Subscription subscription : activeOwnedSubscriptions) {
			// with an ALL subscription with showPeerMetrics... you are good to go
			if (subscription.getSubscriptionType().equals(SubscriptionType.ALL)) {
				if (subscription.isPeerGroups() && subscription.isShowPeerMetrics()) {
					return true;
				}
			}
			
			// for SINGLE subscription this must be a CDFI type of peer group,
			// and the isShowPeerMetrics must be set,
			// and the CDFI associated to the subscription must be this CDFI
			else if (subscription.getSubscriptionType().equals(SubscriptionType.SINGLE)) {
				if (null == getCdfi()) {
					continue;
				}
				if (!subscription.isPeerGroups() || !subscription.isShowPeerMetrics()) {
					continue;
				}
				for (Access access : subscription.getCdfis()) {
					if  (access.getCompany().equals(getCdfi())) {
						return true;
					}
				}
			}
		}
		
		return false;
    }
    
    
    private void fillGraph(Equation equation, Graph graph, boolean showPeers, List<Map<Company, ValueFact>> listOfFactByCompany) {

		graph.setTitle(equation.getName());
		List<Row> graphDatas = new ArrayList<Row>();
		graph.setDatas(graphDatas);
		
		Row headerRow = new Row();
		List<Value> headerValues = new ArrayList<Value>();
		for (String date : this.getDateLabels()) {
			headerValues.add(new Value(date));
		}
		headerRow.setValues(headerValues);
		graph.setColumnHeaders(headerRow);

		// Create the initial Peer row objects to have their values filled in as we traverse the fact collection
		Map<Company, List<Value>> metricDataMap = new HashMap<Company, List<Value>>();
		for (Company peer : peerGroup.getPeers()) {
			Row peerRow = new Row();
			peerRow.setLabel(peer.getName());
			List<Value> peerValues = new ArrayList<Value>();
			peerRow.setValues(peerValues);
			if (showPeers || peer.equals(this.getCdfi())) {
			    graphDatas.add(peerRow);
			}
			metricDataMap.put(peer, peerValues);
		}
		
		// create the bottom/median/top rows
		Row bottomRow = new Row();
		bottomRow.setLabel(BOTTOM_QUARTILE);
		List<Value> bottomValues = new ArrayList<Value>();
		bottomRow.setValues(bottomValues);
		graphDatas.add(bottomRow);
		Row medianRow = new Row();
		medianRow.setLabel(MEDIAN);
		List<Value> medianValues = new ArrayList<Value>();
		medianRow.setValues(medianValues);
		graphDatas.add(medianRow);
		Row topRow = new Row();
		topRow.setLabel(TOP_QUARTILE);
		List<Value> topValues = new ArrayList<Value>();
		topRow.setValues(topValues);
		graphDatas.add(topRow);
		
		// loop through each period 
    	//for (Map<Equation, Map<Company, ValueFact>> factByCompanyByEquation : listOFactByCompanyByEquation) {
        for (Map<Company, ValueFact> factByCompany : listOfFactByCompany) {
    		List<Double> allValues = new ArrayList<Double>();
 			
			// add the value for each peer
			//Map<Company, ValueFact> factByCompany = factByCompanyByEquation.get(equation);
			for (Company peer : factByCompany.keySet()) {				
    			ValueFact valueFact = factByCompany.get(peer);
    			Double amount;
    			String valueString;
    			if (null == valueFact) {
    				valueString = "";
    				amount = null;
    			}
    			else {
        			amount = valueFact.getAmount();
        			valueString = getValueString(amount, equation); // TODO use override here for formatting? the value is correct.
        			allValues.add(amount);	
    			}
    			List<Value> peerValues = metricDataMap.get(peer);
    			peerValues.add(new Value(amount, equation, valueString));
    		}
			
			// calculate the bottom/top/median, which mean different things depending on the equation
			Double[] sortedValues = allValues.toArray(new Double[allValues.size()]);
			Arrays.sort(sortedValues);
			double[] finalValues = ArrayUtils.toPrimitive(sortedValues);
			if (equation.isSmallerBetter()) {
				bottomValues.add(getPercentile(finalValues, equation, 75));
				topValues.add(getPercentile(finalValues, equation, 25));				
			}
			else {
				bottomValues.add(getPercentile(finalValues, equation, 25));
				topValues.add(getPercentile(finalValues, equation, 75));
			}
			medianValues.add(getPercentile(finalValues, equation, 50));
    	}
	}

    
    
	private Value getPercentile(double[] allValues, Equation equation, double percentile) {
		if (allValues.length == 0) {
			return new Value(null, equation, null);
		}
		
		Percentile p = new Percentile();		

		double result = p.evaluate(allValues, percentile);

        return new Value(result, equation, getValueString(result, equation));
	}

}