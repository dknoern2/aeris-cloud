package com.cars.platform.service.export;

import com.cars.platform.util.ExcelHelper;
import com.cars.platform.viewmodels.graph.Chart;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Value;
import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;

public abstract class ChartProcessor<T extends Chart> extends GraphProcessor<Chart> {

	public ChartProcessor(Chart chart, WorkBook wb, SheetContext sheetContext) {
		super(chart, wb, sheetContext);
	}
	
	@Override
	boolean createsFigure() {
		return true;
	}

	@Override
	Range fillTableData(int startCol) throws Exception {
		int startRow = sheetContext.irow;
		int maxCol = startCol;
		
		// column headers
		Row headerRow = graph.getColumnHeaders();
		maxCol = fillTextRow(headerRow, startCol, maxCol);

		// data rows
		for (Row dataRow : graph.getDatas()) {
			maxCol = fillDataRow(dataRow, startCol, maxCol);
		}
		
		// No totals
		return new Range(startRow, startCol, sheetContext.irow-1, maxCol);
	}
	
	
	// TODO push up
	private int fillTextRow(Row row, int startCol, int maxCol) throws Exception {
		if (null == row) {
			return maxCol;
		}
		
		int icol = startCol;
		wb.setText(sheetContext.irow, icol++, row.getLabel());
		
		for (Value value : row.getValues()) {
			wb.setText(sheetContext.irow, icol++, value.getValue());				
		}
		
		maxCol = (icol-1>maxCol) ? icol-1 : maxCol;
		sheetContext.irow++;
		
		return maxCol;
	}
	
	// TODO push up
	private int fillDataRow(Row row, int startCol, int maxCol) throws Exception {
		if (null == row) {
			return maxCol;
		}
		
		int icol = startCol;
		wb.setText(sheetContext.irow, icol++, row.getLabel());
		
		for (Value value : row.getValues()) {
			Double amount = value.getAmount();
			if (null != amount) {
				wb.setNumber(sheetContext.irow, icol, value.getAmount());
			}
			icol++;
		}
		
		maxCol = (icol-1>maxCol) ? icol-1 : maxCol;
		sheetContext.irow++;
		
		return maxCol;
	}
	
	
	@Override
	protected void formatTable(Range tableRange) throws Exception {
		try{
		
		super.formatTable(tableRange);
		
		String cellFormat = ExcelHelper.getCellFormat(graph.getUnitType() , graph.getDecimalPlaces());

		RangeStyle dataStyle =
				wb.getRangeStyle(tableRange.row1+1, tableRange.col1+1, tableRange.row2, tableRange.col2);
		dataStyle.setCustomFormat(cellFormat);
		wb.setRangeStyle(dataStyle, tableRange.row1+1, tableRange.col1+1, tableRange.row2, tableRange.col2);
		} catch(Exception e){
			int a = 0;
			a=a;
		}
	}

}
