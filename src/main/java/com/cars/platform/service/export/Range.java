package com.cars.platform.service.export;

import com.cars.platform.util.ExcelHelper;

/**
 * Simple data class representing a range on a xls sheet
 * smartxls has a RangeArea class but it has no public constructor
 * @author kurtl
 *
 */
class Range {
	int row1;
	int col1;
	int row2;
	int col2;
	
	public Range(){};
	
	public Range(int row1, int col1, int row2, int col2) {
		this.row1 = row1;
		this.col1 = col1;
		this.row2 = row2;
		this.col2 = col2;
	}
	
	public Range getCopy() {
		return new Range(row1, col1, row2, col2);
	}
	
	public String getExcelRange() {
		return ExcelHelper.getRange(row1, col1, row2, col2);
	}
}
