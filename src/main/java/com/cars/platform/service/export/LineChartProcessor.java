package com.cars.platform.service.export;

import com.cars.platform.domain.GraphType;
import com.cars.platform.util.ExcelHelper;
import com.cars.platform.viewmodels.graph.LineChart;
import com.smartxls.ChartShape;
import com.smartxls.WorkBook;


public class LineChartProcessor extends ChartProcessor<LineChart> {

	public LineChartProcessor(LineChart lineChart, WorkBook wb, SheetContext sheetContext) {
		super(lineChart, wb, sheetContext);
		// TODO Auto-generated constructor stub
	}

	@Override
	void formatChart(ChartShape chart) throws Exception {
		chart.setChartType(ChartShape.Line);
		chart.setAutoMinimumScale(ChartShape.YAxis, 0, false);
		chart.setLinkRange(getTableRangeAsString(), true);
	}


}
