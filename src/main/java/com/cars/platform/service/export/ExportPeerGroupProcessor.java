package com.cars.platform.service.export;

import static com.cars.platform.service.PeerGroupReportService.BOTTOM_QUARTILE;
import static com.cars.platform.service.PeerGroupReportService.MEDIAN;
import static com.cars.platform.service.PeerGroupReportService.TOP_QUARTILE;

import com.cars.platform.domain.Company;
import com.cars.platform.viewmodels.graph.Chart;
import com.cars.platform.viewmodels.peergroup.PeerMetrics;
import com.smartxls.ChartShape;
import com.smartxls.WorkBook;

public class ExportPeerGroupProcessor extends GraphProcessor<Chart> {
	
	private LineChartProcessor lineChartProcessor;
	private TableProcessor tableProcessor;
	private Company company;

	public ExportPeerGroupProcessor(PeerMetrics peerMetrics, WorkBook wb, SheetContext sheetContext, Company company) {
		super(peerMetrics.getLineChart(), wb, sheetContext);
		lineChartProcessor = new LineChartProcessor(peerMetrics.getLineChart(), wb, sheetContext);
		tableProcessor = new TableProcessor(peerMetrics.getTable(), wb, sheetContext);
		this.company = company;
	}

	@Override
	boolean createsFigure() {
		return lineChartProcessor.createsFigure();
	}

	@Override
	Range fillTableData(int startCol) throws Exception {
		return tableProcessor.fillTableData(startCol);
	}
	
	@Override
	protected void buildTable() throws Exception {
		tableProcessor.buildTable();
	}

	/**
	 * If company is null (global peer group), show only last 3 lines on chart - bottom quartile, median and top quartile
	 * Else add data of the company.
	 */
	@Override
	void formatChart(ChartShape chart) throws Exception {
		chart.setChartType(ChartShape.Line);
		chart.setAutoMinimumScale(ChartShape.YAxis, 0, false);
		
		Range range = tableProcessor.getTableRange().getCopy();
		final int row1 = range.row1;
	    // skip labels column
		range.col1++;

		final int rowsPaddingFromBottomWithCompany = 3;
		final int rowsPaddingFromBottomWithoutCompany = 2;
		int chartRowNumber = 0;
		
		if (company != null) {
			chart.addSeries();
			chart.setSeriesName(chartRowNumber, company.getName());
			range.row1 = range.row2 - rowsPaddingFromBottomWithCompany;
			range.row2 -= rowsPaddingFromBottomWithCompany;
			chart.setSeriesYValueFormula(chartRowNumber, range.getExcelRange());
			
			chartRowNumber++;
			range.row1++;
			range.row2++;
		} else {
			range.row1 = range.row2 - rowsPaddingFromBottomWithoutCompany;
			range.row2 -= rowsPaddingFromBottomWithoutCompany;
		}
		
		chart.addSeries();
		chart.setSeriesName(chartRowNumber, BOTTOM_QUARTILE);
		chart.setSeriesYValueFormula(chartRowNumber, range.getExcelRange());
		chartRowNumber++;
		
		chart.addSeries();
		chart.setSeriesName(chartRowNumber, MEDIAN);
		range.row1++;
		range.row2++;
		chart.setSeriesYValueFormula(chartRowNumber, range.getExcelRange());
		chartRowNumber++;
		
		chart.addSeries();
		chart.setSeriesName(chartRowNumber, TOP_QUARTILE);
		range.row1++;
		range.row2++;
		chart.setSeriesYValueFormula(chartRowNumber, range.getExcelRange());
		
		range.row1 = row1;
		range.row2 = row1;
		chart.setCategoryFormula(range.getExcelRange());
    }

}
