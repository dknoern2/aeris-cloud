package com.cars.platform.service.export;


import static com.cars.platform.service.ReportService.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.CdfiFilterType;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.PeerGroup;
import com.cars.platform.domain.PeerGroupFilter;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.service.FormulaService;
import com.cars.platform.service.PeerGroupReportService;
import com.cars.platform.service.ReportService;
import com.cars.platform.service.ReportService2;
import com.cars.platform.service.graph.GraphService;
import com.cars.platform.service.graph.breakdown.BreakdownGraphService;
import com.cars.platform.util.ExcelHelper;
import com.cars.platform.util.ExportHelper;
import com.cars.platform.util.SmartXLSHelper;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.FootnotePOJO;
import com.cars.platform.viewmodels.graph.PieChart;
import com.cars.platform.viewmodels.peergroup.CdfiCategory;
import com.cars.platform.viewmodels.peergroup.CdfiMetric;
import com.cars.platform.viewmodels.peergroup.ExportRequest;
import com.cars.platform.viewmodels.peergroup.PeerMetricBase;
import com.cars.platform.viewmodels.peergroup.PeerMetrics;
import com.cars.platform.viewmodels.report.Category;
import com.cars.platform.viewmodels.report.Data;
import com.cars.platform.viewmodels.report.Equation;
import com.cars.platform.viewmodels.report.Report;
import com.smartxls.DataValidation;
import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;

public class ExportService {
	
	private static final String INPUT_DATA_PAGE  = "Input Data";
	
    private static final Logger logger = LoggerFactory.getLogger(ExportService.class);
    
    public static final int PERIOD_END_DATE_HEADER_ROW = 3;
    public static final int FIRST_DATA_COLUMN = 2;

	private static String[] letters = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
		"q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

	private Company company = null;
	

	// all footnotes for financials
	List<FootnotePOJO> footnotes = null;
	
	Map<Period, Integer> inputPagePeriodHeaders = new HashMap<Period,Integer>();

	LinkedHashMap<String, String> renderedFootnotes = new LinkedHashMap<String, String>();
	// test
	int maxCol = 0; // merge cells for footnote to display
	int maxFootnoteIndex = 0;
	ReportService reportService = null;
	ReportService2 reportService2 = null;
	
	SmartXLSHelper smartXlsHelper = null;
	
	public ExportService() {
		smartXlsHelper = new SmartXLSHelper();
	}

	public void exportFinancials(Company company, OutputStream outputStream,
			boolean allRows, boolean showChange, boolean showPctChange, boolean showAllYears) throws Exception {

		this.company = company;

		WorkBook wb = new WorkBook();
		wb.insertSheets(0, 6);

		reportService = new ReportService(company, allRows, false);
		reportService2 = new ReportService2(company);
		

		footnotes = reportService.getFootnotes();

		int sheetNumber = 0;
		List<String> periods = reportService.getPeriodsWithFootnoteIndicators();
		
		buildPage(wb, sheetNumber++, company.getName(), INPUT_TAB, periods,
				reportService.getReport(ReportPage.INPUT));
		buildPage(wb, sheetNumber++, company.getName(),  POSITION_TAB, periods,
				reportService.getReport(ReportPage.POSITION));
		buildPage(wb, sheetNumber++, company.getName(), ACTIVITY_TAB, periods,
				reportService.getReport(ReportPage.ACTIVITY));
		buildPageNew(wb, sheetNumber++, company.getName(), SUMMARY_TAB,
				periods, reportService2.getReportData(ReportPage.SUMMARY, false, false));
		buildPageNew(wb, sheetNumber++, company.getName(), ADDITIONAL_TAB,
				periods, reportService2.getReportData(ReportPage.ADDITIONAL, false, false));
		buildGraphs(wb, sheetNumber++, company);
		buildAdditionalGraphs(wb, sheetNumber, company);
		
		
		// Now hide columns that weren't being shown online
				int numPeriodsToHide = 0;
				if (!showAllYears) {
					int numAuditPeriods = 0;
					for (String period : periods) {
						PeriodType periodType = reportService.getPeriodByEndDateMap().get(period).getPeriodType();
						if (PeriodType.AUDIT.equals(periodType)) {
							numAuditPeriods++;
						}
					}
					numPeriodsToHide = Math.max(0, numAuditPeriods - 5);
					wb.setSheet(0); // Input tab doesn't have change columns
					for (int icol = 2; icol<2+(numPeriodsToHide); icol++) {
						wb.setColHidden(icol, true);			
					}
				}
				for (sheetNumber = 1; sheetNumber <= 4; sheetNumber++) {
					wb.setSheet(sheetNumber);
					hideColumns(wb, showChange, showPctChange, numPeriodsToHide);
				}

		for (int sheetNum = 0; sheetNum <= 4; sheetNum++) {
			wb.setSheet(sheetNum);
			wb.insertRange(0, 2, 0, 2, WorkBook.ShiftColumns);
			wb.setColWidth(2, 8000);
			wb.freezePanes(0, 0, 4, 3, false);
		}
		wb.setSheet(0);
		if (UserHelper.getCurrentUser().getCompany().getId() != company.getId()
				&& !UserHelper.getCurrentUser().getCompany().isCARS()) {
			//wb.deleteSheets(0, 1); // since we are referencing this sheet in formulas it has to be present
			wb.setSheet(1);
		}
		try {
			wb.writeXLSX(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void exportPeerGroup(PeerGroup peerGroup, ExportRequest exportRequest, OutputStream outputStream) throws Exception
	{
		this.company = peerGroup.getCdfi();
		
		WorkBook wb = new WorkBook();
		wb.setNumSheets(company==null ? 1 : 2);
		wb.setSheet(0);
		wb.setSheetName(0, "Peer Group Report");
		
		if (null != company) {
		    writeCompanyName(wb);
		}
		
		PeerGroupReportService pgrs;
		if (null == exportRequest) {
			pgrs = new PeerGroupReportService(peerGroup);
		}
		else {
			pgrs = new PeerGroupReportService(peerGroup, exportRequest.isAllYears(), exportRequest.isShowInterim(), exportRequest.isShowIncomplete());
		}
		com.cars.platform.viewmodels.peergroup.Report report = pgrs.getReportData();
		
		int irow = 3;
		wb.setText(irow, 0, "Number of Peers: " + peerGroup.getPeers().size());
		smartXlsHelper.boldCellPrefix(wb, irow, 0, 16);
		
	    irow = 5;
	    wb.setText(irow, 0, "Peers");
	    smartXlsHelper.boldCellPrefix(wb, irow, 0, 5);
	    irow++;
	    for (Company peer : peerGroup.getPeers()) {
	    	wb.setText(irow, 1, peer.getName());
	    	irow++;
		}
		irow++;
		List<PeerGroupFilter> filters = peerGroup.getFilters();
		if (null != filters && filters.size() > 0) {
			wb.setText(irow,  0, "Peer Criteria");
			setBoldForCell(wb, irow, 0);
			irow++;
			for (PeerGroupFilter filter : filters) {
				String name = filter.getDisplayName();
				if (filter.getFilter() == CdfiFilterType.TOTAL_ASSETS) {
					if (filter.getValues().length == 2) {
						wb.setText(irow, 1, name + ": $" + filter.getValues()[0] + " to $" + filter.getValues()[1]);
						smartXlsHelper.boldCellPrefix(wb, irow, 1, name.length()+1);
						irow++;
					}
				} else {
					wb.setText(irow, 1, name + ": " + StringUtils.join(filter.getValues(), ", "));
					smartXlsHelper.boldCellPrefix(wb, irow, 1, name.length()+1);
					irow++;
				}
			}
		}		
		irow++;		
				
		int tableNumber = 1;
		int figureNumber = 1;
		SheetContext sheetContext = new SheetContext(0, tableNumber, figureNumber, irow);
		
		for (PieChart pieChart : report.getRatings()) {
			PieChartProcessor processor = new PieChartProcessor(pieChart, wb, sheetContext);
			sheetContext = processor.process();
			irow = sheetContext.irow;
		}
	
		
		List<Long> equationIds = new ArrayList<Long>();
		if (null != exportRequest && null != exportRequest.getEquationIds()) {
			equationIds = Arrays.asList(exportRequest.getEquationIds());
		}
		boolean equationRendered = false;
		for (PeerMetricBase peerMetricBase : report.getPeerMetrics()) {
			Long equationId = peerMetricBase.getEquationId();
			if (equationIds.contains(equationId)) {
				equationRendered = true;
				com.cars.platform.domain.Equation equation =
						com.cars.platform.domain.Equation.findEquation(equationId);
				PeerMetrics peerMetrics = pgrs.getPeerMetric(equation);
				ExportPeerGroupProcessor processor = new ExportPeerGroupProcessor(peerMetrics, wb, sheetContext, company);
				sheetContext = processor.process();
				irow = sheetContext.irow;
			}			
		}
		
		if (!equationRendered) {
			wb.setText(irow, 0, "No peer analysis metrics were selected. Please return to the platform and expand the metrics you would like to export");
			setBoldForCell(wb, irow, 0);
			this.setFontSizeForCell(wb, irow, 0, 14);
		}
		
		
		// widths are 3350*inches
		wb.setColWidth(0, 1880); // .56"
		wb.setColWidth(1, 7923); // 2.36"
		for (int icol = 2; icol < 12; icol++) {
			wb.setColWidth(icol, 3400);
		}
		
		if (company != null) {
			int row = 3;
			wb.setSheet(1);
			wb.setSheetName(1, "Performance Metrics");
			int ncol = report.getDates().size();
			// bold header line
			RangeStyle style = wb.getRangeStyle(row, 0, row, ncol);
			style.setFontBold(true);
			style.setTopBorder(RangeStyle.BorderMedium);
			style.setBottomBorder(RangeStyle.BorderMedium);
			wb.setRangeStyle(style, row, 0, row, ncol);
		
			int col = 0;
			wb.setColWidth(col, 20000);
			wb.setText(row, col++, report.getCdfi().getName() + " Performance Metrics");
			
		    for (String period : report.getDates()) {
		    	wb.setColWidth(col, 3400);
				wb.setText(row, col++, period);
			}
			
			for (CdfiCategory category : report.getCdfi().getCategories()) {
				row = buildCateogry(wb, row, category, true);
			}
			
			// right justify headers
			style = wb.getRangeStyle(3, 1, row, ncol);
			style.setFontBold(true);
			style.setHorizontalAlignment(RangeStyle.HorizontalAlignmentRight);
			wb.setRangeStyle(style, 3, 1, row, ncol);
		}
		
		wb.setSheet(0);
		
		try {
			wb.write(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}



	
	private void buildGraphs(WorkBook wb, int sheetNumber, Company company) throws Exception {
		wb.setSheet(sheetNumber);
		wb.setSheetName(sheetNumber, "Graphs");

		writeCompanyName(wb);
		
		GraphService graphService = new GraphService(company);
		
		List<com.cars.platform.viewmodels.graph.Graph> graphs = graphService.getGraphs(false, true);
		
		renderGraphs(wb, sheetNumber, graphs);
	}

	
	private void buildAdditionalGraphs(WorkBook wb, int sheetNumber, Company company) throws Exception {
		wb.setSheet(sheetNumber);
		wb.setSheetName(sheetNumber, "Additional Graphs");

		writeCompanyName(wb);
		
		BreakdownGraphService graphService = new BreakdownGraphService(company);
		
		List<com.cars.platform.viewmodels.graph.Graph> graphs = graphService.getBreakdownGraphs(false, true);
		
		renderGraphs(wb, sheetNumber, graphs);
	}

	
	private void renderGraphs(WorkBook wb, int sheetNumber,
			List<com.cars.platform.viewmodels.graph.Graph> graphs)
			throws Exception {
		int tableNumber = 1;
		int figureNumber = 1;
		int irow = 3;
		SheetContext sheetContext = new SheetContext(sheetNumber, tableNumber, figureNumber, irow);
		
		for (com.cars.platform.viewmodels.graph.Graph graph : graphs) {
			GraphProcessor processor = GraphProcessor.getProcessor(graph, wb, sheetContext);
			if (null != processor) {
				sheetContext = processor.process();
				irow = irow + 2;
			}
		}
		
		// widths are 3350*inches
		wb.setColWidth(0, 1880); // .56"
		wb.setColWidth(1, 7923); // 2.36"
		for (int icol = 2; icol < 12; icol++) {
			wb.setColWidth(icol, 3400);
		}
	}




	protected void setBoldForCell(WorkBook wb, int irow, int icol) {
		RangeStyle rangeStyle;
		try {
			rangeStyle = wb.getRangeStyle(irow, icol, irow, icol);
			rangeStyle.setFontBold(true);
			wb.setRangeStyle(rangeStyle, irow, icol, irow, icol);
		} catch (Exception ignore) {
		}
	}

	protected void setFontSizeForCell(WorkBook wb, int irow, int icol, int fontSize) {
		RangeStyle rangeStyle;
		try {
			rangeStyle = wb.getRangeStyle(irow, icol, irow, icol);
			rangeStyle.setFontSize(fontSize * 20);
			wb.setRangeStyle(rangeStyle, irow, icol, irow, icol);
		} catch (Exception ignore) {
		}
	}

	private void buildPage(WorkBook wb, int sheetNumber, String companyName, String sheetName, List<String> periods,
			ArrayList<ArrayList<String>> data) throws Exception {

		wb.setSheet(sheetNumber);
		wb.setSheetName(sheetNumber, sheetName);

		writeCompanyName(wb);

		int icol = 2;

		String pageName = INPUT_DATA_PAGE;

		if (sheetName.equalsIgnoreCase(POSITION_TAB)) {
			pageName = "Statement of Financial Position";
		} else if (sheetName.equalsIgnoreCase(ACTIVITY_TAB)) {
			pageName = "Statement of Activities";
		}

		wb.setText(3, 0, pageName);
		
		final Map<String, Period> periodByEndDate = reportService.getPeriodByEndDateMap(); 

		for (final String period : periods) {

			final String periodWithFootnote = handleFootnoteIndicator(period, sheetName.toLowerCase());

			wb.setText(2, icol, periodByEndDate.get(period).getPeriodType().name());
			wb.setText(PERIOD_END_DATE_HEADER_ROW, icol, periodWithFootnote);

			if (!INPUT_TAB.equals(sheetName)) {
				if (icol > 2) {
					icol++;
					wb.setText(3, icol, "CH");
					wb.setColHidden(icol, true);
					icol++;
					wb.setText(3, icol, "%CH");
					wb.setColWidth(icol, 2400);
				}
			}
			else {
				inputPagePeriodHeaders.put(periodByEndDate.get(period), icol);
			}

			icol++;
			if (icol > maxCol) {
				maxCol = icol;
			}
		}

		// bold header line
		RangeStyle style = wb.getRangeStyle(3, 0, 3, icol - 1);
		style.setFontBold(true);
		style.setTopBorder(RangeStyle.BorderMedium);
		style.setBottomBorder(RangeStyle.BorderMedium);
		wb.setRangeStyle(style, 3, 0, 3, icol - 1);

		// center headers from column C on
		style = wb.getRangeStyle(2, 2, 3, icol - 1);
		style.setFontBold(true);
		style.setHorizontalAlignment(RangeStyle.HorizontalAlignmentCenter);
		wb.setRangeStyle(style, 2, 2, 3, icol - 1);

		int irow = 5;

		for (ArrayList<String> line : data) {

			icol = 1;
			for (String item : line) {

				item = item.replaceAll("\\[", "");
				item = item.replaceAll("\\{", "");

				if (INPUT_TAB.equals(sheetName) && icol == 1) {
					// special case, fill first column with account code
					String accountCode = item.substring(0, item.indexOf("-") - 1);
					// Cell cell = row.createCell(0);

					wb.setText(irow, icol - 1, accountCode);

					item = item.substring(item.indexOf("-") + 1);
				}

				if (item.contains("<b>")) {

					setBoldForCell(wb, irow, icol);

					item = item.replaceAll("<b>", "");
					item = item.replaceAll("</b>", "");
				}

				wb.setSelection(irow, icol, irow, icol);
				RangeStyle rs = wb.getRangeStyle();

				if (item.contains("<span style=\"display:block; border-top: 1px solid black\">")) {
					rs.setTopBorder(RangeStyle.BorderThin);
					wb.setRangeStyle(rs);
					item = item.replaceAll("<span style=\"display:block; border-top: 1px solid black\">", "");
					item = item.replaceAll("</span>", "");
				}

				if (item.contains("<span style=\"padding:15px\"></span>")) {
					item = item.replaceAll("<span style=\"padding:15px\"></span>", "          ");
				}

				item = handleFootnoteIndicator(item, sheetName.toLowerCase());

				// make sure there is at least a space to blank out run over
				// text
				if (item == null || item.length() == 0) {
					item = " ";
				}
				// ****

				if (item.matches("[\\(\\-\\$]*([\\d]*[\\.\\,]*[\\d]*)*[\\d]+[\\%\\)]*")) {

					if (item.contains(",")) {
						item = item.replace(",", "");
						// rs.setCustomFormat("#,##0;(#,##0)");
						rs.setCustomFormat("_(* #,##0_);_(* (#,##0);_(* \"-\"_);_(@_)");
					}

					if (item.contains("$")) {
						item = item.replace("$", "");
						// rs.setCustomFormat("$#,##0;($#,##0)");
						rs.setCustomFormat("_($#,##0_);_($(#,##0);_(* \"-\"_);_(@_)");
					}

					if (item.contains("(")) {
						item = item.replace("(", "-");
						item = item.replace(")", "");
					}

					if (item.contains("%")) {
						item = item.replace("%", "");
						item = "" + (Double.parseDouble(item) / 100);
						// rs.setCustomFormat("0.0%");
						rs.setCustomFormat("0.0%;-0.0%;* \"-\"_);_(@_)");
					}
					try {
						final double value = Double.parseDouble(item);
						// Display "-" instead of 0 in Excel
						if (Math.abs(value) < ReportService2.SMALLEST_DISPLAYED_VALUE) {
							rs.setCustomFormat("_(#,##0_);_((#,##0);_(* \"-\"_);_(@_)");
						}
						
						wb.setNumber(irow, icol, value);
						wb.setRangeStyle(rs);
					} catch (Exception e) {
						System.out.println(item);
					}

				} else {
					wb.setText(irow, icol, item);
				}

				icol++;
				if (icol > maxCol) {
					maxCol = icol;
				}
			}

			irow++;

		}

		// input page needs one more line
		if (INPUT_DATA_PAGE.equals(pageName)) {
			irow++;
		}

		irow = addFootnotes(wb, irow);

		rightJustifyCells(wb, irow, icol);

		// widths are 3350*inches
		wb.setColWidth(0, 1880); // .56"
		wb.setColWidth(1, 12000); // 2.36"
		for (icol = 2; icol < maxCol; icol++) {
			wb.setColWidth(icol, 3400);
		}

		wb.freezePanes(0, 0, 4, 2, false);

		// topRow - The top row of the top frozen pane. (Ignored if splitRow is
		// 0.)
		// leftCol - The left column of the left frozen pane. (Ignored if
		// splitCol is 0.)
		// splitRows - The number of rows that should be visible in the top
		// pane. Use 0 to specify no top frozen pane.
		// splitCols - The number of cols that should be visible in the left
		// pane. Use 0 to specify no left frozen pane.
		// splitView - Specifies whether this is a frozen split view. if true,
		// autoFreezePanes and unfreezePanes will convert the


	}




	// TODO use periods (column headers) from Report instead of passing them in (from deprecated ReportService)
	private void buildPageNew(WorkBook wb, int sheetNumber, String companyName, String sheetName, List<String> periods, Report report) throws Exception
	{

		wb.setSheet(sheetNumber);
		wb.setSheetName(sheetNumber, sheetName);

		writeCompanyName(wb);

		int icol = 2;

		String pageName = null;
        if (sheetName.equalsIgnoreCase(SUMMARY_TAB)) {
			pageName = "Summary Data";
		} else if (sheetName.equalsIgnoreCase(ADDITIONAL_TAB)) {
			pageName = "Additional Data";
		} else {
			throw new IllegalArgumentException("Unexpected page name: " + sheetName);
		}

		wb.setText(3, 0, pageName);
		
		final Map<String, Period> periodsByEndDate  = reportService.getPeriodByEndDateMap();
		
		for (final String period : periods) {

			final String periodWithFootnote = handleFootnoteIndicator(period, sheetName.toLowerCase());

			wb.setText(2, icol, periodsByEndDate.get(period).getPeriodType().name());
			wb.setText(PERIOD_END_DATE_HEADER_ROW, icol, periodWithFootnote);
			
			if (!INPUT_TAB.equals(sheetName)) {
				if (icol > 2) {
					icol++;
					wb.setText(PERIOD_END_DATE_HEADER_ROW, icol, "CH");
					wb.setColHidden(icol, true);
					icol++;
					wb.setText(PERIOD_END_DATE_HEADER_ROW, icol, "%CH");
					wb.setColWidth(icol, 2400);
				}
			}

			icol++;
			if (icol > maxCol) {
				maxCol = icol;
			}
		}

		// bold header line
		RangeStyle style = wb.getRangeStyle(3, 0, 3, icol - 1);
		style.setFontBold(true);
		style.setTopBorder(RangeStyle.BorderMedium);
		style.setBottomBorder(RangeStyle.BorderMedium);
		wb.setRangeStyle(style, 3, 0, 3, icol - 1);

		// center headers from column C on
		style = wb.getRangeStyle(2, 2, 3, icol - 1);
		style.setFontBold(true);
		style.setHorizontalAlignment(RangeStyle.HorizontalAlignmentCenter);
		wb.setRangeStyle(style, 2, 2, 3, icol - 1);

		int irow = 4;
		
		// this loop is the only difference between old and new methods
		for (Category category : report.getCategories()) {
			irow = buildCategory(wb, sheetName, irow, category, periodsByEndDate, true);
		}

		if (INPUT_DATA_PAGE.equals(pageName)) {
			irow++;
		}

		irow = addFootnotes(wb, irow);

		rightJustifyCells(wb, irow, icol);

		// widths are 3350*inches
		wb.setColWidth(0, 1880); // .56"
		wb.setColWidth(1, 12000); // 2.36"
		for (icol = 2; icol < maxCol; icol++) {
			wb.setColWidth(icol, 3400);
		}
	}
	
	private int addFootnotes(WorkBook wb, int irow) throws Exception {
		irow++;

		wb.setText(irow, 1, "Notes:");

		this.setBoldForCell(wb, irow, 1);

		for (String key : renderedFootnotes.keySet()) {
			String value = renderedFootnotes.get(key);

			irow++;
			wb.setText(irow, 1, "(" + key + ") " + value);

			RangeStyle rangeStyle = wb.getRangeStyle(irow, 1, irow, 1);
			rangeStyle.setWordWrap(false);
			wb.setRangeStyle(rangeStyle, irow, 1, irow, 1);
		}
		renderedFootnotes.clear();
		maxFootnoteIndex = 0;

		return irow;
	}
	
	private void rightJustifyCells(WorkBook wb, int irow, int icol) throws Exception {
		RangeStyle style = wb.getRangeStyle(5, 2, irow, icol);
		style.setHorizontalAlignment(RangeStyle.HorizontalAlignmentRight);
		wb.setRangeStyle(style, 5, 2, irow, icol);
	}


	private int buildCategory(WorkBook wb, String sheetName, int irow, Category category, Map<String,Period> periodsByEndDate, boolean root) throws Exception {
		int icol = 1;
		irow++; // blank row above each category
		
		String categoryName = category.getName();
		categoryName = this.handleFootnoteIndicator(categoryName, sheetName.toLowerCase());
		if (!root) {
			categoryName = "          " + categoryName;
		}
		setBoldForCell(wb, irow, icol);
		wb.setText(irow, icol, categoryName);
		irow++;
		
		for (Equation rowData : category.getDatas()) {
			buildDataRow(wb, sheetName, irow, (Data)rowData, periodsByEndDate, category);
			irow++;
		}
		
		if (null != category.getSubCategories()) {
			for (Category subCategory : category.getSubCategories()) {
				irow = buildCategory(wb, sheetName, irow, subCategory, periodsByEndDate, false);
			}
		}

		return irow;
	}
	
	private int buildCateogry(WorkBook wb, int irow, CdfiCategory category, boolean root) throws Exception {
		int icol = 0;
		irow++; // blank row above each category
		
		String categoryName = category.getName();
		if (!root) {
			categoryName = "          " + categoryName;
		}
		setBoldForCell(wb, irow, icol);
		wb.setText(irow, icol, categoryName);
		irow++;
		
		for (CdfiMetric rowData : category.getMetrics()) {
			buildDataRow(wb, irow, rowData);
			irow++;
		
		}
		
		return irow;
	}

	private void buildDataRow(WorkBook wb, int irow, CdfiMetric rowData) throws Exception {
		int icol = 0;
		
		String dataName = rowData.getMetric();		
		dataName = "          " + dataName;
		wb.setText(irow, icol++, dataName);
				
		for (String formula : rowData.getValues()) {
			RangeStyle range = wb.getRangeStyle(irow, icol, irow, icol);
			
			wb.setText(irow, icol, formula);
			
			wb.setRangeStyle(range, irow, icol, irow, icol);
			icol++;
		}
	}
	
	String extractPeriod(String footnote) {
		Pattern pattern = Pattern.compile("(\\d{1,2}/\\d{1,2}/\\d{4})\\s?+.*");
		Matcher matcher = pattern.matcher(footnote);
		if (matcher.find()) {
			return matcher.group(1);
		}
		else {
			return footnote;
		}
	}
	
	boolean isLastQuarter(WorkBook wb, int column, Map<String,Period> periodsByEndDate) throws Exception {
		if (column == FIRST_DATA_COLUMN) {
			return true;
		}
		final String periodEndDateWithFootnote = wb.getFormattedText(PERIOD_END_DATE_HEADER_ROW, column);
		final String periodEndDate = extractPeriod(periodEndDateWithFootnote);
		final Period period = periodsByEndDate.get(periodEndDate);
		if (period == null) {
			throw new IllegalStateException("No period found with end date = " + periodEndDate);
		}
		
		return period.getQuarter() >= Period.LAST_QUARTER;
	}

	private void buildDataRow(WorkBook wb, String sheetName, int irow, Data rowData, Map<String, Period> periodsByEndDate, Category category) throws Exception {
		int icol = 1;

		String dataName = rowData.getName();
		dataName = this.handleFootnoteIndicator(dataName, sheetName.toLowerCase());
		dataName = "          " + dataName;
		if (!StringUtils.EMPTY.equals(rowData.getDefinition())) {
			wb.addComment(irow, icol, rowData.getDefinition(), "Aeris");
		}
		wb.setText(irow, icol++, dataName);

		String cellFormat = ExcelHelper.getCellFormat(rowData.getUnitType(), rowData.getDecimalPlaces());

		int prevChangeColumn = FIRST_DATA_COLUMN;
		int inputCol = 2;
		for (String formula : rowData.getValues()) {
			RangeStyle range = wb.getRangeStyle(irow, icol, irow, icol);

			int columnMod = ((icol - 4) % 3);
			if (icol < 4 || columnMod == 2) {
				String endDate = extractPeriod(wb.getFormattedText(PERIOD_END_DATE_HEADER_ROW, icol));
				String newFormula = replaceAccountCodes(wb, formula, inputCol, endDate);
				try {
					wb.setFormula(irow, icol, newFormula);
					logger.debug("good formula [" + formula + "] [" + newFormula + "]");
				} catch (Exception e) {
					logger.error("bad formula [" + formula + "] [" + newFormula + "]");
					// e.printStackTrace();
				}

				range.setCustomFormat(cellFormat);
				inputCol++;
			} else {
				boolean renderChange = Boolean.parseBoolean(formula);
				if (renderChange) {
					String row = Integer.toString(irow + 1);
					String newFormula = null;
					try {
						// CH column
						if (columnMod == 0) {
							String cellNew = getExcelColumn(icol - 1);
							String cellOld = getExcelColumn(prevChangeColumn);
							newFormula = cellNew + row + "-" + cellOld + row;
							wb.setFormula(irow, icol, newFormula);
							// range.setCustomFormat("#,##0_);(#,##0)");
							range.setCustomFormat(cellFormat);
						}
						// %CH column
						else if (ExportHelper.showCH(sheetName, category, rowData)) { // (columnMod == 1)
							String cellDiff = getExcelColumn(icol - 1);
							String cellOld = getExcelColumn(prevChangeColumn);
							newFormula = cellDiff + row + "/ABS(" + cellOld + row + ")";
							wb.setFormula(irow, icol, newFormula);
							range.setCustomFormat("[<-0.1]-###,###%;[<0.1]0.0%;###,###%"); // matches online formatting logic in ReportService2
						}
					} catch (Exception e) {
						logger.error("bad formula [" + formula + "] [" + newFormula + "]");
					}
				}
			}
			if ((columnMod == 1) && (icol > FIRST_DATA_COLUMN)){
				final int dataColumn = icol - 2;
				if (isLastQuarter(wb, dataColumn, periodsByEndDate)) {
					prevChangeColumn = dataColumn;
				}
			}
			
			wb.setRangeStyle(range, irow, icol, irow, icol);
			icol++;
		}
	}

	private String replaceAccountCodes(WorkBook wb, String formula, int inputCol, String endDate) throws Exception {
		Pattern accountCodePattern = Pattern.compile("\\d+(\\#|P)");
		Matcher matcher = accountCodePattern.matcher(formula);
		while (matcher.find()) {
			String accountCode = matcher.group();
			String replacement = getInputCellForAccountCode(wb, accountCode, inputCol, endDate);
			formula = matcher.replaceFirst(replacement);
			matcher = matcher.reset(formula);
		}
	
		return formula;
	}
	
	private String getInputCellForAccountCode(WorkBook wb, String accountCode, int inputCol, String endDate) throws Exception {
		int irow = 5;
		if (accountCode.contains("P")) {
			Period prevPeriod = FormulaService.getPriorYearPeriod(endDate, inputPagePeriodHeaders.keySet());
			inputCol = inputPagePeriodHeaders.get(prevPeriod);
			//inputCol--;
			accountCode = accountCode.replace("P", "");
		}
		else {
			accountCode = accountCode.replace("#", "");
		}
		while (true) {
			String cellText = wb.getText(0, irow, 0);
			if (cellText == null || cellText.length() == 0) {
				logger.error("No account code: " + accountCode + " found on input tab");
				return "";
			}
			if (cellText.equals(accountCode)) {
				return wb.getSheetName(0) + "!" + getExcelColumn(inputCol) + (irow+1);
			}
			irow++;
		}
	}
	

    public static String getExcelColumn(int icol) {
        String converted = "";
        while (icol >= 0)
        {
            int remainder = icol % 26;
            converted = (char)(remainder + 'A') + converted;
            icol = (icol / 26) - 1;
        }
 
        return converted;
    }

	
	private String handleFootnoteIndicator(String text, String sheetName) {

		String text2 = text;
		for (FootnotePOJO footnote : footnotes) {

			if (sheetName.equals(footnote.getReportPage()) && footnote.getLabel().trim().equals(text.trim())) {

				String footnoteIndicator = getExistingFootnoteIndicator(footnote.getComment());
				if (footnoteIndicator == null) {
					footnoteIndicator = letters[maxFootnoteIndex];
					maxFootnoteIndex++;
					renderedFootnotes.put(footnoteIndicator, footnote.getComment());
				}
				text2 += " (" + footnoteIndicator + ")";
			}

		}

		return text2;
	}

	private String getExistingFootnoteIndicator(String comment) {
		String footnoteIndicator = null;

		for (String key : renderedFootnotes.keySet()) {
			String value = renderedFootnotes.get(key);
			if (value.equals(comment)) {
				footnoteIndicator = key;
				break;
			}
		}

		return footnoteIndicator;
	}

	private void writeCompanyName(WorkBook wb) throws Exception {
		wb.setText(1, 0, company.getName());
		setBoldForCell(wb, 1, 0);
		setFontSizeForCell(wb, 1, 0, 14);
		wb.setRowHeight(1, 371); // .26 in
	}
	
	private void hideColumns(WorkBook wb, boolean showChange, boolean showPctChange, int numPeriodsToHide) throws Exception {
		if (numPeriodsToHide > 0) {
			wb.setColHidden(2, true);
			numPeriodsToHide--;
		}
		for (int icol = 3; icol<3+(numPeriodsToHide*3); icol++) {
			wb.setColHidden(icol, true);			
		}
		if (!showChange) {
			for (int icol = 4; icol<maxCol; icol+=3) {
				wb.setColHidden(icol, true);			
			}
		}
		if (!showPctChange) {
			for (int icol = 5; icol<maxCol; icol+=3) {
				wb.setColHidden(icol, true);			
			}
		}
		
	}

}