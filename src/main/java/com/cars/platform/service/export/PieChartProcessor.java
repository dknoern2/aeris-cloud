package com.cars.platform.service.export;

import java.awt.Color;

import com.cars.platform.viewmodels.graph.PieChart;
import com.smartxls.ChartFormat;
import com.smartxls.ChartShape;
import com.smartxls.WorkBook;

public class PieChartProcessor extends ChartProcessor<PieChart> {

	public PieChartProcessor(PieChart pieChart, WorkBook wb, SheetContext sheetContext) {
		super(pieChart, wb, sheetContext);
	}

	
	@Override
	void formatChart(ChartShape chart) throws Exception {
		chart.setChartType(ChartShape.Pie);
		chart.setLinkRange(getTableRangeAsString(), false);
		
		chart.setVaryColors(true);
		
		ChartFormat format = chart.getDataLabelFormat(0);
		//format.setDataLabelType(ChartFormat.DataLabelCategoryAndPercent); // this causes Excel to think the file has a security risk
		format.setDataLabelType(ChartFormat.DataLabelPercent);
		format.setCustomFormat("0%");
		chart.setDataLabelFormat(0, format);
		
/* this doesn't work so depending on vary colors above
 		int datapointCount = chart.getDataPointCount(0);

		for (int i = 0; i < datapointCount; i++) {
			ChartFormat format = chart.getDataPointFormat(0, i);
			// seriesformat.setSolid();

			if (i == 0)
				format.setForeColor(Color.RED.getRGB());
			else if (i == 1)
				format.setForeColor(Color.BLUE.getRGB());
			else if (i == 2)
				format.setForeColor(Color.GREEN.getRGB());
			else if (i == 3)
				format.setForeColor(Color.YELLOW.getRGB());
			else if (i == 4)
				format.setForeColor(Color.MAGENTA.getRGB());
			else if (i == 5)
				format.setForeColor(Color.PINK.getRGB());
			else if (i == 6)
				format.setForeColor(Color.ORANGE.getRGB());

			format.setBackColor(format.getBackColor());

			chart.setDataPointFormat(0, i, format);
		}*/
	}

}
