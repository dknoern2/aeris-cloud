package com.cars.platform.service.export;

import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.service.AbstractReportService;
import com.cars.platform.service.ReportService2;
import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;

public class MetricValuesService {
	
	private OutputStream outputStream;
	private WorkBook wb;
	private Map<Metric, Map<Company, MetricValue>> metricValuesByCompany = new HashMap<Metric, Map<Company, MetricValue>>();
	private TreeSet<Metric> allMetrics = new TreeSet<Metric>();
	private TreeSet<Company> allCompanies = new TreeSet<Company>();
	private static final int MAX_FRACTIONAL_DIGITS_FOR_INPUT_DATA_LESS_THAN_ONE = 2;

	public MetricValuesService(OutputStream outputStream) {
		this.outputStream = outputStream;
		wb = new WorkBook();
	}
	
	public void export(int quarter, int year, boolean active, boolean rated) throws Exception {
	   	List<MetricValue> allValues = MetricValue.findAllMetricValues();
	
		for (MetricValue mv : allValues) {
			Metric m = mv.getMetric();
			Company c = mv.getCompany();
			
			if ((active && !c.isActive()) || (rated && !c.isRated())) {
				continue;
			}
			
			allMetrics.add(m);
			allCompanies.add(c);
			Map<Company, MetricValue> metricValues = metricValuesByCompany.get(m);
			if (null == metricValues) {
				metricValues = new HashMap<Company, MetricValue>();
				metricValuesByCompany.put(m, metricValues);
			}
			
			if (mv.getQuarter() == quarter && mv.getYear() == year && mv.getPeriod() != null && mv.getPeriod().getPeriodType() != PeriodType.DRAFT) {
				metricValues.put(c, mv);
			}
		}
		
		generateSpread(); 
	}
	
	private void generateSpread() throws Exception {
		wb.setSheet(0);
		wb.setSheetName(0, "Metric values by company");
		int irow = 0;
		headerRow();
		irow++;
		for (Metric m : allMetrics) {
			metricRow(irow++, m);
		}
		
		wb.setColWidth(0, 8000);
		int offset = 1;
		for (int icol = 0; icol < allCompanies.size(); icol++) {
			wb.setColWidth(icol + offset, 3400);
		}
		wb.write(outputStream);
	}
	
	private void headerRow() throws Exception {
		int icol = 0;
		wb.setText(0, icol++, "Metric Name");
		wb.setText(0, icol++, "Metric Code");
		for (Company c : allCompanies) {
			wb.setText(0, icol++, c.getName());
		}
	}
	
	
	private void metricRow(int irow, Metric m) throws Exception {
		int icol = 0;
		wb.setText(irow, icol++, m.getName());
		wb.setText(irow, icol++, m.getFullAccountCode());
		Map<Company, MetricValue> metricValues = metricValuesByCompany.get(m);
		for (Company c : allCompanies) {
			MetricValue metricValue = metricValues.get(c);
			String text = getTextByMetricValue(metricValue);
			try {
				setCellWithValue(text, irow, icol++);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private String getTextByMetricValue(MetricValue metricValue) {
		String text = "";
		if (metricValue == null) {
			return text;
		}
		
		DecimalFormat decimalFormatWithFraction = new DecimalFormat("#,###");
		decimalFormatWithFraction.setMaximumFractionDigits(MAX_FRACTIONAL_DIGITS_FOR_INPUT_DATA_LESS_THAN_ONE);
		decimalFormatWithFraction.setNegativePrefix("(");
		decimalFormatWithFraction.setNegativeSuffix(")");
		
		DecimalFormat decimalFormat = new DecimalFormat("#,###");
		decimalFormat.setNegativePrefix("(");
		decimalFormat.setNegativeSuffix(")");
		
		if (metricValue.getAmountValue() != null) {
			if (Math.abs(metricValue.getAmountValue()) < AbstractReportService.SMALLEST_DISPLAYED_VALUE) {
				text = "-";
			} else if (Math.abs(metricValue.getAmountValue()) < 1) {
				text = decimalFormatWithFraction.format(metricValue.getAmountValue());
			} else {
				text = decimalFormat.format(metricValue.getAmountValue());
			}
		} else if (metricValue.getTextValue() != null && metricValue.getTextValue().length() > 0) {
			text = metricValue.getTextValue();
		}
		return text;
	}
	
	private void setCellWithValue(String item, int irow, int icol) throws Exception {
		wb.setSelection(irow, icol, irow, icol);
		RangeStyle rs = wb.getRangeStyle();
		if (item.matches("[\\(\\-\\$]*([\\d]*[\\.\\,]*[\\d]*)*[\\d]+[\\%\\)]*")) {

			if (item.contains(",")) {
				item = item.replace(",", "");
				// rs.setCustomFormat("#,##0;(#,##0)");
				rs.setCustomFormat("_(* #,##0_);_(* (#,##0);_(* \"-\"_);_(@_)");
			}

			if (item.contains("$")) {
				item = item.replace("$", "");
				// rs.setCustomFormat("$#,##0;($#,##0)");
				rs.setCustomFormat("_($#,##0_);_($(#,##0);_(* \"-\"_);_(@_)");
			}

			if (item.contains("(")) {
				item = item.replace("(", "-");
				item = item.replace(")", "");
			}

			if (item.contains("%")) {
				item = item.replace("%", "");
				item = "" + (Double.parseDouble(item) / 100);
				// rs.setCustomFormat("0.0%");
				rs.setCustomFormat("0.0%;-0.0%;* \"-\"_);_(@_)");
			}
			try {
				final double value = Double.parseDouble(item);
				// Display "-" instead of 0 in Excel
				if (Math.abs(value) < ReportService2.SMALLEST_DISPLAYED_VALUE) {
					rs.setCustomFormat("_(#,##0_);_((#,##0);_(* \"-\"_);_(@_)");
				}
				
				wb.setNumber(irow, icol, value);
				wb.setRangeStyle(rs);
			} catch (Exception e) {
				System.out.println(item);
			}

		} else {
			wb.setText(irow, icol, item);
		}
	}
}


