package com.cars.platform.service.export;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.util.ExcelHelper;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Table;
import com.cars.platform.viewmodels.graph.Value;
import com.smartxls.ChartShape;
import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;

public class TableProcessor extends GraphProcessor<Table> {

	private static final Logger logger = LoggerFactory.getLogger(TableProcessor.class);
	
	
	public TableProcessor(Table table, WorkBook wb, SheetContext sheetContext) {
		super(table, wb, sheetContext);
	}

	@Override
	boolean createsFigure() {
		return false;
	}

	
	@Override
	Range fillTableData(int startCol) throws Exception {
		int startRow = sheetContext.irow;
		int maxCol = startCol;
		
		// column headers
		Row headerRow = graph.getColumnHeaders();
		maxCol = fillRow(headerRow, startCol, maxCol);

		// data rows
		for (Row dataRow : graph.getDatas()) {
			maxCol = fillRow(dataRow, startCol, maxCol);
		}
		
		// totals
		Row totalRow = graph.getTotals();
		maxCol = fillRow(totalRow, startCol, maxCol);

		Range tableRange = new Range(startRow, startCol, sheetContext.irow-1, maxCol);
		return tableRange;
	}
	
	
	private int fillRow(Row row, int startCol, int maxCol) throws Exception {
		if (null == row) {
			return maxCol;
		}
		
		int icol = startCol;
		wb.setText(sheetContext.irow, icol++, row.getLabel());
		
		for (Value value : row.getValues()) {
			if (null == value.getAmount()) {
				if (null != value.getValue()) {
					wb.setText(sheetContext.irow, icol, value.getValue());
				}
			}
			else {
				wb.setNumber(sheetContext.irow, icol, value.getAmount());
				if (null != value.getDecimalPlaces() || null != value.getDecimalPlaces()) {
					String cellFormat = ExcelHelper.getCellFormat(value.getUnitType() , value.getDecimalPlaces());
					RangeStyle dataStyle =
							wb.getRangeStyle(sheetContext.irow, icol, sheetContext.irow, icol);
					dataStyle.setCustomFormat(cellFormat);
					wb.setRangeStyle(dataStyle, sheetContext.irow, icol, sheetContext.irow, icol);
				}
				else {
					logger.error("Value does not have an IEquationFormatter");
				}
			}
			icol++;
		}
		
		maxCol = (icol-1>maxCol) ? icol-1 : maxCol;
		sheetContext.irow++;
		
		return maxCol;
	}

	
	@Override
	protected void formatTable(Range tableRange) throws Exception {
		super.formatTable(tableRange);
		
		if (null != graph.getTotals()) {
			RangeStyle totalsStyle =
					wb.getRangeStyle(tableRange.row2, tableRange.col1, tableRange.row2, tableRange.col2);
			totalsStyle.setTopBorder(RangeStyle.BorderDouble);
			totalsStyle.setFontBold(true);
			wb.setRangeStyle(totalsStyle, tableRange.row2, tableRange.col1, tableRange.row2, tableRange.col2);
		}
	}

	@Override
	void formatChart(ChartShape chart) throws Exception {
		// I don't do charts
	}

}
