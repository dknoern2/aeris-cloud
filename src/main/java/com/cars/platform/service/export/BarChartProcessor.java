package com.cars.platform.service.export;

import com.cars.platform.viewmodels.graph.BarChart;
import com.smartxls.ChartShape;
import com.smartxls.WorkBook;

public class BarChartProcessor extends ChartProcessor<BarChart> {

	public BarChartProcessor(BarChart barChart, WorkBook wb, SheetContext sheetContext) {
		super(barChart, wb, sheetContext);
	}

	@Override
	void formatChart(ChartShape chart) throws Exception {
		chart.setChartType(ChartShape.Column);
		// chart.setPercent(true);
		chart.setPlotStacked(true);
		chart.setBarGapRatio(-100); // required for stacking in
		chart.setLinkRange(getTableRangeAsString(), true);
	}

}
