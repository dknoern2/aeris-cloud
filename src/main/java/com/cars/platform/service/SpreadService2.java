package com.cars.platform.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.util.ExcelHelper;
import com.cars.platform.util.SmartXLSHelper;
import com.cars.platform.viewmodels.MappedPage;
import com.cars.platform.viewmodels.MappedRow;
import com.cars.platform.viewmodels.SpreadsheetCell;
import com.smartxls.WorkBook;

public class SpreadService2 {

	private static final Logger logger = LoggerFactory.getLogger(SpreadService2.class);

	public String getSheetAsHTML(WorkBook wb, String sheetName, int r1, int r2, int[] c) {

		SmartXLSHelper smartXLSHelper = new SmartXLSHelper();

		try {
			int index = smartXLSHelper.getSheetByName(wb, sheetName);

			if (index > -1) {
				wb.setSheet(index);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		StringBuffer sb = new StringBuffer();
		sb.append("<table class=\"chart\">\n");

		for (int i = r1; i <= r2; i++) {
			sb.append("  <tr>\n");
			for (int j : c) {

				String evaluatedCell = "";
				evaluatedCell = smartXLSHelper.getValue(wb, i, j);
				if ("% CH".equals(evaluatedCell)) {
					evaluatedCell = "%CH";
				}

				String align = "";
				if (j == c[0])
					align = " align=\"left\"";
				if (i == r1)
					sb.append("    <th" + align + ">" + evaluatedCell + "</th>\n");
				else
					sb.append("    <td" + align + ">" + evaluatedCell + "</td>\n");
			}
			sb.append("  </tr>\n");
		}

		sb.append("</table>\n");
		return sb.toString();
	}

	/**
	 * Pull a section of a sheet from the workbook. Inputs are first row, col
	 * and last row,col
	 * 
	 */
	public String getSheetAsHTML(WorkBook wb, String sheetName, int r1, int c1, int r2, int c2) {

		int[] c = new int[c2 - c1 + 1];
		for (int i = 0; i < c2 - c1 + 1; i++) {
			c[i] = c1 + i;
		}
		return getSheetAsHTML(wb, sheetName, r1, r2, c);
	}

	public String getSheetAsHTML(String filename, InputStream is, int sheetIndex) throws IOException {

		WorkBook wb = new WorkBook();
		try {
			ExcelHelper.readWorkBook(wb, is);
			wb.setSheet(sheetIndex);

			int r1 = 0;
			int r2 = wb.getLastRow();

			int c1 = 0;
			int c2 = 0;

			for (int i = 0; i < r2; i++) {
				if (wb.getLastColForRow(i) > c2) {
					c2 = wb.getLastColForRow(i);
				}
			}
			return this.getSheetAsHTML(wb, wb.getSheetName(sheetIndex), r1, c1, r2, c2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return"";
	}

	public Map<Integer, Map<Integer, List<SpreadsheetCell>>> getWorkbookAsArray(String filename, InputStream is) throws IOException {

		Map<Integer, Map<Integer, List<SpreadsheetCell>>> map = new HashMap<Integer, Map<Integer, List<SpreadsheetCell>>>();

		WorkBook wb = new WorkBook();
		try {
			ExcelHelper.readWorkBook(wb, is);

			int numberOfSheets = wb.getNumSheets();

			for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
				wb.setSheet(sheetIndex);

				int r1 = 0;
				int r2 = wb.getLastRow();

				int c1 = 0;
				int c2 = 0;

				for (int i = 0; i < r2; i++) {
					if (wb.getLastColForRow(i) > c2) {
						c2 = wb.getLastColForRow(i);
					}
				}

				int[] c = new int[c2 - c1 + 1];
				for (int i = 0; i < c2 + 1; i++) {
					c[i] = i;
				}

				map.put(sheetIndex, this.getSheetAsArray(wb, r1, r2, c, sheetIndex));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;

	}

	public Map<Integer, List<SpreadsheetCell>> getSheetAsArray(String filename, InputStream is, int sheetIndex) throws IOException {

		WorkBook wb = new WorkBook();
		try {
			ExcelHelper.readWorkBook(wb, is);
		} catch (Exception e) {
			e.printStackTrace();
		}

		int r1 = 0;
		int r2 = wb.getLastRow();

		int c1 = 0;
		int c2 = 0;

		for (int i = 0; i < r2; i++) {
			try {
				if (wb.getLastColForRow(i) > c2) {
					c2 = wb.getLastColForRow(i);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		int[] c = new int[c2 - c1 + 1];
		for (int i = 0; i < c2 + 1; i++) {
			c[i] = i;
		}
		return this.getSheetAsArray(wb, r1, r2, c, sheetIndex);
	}

	public Map<Integer, List<SpreadsheetCell>> getSheetAsArray(WorkBook wb, int r1, int r2, int[] c, int z) {

		Map<Integer, List<SpreadsheetCell>> map = new HashMap<Integer, List<SpreadsheetCell>>();

		SmartXLSHelper smartXLSHelper = new SmartXLSHelper();

		for (int i = r1; i <= r2; i++) {
			List<SpreadsheetCell> entry = new ArrayList<SpreadsheetCell>();
			map.put(i, entry);
			for (int j : c) {
				String evaluatedCell = smartXLSHelper.getValue(wb, i, j);
				if (!evaluatedCell.equals("")) {
					entry.add(new SpreadsheetCell(i, j, z, evaluatedCell));
				}
			}
		}
		return map;
	}

	public List<MappedPage> getWorkbookAsMappedPages(String filename, InputStream is) throws IOException {

		logger.info("getting workbook from input stream");

		List<MappedPage> map = new ArrayList<MappedPage>();

		WorkBook wb = new WorkBook();
		try {
			ExcelHelper.readWorkBook(wb, is);

			int numberOfSheets = wb.getNumSheets();

			logger.info(numberOfSheets + " sheets to parse");

			for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {

				logger.info("parsing sheet " + sheetIndex);
				System.out.println("parsing sheet " + sheetIndex);

				wb.setSheet(sheetIndex);

				if (!(wb.getHiddenState() == WorkBook.SheetStateHidden)) {

					int r1 = 0;
					int r2 = wb.getLastRow();

					int c1 = 0;
					int c2 = 0;

					for (int i = 0; i < r2; i++) {
						if (wb.getLastColForRow(i) > c2) {
							c2 = wb.getLastColForRow(i);
						}
					}

					int[] c = new int[c2 - c1 + 1];
					for (int i = 0; i < c2 + 1; i++) {
						c[i] = i;
					}

					List<MappedRow> mappedRows = getSheetAsMappedRows(wb, r1, r2, c, wb.getSheet());
					MappedPage page = new MappedPage();
					page.setPage(mappedRows);
					map.add(page);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public List<MappedRow> getSheetAsMappedRows(WorkBook wb, int r1, int r2, int[] c, int z) {
		// if you see more than 9 blank rows in a row, exit!
		// (reduces Knockout load)
		List<MappedRow> map = new ArrayList<MappedRow>();

		SmartXLSHelper smartXLSHelper = new SmartXLSHelper();

		int blankLinesInARow = 0;
		int lastRowIndex = 0;

		for (int i = r1; i <= r2; i++) {
			MappedRow entry = new MappedRow();
			entry.setRow(new ArrayList<SpreadsheetCell>());
			map.add(entry);

			boolean blankLine = true;
			try {
				for (int j : c) {
					String evaluatedCell = "";
					try {
						evaluatedCell = smartXLSHelper.getValue(wb, i, j); // format but then remove bold tags
						evaluatedCell = evaluatedCell.replace("<b>", "");
						evaluatedCell = evaluatedCell.replace("</b>", "");
					} catch (IllegalArgumentException e) {
						System.out.println("trouble parsing sheet " + wb.getSheetName(wb.getSheet()) + ", row " + i + ", column " + j + ", setting to blank");
					} catch (Exception e) {
						System.out.println("trouble parsing sheet " + wb.getSheetName(wb.getSheet()) + ", row " + i + ", column " + j + ", setting to blank");
						e.printStackTrace();
					}
					
					int size = entry.getRow().size();
					
					if (evaluatedCell.length() > 0) {
						blankLine = false;						
						if (lastRowIndex < size)
						{
							lastRowIndex = size;
						}

					}
					else{
						if (size > 499)
							break;
					}
					
					entry.getRow().add(new SpreadsheetCell(i, j, z, evaluatedCell));
				}

				if (blankLine) {
					blankLinesInARow++;
				} else {
					blankLinesInARow = 0;
				}

				if (blankLinesInARow > 9) {
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		for(int i=0; i<map.size(); i++)
		{
			MappedRow entry = map.get(i);
			List<SpreadsheetCell> row = entry.getRow();
			int size = row.size();
			for(int j = size - 1; j > lastRowIndex; j--)
			{
				row.remove(j);
			}
		}
		return map;
	}
}