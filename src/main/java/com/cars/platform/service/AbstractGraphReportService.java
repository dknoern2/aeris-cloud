package com.cars.platform.service;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.Review;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.UserHelper;

/**
 * Abstract class which provides for a common representation of date column headers for graphs
 * @author kurtl
 *
 */
public abstract class AbstractGraphReportService extends AbstractReportService {

	protected Company company; // can be null (PeerGroupReportService in which case it is the optional CDFI)
	protected Person person;
	protected Review latestFullReview;
	
	public AbstractGraphReportService(Company company) {
		super();
		
		this.company = company;
		
		// subscribers don't see the rating period
		this.person = UserHelper.getCurrentUser();
		if (null == person || person.isSubscriber() || null == company) {
			this.latestFullReview = null;
		}
		else {
			this.latestFullReview = Review.findMostRecentCompletedFullAnalysisReview(company);
		}
	}
	

	protected String getDateString(Period period) {
		return getDateString(period.getYear(), period.getQuarter());
	}
	
	
	protected String getDateString(int year, int quarter) {
		if (null != latestFullReview) {
			if (latestFullReview.getQuarter() == quarter &&
					latestFullReview.getYear() == year)
			{
				return "Rating Q" + quarter + " " + year;
			}
		}
		
		return DateHelper.getFiscalDateString(year, quarter);
	}

}
