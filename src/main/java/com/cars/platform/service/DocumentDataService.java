package com.cars.platform.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.cars.platform.util.PropertiesHelper;

@Service
public class DocumentDataService {
	
	@Autowired
	PropertiesHelper propertiesHelper;

    AmazonS3 s3 = null;
    String bucketName = null;
    String dirName = null;

    @PostConstruct
    private void init() {
        try {

//            String configFile = "/usr/local/etc/garage.properties";
//            prop.load(new FileInputStream(configFile));

            bucketName = propertiesHelper.getProperty("bucketName");
            
            String accessKey = propertiesHelper.getProperty("accessKey");
            String secretKey = propertiesHelper.getProperty("secretKey");
            if (null != accessKey && null != secretKey) {
            	// This is for dev environments that wish to connect to their own instance of s3
	            BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);
	            s3 = new AmazonS3Client(creds);
            }
            else {
            	// See javadocs for this constructor for the provider chains used
            	// In test and prod we use IAM role created for elastic beanstalk given 'all' access policy to EC2 instances
            	s3 = new AmazonS3Client();
            }

        } catch (Exception e) {
            dirName="carsdocs";
//            if (propertiesHelper.isProduction()) {
//              System.out.println("COULD NOT load S3 settings EXITING.");
//            	System.exit(-1);
//            }
            System.out.println("COULD NOT load S3 settings, will use local filesystem only");
        }
    }

    public void createDocumentData(long companyId, long documentId,
            InputStream in) throws IOException {

        if (s3 != null && bucketName != null) {
            ObjectMetadata metadata = null;
            s3.putObject(bucketName, getKey(companyId, documentId), in,
                    metadata);
        } else {
            File dir = new File(dirName + "/" + companyId);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(getLocalPath(companyId, documentId));
            FileOutputStream out = new FileOutputStream(file);

            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }

            out.flush();
            out.close();
        }
    }

    public void deleteDocumentData(long companyId, long documentId) {
        if (s3 != null && bucketName != null) {
            s3.deleteObject(bucketName, getKey(companyId, documentId));
        } else {
            File file = new File(getLocalPath(companyId, documentId));
            file.delete();
        }
    }

    public InputStream getDocumentData(long companyId, long documentId)
            throws IOException {
        InputStream in = null;

        if (s3 != null && bucketName != null) {
            S3Object s3object = s3.getObject(bucketName,
                    getKey(companyId, documentId));
            in = s3object.getObjectContent();
        } else {
            File file = new File(getLocalPath(companyId, documentId));
            in = new FileInputStream(file);
        }
        return in;
    }

    private String getKey(long companyId, long documentId) {
        return Long.toString(companyId) + "/" + Long.toString(documentId);
    }

    private String getLocalPath(long companyId, long documentId) {
        return dirName + "/" + getKey(companyId, documentId);
    }

    public void createDocumentData(long companyId, long documentId, byte[] data) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        createDocumentData(companyId,documentId,bis);
    }

}
