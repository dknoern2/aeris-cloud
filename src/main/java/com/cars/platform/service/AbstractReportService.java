package com.cars.platform.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.cars.platform.domain.IEquationFormat;
import com.cars.platform.domain.IGetValueString;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.UnitType;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.util.DateHelper;

public abstract class AbstractReportService implements IGetValueString {
	
	// if absolute value is less than this then a '-' is displayed
	public static double SMALLEST_DISPLAYED_VALUE = .001;


    private DecimalFormat decimalFormat = new DecimalFormat("#,###");
    //protected NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    private NumberFormat percentFormat = NumberFormat.getPercentInstance();


	protected DecimalFormat changeDecimalFormat = new DecimalFormat("#,###");
	protected NumberFormat changePercentFormat = NumberFormat.getPercentInstance();

	public AbstractReportService() {
		super();
		//currencyFormat.setMaximumFractionDigits(0);
		decimalFormat.setNegativePrefix("(");
		decimalFormat.setNegativeSuffix(")");
		changeDecimalFormat.setNegativePrefix("(");
		changeDecimalFormat.setNegativeSuffix(")");

	}

	protected String getValueString(ValueFact valueFact) {
		if (null == valueFact || null == valueFact.getAmount()) {
			return "";
		}
		
		return getValueString(valueFact.getAmount(), valueFact.getEquation());
	}
	
	public String getValueString(double value, IEquationFormat equation) {
		String text;
		int decimalPlaces = equation.getDecimalPlaces();
		// Spreadsheets in the financial industry don't show values of zero... they use '-' instead
	    if (Math.abs(value) < .00001) {
	    	text = "-";
	    }
	    else if (UnitType.PERCENTAGE.equals(equation.getUnitType())) {
			percentFormat.setMaximumFractionDigits(decimalPlaces);
			percentFormat.setMinimumFractionDigits(decimalPlaces);
			text = percentFormat.format(value);
	    }
	    else {
			decimalFormat.setMaximumFractionDigits(decimalPlaces);
			decimalFormat.setMinimumFractionDigits(decimalPlaces);
			text = decimalFormat.format(value);
	    }
	    // for online reports number and currency are displayed the same
//	    else if (UnitType.NUMBER.equals(equation.getUnitType())) {
//			decimalFormat.setMaximumFractionDigits(decimalPlaces);
//			decimalFormat.setMinimumFractionDigits(decimalPlaces);
//			text = decimalFormat.format(value);
//	    }
//	    else {
//	    	currencyFormat.setMaximumFractionDigits(decimalPlaces);
//	    	currencyFormat.setMinimumFractionDigits(decimalPlaces);
//			text = currencyFormat.format(value);
//	    }
	    
	    return text;
	}





}