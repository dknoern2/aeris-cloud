package com.cars.platform.service.graph.breakdown;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.BreakdownGraph;
import com.cars.platform.domain.BreakdownHistory;
import com.cars.platform.domain.BreakdownPie;
import com.cars.platform.domain.BreakdownTable;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.GraphType;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.service.graph.BaseGraphProcessor;
import com.cars.platform.viewmodels.graph.Chart;
import com.cars.platform.viewmodels.graph.Graph;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Value;

abstract class BreakdownProcessor<T extends BreakdownGraph, U extends Graph> extends BaseGraphProcessor<U> {

    @SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(BreakdownProcessor.class);

	public BreakdownProcessor(Company company, T breakdownGraph,
			Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod,
			MetricBreakdownItem summationItem, boolean showRatingPeriods)
	{
		super(company, showRatingPeriods);
		this.breakdownGraph = breakdownGraph;
		this.valueByCodeByItemByPeriod = valueByCodeByItemByPeriod;
		this.summationItem = summationItem;
	}
	
	protected T breakdownGraph;
		
	protected Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod;
	
	protected MetricBreakdownItem summationItem;
	
	//static BreakdownProcessor<BreakdownGraph, Graph> getProcessor(BreakdownGraph breakdownGraph,
	@SuppressWarnings("rawtypes")
	static BreakdownProcessor getProcessor(Company company, BreakdownGraph breakdownGraph,
			Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod,
			MetricBreakdownItem summationItem, boolean showRatingPeriods)
	{
		// if no metrics then no graph 
		if (null == valueByCodeByItemByPeriod) {
			return null;
		}
		if (breakdownGraph instanceof BreakdownTable) {
			return new BreakdownTableProcessor(company, (BreakdownTable)breakdownGraph, valueByCodeByItemByPeriod, summationItem, showRatingPeriods);
		}
		else if (breakdownGraph instanceof BreakdownPie) {
			return new BreakdownPieProcessor(company, (BreakdownPie)breakdownGraph, valueByCodeByItemByPeriod, summationItem, showRatingPeriods);
		}
		else if (breakdownGraph instanceof BreakdownHistory) {
			BreakdownHistory bh = (BreakdownHistory)breakdownGraph;
			if (bh.getGraphType() == GraphType.TABLE) {
			    return new BreakdownHistoryTableProcessor(company, bh, valueByCodeByItemByPeriod, summationItem, showRatingPeriods);
				
			}
			else {
			    return new BreakdownHistoryChartProcessor<Chart>(company, bh, valueByCodeByItemByPeriod, summationItem, showRatingPeriods);
			}
		}
		else {
			throw new UnsupportedOperationException("Can't handle this type of graph: " + breakdownGraph.getClass().getName());
		}
	}
	
	@Override
	public final U process() {
		super.process();
		
		outputGraph.setTitle(breakdownGraph.getTitle());
		outputGraph.setDate(getDateString(getLatestPeriod()));
		
		Row columnHeaders = new Row();
		columnHeaders.setLabel(breakdownGraph.getBreakdown().getLabel());
		columnHeaders.setValues(getColumnHeaderValues());
		outputGraph.setColumnHeaders(columnHeaders);
		
		outputGraph.setDatas(getDataRows());
		
		postProcess();

		return outputGraph;
	}
	
	
	@Override
	protected Set<Period> getAvailablePeriods() {
		return valueByCodeByItemByPeriod.keySet();
	}
	
	
	/**
	 * Subclasses can implement this for subclass specific content (total rows for example)
	 */
	protected void postProcess() {
	}


	protected abstract Period getLatestPeriod();
	protected abstract List<Value> getColumnHeaderValues();
	protected abstract List<Row> getDataRows();

	
	protected CellValue getCellValue(BreakdownColumn bc, MetricBreakdownItem mbi,
			Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem,
			boolean getTotal)
	{
		return bc.caclulateCellValue(mbi, summationItem, valueByCodeByItem, this, getTotal);
	}

	@Override
	protected boolean forFactSheet() {
		return false;
	}
}
