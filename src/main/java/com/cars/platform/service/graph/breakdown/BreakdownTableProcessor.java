package com.cars.platform.service.graph.breakdown;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.BreakdownTable;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Table;
import com.cars.platform.viewmodels.graph.Value;

class BreakdownTableProcessor extends BreakdownProcessor<BreakdownTable, Table> {

	public BreakdownTableProcessor(Company company, BreakdownTable breakdownGraph,
			Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod,
			MetricBreakdownItem summationItem, boolean showRatingPeriods)
	{
		super(company, breakdownGraph, valueByCodeByItemByPeriod, summationItem, showRatingPeriods);
		
		period = getMostRecentFYE();
		
		valueByCodeByItem = valueByCodeByItemByPeriod.get(period);
	}
	
	private Period period;
	private Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem;
    private Map<BreakdownColumn, Double> totals; // TODO remove this


	@Override
	protected Table createOutputGraph() {
        return new Table();
	}

	@Override
	protected Period getLatestPeriod() {
		return period;
	}

	@Override
	protected List<Value> getColumnHeaderValues() {
		List<Value> columnHeaders = new ArrayList<Value>();
		for (BreakdownColumn bc : breakdownGraph.getColumns()) {
			columnHeaders.add(new Value(bc.getName()));
		}
		return columnHeaders;
	}
	
	
	@Override
	protected List<Row> getDataRows() {

		List<Row> dataRows = new ArrayList<Row>();
		totals = new HashMap<BreakdownColumn, Double>();
		for (MetricBreakdownItem mbi : breakdownGraph.getBreakdown().getItems()) {
			// don't include breakdown items if there is no data
			if (null == valueByCodeByItem.get(mbi)) {
				continue;
			}
			
			Row row = new Row();
			row.setLabel(mbi.getName());
			
			ArrayList<Value> values = new ArrayList<Value>();
			for (BreakdownColumn bc : breakdownGraph.getColumns()) {
				CellValue cellValue = getCellValue(bc, mbi, valueByCodeByItem, false);
				// TODO check for evaluation exception... what to do?
				Value value = new Value(cellValue.amount, bc, cellValue.value);
				values.add(value);
				
				// TODO get rid of this total calculation once History is done, may be needed there
				if (null != cellValue.amount) {
					Double total = totals.get(bc);
					if (null == total) {
						total = cellValue.amount;
						totals.put(bc, total);
					}
					else {
						totals.put(bc, total + cellValue.amount);
					}
				}

			}
			row.setValues(values);
			
			dataRows.add(row);
		}
		
		return dataRows;
	}

	
	@Override
	protected void postProcess() {
		outputGraph.setTitle(breakdownGraph.getTitle() + " (" + getDateString(getLatestPeriod()) + ")");
		
		Row totalRow = new Row();
		List<Value> totalValues = new ArrayList<Value>();
		boolean totalFound = false;
		for (BreakdownColumn bc : breakdownGraph.getColumns()) {
			if (bc.isShowTotal()) {
				CellValue cellValue = getCellValue(bc, summationItem, valueByCodeByItem, true);
				// TODO check for evaluation exception... what to do?
				Value value = new Value(cellValue.amount, bc, cellValue.value);
				totalValues.add(value);
				if (cellValue.amount != null) {
					totalFound = true;
				}
			}
			else {
				totalValues.add(new Value(""));
			}
		}
		totalRow.setLabel(totalFound ? "Total" : "");
		totalRow.setValues(totalValues);
		outputGraph.setTotals(totalRow);
	}

	@Override
	protected boolean showInterim() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected int showYears() {
		// TODO Auto-generated method stub
		return 0;
	}

}
