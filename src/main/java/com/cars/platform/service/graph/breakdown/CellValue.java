package com.cars.platform.service.graph.breakdown;

/**
 * Class that holds a cell's value as a string and double. Needed for summation of total row values
 * @author kurtl
 *
 */
public class CellValue {
	
	public CellValue(Double amount, String value) {
		this.amount = amount;
		this.value = value;
		this.e = null;
	}
	
	public CellValue(Exception e) {
		this.amount = null;
		this.value = "?"; // TODO do something better?
		this.e = e;
	}
	
	public Double amount;
	public String value;
	public Exception e;
}


