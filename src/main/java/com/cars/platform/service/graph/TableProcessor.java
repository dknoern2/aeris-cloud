package com.cars.platform.service.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.viewmodels.graph.Table;
import com.cars.platform.viewmodels.graph.Value;

class TableProcessor extends GraphProcessor<Table> {

	public TableProcessor(com.cars.platform.domain.Graph domainGraph, Company company,
			Map<Period, Map<Equation, ValueFact>> factByEquationByPeriod, boolean showRatingPeriods)
	{
		super(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
	}

    @Override
	protected Table createOutputGraph() {
        return new Table();
	}
    
    /**
     * Ext.js require column headers to be unique in the model otherwise it's displayed not correctly.
     * To avoid header collissions, append (2), (3), ... to the duplicated values.
     * 
     * 
     */
    @Override
    protected List<Value> getColumnHeaderValues() {
		final List<Value> columnHeaders = new ArrayList<Value>();
		String previousDate = null;
		int duplicated = 1;
		for (Period period : periodsToShow) {
			String dateString = getDateString(period);
			if ((dateString != null) && dateString.equals(previousDate)) {
				duplicated++;
				dateString += " (" + duplicated + ")";
			}
			else {
				duplicated = 1;
			}
			Value value = new Value(dateString);
			columnHeaders.add(value);
			previousDate = dateString;
		}
		return columnHeaders;
    }

}
