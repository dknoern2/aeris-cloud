package com.cars.platform.service.graph.breakdown;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cars.platform.domain.BreakdownColumn;
import com.cars.platform.domain.BreakdownPie;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.viewmodels.graph.PieChart;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Value;

public class BreakdownPieProcessor extends BreakdownProcessor<BreakdownPie, PieChart> {

	public BreakdownPieProcessor(
			Company company, BreakdownPie breakdownGraph,
			Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod,
			MetricBreakdownItem summationItem, boolean showRatingPeriods)
	{
		super(company, breakdownGraph, valueByCodeByItemByPeriod, summationItem, showRatingPeriods);
		
		period = getMostRecentFYE();
		valueByCodeByItem = valueByCodeByItemByPeriod.get(period);
	}
	
	private Period period;
	private Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem;

	@Override
	protected PieChart createOutputGraph() {
		return new PieChart();
	}


	@Override
	protected Period getLatestPeriod() {
		return period;
	}


	@Override
	protected List<Value> getColumnHeaderValues() {
		List<Value> columnHeaders = new ArrayList<Value>();
		Value value = new Value(breakdownGraph.getEquation().getName());
		columnHeaders.add(value);
		return columnHeaders;
	}

	
	@Override
	protected List<Row> getDataRows() {

		List<Row> dataRows = new ArrayList<Row>();
		BreakdownColumn bc = breakdownGraph.getEquation();

		for (MetricBreakdownItem mbi : breakdownGraph.getBreakdown().getItems()) {
			// don't include breakdown items if there is no data
			if (null == valueByCodeByItem.get(mbi)) {
				continue;
			}
			
			Row row = new Row();
			row.setLabel(mbi.getName());
			
			ArrayList<Value> values = new ArrayList<Value>();
			CellValue cellValue = getCellValue(bc, mbi, valueByCodeByItem, false);
			// TODO check for evaluation exception... what to do?
			Value value = new Value(cellValue.amount, bc, cellValue.value);
			values.add(value);

			row.setValues(values);
			dataRows.add(row);
		}
		
		return dataRows;
	}

	@Override
	protected void postProcess() {
		outputGraph.setUnitType(breakdownGraph.getEquation().getUnitType());
		outputGraph.setDecimalPlaces(breakdownGraph.getEquation().getDecimalPlaces());
		
		outputGraph.setTitle(breakdownGraph.getTitle() + " (" + getDateString(getLatestPeriod()) + ")");
	}


	@Override
	protected boolean showInterim() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	protected int showYears() {
		// TODO Auto-generated method stub
		return 0;
	}


}
