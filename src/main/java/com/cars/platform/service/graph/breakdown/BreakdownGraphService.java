package com.cars.platform.service.graph.breakdown;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.BreakdownGraph;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.service.AbstractReportService;
import com.cars.platform.viewmodels.graph.Graph;

public class BreakdownGraphService extends AbstractReportService {
	
    private static final Logger logger = LoggerFactory.getLogger(BreakdownGraphService.class);
    
    private Company company;
       
    // All the MetricValues for the company are fetched during construction and organized into this HashMap hierarchy 
    // MetricBreakdown
    //     Period
    //         MetricBreakdownItem
    //             fullAccountCode
    //                 MetricValue
    private Map<MetricBreakdown, Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>>> valueByCodeByItemByPeriodByBreakdown;
    
    private List<Period> allPeriods;
    
    private MetricBreakdownItem summationItem = null;
        
    /**
     * The typical way to create the service
     * @param company
     */
    public BreakdownGraphService(Company company) {
		this(company, MetricValue.findBreakdownMetricValues(company), Period.findViewablePeriodsByCompanySorted(company));
    }
    
    /**
     * A constructor which allows tests to instantiate these objects without having database dependency
     */
    public BreakdownGraphService(Company company, List<MetricValue> metricValues, List<Period> allPeriods) {
    	this.company = company;
    	
		summationItem = new MetricBreakdownItem();
		summationItem.setName("SUM");
		
		this.allPeriods = allPeriods;

    	this.getMetricValues(metricValues);
    }
    
    public List<Graph> getBreakdownGraphs(boolean online, boolean showRatingPeriods) {
    	List<BreakdownGraph> allBreakdownGraphs = BreakdownGraph.findAllBreakdownGraphs();
    	List<Graph> graphs = new ArrayList<Graph>();
    	for (BreakdownGraph breakdownGraph : allBreakdownGraphs) {
    		if (!online && !breakdownGraph.isExport()) {
    			continue;
    		}
    		Graph graph = getGraph(breakdownGraph, showRatingPeriods);
    		if (null != graph) {
    			graphs.add(graph);
    		}
    	}
    	return graphs;
    }

	public Graph getGraph(BreakdownGraph breakdownGraph, boolean showRatingPeriods) {
		Graph graph = null;
		MetricBreakdown mb = breakdownGraph.getBreakdown();
		Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod = valueByCodeByItemByPeriodByBreakdown.get(mb);
		
		@SuppressWarnings("unchecked")
		BreakdownProcessor<BreakdownGraph, Graph> processor =
				BreakdownProcessor.getProcessor(company, breakdownGraph, valueByCodeByItemByPeriod, summationItem, showRatingPeriods);
		
		if (null == processor) {
			return null;
		}

		graph = processor.process();

        return graph;
	}
	
	private void getMetricValues(List<MetricValue> metricValues) {		
	    valueByCodeByItemByPeriodByBreakdown = new HashMap<MetricBreakdown, Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>>>();
	    for (MetricValue metricValue : metricValues) {
	    	MetricBreakdownItem mbi = metricValue.getBreakdownItem();    	
	    	MetricBreakdown mb = mbi.getMetricBreakdown();
	    	Period period = getPeriodforMetricValue(metricValue);
	    	if (null == period) {
	    		logger.error("Unable to find Period for MetricValue id [" + metricValue.getId() + "]");
	    		continue;
	    	}
	    	
	    	Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod = valueByCodeByItemByPeriodByBreakdown.get(mb);
	    	if (null == valueByCodeByItemByPeriod) {
	    		valueByCodeByItemByPeriod = new HashMap<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>>();
	    		valueByCodeByItemByPeriodByBreakdown.put(mb, valueByCodeByItemByPeriod);
	    	}
	    	
	    	Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem = valueByCodeByItemByPeriod.get(period);
	    	if (null == valueByCodeByItem) {
	    		valueByCodeByItem = new HashMap<MetricBreakdownItem, Map<String, MetricValue>>();
	    		valueByCodeByItemByPeriod.put(period, valueByCodeByItem);
	    	}
	    	
	    	Map<String, MetricValue> valueByCode = valueByCodeByItem.get(mbi);
	    	if (null == valueByCode) {
	    		valueByCode = new HashMap<String, MetricValue>();
	    		valueByCodeByItem.put(mbi, valueByCode);
	    	}
	    	
	    	valueByCode.put(metricValue.getMetric().getFullAccountCode(), metricValue);
	    }
	    
	    calculateMetricValueTotals();
	}
	
	private Period getPeriodforMetricValue(MetricValue mv) {
		for (Period period : allPeriods) {
			if (mv.getYear() == period.getYear() && mv.getQuarter() == period.getQuarter()) {
				return period;
			}
		}
		return null;
	}

	
	// Creates a transient MetricBreakdownItem to hold the sum of metric values for all other MetricBreakdownItems
	private void calculateMetricValueTotals() {
		for (MetricBreakdown breakdown : valueByCodeByItemByPeriodByBreakdown.keySet()) {
		    Map<Period, Map<MetricBreakdownItem, Map<String, MetricValue>>> valueByCodeByItemByPeriod = valueByCodeByItemByPeriodByBreakdown.get(breakdown);
						
			for (Period period : valueByCodeByItemByPeriod.keySet()) {
				Map<MetricBreakdownItem, Map<String, MetricValue>> valueByCodeByItem = valueByCodeByItemByPeriod.get(period);
				
				Map<String, MetricValue> sumByCode = new HashMap<String, MetricValue>();				
				for (MetricBreakdownItem mbi : valueByCodeByItem.keySet()) {
					Map<String, MetricValue> valueByCode = valueByCodeByItem.get(mbi);
					
					for (String code : valueByCode.keySet()) {
						MetricValue mv = valueByCode.get(code);
						
						if (mv.getMetric().getType() == MetricType.TEXT) {
							continue; // can't sum up text metrics
						}
						MetricValue sumMetric = sumByCode.get(code);
						if (null == sumMetric) {
							sumMetric = new MetricValue(mv);
							sumByCode.put(code, sumMetric);
						}
						else {
							sumMetric.setAmount(sumMetric.getAmount() + mv.getAmount());
						}
					}
				}
				
				valueByCodeByItem.put(summationItem, sumByCode);
			}		
		}
	}

}
