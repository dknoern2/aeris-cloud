package com.cars.platform.service.graph;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.service.AbstractGraphReportService;
import com.cars.platform.util.PeriodHelper;
import com.cars.platform.viewmodels.graph.Graph;

public abstract class BaseGraphProcessor<U extends Graph> extends AbstractGraphReportService {

	protected U outputGraph;
	
	//Rating periods should be shown only on all tabs of the Financials page. 
	protected final boolean showRatingPeriods;


	public BaseGraphProcessor(Company company, boolean showRatingPeriods) {
		super(company);
		this.showRatingPeriods = showRatingPeriods;
	}

	protected abstract U createOutputGraph();

	protected abstract Set<Period> getAvailablePeriods();
	
	protected abstract boolean showInterim();
	
	protected abstract boolean forFactSheet();
	
	protected abstract int showYears();
	
	public U process() {
		outputGraph = createOutputGraph();
		return outputGraph;
	}
	
	
	
	/**
	 * Most graphs will use the most recent FYE period
	 * Note that for subscribers this will be an AUDIT period as they are not allowed to view DRAFT periods
	 */
	protected final Period getMostRecentFYE() {
		Set<Period> auditPeriods = new HashSet<Period>();
		for (Period period : getAvailablePeriods()) {
			if (period.isFiscalYearEnd()) {
				auditPeriods.add(period);
			}
		}
		return PeriodHelper.getMostRecent(auditPeriods);
	}
	
	
	protected TreeSet<Period> getPeriodsToShow() {
		TreeSet<Period> periods = new TreeSet<Period>();
		
		if (null == getMostRecentFYE()) {
			return periods;
		}
		
		int lastAuditedYear = getMostRecentFYE().getYear();
		for (Period period : getAvailablePeriods()) {
			if (lastAuditedYear - showYears() > period.getYear() - 1) {
				continue; // too far in the past
			}
			if (forFactSheet() && (period.getPeriodType() == PeriodType.DRAFT)) {
				continue;
			}
			if (period.getQuarter() == Period.LAST_QUARTER                  // show all FYE
				    || (!supressExtraFYE() && period.getQuarter() > Period.LAST_QUARTER)  // and extra FYE unless supressed (FactSheet)
				    || showInterim() && period.getYear() > lastAuditedYear  // interims since last FYE
				    || showRatingPeriod(period)                                // or its a rating period
				)
			{
				periods.add(period);
			}
		}
		
		return periods;
	}
	
	// When companies change their fiscal year they end up with two FYE in the same calendar year
	// by default both of these FYEs are shown but this method allows that behaviour to be overriden
	// See the FactSheetService and GraphProcessor
	protected boolean supressExtraFYE() {
		return false;
	}
	
	// Late requirement change was to not unconditionally show the RATING periods
	// Note we don't show rating periods for subscribers, latestFullReview will be null 
	private boolean showRatingPeriod(Period period) {
		if (!showRatingPeriods) {
			return false;
		}
		
		if (latestFullReview != null &&                           
			    latestFullReview.getYear() == period.getYear() &&
			    latestFullReview.getQuarter() == period.getQuarter())
		{
			return true;
		}
		
		return false;
	}



}