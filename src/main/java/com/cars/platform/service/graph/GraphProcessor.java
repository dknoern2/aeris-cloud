package com.cars.platform.service.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.GraphType;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.viewmodels.graph.Graph;
import com.cars.platform.viewmodels.graph.Row;
import com.cars.platform.viewmodels.graph.Value;

abstract class GraphProcessor<U extends Graph> extends BaseGraphProcessor<U> {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(GraphProcessor.class);

	public GraphProcessor(com.cars.platform.domain.Graph domainGraph, Company company,
			Map<Period, Map<Equation, ValueFact>> factByEquationByPeriod, boolean showRatingPeriods)
	{
		super(company, showRatingPeriods);

		this.domainGraph = domainGraph;
		this.factByEquationByPeriod = factByEquationByPeriod;		
		this.periodsToShow = getPeriodsToShow();
	}
	
	protected com.cars.platform.domain.Graph domainGraph;
		
	protected Map<Period, Map<Equation, ValueFact>> factByEquationByPeriod;

	protected TreeSet<Period> periodsToShow;
	
	// The equations that actually make it onto the graph (consider missing data or malformed formula)
	protected List<Equation> equationsOnGraph;
	
	//static BreakdownProcessor<BreakdownGraph, Graph> getProcessor(BreakdownGraph breakdownGraph,
	@SuppressWarnings("rawtypes")
	static GraphProcessor getProcessor(com.cars.platform.domain.Graph domainGraph, Company company,
			Map<Period, Map<Equation, ValueFact>> factByEquationByPeriod, boolean showRatingPeriods)
	{
		// if no metrics then no graph 
		if (domainGraph.getGraphType() == GraphType.TABLE) {
			return new TableProcessor(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
		}
		else if (domainGraph.getGraphType() == GraphType.COLUMN) {
			return new ColumnProcessor(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
		}
		else if (domainGraph.getGraphType() == GraphType.LINE) {
			return new LineProcessor(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
		}
		else if (domainGraph.getGraphType() == GraphType.PIE) {
			return new PieProcessor(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
		}
		else {
			return null;
			//throw new UnsupportedOperationException("Can't handle this type of graph: " + domainGraph.getGraphType());
		}
	}
	
	@Override
	public final U process() {
		super.process();
		
		if (periodsToShow.isEmpty()) {
			return null;
		}
		
		outputGraph.setTitle(domainGraph.getTitle());
		outputGraph.setDate(getDateString(getLatestPeriod()));
		
		Row columnHeaders = new Row();
		columnHeaders.setLabel(""); // leave the header of metrics column blank
		columnHeaders.setValues(getColumnHeaderValues());
		outputGraph.setColumnHeaders(columnHeaders);
		
		outputGraph.setDatas(getDataRows());
		outputGraph.setNotes(domainGraph.getNotes());
		if (equationsOnGraph.size() == 0) {
			return null;
		}
		
		postProcess();

		return outputGraph;
	}
	
	
	@Override
	protected Set<Period> getAvailablePeriods() {
		return factByEquationByPeriod.keySet();
	}

    @Override
	protected boolean showInterim() {
    	return domainGraph.isShowInterim();
	}
    
    @Override
    protected boolean forFactSheet() {
    	return domainGraph.isForFactSheet();
    };

	@Override
	protected int showYears() {
		return domainGraph.getShowYears();
	}
	
	@Override protected boolean supressExtraFYE() {
		return domainGraph.isSupressExtraFYE();
	}
	
	protected Period getLatestPeriod() {
		return periodsToShow.last();
	}

	protected List<Value> getColumnHeaderValues() {
		List<Value> columnHeaders = new ArrayList<Value>();
		for (Period period : periodsToShow) {
			String dateString = getDateString(period);
			Value value = new Value(dateString);
			columnHeaders.add(value);
		}
		return columnHeaders;
	}
	

	protected List<Row> getDataRows() {
		List<Row> dataRows = new ArrayList<Row>();
		equationsOnGraph = new ArrayList<Equation>();
		
		for (Equation equation : domainGraph.getEquations(company)) {
			Row row = new Row();
			row.setLabel(equation.getName());
			ArrayList<Value> values = new ArrayList<Value>();
			boolean valueFound = false;
			for (Period period : periodsToShow) {
				Value value;
				Map<Equation, ValueFact> factByEquation = factByEquationByPeriod.get(period);
				ValueFact valueFact = factByEquation.get(equation);
				if (null == valueFact) {
					value = new Value("");
				}
				else {	
					value = new Value(valueFact.getAmount(), equation, getValueString(valueFact));
					valueFound = true;
				}
				values.add(value);
	
			}
			// don't include formula if there is no data
			if (valueFound) {
				row.setValues(values);
				dataRows.add(row);
				equationsOnGraph.add(equation);
			}
		}
		
		return dataRows;
	}

	
	/**
	 * Subclasses can implement this for subclass specific content (total rows for example)
	 */
	protected void postProcess() {
	}

	
}
