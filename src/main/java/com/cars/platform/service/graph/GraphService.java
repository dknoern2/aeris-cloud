package com.cars.platform.service.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.PerformanceView;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodDim;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.service.AbstractReportService;
import com.cars.platform.util.UserHelper;
import com.cars.platform.viewmodels.graph.Graph;

public class GraphService extends AbstractReportService {
	       
    private List<com.cars.platform.domain.Graph> domainGraphs;
    
	// The facts in this collection are for the CDFI overrides if they exist.
	private Map<Period, Map<Equation, ValueFact>> factByEquationByPeriod;
	
	// A cache to go between Period and PeriodDim
	// We need Period for logic based on PeriodType but the ValueFacts are related to PeriodDim
	private Map<Period, PeriodDim> periodMap;
	
    private Company company;
    
        
    /**
     * The typical way to create the service
     * @param company
     */
    public GraphService(Company company) {
    	this.company = company;
    	
    	List<Period> periods = Period.findViewablePeriodsByCompanySorted(company);
    	
    	this.periodMap = new TreeMap<Period, PeriodDim>();
    	for (Period period : periods) {
    		PeriodDim periodDim = PeriodDim.findByPeriod(period);
    		periodMap.put(period, periodDim);
    	}
    	
	    domainGraphs = com.cars.platform.domain.Graph.findAllGraphsByCompany(company);
	    
	    fetchValueFacts();
    }
    
    /**
     * Fills the list of facts by company by equation collection
     * Note that the Equation key here is the global equation. The ValueFact can be for a company specific override
     */
    private void fetchValueFacts() {
        Set<Equation> allEquations = new HashSet<Equation>();
        for (com.cars.platform.domain.Graph domainGraph : domainGraphs) {
        	allEquations.addAll(domainGraph.getEquations(company));
        }
    	
        factByEquationByPeriod = new HashMap<Period, Map<Equation, ValueFact>>();
		for (Period period : periodMap.keySet()) {
			Map<Equation, ValueFact> factByEquation = new HashMap<Equation, ValueFact>();
			factByEquationByPeriod.put(period, factByEquation);
			PeriodDim periodDim = periodMap.get(period);
    		for (Equation equation : allEquations) {
				ValueFact valueFact = ValueFact.findValueFact(periodDim, equation, company);
				factByEquation.put(equation, valueFact);
    		}
		}
    }
    
    public List<Graph> getPerformanceViewGraphs() {
    	PerformanceView performanceView = null;
    	List<Graph> graphs = null;
    	try {
    		performanceView = PerformanceView.findPerformanceViewsByCompany(company);
    	}
    	catch (Exception ignore) {} // findPerformanceViewsByCompany throws if there are none

    	if (performanceView != null) {
    		List<com.cars.platform.domain.Graph> domainGraphs = performanceView.getGraphs();
    		graphs = new ArrayList<Graph>();
    		for (com.cars.platform.domain.Graph domainGraph : domainGraphs) {
    			Graph graph = getGraph(domainGraph, false);
    			if (null != graph) {
    				graphs.add(graph);
    			}
    		}
    	}
    	return graphs;	
    }
    
    public List<Graph> getGraphs(boolean online, boolean showRatingPeriods) {
    	List<Graph> graphs = new ArrayList<Graph>();
    	for (com.cars.platform.domain.Graph domainGraph : domainGraphs) {
//    		if (!online && !breakdownGraph.isExport()) {
//    			continue;
//    		}
    		Graph graph = getGraph(domainGraph, showRatingPeriods);
    		if (null != graph) {
    			graphs.add(graph);
    		}
    	}
    	return graphs;
    }

	public Graph getGraph(com.cars.platform.domain.Graph domainGraph, boolean showRatingPeriods) {
		Graph graph = null;
		
		GraphProcessor<Graph> processor =
				GraphProcessor.getProcessor(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
		
		if (null == processor) {
			return null;
		}

		graph = processor.process();

        return graph;
	}
	

}
