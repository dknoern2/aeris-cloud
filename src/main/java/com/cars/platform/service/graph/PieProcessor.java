package com.cars.platform.service.graph;

import java.util.Map;
import java.util.TreeSet;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.viewmodels.graph.BarChart;
import com.cars.platform.viewmodels.graph.LineChart;
import com.cars.platform.viewmodels.graph.PieChart;

class PieProcessor extends ChartProcessor<PieChart> {

	public PieProcessor(com.cars.platform.domain.Graph domainGraph, Company company,
			Map<Period, Map<Equation, ValueFact>> factByEquationByPeriod, boolean showRatingPeriods)
	{
		super(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
	}

    @Override
	protected PieChart createOutputGraph() {
        return new PieChart();
	}
    
    @Override
	protected TreeSet<Period> getPeriodsToShow() {
		TreeSet<Period> periods = new TreeSet<Period>();
		
		periods.add(getMostRecentFYE());
		
		return periods;
    }
    
    @Override
	protected void postProcess() {
		outputGraph.setTitle(domainGraph.getTitle() + " (" + getDateString(getLatestPeriod()) + ")");
	}


}
