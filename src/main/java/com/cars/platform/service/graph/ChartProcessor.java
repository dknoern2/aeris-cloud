package com.cars.platform.service.graph;

import java.util.Map;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.viewmodels.graph.BarChart;
import com.cars.platform.viewmodels.graph.Chart;
import com.cars.platform.viewmodels.graph.LineChart;
import com.cars.platform.viewmodels.graph.PieChart;

abstract class ChartProcessor<U extends Chart> extends GraphProcessor<Chart> {

	public ChartProcessor(com.cars.platform.domain.Graph domainGraph, Company company,
			Map<Period, Map<Equation, ValueFact>> factByEquationByPeriod, boolean showRatingPeriods)
	{
		super(domainGraph, company, factByEquationByPeriod, showRatingPeriods);
	}


    @Override
    protected void postProcess() {
    		
    	outputGraph.setUnitType(domainGraph.getUnitType());
    	
    	// this method isn't called if there aren't formulas on the graph but we'll be anal here
    	// and avoid index out of bounds
    	if (equationsOnGraph.size() > 0) {
	    	Equation firstEquation = equationsOnGraph.get(0);
	    	outputGraph.setDecimalPlaces(firstEquation.getDecimalPlaces());
    	}
    	else {
    		outputGraph.setDecimalPlaces(0);
    	}
    }

}
