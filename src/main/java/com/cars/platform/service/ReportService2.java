package com.cars.platform.service;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Equation;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodDim;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.domain.ReportCategory;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.domain.UnitType;
import com.cars.platform.domain.ValueFact;
import com.cars.platform.viewmodels.report.Category;
import com.cars.platform.viewmodels.report.Data;
import com.cars.platform.viewmodels.report.Report;


public class ReportService2 extends AbstractReportService {
	
	private Company company;
	
	// do we return rows without metric data?
	private boolean allRows;
	
	// online and offline (export) need different data
	// online receives the data values to display
	// offline receives formulas in terms of account codes on the input tab
	private boolean online;
	
	// The period and period dimensions for the company sorted ascending by date
	// example key: 2013Q3
	private SortedMap<String, Period> periodMap;
	private SortedMap<String, PeriodDim> periodDimMap;
	
    public ReportService2(Company company) {
    	super();
		this.company = company;
	}
	
	
	public Report getReportData(ReportPage reportPage, boolean allRows, boolean online) {
		this.allRows = allRows;
		this.online = online;
		
		getPeriodDims();
				
		Report report = new Report(reportPage);
		
		report.setColumns(getColumns());
		report.setCategories(getCategories(reportPage, null));

		return report;
	}

 
	private void getPeriodDims() {
		periodMap = Period.findPeriodMapByCompany(company);
		periodDimMap = new TreeMap<String, PeriodDim>();	
		for (String key : periodMap.keySet()) {
			PeriodDim periodDim = PeriodDim.findByPeriod(periodMap.get(key));
			periodDimMap.put(key, periodDim);
		}
	}


	private List<String> getColumns() {
		boolean first = true;

		List<String> columns = new ArrayList<String>();
		for (String periodKey : periodMap.keySet()) {

		    String periodFieldLabel = getPeriodFieldLabel(periodMap.get(periodKey));
		    columns.add(periodFieldLabel);
		    if (!first) {
		    	columns.add("CH");
		    	columns.add("%CH");
		    }

		    first = false;
		}

		return columns;
	}
	
	// TODO handle Full Ratings
    private String getPeriodFieldLabel(Period period) {

		String fieldLabel = "DRAFT";
		if (PeriodType.AUDIT.equals(period.getPeriodType())) {
		    fieldLabel = "AUDIT";
		}
		else if (PeriodType.INTERIM.equals(period.getPeriodType())) {
		    fieldLabel = "INTERIM";
		}
		else if (PeriodType.INTERNAL.equals(period.getPeriodType())) {
		    if (period.getQuarter() == Period.LAST_QUARTER)
		    	fieldLabel = "UNAUDITED";
		    else
		    	fieldLabel = "INTERNAL";
		}

		fieldLabel += " " + period.getEndDate();

		return fieldLabel;
    }

	
	private List<Category> getCategories(ReportPage reportPage, ReportCategory parent) {
     	List<ReportCategory> reportCategories = ReportCategory.findByReportPageAndParent(reportPage, parent);
     	if (null == reportCategories || reportCategories.size() == 0) {
     		return null;
     	}
     	
	    List<Category> categories = new ArrayList<Category>();
	    
    	for (ReportCategory reportCategory : reportCategories) {
    		Category category = new Category(reportCategory);
        		category.setDatas(getDatas(reportCategory));
    		List<Category> subCategories = getCategories(reportPage, reportCategory);
    		category.setSubCategories(subCategories);	
    		categories.add(category);	
    	}
    	
    	return categories;
	}


	private List<com.cars.platform.viewmodels.report.Equation> getDatas(ReportCategory reportCategory) {
		List<Equation> equations = reportCategory.getEquations(company, false);
		List<com.cars.platform.viewmodels.report.Equation> datas = new ArrayList<com.cars.platform.viewmodels.report.Equation>();
		
		for (Equation equation : equations) {
			List<String> dataValues = getDataValues(equation);
			if (null != dataValues) {
				Data data = new Data(equation);
				data.setValues(dataValues);
				datas.add(data);
			}
		}
		
		return datas;
	}
	
	
	// returns null if no data found and allRows is false
	private List<String> getDataValues(Equation equation) {
		List<String> values = new ArrayList<String>();
		Double previousAmount = null;
		boolean foundData = false;
		
		boolean first = true;
		for (PeriodDim periodDim : periodDimMap.values()) {
    		ValueFact valueFact = ValueFact.findValueFact(periodDim, equation, company);
    		if (online) {
    			values.add(this.getValueString(valueFact));
    		}
    		else {
    			if (null == valueFact || null == valueFact.getSpreadFormula()) {
    				values.add("");
    			}
    			else {
    				values.add(valueFact.getSpreadFormula());
    			}
    		}
        	
        	Double amount = (null == valueFact) ? null : valueFact.getAmount();
        	if (null != amount) {
        		foundData = true;
        	}
 
        	// No deltas for the first period
        	if (!first) {
				if (!UnitType.PERCENTAGE.equals(equation.getUnitType())
						&& amount != null
						&& previousAmount != null
						&& previousAmount > .1)
				{
					if (online) {
					    double change = amount - previousAmount;
					    int decimalPlaces = equation.getDecimalPlaces();
					    changeDecimalFormat.setMaximumFractionDigits(decimalPlaces);
					    changeDecimalFormat.setMinimumFractionDigits(decimalPlaces);
					    values.add(changeDecimalFormat.format(change));
		
					    double pctChange = (change) / Math.abs(previousAmount);
					    if (Math.abs(pctChange) < 0.1) {
					    	changePercentFormat.setMaximumFractionDigits(1);
					    	changePercentFormat.setMinimumFractionDigits(1);
					    }
					    else {
					    	changePercentFormat.setMaximumFractionDigits(0);
					    	changePercentFormat.setMinimumFractionDigits(0);
					    }
					    values.add(changePercentFormat.format(pctChange));
					}
					else {
						// a bit of a hack but overriding meaning here to boolean telling offline whether or not to render a formula or leave cell  blank
						values.add("true");
						values.add("true");
					}
				} else {
					if (online) {
					    values.add("");
					    values.add("");
					}
					else {
						values.add("false");
						values.add("false");			
					}
				}
        	}
        	
        	first = false;
    	    if (periodDim.getQuarter() > 3) {
    	    	previousAmount = amount;
    		}
		}

		if (allRows || foundData) {
			return values;
		}
		else {
			return null;
		}
	}
	

}
