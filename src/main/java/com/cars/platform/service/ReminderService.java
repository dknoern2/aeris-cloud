package com.cars.platform.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cars.platform.domain.Access;
import com.cars.platform.domain.ActionItem;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentTypeCategory;
import com.cars.platform.domain.DocumentTypeFolder;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.PropertiesHelper;

@Service
public class ReminderService {
	
	@Autowired
	ServletContext servletContext;

	@Autowired
	EmailService emailService;
	
	@Autowired
	PropertiesHelper propertyHelper;

	private static final Logger logger = LoggerFactory
			.getLogger(ReminderService.class);

	// every day at 9 am
	// sec min hour dom mon dow
	@Scheduled(cron = "0 0 9 * * *")
	//@Scheduled(fixedDelay=500000000)
	@Transactional
	public void processReminders() {
		
		long start = System.currentTimeMillis();

		logger.info("Running reminder service");
		System.out.println("Running reminder service");

		processActionItemReminders();
		processNewReportReminders();
		processSubscriptionReminders();
		processSelectSubscriptionReminders();
		
		List<Period> finalizedPeriods = Period.findPeriodsFinalizedSinceDate(getDateDaysFromNow(-1));
		String finalizedFileName = "/WEB-INF/email/NewFinancialsNotification.txt";;
		String finalizedSubject = "Aeris New Quarterly Financials Notification";
		processNewFinancialsReminders(finalizedPeriods, finalizedFileName, finalizedSubject);
		
		List<Period> auditedPeriods = Period.findPeriodsAuditedSinceDate(getDateDaysFromNow(-1));
		String auditedFileName = "/WEB-INF/email/NewAuditedFinancialsNotification.txt";
		String auditedSubject = "Aeris New Audited Financials Notification";
		processNewFinancialsReminders(auditedPeriods, auditedFileName, auditedSubject);
		
		long duration = System.currentTimeMillis() - start;
		System.out.println("Reminder Service Finished in " + duration + "ms");
	}
	
	public boolean processLibraryNotification(List<Company> viewers, Company cdfi) {
		boolean ans=true;
		if (viewers != null && cdfi != null) {
			for (Company viewer : viewers) {
				try {
					sendLibraryNotification(viewer, cdfi);
				} catch (Throwable t) {
					logger.error("Unexpected exception processing notification for viewer: " + viewer.getName()
						+ ". [" + t.getMessage() + "]");
					t.printStackTrace();
					ans = false;
				}
			}
		}
		return ans;
	}

	// every day at 9 am
	// sec min hour dom mon dow
	//@Scheduled(cron = "0 0 9 * * *")
	//@Scheduled(fixedDelay=5000)
	public void processActionItemReminders() {
		List<Company> cdfis = Company.findCompanysByCompanyTypeAlphabetically(CompanyType.CDFI);
		for (Company cdfi : cdfis) {
			try {
				int emailsSent = sendActionItemReminders(cdfi, null);
				if (emailsSent > 0) {
					System.out.println("Sent " + emailsSent + " email(s) to CDFI: " + cdfi.getName());				
				}
			}
			catch (Throwable t) {
				logger.error("Unexpected exception processing action item reminders for CDFI: " + cdfi.getName()
						+ ". [" + t.getMessage() + "]");
				t.printStackTrace();
			}
		}
	}
	
	private void processNewReportReminders() {
		try {

			// find new rating and opinion docs;

			List<Document> documents = Document
					.findNewApprovedDocumentsByDocumentTypeFolder(
							DocumentTypeFolder.CARS_REPORT,
							getDateDaysFromNow(-1), getDateDaysFromNow(0));

			if (documents.size() == 0) {
				System.out.println("no new reports found");
				return;
			}

			List<Subscription> subscriptions = Subscription
					.findSubscriptionsByExpirationDateGreaterThan(new Date())
					.getResultList();


			if (subscriptions == null || subscriptions.size() == 0) {
				System.out.println("no active subscriptions found");
				return;
			}

			System.out.println("found " + documents.size() + " new reports");

			HashMap<Company, HashSet<Document>> notifications = new HashMap<Company, HashSet<Document>>();

			for (Document document : documents) {

				// create a unique list of subscribers that have access.
				// could be subscription that subscriber requested
				// or cars select subscription provided by CDFI

				for (Subscription subscription : subscriptions) {
					if (subscription.getExpirationDate().before(new Date())) {
						continue;
					}

					List<Company> subscribers = subscription.getSubscribersForDocument(document);
					if (null == subscribers || subscribers.size() == 0) {
						continue;
					}
					for (Company subscriber : subscribers) {
						HashSet<Document> docsForCompany = notifications.get(subscriber);
						if (null == docsForCompany) {
							docsForCompany = new HashSet<Document>();
							notifications.put(subscriber, docsForCompany);
						}
						docsForCompany.add(document);
					}
				}
			}

			for (Map.Entry<Company, HashSet<Document>> entry : notifications.entrySet()) {
				Company company = entry.getKey();
				HashSet<Document> companyDocs = entry.getValue();
				try {
					sendReportReminder(company, companyDocs);
				}
				catch (Exception e) {
					logger.error("Unexpected Exception sending report reminder for subscriber: " + company.getName() + ": " +  e.getMessage());
					e.printStackTrace();
				}
			}
		}
		catch (Exception e) {
			logger.error("Unexpected Exception processing report reminders: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void processSubscriptionReminders() {
		List<Subscription> subscriptions = Subscription
				.findSubscriptionsByExpirationDateBetween(
						getDateDaysFromNow(-1),
						getDateDaysFromNow(31))
						.getResultList();
		
		for (Subscription subscription : subscriptions) {
			try {
				boolean emailSent = false;
				if (subscription.getSubscriptionType() == SubscriptionType.SELECT) {
					continue;
				}
				else {
					emailSent = sendSubscriptionReminder(subscription);
					if (emailSent) {
						System.out.println("Sent subscription reminder email to: " + subscription.getOwner().getName());				
					}
				}
			}
			catch (Throwable t) {
				logger.error("Unexpected exception processing subscription reminder for: " + subscription.getOwner().getName()
						+ ". [" + t.getMessage() + "]");
				t.printStackTrace();
			}
		}
	}

	
	private void processSelectSubscriptionReminders() {
		List<Subscription> subscriptions = Subscription
				.findSubscriptionsBySubscriptionTypeAndExpirationDateGreaterThan(
						SubscriptionType.SELECT,
						getDateDaysFromNow(-1));
		
		for (Subscription subscription : subscriptions) {
			try {
				boolean emailSent = false;
				for (Access access : subscription.getSubscribers()) {
				    emailSent = sendSelectReminders(access);
					if (emailSent) {
						System.out.println("Sent select reminder email to: " + access.getCompany().getName());				
					}
				}
			}
			catch (Throwable t) {
				logger.error("Unexpected exception processing subscription reminder for: " + subscription.getOwner().getName()
						+ ". [" + t.getMessage() + "]");
				t.printStackTrace();
			}
		}
	}
	
	
	private void processNewFinancialsReminders(List<Period> periods, String fileName, String subject) {
		try {
			if (null == periods || periods.size() == 0) {
				logger.info("no new finalized financials");
				return;
			}
	
			List<Subscription> subscriptions = Subscription
					.findSubscriptionsByExpirationDateGreaterThan(new Date())
					.getResultList();
	
	
			if (subscriptions == null || subscriptions.size() == 0) {
				logger.info("no active subscriptions found");
				return;
			}
	
			System.out.println("found " + periods.size() + " new finalized financials");
	
			HashMap<Company, HashSet<Company>> notifications = new HashMap<Company, HashSet<Company>>();
			for (Period period : periods) {
				Company cdfiWithFinancials = period.getCompany();
	
				for (Subscription subscription : subscriptions) {
					if (!subscription.isFinancials()) {
						continue;
					}
					if (subscription.getExpirationDate().before(new Date())) {
						continue;
					}
					
					Company subscriber = subscription.getOwner();
					for (Access cdfiAccess : subscription.getCdfis()) {
						Company cdfi = cdfiAccess.getCompany();
						if (cdfi.getId().equals(cdfiWithFinancials.getId())) {
							HashSet<Company> cdfisForSubscriber = notifications.get(subscriber);
							if (null == cdfisForSubscriber) {
								cdfisForSubscriber = new HashSet<Company>();
								notifications.put(subscriber, cdfisForSubscriber);
							}
							cdfisForSubscriber.add(cdfi);
						}
					}
				}
			}
	
			for (Map.Entry<Company, HashSet<Company>> entry : notifications.entrySet()) {
				Company subscriber = entry.getKey();
				HashSet<Company> cdfis = entry.getValue();
				try {
					sendFinancialsReminder(subscriber, cdfis, fileName, subject);
				}
				catch (Exception e) {
					logger.error("Unexpected Exception processing financials reminder for subscriber: " + subscriber.getName() + ": " +  e.getMessage());
					e.printStackTrace();
				}
					
			}
		}
		catch (Exception e) {
			logger.error("Unexpected Exception processing financial reminders: " + e.getMessage());
			e.printStackTrace();
		}
	}


	
	private void sendLibraryNotification(Company viewer, Company cdfi) {
		String recipients = getRecipients(viewer, null, ReminderType.NOTIFICATION);
		if (null == recipients) {
			logger.info("No email recipients, not sending notification to: " + viewer.getName());
			return;
		}
		
		String body = getNotificationBody(cdfi);
		if (null != body) {	
			emailService.sendEmail(recipients, "New Documents Notification", body, true);
			return;
		}

	}
		
	private void sendReportReminder(Company company, Set<Document> documents) {
		String recipients = getRecipients(company, null, ReminderType.REPORTS);
		if (null == recipients) {
			logger.info("No email recipients, not sending report reminder to: " + company.getName());
			return;
		}
		
		Set<Company> cdfis = new HashSet<Company>();
		for (Document document : documents) {
			cdfis.add(document.getCompany());
		}

		
		String body = getReportBody(cdfis);
		if (null != body) {	
			emailService.sendEmail(recipients, "Aeris Report Release Notification", body, true);
			System.out.println("Sent report reminder to " + company.getName());				
			return;
		}

	}

	private boolean sendSelectReminders(Access access) {
		Date expirationDate = access.getExpirationDate();
		if (null == expirationDate) {
			logger.warn("No expiration data on Access for a SELECT subscription");
			return false;
		}
		int expireDays =  DateHelper.getDaysUntilDue(expirationDate);
		String dayString = null;
		if (expireDays == 30) {
			dayString = "30";
		}
		else if (expireDays == 0) {
			dayString = "0";
		}
		else {
			return false;
		}

		Company company = access.getCompany();
		String recipients = getRecipients(company, null, ReminderType.SUBSCRIPTION);
		if (null == recipients) {
			logger.info("No email recipients, not sending select reminder to: " + company.getName());
			return false;
		}
		
		String body = getSelectBody(access, dayString);
		if (null != body) {	
			emailService.sendEmail(recipients, "Aeris Subscription Reminder", body, true);
			return true;
		}
		
		return false;
	}

	private boolean sendSubscriptionReminder(Subscription subscription) {
		int expireDays = subscription.getDaysUntilExpiration();
		String dayString = null;
		if (expireDays == 30) {
			dayString = "30";
		}
		else if (expireDays == 7) {
			dayString = "7";
		}
		else if (expireDays == 0) {
			dayString = "0";
		}
		else {
			return false;
		}
		
		Company company = subscription.getOwner();
		String recipients = getRecipients(company, null, ReminderType.SUBSCRIPTION);
		if (null == recipients) {
			logger.info("No email recipients, not sending subscription reminder to: " + company.getName());
			return false;
		}
		
		String body = getSubscriptionBody(subscription, dayString);
		if (null != body) {	
			emailService.sendEmail(recipients, "Aeris Subscription Reminder", body, true);
			return true;
		}
		return false;
	}

	
	private boolean sendFinancialsReminder(Company subscriber, Set<Company> cdfis, String fileName, String subject) {
		String recipients = getRecipients(subscriber, null, ReminderType.NEW_FINANCIALS);
		if (null == recipients) {
			logger.info("No email recipients, not sending financial reminder to: " + subscriber.getName());
			return false;
		}
		
		String body = getFinancialsBody(subscriber, cdfis, fileName);
		if (null != body) {	
			emailService.sendEmail(recipients, subject, body, true);
			return true;
		}
		return false;
	}

	

	private Date getDateDaysFromNow(int days) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, cal.get(Calendar.DATE) + days);
		return cal.getTime();
	}


	
	/**
	 * 
	 * @param company is the CDFI for which reminders emails will be sent
	 * @param overridToAddress is an optional email address to send the reminders to
	 */
	public int sendActionItemReminders(Company company, String toAddressOverride) {
		ActionItemService actionItemService = new ActionItemService(company);
		List<ActionItem> actionItems = actionItemService.getActionItems(true);
		int emailsSent = 0;
		String subject = null;
		for (ActionItem actionItem : actionItems) {
			String typeString = null;
			if (actionItem.getTaskType() == ActionItem.TASK_TYPE_ANNUAL_REVIEW_DOCUMENTS) {
				typeString = "ANNUAL";
				subject = "Aeris Cloud Document Request Reminder";
			}
			else if (actionItem.getTaskType() == ActionItem.TASK_TYPE_FULL_ANALYSIS_DOCUMENTS) {
				typeString = "FULL";
				subject = "Aeris Cloud Document Request Reminder";
			}
			else if (actionItem.getTaskType() == ActionItem.TASK_TYPE_UPLOAD_QUARTERLY) {
				typeString = "QUARTER";
				subject = "Aeris Quarterly Data Reminder";
			}
			else {
				continue;
			}
			
			int dueDays = actionItem.getDaysUntilDue();
			String dayString = null;
			if (dueDays == 30) {
				dayString = "30";
			}
			else if (dueDays == 7) {
				dayString = "7";
			}
			else if (dueDays == 0) {
				dayString = "0";
			}
			else if (dueDays < 0 && dueDays%7 == 0) {
			//else if (dueDays < 0 ) {
				dayString = "PASTDUE";
			}
			else {
				continue;
			}
			
			String recipients = getRecipients(company, toAddressOverride, ReminderType.ACTION_ITEM);
			if (null == recipients) {
				logger.info("No email recipients, not sending action item reminder for: " + company.getName());
				return 0;
			}
			
			
			String body = getActionBody(company, typeString, dayString);
			if (null != body) {	
				emailService.sendEmail(recipients, subject, body, true);
				emailsSent++;
			}
		}
		
		return emailsSent;
	}
	
	
	private String getRecipients(Company company, String override, ReminderType reminderType) {
		if (null != override) {
			return override;
		}
		
		List<Person> employees = Person.findPeopleByCompany(company).getResultList();
		List<String> recipients = new ArrayList<String>();
		for (Person person : employees) {
			String emailAddress = person.getEmail();
			if (null == emailAddress) {
				continue;
			}
			if (ReminderType.ACTION_ITEM.equals(reminderType) && !person.isEmailUploadRemindersEnabled()) {
				continue;
			}
			if (ReminderType.SUBSCRIPTION.equals(reminderType) && !person.isEmailRemindersEnabled()) {
				continue;
			}
			
			if (ReminderType.REPORTS.equals(reminderType) && !person.isEmailRemindersEnabled()) {
				continue;
			}
			
			if (ReminderType.NEW_FINANCIALS.equals(reminderType) && !person.isNewFinancialRemindersEnabled()) {
				continue;
			}
			
			try {
				InternetAddress.parse(emailAddress);
				recipients.add(emailAddress);
			} catch (AddressException e) {
				logger.error("Failed to send email for company: " + company.getName()
						+ "to malformed email address: " + emailAddress);
			}	
		}
		
		if (recipients.size() == 0) {
			return null;
		}
		
		return StringUtils.join(recipients, ',');
	}
	
	private String getNotificationBody(Company cdfi) {
		String fileName = "/WEB-INF/email/LibraryNotification.txt";
		
		String template = getTemplate(fileName);
		if (null == template) {
			return null;
		}
		
		template = template.replace("CDFI_NAME", cdfi.getName());
		
		template = template.replace("BASE_URL", propertyHelper.getPlatformUrl());
		return template.replace("CDFI_ID", Long.toString(cdfi.getId()));
	}
	
	
	private String getReportBody(Set<Company> cdfis) {
		String fileName = "/WEB-INF/email/ReportReminder.txt";
		
		String template = getTemplate(fileName);
		if (null == template) {
			return null;
		}
		
		StringBuilder sb = new StringBuilder();
		for (Company company : cdfis) {
			sb.append("<li>" + company.getName() + "</li>");
		}
		template = template.replace("CDFIS", sb.toString());
		template = template.replace("BASE_URL", propertyHelper.getPlatformUrl());
		
		return template;
	}



	private String getActionBody(Company company, String typeString, String dayString) {
		String fileName = "/WEB-INF/email/UploadReminder_" + typeString + "_" + dayString + ".txt";
		
		String template = getTemplate(fileName);
		if (null == template) {
			return null;
		}
		
		template = template.replace("BASE_URL", propertyHelper.getPlatformUrl());
		return template.replace("CDFI_ID", Long.toString(company.getId()));
	}
	

	
	private String getSubscriptionBody(Subscription subscription, String dayString) {
		String fileName = "/WEB-INF/email/SubscriptionReminder_" + dayString + ".txt";
		
		String template = getTemplate(fileName);
		if (null == template) {
			return null;
		}
		
		template = template.replace("BASE_URL", propertyHelper.getPlatformUrl());
		
		StringBuilder sb = new StringBuilder();
		if (subscription.isRatings()) {
			sb.append("<li>Rating Reports</li>");
		}
		if (subscription.isOpinions()) {
			sb.append("<li>Opinion Reports</li>");
		}
		if (subscription.isFinancials()) {
			sb.append("<li>Quarterly Financials</li>");
		}
		if (subscription.isPeerGroups()) {
			sb.append("<li>Peer Group Reports</li>");
		}
		if (subscription.isLibrary()) {
			sb.append("<li>Library Access</li>");
		}
		template = template.replace("SERVICES", sb.toString());

		return template;
	}

	
	private String getSelectBody(Access access, String dayString) {
		String fileName = "/WEB-INF/email/SelectReminder_" + dayString + ".txt";
		
		String template = getTemplate(fileName);
		if (null == template) {
			return null;
		}
		
		template = template.replace("BASE_URL", propertyHelper.getPlatformUrl());
		template = template.replace("MARKETING_URL", propertyHelper.getMarketingUrl());
		return template;
	}
	
	
	private String getFinancialsBody(Company subscriber, Set<Company> cdfis, String fileName) {
		String template = getTemplate(fileName);
		if (null == template) {
			return null;
		}
		
		StringBuilder sb = new StringBuilder();
		for (Company company : cdfis) {
			sb.append("<li>" + company.getName() + "</li>");
		}
		template = template.replace("CDFIS", sb.toString());
		template = template.replace("BASE_URL", propertyHelper.getPlatformUrl());
		
		return template;
	}
	
	private String getTemplate(String fileName) {
		FileInputStream is;
    	
    	String pathname = servletContext.getRealPath(fileName);
		try {			
			is = new FileInputStream(pathname);

		} catch (FileNotFoundException e) {
			logger.error("Can't find email template named: " + fileName);
			return null;
		}
		
		String template;
		try {
			template = IOUtils.toString(is);
		} catch (IOException e) {
			logger.error("Unable to convert file to String: " + fileName);
			return null;
		}
		return template;
	}

	
	private static enum ReminderType {
		SUBSCRIPTION,
		ACTION_ITEM,
		REPORTS,
		NOTIFICATION,
		NEW_FINANCIALS
	}
}
