package com.cars.platform.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.transaction.annotation.Transactional;

import com.cars.platform.domain.ActionItem;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricMap;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.ReviewActionItem;
import com.cars.platform.domain.ReviewType;
import com.cars.platform.util.DateHelper;

public class ActionItemService {

	/**
	 * Number of days after quarter end, during which submissions are prepared.
	 * End of quarter date + this interval = due date.
	 */
	private static final int MAX_QUARTERY_FINANCIALS_SUBMISSION_INTERVAL_DAYS = 45;
	private static final int MAX_ENTER_WEBFORM_SUBMISSION_INTERVAL_DAYS = 45;
	
	private Company company;
	private Map<Long, List<Document>> docCache;
	
	public ActionItemService(Company company) {
		this.company = company;
		
		docCache = Document.findDocumentsByTypeForCompany(company);
//		if (company.getId() == 134) {
//			showDocCache();
//		}
	}
	
	private void showDocCache() {
		for (Entry<Long, List<Document>>  entry : docCache.entrySet()) {
			System.out.println(entry.getKey());
			for (Document doc : entry.getValue()) {
				System.out.println("    " + doc.getFiscalYear() + "/" + doc.getFiscalQuarter() + ": " + doc.getFileName());
			}
		}
	}

	//@Transactional not sure what the intent was here
	public List<ActionItem> getActionItems(Boolean useSkipNotification) {

		List<ActionItem> actionItems = new ArrayList<ActionItem>();

		if (company.getCompanyType() == CompanyType.CDFI) {

			// quarterly data is required but only if quarterly reports have
			// started or if annual review is scheduled
			//if (Review.findMostRecentCompletedReview(company) != null
			//|| MetricValue.getMostRecentReportedPeriod(company) != null) {

			// not sure we really need a call to action for historic
			/*
			 * DocumentType historicFinancialsDocumentType = DocumentType
			 * .findDocumentTypesByNameEquals
			 * (DocumentType.DOCUMENT_TYPE_HISTORIC_FINANCIALS
			 * ).getSingleResult();
			 * 
			 * // check for historic financials
			 * 
			 * List<Document> historicFinancials =
			 * Document.findDocument(company,
			 * historicFinancialsDocumentType, 0, 0); if
			 * (company.isRated()&&(historicFinancials == null ||
			 * historicFinancials.size() == 0)) {
			 * 
			 * ActionItem actionItem = new ActionItem();
			 * actionItems.add(actionItem);
			 * 
			 * actionItem.setDaysUntilDue(7);
			 * 
			 * String title = "Historic financial data upload";
			 * 
			 * actionItem.setTitle(title); actionItem.setDescription(title);
			 * actionItem.setPercentComplete(0);
			 * actionItem.setTaskType(ActionItem
			 * .TASK_TYPE_UPLOAD_HISTORIC_FINANCIALS); }
			 */
			int currentYear = DateHelper.getCurrentYear(company.getFiscalYearStart());
			int currentQuarter = DateHelper.getCurrentQuarter(company.getFiscalYearStart());

			int year = company.getStartRequestingYear();
			int quarter = company.getStartRequestingQuarter();

			if (year == 0 || quarter == 0)
			{
				year = currentYear;
				quarter = currentQuarter;

				DocumentType documentType = DocumentType.findDocumentType(DocumentType.DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID);
				List<Document> documents = docCache.get(documentType.getId());
				if (null != documents) {
					for (Document document: documents)
					{
						int documentYear = document.getFiscalYear();
						int documentQuarter = document.getFiscalQuarter();
						if (DateHelper.DeltaQuarters(documentYear, documentQuarter, year, quarter) < 0)
						{
							year = documentYear;
							quarter = documentQuarter;
						}
					}
				}

				year = year + quarter/4;
				quarter = quarter%4 + 1;
			}
			
			
			List<DocumentType> allDocTypes = DocumentType.findAllDocumentTypesSorted(company);
			while (DateHelper.DeltaQuarters(year, quarter, currentYear, currentQuarter) < 0)
			{
				processQuarterlyActionItems(allDocTypes, actionItems, year, quarter);
				year = year + quarter/4;
				quarter = quarter%4 + 1;
			}
		}

		// check for review cycle
		ReviewActionItem reviewActionItem = getReviewActionItem(useSkipNotification);
		if (reviewActionItem != null) {
			actionItems.add(reviewActionItem);
		}

		return actionItems;
	}

	private void processQuarterlyActionItems(List<DocumentType> allDocTypes, List<ActionItem> actionItems, int year, int quarter) {
		List<DocumentType> missingDocTypes = new ArrayList<DocumentType>();
		boolean financialsFound = false;
		for (DocumentType docType : allDocTypes) {
			if (!docType.getDocumentTypeFolder().cdfiResponsible()) {
				continue;
			}
			
			if (!docType.isQuarterly()) {
				continue;
			}

			List<Document> documents = getDocuments(docType, year, quarter);
			if (null == documents || documents.size() == 0) {
				missingDocTypes.add(docType);
				continue;
			}
			
			if (docType.getId() == DocumentType.DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID) {
				financialsFound = true;
			}
		}


		// if not there, create task
		if (missingDocTypes.size() > 0) {
			ActionItem actionItem = new ActionItem();
			actionItems.add(actionItem);

			// figure out due date: 45 days after end of quarter
			Calendar dueDate = DateHelper.getQuarterStart(company.getFiscalYearStart(), year, quarter + 1);
			dueDate.set(Calendar.DATE, dueDate.get(Calendar.DATE) + MAX_QUARTERY_FINANCIALS_SUBMISSION_INTERVAL_DAYS);

			int daysUntilDue = DateHelper.getDaysUntilDue(dueDate);

			actionItem.setDaysUntilDue(daysUntilDue);

			actionItem.setTitle(year + "Q" + quarter + " Quarterly upload " + DateHelper.getDueInString(daysUntilDue));
			actionItem.setDescription("Please upload the documents that are required on a quarterly basis.");

			actionItem.setPercentComplete(0);
			actionItem.setYear(year);
			actionItem.setQuarter(quarter);
			actionItem.setTaskType(ActionItem.TASK_TYPE_UPLOAD_QUARTERLY);
			actionItem.setDocumentTypes(missingDocTypes);
		}
		
		if (financialsFound) {

			// check to see if mapping is required

			List<MetricMap> metricMaps = MetricMap.findMetricMaps(company, year, quarter);
			if (metricMaps == null || metricMaps.size() == 0) {
				// mapping needs to take place

				ActionItem actionItem = new ActionItem();
				actionItems.add(actionItem);

				int daysUntilDue = 2;
				actionItem.setDaysUntilDue(daysUntilDue);

				actionItem.setTitle(year + "Q" + quarter + " Financial statement mapping required "
						+ DateHelper.getDueInString(daysUntilDue));
				actionItem
				.setDescription("The Aeris administrator needs to map your recent quarterly financials upload.  You will be contacted if any additional information is required."
						+ ".");

				actionItem.setPercentComplete(0);
				actionItem.setYear(year);
				actionItem.setQuarter(quarter);
				actionItem.setTaskType(ActionItem.TASK_TYPE_MAP_QUARTERLY_FINANCIALS);

			} else {
				// check if web form is required
				List<Metric> requiredMetrics = company.getRequiredMetrics();
				List<MetricValue> metricValues = MetricValue.findMetricValues(quarter, year, company.getId());
				HashSet<Metric> metricsWithValues = new HashSet<Metric>();
				for (MetricValue metricValue : metricValues) {
					metricsWithValues.add(metricValue.getMetric());
				}

				int missingMetricValues = 0;
				for (Metric requiredMetric : requiredMetrics) {
					if (!metricsWithValues.contains(requiredMetric)) {
						missingMetricValues++;
					}
				}

				if (missingMetricValues > 0) {

					ActionItem actionItem = new ActionItem();
					actionItems.add(actionItem);

					Calendar dueDate = DateHelper.getQuarterStart(company.getFiscalYearStart(), year, quarter + 1);
					dueDate.set(Calendar.DATE, dueDate.get(Calendar.DATE) + MAX_ENTER_WEBFORM_SUBMISSION_INTERVAL_DAYS);

					int daysUntilDue = DateHelper.getDaysUntilDue(dueDate);

					actionItem.setDaysUntilDue(daysUntilDue);

					actionItem.setTitle(year + "Q" + quarter + " Additional data submission "
							+ DateHelper.getDueInString(daysUntilDue));
					actionItem.setDescription("Please enter your web form data for the last financial quarter.");

					actionItem.setPercentComplete(0);
					actionItem.setYear(year);
					actionItem.setQuarter(quarter);
					actionItem.setTaskType(ActionItem.TASK_TYPE_ENTER_WEBFORM);

				}
			}
		}
	}

	
	private List<Document> getDocuments(DocumentType docType, int year, int quarter) {
		List<Document> result = new ArrayList<Document>();
		
		List<Document> docsForType = docCache.get(docType.getId());
		if (null != docsForType) {
			for (Document document : docsForType) {
				if (document.getFiscalYear() == year && document.getFiscalQuarter() == quarter) {
					result.add(document);
				}
			}
		}
		return result;
	}

	
	private ReviewActionItem getReviewActionItem(Boolean useSkipNotification) {

		ReviewActionItem actionItem = null;

		// could be more than one, but handle first one only
		List<Review> reviews = Review.findReviewsByCompanySorted(company);

		for (Review review : reviews) {

			if (review.isPending()&& (!useSkipNotification || useSkipNotification && !review.isSkipNotification())) {
				List<DocumentType> docTypes = getDocumentTypesForReview(review);
				if (docTypes.size() == 0) {
					continue;
				}
				actionItem = new ReviewActionItem(review);
				actionItem.setDocumentTypes(docTypes);

				// figure out due date: 30 days before analysis date
				Calendar dueDate = Calendar.getInstance();
				dueDate.setTime(review.getDateOfAnalysis());
				dueDate.set(Calendar.DATE, dueDate.get(Calendar.DATE) - 30);

				// DateUtils.addDays(review.getDateOfAnalysis(), -30);

				int daysUntilDue = DateHelper.getDaysUntilDue(dueDate);

				actionItem.setDaysUntilDue(daysUntilDue);

				int year = review.getYear();
				if (year == 0) {
					final Calendar cal = Calendar.getInstance();
					cal.setTime(review.getDateOfAnalysis());
					year = cal.get(Calendar.YEAR);
				}

				String title = year + " ";

				if (review.getReviewType().equals(ReviewType.ANNUAL_RATING)) {
					title += "Full analysis document upload ";
					actionItem.setTaskType(ActionItem.TASK_TYPE_FULL_ANALYSIS_DOCUMENTS);

				} else if (review.getReviewType().equals(ReviewType.ANNUAL)) {
					title += "Annual review document upload ";
					actionItem.setTaskType(ActionItem.TASK_TYPE_ANNUAL_REVIEW_DOCUMENTS);

				}

				title += DateHelper.getDueInString(daysUntilDue);

				actionItem.setTitle(title);
				actionItem
				.setDescription("Please upload the required documents so that Aeris can perform the scheduled review.");
				actionItem.setPercentComplete(0);
				actionItem.setYear(year);
				actionItem.setQuarter(0);
				actionItem.setYear(year);

				return actionItem;

			}
		}

		return actionItem;

	}

	/**
	 * Returns the documents that have yet to be uploaded for the given review
	 */
	private List<DocumentType> getDocumentTypesForReview(Review review) {
		List<DocumentType> allDocTypes = DocumentType.findAllDocumentTypesSorted(review.getCompany());
		List<DocumentType> missingDocTypes = new ArrayList<DocumentType>();
		
		for (DocumentType docType : allDocTypes) {
			if (!docType.getDocumentTypeFolder().cdfiResponsible()) {
				continue;
			}
			
			if (review.isFull() && !(docType.isFull() || docType.isAnnual() || docType.isAnnualIfAvailable())) {
				continue;
			}
			if (review.isAnnual() && !docType.isAnnual()) {
				continue;
			}

			List<Document> documents = docCache.get(docType.getId());
			if (null == documents || documents.size() == 0) {
				missingDocTypes.add(docType);
				continue;
			}
			
			if (docType.isOneTime()) {
				continue;
			}
			
			// document must be within the last 4 quarters of the review
			boolean match = false;
			for (Document document : documents) {
				int reviewQuarters = 4*review.getYear() + review.getQuarter();
				int docQuarters = 4*document.getFiscalYear() + document.getFiscalQuarter();
				if ((reviewQuarters - docQuarters) >= 0 && (reviewQuarters - docQuarters) < 4) {
					match = true;
					break;
				}
			}
			if (!match) {
				missingDocTypes.add(docType);
			}
		}
		
		return missingDocTypes;
	}

}
