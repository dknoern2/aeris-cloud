package com.cars.platform.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.CompanyType;


@Service
public class ValueFactLoader {
    private static final Logger logger = LoggerFactory.getLogger(ValueFactLoader.class);
    
    private boolean inProgress = false;
    
    private int processedCompanies;
    private int totalCompanies;

	// made package scope, should only be called from ValueFactExecutor
    @Async
    public void updateAllCompanies() {
    	// TODO this doesn't do much good as the exception is for a separate thread. Need to make sure all callers check for inProgress before calling
    	if (inProgress) {
    		logger.error("Illegal attempt to update all value facts while a separate request is in progress");
    		return;
    	}
    	
    	inProgress = true;
    	StringBuffer sb = new StringBuffer("Company, Facts Created, time\n");
    	long begin = System.currentTimeMillis();
    	Throwable threw = null;
    	
    	try {
	    	List<Company> companies = Company.findCompanysByCompanyTypeAlphabetically(CompanyType.CDFI);
	    	
	    	totalCompanies = companies.size();
	    	processedCompanies = 0;
	    	
	    	sb = new StringBuffer("Company, Facts Created, time\n");
	
	    	for (Company company : companies) {
	    		long start = System.currentTimeMillis();
	        	ValueFactService valueFactService = new ValueFactService(company);
	        	int factsCreated = valueFactService.updateCompany();
	        	String status = company.getName() + ", " + factsCreated + ", " + (System.currentTimeMillis()-start);
	        	logger.info(status);
	        	sb.append(status + "\n");
	        	processedCompanies++;
	    	}
    	}
    	catch (Throwable t) {
    		threw = t;
    	}
    	finally {
        	if (null == threw) {
            	String resultInfo = sb + "\n --- Total Time: " + (System.currentTimeMillis()-begin);
        	    logger.info(resultInfo);
        	}
        	else {
        		logger.error("Unexpected exception: "+ threw.getMessage());
        		threw.printStackTrace();
        	}	
    	    inProgress = false;
	    	totalCompanies = 0;
	    	processedCompanies = 0;

    	}
    	
    	//return new AsyncResult<String>(sb.toString());
    }
    
    public boolean inProgress() {
    	return inProgress;
    }
    
    public int progressPercent() {
    	if (!inProgress) {
    		return 0;
    	}
    	
    	return (int)(((double)processedCompanies / (double)totalCompanies) * 100.0);
    }

}
