package com.cars.platform.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.script.ScriptException;

import net.sourceforge.jeval.EvaluationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.Comment;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Footnote;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.domain.TaxType;
import com.cars.platform.domain.UnitType;
import com.cars.platform.viewmodels.FootnotePOJO;
import com.cars.platform.viewmodels.TentativeChange;

public class ReportService {

	public static final String DIV_BY_ZERO = "#DIV/0!";
	public static final String INPUT_TAB = "Input";
	public static final String POSITION_TAB = "Position";
	public static final String ACTIVITY_TAB = "Activity";
	public static final String SUMMARY_TAB = "Summary";
	public static final String ADDITIONAL_TAB = "Additional";
	
	private static final int MAX_FRACTIONAL_DIGITS_FOR_INPUT_DATA_LESS_THAN_ONE = 2;

	private static final Logger logger = LoggerFactory.getLogger(ReportService.class);

	private Map<String, TentativeChange> tentativeChanges = new HashMap<String, TentativeChange>();

	
	private final boolean online;
	private boolean allRows = false;
	int fiscalYearStart = 1;
	Company company = null;

	DecimalFormat decimalFormat = new DecimalFormat("#,###");
	DecimalFormat decimalFormatWithFraction = new DecimalFormat("#,###");
	NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
	NumberFormat percentFormat = NumberFormat.getPercentInstance();

	DecimalFormat floatFormat = new DecimalFormat("#.##");

	HashMap<String, MetricValue> metricValueMap = new HashMap<String, MetricValue>();
	HashMap<String, String> metricNames = new HashMap<String, String>();
	TreeSet<String> accountCodes = new TreeSet<String>();
	
	// key=2012Q3
	protected SortedMap<String, Period> periodMap = new TreeMap<String, Period>();
	// key=9/30/2012	
	protected Map<String, Period> periodByEndDate = new HashMap<String, Period>();

	SortedMap<String, Metric> metricMap = new TreeMap<String, Metric>();

	FormulaService formulaService = null;

	List<Footnote> allFootnotes = null;

	String columnHeaders = null;

	List<String> periodsWithFootnoteIndicators = new ArrayList<String>();

	private boolean tentativeChangesSet = false;

	public ReportService(Company company, HashMap<String, TentativeChange> tentativeChanges, boolean allRows, boolean online) {
		this.company = company;
		this.tentativeChanges = tentativeChanges;

		if (tentativeChanges != null && tentativeChanges.size() > 0) {
			tentativeChangesSet = true;
		}
		this.allRows = allRows;
		this.online = online;
		init();
	}

	public ReportService(Company company, boolean allRows, boolean online) {
		this.company = company;
		this.allRows = allRows;
		this.online = online;
		init();
	}


	protected ReportService(boolean online) {
		this.online = online;
	}

	protected void init() {

		if (allRows) {
			insertAllRowsForInput();
		}

		decimalFormatWithFraction.setMaximumFractionDigits(MAX_FRACTIONAL_DIGITS_FOR_INPUT_DATA_LESS_THAN_ONE);
		decimalFormatWithFraction.setNegativePrefix("(");
		decimalFormatWithFraction.setNegativeSuffix(")");
		
		decimalFormat.setNegativePrefix("(");
		decimalFormat.setNegativeSuffix(")");

		periodMap = Period.findPeriodMapByCompany(company);

		long elapsed = System.currentTimeMillis();

		currencyFormat.setMaximumFractionDigits(0);
		percentFormat.setMinimumFractionDigits(1);
		percentFormat.setMaximumFractionDigits(1);
		fiscalYearStart = company.getFiscalYearStart();

		formulaService = new FormulaService(fiscalYearStart);
		for (Metric metric : Metric.findAllMetrics()) {
			metricMap.put(metric.getFullAccountCode(), metric);
		}

		elapsed = showElapsedTime("   got all metrics ", elapsed);

		List<MetricValue> metricValues = MetricValue.findMetricValues(company);

		elapsed = showElapsedTime("   found all metric values ", elapsed);

		for (MetricValue metricValue : metricValues) {
			// create sets and maps for later lookup

			int year = metricValue.getYear();
			int quarter = metricValue.getQuarter();

			String fiscalKey = Integer.toString(year) + "Q" + Integer.toString(quarter);
			String periodEndDate = null;

			Period period = periodMap.get(fiscalKey);

			if (period != null) {
				periodEndDate = periodMap.get(fiscalKey).getEndDate();

				String key = periodEndDate + "|" + metricValue.getMetric().getFullAccountCode();

				accountCodes.add(metricValue.getMetric().getFullAccountCode());

				metricNames.put(metricValue.getMetric().getFullAccountCode(), metricValue.getMetric().getName());

				metricValueMap.put(key, metricValue);
			} else {
				logger.warn("can't find period: " + fiscalKey);
			}
		}

		// apply tentative change (but do not merge!)
		if (tentativeChangesSet) {

			for (TentativeChange tentativeChange : tentativeChanges.values()) {
				String key = tentativeChange.getPeriod() + "|" + tentativeChange.getMetricFullAccountCode();
				MetricValue metricValue = metricValueMap.get(key);
				if (metricValue == null) {
					metricValue = new MetricValue();
					metricValue.setCompany(company);
					Metric metric = Metric.findMetricByFullAccountCode(tentativeChange.getMetricFullAccountCode(), company);

					Period period = Period.findPeriod(company, tentativeChange.getPeriod());
					metricValue.setQuarter(period.getQuarter());
					metricValue.setYear(period.getYear());
					metricValue.setMetric(metric);
					metricValueMap.put(key, metricValue);
				}

				if (!MetricType.TEXT.equals(metricValue.getMetric().getType())) {
					Double changedValue = Double.parseDouble(tentativeChange.getNewValue());
					metricValue.setAmount(changedValue);
				}
			}
		}

		elapsed = showElapsedTime("   made metric value map ", elapsed);

		allFootnotes = Footnote.findFootnotesByCompany(company).getResultList();

		// as we look for the footnotes, also build column headers
		StringBuffer sb = new StringBuffer();
		sb.append("[{\n");
		sb.append("            text     : '',\n");
		sb.append("            locked   : true,\n");
		sb.append("            width    : 251,\n");
		sb.append("            sortable : false,\n");
		sb.append("            dataIndex: 'company'\n");
		sb.append("        }");

		boolean first = true;

		for (String periodKey : periodMap.keySet()) {

			Period period = periodMap.get(periodKey);

			String periodValue = periodMap.get(periodKey).getEndDate();
			String periodFieldLabel = getPeriodFieldLabel(period);
			String periodFieldLabelExcel = periodValue;

			periodsWithFootnoteIndicators.add(periodFieldLabelExcel);
			
			periodByEndDate.put(periodValue, period);

			sb.append("        ,{\n");
			sb.append("            text     : '" + periodFieldLabel + "',\n");
			sb.append("            sortable : false,\n");
			sb.append("            align: 'right',\n");
			sb.append("            width    : 95,\n");
			sb.append("            renderer : highlighter,\n");
			sb.append("            dataIndex: '" + getPeriodFieldName(period) + "'\n");
			sb.append("        }");

			if (!first) {
				sb.append("        ,{\n");
				sb.append("            text     : 'CH',\n");
				sb.append("            sortable : false,\n");
				sb.append("            align: 'right',\n");
				sb.append("            width    : 95,\n");
				sb.append("            renderer : highlighter,\n");
				sb.append("            tdCls: 'shaded-col',\n");
				sb.append("            dataIndex: '" + getPeriodFieldName(period) + "amtchange'\n");
				sb.append("        }");

				sb.append("        ,{\n");
				sb.append("            text     : '%CH',\n");
				sb.append("            sortable : false,\n");
				sb.append("            align: 'right',\n");
				sb.append("            width    : 60,\n");
				sb.append("            renderer : highlighter,\n");
				sb.append("            tdCls: 'shaded-col',\n");
				sb.append("            dataIndex: '" + getPeriodFieldName(period) + "pctchange'\n");
				sb.append("        }");
			}
			first = false;

		}

		sb.append("];\n");

		columnHeaders = sb.toString();

	}

	// Pre-polulate accountCodes and metricName so that even blank lines will
	// show
	private void insertAllRowsForInput() {
		List<Metric> allMetrics = Metric.findAllCompanyMetrics(company.getId());
		for (Metric metric : allMetrics) {
			accountCodes.add(metric.getFullAccountCode());
			metricNames.put(metric.getFullAccountCode(), metric.getName());
		}
	}

	public ArrayList<ArrayList<String>> getReport(ReportPage reportPage) {
		ArrayList<ArrayList<String>> report = null;

		if (reportPage.equals(ReportPage.POSITION)) {
			report = getRollupData(reportPage);
		} else if (reportPage.equals(ReportPage.ACTIVITY)) {
			report = getRollupData(reportPage);
// These are handled by ReportService2
//		} else if (reportPage.equals(ReportPage.SUMMARY)) {
//			report = getOutputData(reportPage);
//		} else if (reportPage.equals(ReportPage.ADDITIONAL)) {
//			report = getOutputData(reportPage);
		} else {
			report = getInputData();
		}
		return report;
	}

	
	private ArrayList<String> getFormulaItem(String formulaName, String formula, UnitType unitType, int decimalPlaces, boolean strong) {
		ArrayList<String> list = new ArrayList<String>();

		if (strong) {
			formulaName = "<b>" + formulaName + "</b>";
		}
		list.add(formulaName);
		Double previousResult = null;
		Double result = null;

		boolean firstPeriod = true;
		for (Period period : periodMap.values()) {
			String endDate = period.getEndDate();

			String text = "";

			try {
				result = null;

				// handle special case where formula contains "P" for prior FYE,
				// need to skip

				if (firstPeriod && formula.contains("P")) {
					// skip this period!
				} else {
					result = formulaService.crunchFormula(formula, endDate, metricValueMap, periodMap.values(), company);
					//			if (formulaName.equals("Total Assets")) {
					//				System.out.println("endDate [" + endDate + "] value [" + result + "] period [" + period.getYear() + "Q" + period.getQuarter() + "]");
					//			}
				}
				if (result != null) {
					text = result.toString();

					// Excel documents should always contain numbers, "-" char should be displayed only in web-interface.
					if (online && (Math.abs(result) < .00001)) {
						text = "-";
					} else if (UnitType.PERCENTAGE.equals(unitType)) {
						percentFormat.setMaximumFractionDigits(decimalPlaces);
						percentFormat.setMinimumFractionDigits(decimalPlaces);
						text = percentFormat.format(result);
					} else if (strong) {
						currencyFormat.setMaximumFractionDigits(decimalPlaces);
						currencyFormat.setMinimumFractionDigits(decimalPlaces);
						text = "<b>" + currencyFormat.format(result) + "</b>";
					} else {
						decimalFormat.setMaximumFractionDigits(decimalPlaces);
						decimalFormat.setMinimumFractionDigits(decimalPlaces);
						text = decimalFormat.format(result);
					}
				}
			} catch (EvaluationException e) {
				logger.error("BADQEN: " + formula);
				text = "BADNUM";
			} catch (NumberFormatException e) {
				text = "BADNUM";
			} catch (ScriptException e) {
				text = "BADEQN";
				logger.error("", e);
			}

			list.add(text);

			if (!firstPeriod) {

				if (!UnitType.PERCENTAGE.equals(unitType) && result != null && previousResult != null
						&& previousResult > .1) {
					double change = result - previousResult;
					list.add(decimalFormat.format(change));

					double pctChange = (change) / Math.abs(previousResult);
					list.add(percentFormat.format(pctChange));
				} else {
					list.add("");
					list.add("");
				}

			}

			firstPeriod = false;
			if (period.getQuarter() > 3) {
				previousResult = result;
			}

		}

		return list;
	}

	private ArrayList<ArrayList<String>> getRollupData(ReportPage reportPage) {

		ArrayList<ArrayList<String>> report = new ArrayList<ArrayList<String>>();

		// header row
		boolean strong = true;
		if (reportPage.equals(ReportPage.POSITION)) {

			String netAssets = "Net Assets";

			if (TaxType.FOR_PROFIT.equals(company.getTaxType())) {
				netAssets = "Stockholders' Equity";
			}
			report.add(getBlankArray(periodMap.size() + 1));
			report.addAll(getRollupItem("Current Assets", "1010"));
			report.addAll(getRollupItem("Noncurrent Assets", "1020"));
			report.add(getFormulaItem("TOTAL ASSETS", "1010#+1020#", UnitType.DOLLAR, 0, strong));
			report.add(getBlankArray(periodMap.size() + 1));
			report.addAll(getRollupItem("Current Liabilities", "1030"));
			report.addAll(getRollupItem("Noncurrent Liabilities", "1040"));
			report.add(getFormulaItem("TOTAL LIABILITIES", "1030#+1040#", UnitType.DOLLAR, 0, strong));
			report.add(getBlankArray(periodMap.size() + 1));

			if (TaxType.FOR_PROFIT.equals(company.getTaxType())) {
				report.addAll(getRollupItem(netAssets, "1080"));
			} else {
				report.addAll(getRollupItem("Unrestricted " + netAssets, "1050"));
				report.addAll(getRollupItem("Temporarily Restricted " + netAssets, "1060"));

				if (TaxType.GOVERNMENT.equals(company.getTaxType())) {
					report.addAll(getRollupItem("Restricted " + netAssets, "1090"));
				} else {
					report.addAll(getRollupItem("Permanently Restricted " + netAssets, "1070"));
				}

				report.add(getFormulaItem("TOTAL " + netAssets.toUpperCase(), "1050#+1060#+1070#+1080#+1090#",
						UnitType.DOLLAR, 0, strong));
				report.add(getBlankArray(periodMap.size() + 1));
			}

			report.add(getFormulaItem("TOTAL LIABILITIES AND " + netAssets.toUpperCase(),
					"1030#+1040#+1050#+1060#+1070#+1080#+1090#", UnitType.DOLLAR, 0, strong));
			report.add(getBlankArray(periodMap.size() + 1));
			report.add(getFormulaItem("check", "(1010#+1020#)-(1030#+1040#+1050#+1060#+1070#+1080#+1090#)",
					UnitType.NUMBER, 0, false));
			report.add(getBlankArray(periodMap.size() + 1));

		} else {
			String feeRevenue = "Fee Revenue";
			String surplusFromOperations = "SURPLUS (DEFICIT) FROM OPERATIONS";
			String otherChangesInUnrestrictedNetAssets = "Other Changes in Unrestricted Net Assets";
			String changeInUnrestrictedNetAssets = "Change in Unrestricted Net Assets";

			if (TaxType.FOR_PROFIT.equals(company.getTaxType())) {
				feeRevenue = "Other Revenue";
				surplusFromOperations = "Net Income before Taxes";
				otherChangesInUnrestrictedNetAssets = "Other Changes in Retained Earnings";
				changeInUnrestrictedNetAssets = "Net Income (Loss)";
			}

			report.add(getBlankArray(periodMap.size() + 1));

			report.addAll(getRollupItem("Financing Revenue", "2020"));
			report.addAll(getRollupItem("Financing Expenses", "2050"));

			report.add(getFormulaItem("Net Financing Income", "2020#-2050#", UnitType.DOLLAR, 0, strong));
			report.add(getBlankArray(periodMap.size() + 1));

			report.addAll(getRollupItem("Real Estate Revenue", "2035"));
			report.addAll(getRollupItem("Real Estate Expenses", "2055"));

			report.add(getFormulaItem("Net Real Estate Income", "2035#-2055#", UnitType.DOLLAR, 0, strong));
			report.add(getBlankArray(periodMap.size() + 1));

			report.addAll(getRollupItem(feeRevenue, "2030"));
			report.addAll(getRollupItem("Contributed Revenue", "2010"));
			report.add(getFormulaItem("NET REVENUE", "2010#+2020#+2030#+2035#-2050#-2055#", UnitType.DOLLAR, 0, strong));
			report.add(getBlankArray(periodMap.size() + 1));
			report.addAll(getRollupItem("Operating Expenses", "2040"));
			report.add(getFormulaItem(surplusFromOperations, "2010#+2020#+2030#+2035#-2040#-2050#-2055#",
					UnitType.DOLLAR, 0, strong));
			report.add(getBlankArray(periodMap.size() + 1));
			report.addAll(getRollupItem(otherChangesInUnrestrictedNetAssets, "2060"));
			report.add(getFormulaItem(changeInUnrestrictedNetAssets, "2010#+2020#+2030#+2035#-2040#-2050#-2055#+2060#",
					UnitType.DOLLAR, 0, strong));
			report.add(getBlankArray(periodMap.size() + 1));

			if (TaxType.FOR_PROFIT.equals(company.getTaxType())) {
				report.add(getFormulaItem("check", "1050P+1060P+1070P-(1050#+1060#+1070#)", UnitType.NUMBER, 0, false));
			} else {

				if (TaxType.GOVERNMENT.equals(company.getTaxType())) {
					report.addAll(getRollupItem("Change in Restricted Net Assets", "2082"));

				} else {
					report.addAll(getRollupItem("Change in Temporarily Restricted Net Assets", "2070"));
					report.addAll(getRollupItem("Change in Permanently Restricted Net Assets", "2080"));
				}
				report.addAll(getRollupItem("Other Changes in Net Assets", "2090"));
				report.add(getFormulaItem("TOTAL CHANGE IN NET ASSETS",
						"2010#+2020#+2030#+2035#-2040#-2050#-2055#+2060#+2070#+2080#+2090#+2082#", UnitType.DOLLAR, 0,
						strong));

				report.add(getBlankArray(periodMap.size() + 1));

				// check = prior year total net assets - current year total net
				// assets + TOTAL CHANGE IN NET ASSETS
				report.add(getFormulaItem(
						"check",
						"1050P+1060P+1070P+1090P-(1050#+1060#+1070#+1090#)+2010#+2020#+2030#+2035#-2040#-2050#-2055#+2060#+2070#+2080#+2082#+2090#",
						UnitType.NUMBER, 0, false));
			}

			report.add(getBlankArray(periodMap.size() + 1));
		}

		return report;
	}

	private ArrayList<String> getBlankArray(String name, int size) {
		ArrayList<String> list = new ArrayList<String>();

		list.add("<b>" + name + "</b>");
		for (int i = 0; i < size - 2; i++) {
			list.add("");
		}
		return list;
	}

	private ArrayList<String> getBlankArray(int size) {
		ArrayList<String> list = new ArrayList<String>();

		for (int i = 0; i < size - 1; i++) {
			list.add("");
		}
		return list;
	}


	/**
	 * Get roll-up item, series of rows that show metrics in a certain category
	 * along with total
	 * 
	 * @param name
	 * @param parentAccountCode
	 * @return
	 */
	private ArrayList<ArrayList<String>> getRollupItem(String name, String parentAccountCode) {

		ArrayList<Metric> metrics = new ArrayList<Metric>();
		ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();

		// find all metrics in the correct category
		for (Metric metric : metricMap.values()) {
			if (metric.getFullAccountCode().startsWith(parentAccountCode)) {
				metrics.add(metric);
			}
		}

		// sort metric by presentation
		Collections.sort(metrics, Metric.sortByRank);

		// if no matching metrics, nothing more to do, otherwise, build rollup
		// item

		if (metrics != null && metrics.size() > 0) {

			ArrayList<String> totalRow = new ArrayList<String>();
			// list.add(totalRow);

			totalRow.add("<span style=\"display:block; border-top: 1px solid black\"><b>Total " + name + "</b></span>");

			boolean foundAny = false;
			boolean first = true;
			Double prevTotal = null;
			// compute sums
			for (Period period : periodMap.values()) {
				String endDate = period.getEndDate();

				Double total = null;

				for (Metric metric : metrics) {

					String key = endDate + "|" + metric.getFullAccountCode();

					MetricValue metricValue = metricValueMap.get(key);

					if (metricValue != null && metricValue.getAmountValue() != null) {
						if (total == null)
							total = new Double(0.0);
						total += metricValue.getAmountValue();
					}
				}

				String text = "";
				if (total != null) {
					foundAny = true;
					text = "<span style=\"display:block; border-top: 1px solid black\"><b>"
							+ currencyFormat.format(total) + "</b></span>";
				}
				totalRow.add(text);

				if (!first) {
					if (total != null && prevTotal != null) {
						double change = total - prevTotal;
						totalRow.add("<span style=\"display:block; border-top: 1px solid black\"><b>"
								+ decimalFormat.format(change) + "</b></span>");
						double pctChange = change / Math.abs(prevTotal);
						String pctChangeString = (Double.isNaN(pctChange) || Double.isInfinite(pctChange)) ? "NA" : percentFormat.format(pctChange);
						totalRow.add("<span style=\"display:block; border-top: 1px solid black\"><b>"
								+ pctChangeString + "</b></span>");
					} else {
						totalRow.add("");
						totalRow.add("");
					}
				}

				first = false;

				if (period.getQuarter() > 3) {
					prevTotal = total;
				}
			}

			if (foundAny || allRows) {
				list.add(getBlankArray(name, periodMap.size() + 1));

				for (Metric metric : metrics) {

					ArrayList<String> itemRow = new ArrayList<String>();

					// build row but only show it if there was data
					boolean foundData = false;

					if (metric.getParent() != null) {
						itemRow.add("<span style=\"padding:15px\"></span>" + metric.getName());
						// itemRow.add(metric.getName());
					} else {
						itemRow.add(metric.getName());
					}

					first = true;
					for (Period period : periodMap.values()) {
						String endDate = period.getEndDate();

						String text = "";

						String key = endDate + "|" + metric.getFullAccountCode();

						MetricValue metricValue = metricValueMap.get(key);
						if (metricValue != null && metricValue.getAmountValue() != null) {

							foundData = true;
							if (online && Math.abs(metricValue.getAmountValue()) < AbstractReportService.SMALLEST_DISPLAYED_VALUE) {
								text = "-";
							} else {
								text = decimalFormat.format(metricValue.getAmountValue());
							}
						}
						itemRow.add(text);

						if (!first) {
							itemRow.add("");
							itemRow.add("");
						}
						first = false;
					}

					if (foundData || allRows) {
						list.add(itemRow);
					}
				}
				list.add(totalRow);
				list.add(getBlankArray(periodMap.size() + 1));
			}

		}

		return list;

	}

	public String getFields() {
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		sb.append("           {name: 'company'}\n");
		boolean first = true;
		for (Period period : periodMap.values()) {
			sb.append("           ,{name: '" + getPeriodFieldName(period) + "'}\n");

			if (!first) {
				sb.append("           ,{name: '" + getPeriodFieldName(period) + "amtchange'}\n");
				sb.append("           ,{name: '" + getPeriodFieldName(period) + "pctchange'}\n");
			}
			first = false;

		}
		sb.append("        ];\n");
		return sb.toString();
	}

	public String getColumns() {
		return columnHeaders;
	}

	private String getPeriodFieldName(Period period) {
		return period.getYear() + "Q" + period.getQuarter();
	}

	private String getPeriodFieldLabel(Period period) {

		String fieldLabel = "DRAFT";
		if (PeriodType.AUDIT.equals(period.getPeriodType())) {
			fieldLabel = "AUDIT";
		} else if (PeriodType.INTERIM.equals(period.getPeriodType())) {
			fieldLabel = "INTERIM";
		} else if (PeriodType.INTERNAL.equals(period.getPeriodType())) {
			if (period.getQuarter() == Period.LAST_QUARTER)
				fieldLabel = "UNAUDITED";
			else
				fieldLabel = "INTERNAL";
		}

		fieldLabel += "<br/>" + period.getEndDate();

		return fieldLabel;
	}

	private ArrayList<ArrayList<String>> getInputData() {

		ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();

		List<MetricValue> metricValuesWithComments = MetricValue.findMetricValueIdsWithComments(company);

		for (String accountCode : accountCodes) {

			ArrayList<String> row = new ArrayList<String>();
			list.add(row);

			row.add(accountCode + " - " + metricNames.get(accountCode));

			for (Period period : periodMap.values()) {

				String key = period.getEndDate() + "|" + accountCode;
				String text = "";
				MetricValue metricValue = metricValueMap.get(key);

				if (metricValue != null) {

					if (metricValue.getAmountValue() != null) {

						if (online && (Math.abs(metricValue.getAmountValue()) < AbstractReportService.SMALLEST_DISPLAYED_VALUE)) {
							text = "-";
						} else if (Math.abs(metricValue.getAmountValue()) < 1) {
							text = decimalFormatWithFraction.format(metricValue.getAmountValue());
						} else {
							text = decimalFormat.format(metricValue.getAmountValue());
						}
					} else if (metricValue.getTextValue() != null && metricValue.getTextValue().length() > 0) {
						text = metricValue.getTextValue();
					}

					// decorate for comments
					try {

						String tentativeChangeKey = TentativeChange.getTentativeChangeKey(metricValue.getId(), period.getEndDate(), accountCode);

						if (tentativeChangesSet && tentativeChanges.get(tentativeChangeKey) != null) {
							text = "~" + text;
						} else if (metricValue != null && metricValuesWithComments.contains(metricValue)) {

							boolean pending = false;

							for (Comment comment : metricValue.getComments()) {
								if (comment.isPending()) {
									pending = true;
								}
							}

							if (pending)
								text = "{" + text;
							else
								text = "[" + text;
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				row.add(text);
			}
		}

		return list;
	}

	public List<String> getPeriodsWithFootnoteIndicators() {
		return Collections.unmodifiableList(periodsWithFootnoteIndicators);
	}

	public Map<String, MetricValue> getMetricValueMap() {
		return Collections.unmodifiableMap(metricValueMap);
	}

	public Map<String, Period> getPeriodByEndDateMap() {
		return Collections.unmodifiableMap(periodByEndDate);
	}

	public List<FootnotePOJO> getFootnotes() {
		List<FootnotePOJO> list = new ArrayList<FootnotePOJO>();

		for (Footnote footnote : allFootnotes) {

			FootnotePOJO footnotePOJO = new FootnotePOJO();
			footnotePOJO.setComment(footnote.getComment());
			footnotePOJO.setLabel(footnote.getLabel());

			if (ReportPage.POSITION.equals(footnote.getReportPage()))
				footnotePOJO.setReportPage("position");
			else if (ReportPage.ACTIVITY.equals(footnote.getReportPage()))
				footnotePOJO.setReportPage("activity");
			else if (ReportPage.SUMMARY.equals(footnote.getReportPage()))
				footnotePOJO.setReportPage("summary");
			else if (ReportPage.ADDITIONAL.equals(footnote.getReportPage()))
				footnotePOJO.setReportPage("additional");

			list.add(footnotePOJO);
		}

		return list;
	}

	private static long showElapsedTime(String activity, long millis) {
		long elapsed = System.currentTimeMillis() - millis;
		logger.info(activity + " took " + elapsed + " ms");
		return System.currentTimeMillis();
	}

}