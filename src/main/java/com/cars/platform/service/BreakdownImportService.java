package com.cars.platform.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricBreakdown;
import com.cars.platform.domain.MetricBreakdownItem;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.ExcelHelper;
import com.smartxls.WorkBook;


/**
 * Creates MetricValues with appropriate relationships to MetricBreakdownItems provided a properly formatted xls file
 * @author kurtl
 *
 */
public class BreakdownImportService {
	
    private static final Logger logger = LoggerFactory.getLogger(BreakdownImportService.class);

    private WorkBook wb = new WorkBook();
    
    private List<MetricBreakdown> allBreakdowns;
    
    private List<Metric> allMetrics;
    
    private Company company;
    
    private Period period;
    private boolean newPeriod = false;
    
    private List<MetricValue> newMetricValues;
    
    private List<String> validationErrors;
    
    public BreakdownImportService() {
    	this(MetricBreakdown.findAllMetricBreakdowns(), Metric.findBreakdownMetrics());
    }
    
    public BreakdownImportService(List<MetricBreakdown> allBreakdowns, List<Metric> allMetrics) {
    	this.allBreakdowns = allBreakdowns;
    	this.allMetrics = allMetrics;
    	this.newMetricValues = new ArrayList<MetricValue>();
    	this.validationErrors = new ArrayList<String>();
    }

    
    
    public List<MetricValue> createMetrics(String filename, InputStream is, Company company, int year, int quarter)
    throws BreakdownImportException
    {
    	this.company = company;
    	determinePeriod(year, quarter);

	    boolean valid = true;
    	
    	try {
        	initWorkbook(filename, is);
		    int numSheets = wb.getNumSheets();
		    for (int i=0; i<numSheets; i++) {
		    	if (!validateSheet(i)) {
		    		valid = false;
		    	}
		    }
		    
		    if (valid) {
			    for (int i=0; i<numSheets; i++) {
			    	processSheet(i);
			    }		    	
		    }
    	}
    	catch (Exception e) { // ugh... all the Workbook methods throw Exception
    		e.printStackTrace();
    		throw new RuntimeException(e);
    		// TODO something better here for a better error message on the upload page
    	}
    	
    	if (!valid) {
    		throw new BreakdownImportException();
    	}
    	return newMetricValues;
    }
    
    
    public void saveMetrics() {
    	if (newPeriod) {
    		period.persist();
    	}
    	else {
    		MetricValue.deleteMetricValues(period.getQuarter(), period.getYear(), company, true, false);
    	}

		for (MetricValue metricValue : newMetricValues) {
			metricValue.persist();
		}
    }


	private void determinePeriod(int year, int quarter) {
    	period = Period.findPeriod(company, year, quarter);
		if (period == null) {
			newPeriod = true;
			
		    period = new Period();
		    period.setCompany(company);
		    String endDate = DateHelper.getQuarterEndDate(year, quarter, company.getFiscalYearStart());
		    period.setEndDate(endDate);
		    period.setQuarter(quarter);
		    period.setYear(year);
		}
	}


	private void initWorkbook(String filename, InputStream is) throws Exception {
		ExcelHelper.readWorkBook(wb, is);
	    
	    int numSheets = wb.getNumSheets();
	    for (int i=0; i<numSheets; i++) {
	    	wb.setSheet(i);
	    	wb.setSheetProtected(i, false, null);
	    	System.out.println(wb.getSheetName(i));
	    }
	    

	}
	
	
	private boolean validateSheet(int sheetNum) throws Exception {
		boolean valid = true;
		wb.setSheet(sheetNum);
		if (wb.getHiddenState() != WorkBook.SheetStateShown) {
			return true;
		}
		
		String sheetName = wb.getSheetName(sheetNum);
		System.out.println("Sheet: " + sheetName);
		MetricBreakdown mb = getBreakdownforSheet(sheetName);
		if (null == mb) {
			captureError(0, 0, "The name of this sheet is unrecognized");
			return false;
		}
		
		int numMetricColumns = wb.getLastColForRow(0);
		for (int j=1; j<=numMetricColumns; j++) {
			String text = wb.getText(0, j);
			if (StringUtils.isBlank(text)) {
				continue;
			}
			Metric m = getMetric(mb, text);
			if (null == m) {
				captureError(0, j, "The name of this column header is unrecognized");
				valid = false;
			}
		}

		
		int numRows = wb.getLastRow();
		for (int i=1; i<=numRows; i++) {
			int numColumns = wb.getLastColForRow(i);
			String itemName = wb.getText(i, 0);
			if (StringUtils.isBlank(itemName)) {
				continue;
			}
			MetricBreakdownItem mbi = getBreakdownItem(mb, itemName);
			// TODO handle free form and year delta types
			if (null == mbi) {
				captureError(1, 0, "This is not in the list of valid breakdown items");
				valid = false;				
			}
			// TODO validate the value formats? 
			System.out.print(i + ": " + itemName);
			for (int j=1; j<=numColumns; j++) {
				String cellValue = wb.getText(i, j);
				System.out.print(", [" + cellValue + "]");
			}
			System.out.println("");
		}
		
		return valid;
	}

	private void processSheet(int sheetNum) throws Exception {
		wb.setSheet(sheetNum);
		if (wb.getHiddenState() != WorkBook.SheetStateShown) {
			return;
		}
		String sheetName = wb.getSheetName(sheetNum);
		MetricBreakdown mb = getBreakdownforSheet(sheetName);
		
		int numRows = wb.getLastRow();
		for (int i=1; i<=numRows; i++) {
			int numColumns = wb.getLastColForRow(i);
			String itemName = wb.getText(i, 0);
			if (StringUtils.isBlank(itemName)) {
				continue;
			}
			MetricBreakdownItem mbi = getBreakdownItem(mb, itemName);
			for (int j=1; j<=numColumns; j++) {
				MetricValue mv = createMetricValue(i, j, mbi);
				if (null != mv) {
				    newMetricValues.add(mv);
				}
			}
		}
	}
	
	private MetricBreakdown getBreakdownforSheet(String sheetName) {
		for (MetricBreakdown mb : allBreakdowns) {
			if (mb.getName().equals(sheetName)) {
				return mb;
			}
		}
		return null;
	}
	

	
	private MetricBreakdownItem getBreakdownItem(MetricBreakdown bd, String itemName) {
		for (MetricBreakdownItem mbi : bd.getItems()) {
			if (mbi.getName().equals(itemName)) {
				return mbi;
			}
		}
		return null;
	}


	private Metric getMetric(MetricBreakdown mb, String metricName) {
		for (Metric m : allMetrics) {
			if (m.getBreakdown().equals(mb) && m.getName().equals(metricName)) {
				return m;
			}
		}
		return null;
	}
	
	
	private MetricValue createMetricValue(int i, int j, MetricBreakdownItem mbi) throws Exception {
		MetricValue mv = new MetricValue();
		String metricName = wb.getText(0, j);
		if (StringUtils.isBlank(metricName)) {
			return null;
		}
		Metric m = getMetric(mbi.getMetricBreakdown(), metricName);
		String textValue = wb.getText(i, j);
		if (StringUtils.isBlank(textValue)) {
			return null;
		}
		if (m.getType().equals(MetricType.NUMERIC)) {
			mv.setAmount(wb.getNumber(i, j));
		}
		else if (m.getType().equals(MetricType.TEXT)) {
			mv.setTextValue(wb.getText(i, j));
		}
		// MetricType.BOTH is deprecated
		else {
			logger.error("Ignoring Metric which has deprecated type of Both [" + m.getName() + "]");
			return null;
		}
		
		mv.setBreakdownItem(mbi);
		mv.setMetric(m);
		mv.setYear(period.getYear());
		mv.setQuarter(period.getQuarter());
		mv.setCompany(company);
		return mv;
	}
	
	
	private void captureError(int row, int column, String message) throws Exception {
		wb.addComment(row, column, message, "Aeris");
		validationErrors.add("row[" + row + "] col[" + column + "]: " + message);
	}

	
	public class BreakdownImportException extends Exception {

		private static final long serialVersionUID = 1L;

		BreakdownImportException() {
			super("Validation errors found in import file");
		}
		
		public WorkBook getWorkBook() {
			return wb;
		}
		
		public List<String> getErrors() {
			return validationErrors;
		}
		
	}


}
