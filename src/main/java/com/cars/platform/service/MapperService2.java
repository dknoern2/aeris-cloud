package com.cars.platform.service;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.cars.platform.cache.ReportDataCache;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;
import com.cars.platform.domain.Footnote;
import com.cars.platform.domain.Metric;
import com.cars.platform.domain.MetricMap;
import com.cars.platform.domain.MetricType;
import com.cars.platform.domain.MetricValue;
import com.cars.platform.domain.Period;
import com.cars.platform.domain.PeriodType;
import com.cars.platform.domain.ReportPage;
import com.cars.platform.util.DateHelper;
import com.cars.platform.util.ExcelHelper;
import com.cars.platform.util.NumberHelper;
import com.cars.platform.util.SmartXLSHelper;
import com.cars.platform.util.SpringApplicationContext;
import com.cars.platform.viewmodels.MappedCell;
import com.smartxls.RangeArea;
import com.smartxls.WorkBook;

/**
 * Given an excel file and a list of metric maps, find metric values
 * 
 * @author dknoern
 * 
 */
public class MapperService2 {

    private static final Logger logger = LoggerFactory.getLogger(MapperService2.class);

    SmartXLSHelper smartXLSHelper = new SmartXLSHelper();

    public int map(String filename, InputStream is, Company company, int year, int quarter) throws IOException {

    	// create Period record if not there
    	createPeriodIfNotExist(company, year, quarter);

    	int cellCount = 0;

    	WorkBook wb = new WorkBook();
    	boolean refresh = false;
    	try {
    		boolean readExcel = ExcelHelper.readWorkBook(wb, is);
		    if (!readExcel) {
    			logger.error("Invalid excel file: " + filename);
    			return 0;
    		}

    		List<MetricMap> metricMapList = MetricMap.findMetricMaps(company, year, quarter);

    		if (metricMapList != null && metricMapList.size() > 0) {

    			logger.info("found {} existing metric maps for {}Q{} ", new Object[] { metricMapList.size(), year,
    					quarter });

    			// loop through maps, updating as required
    			for (MetricMap metricMap : metricMapList) {
    				try {
    					// search for label in expected cell or below
    					int findAfterRowIndex = -1;
					    int findBeforeRowIndex = -1;
					    
					    int startSearchRow = metricMap.getLabelX();
					    if (metricMap.getFoundBefore() != null) {
					    	findBeforeRowIndex = findAfterBeforeTerm(wb, metricMap.getFoundBefore(), metricMap.getLabelZ());
					    	startSearchRow = 0;
					    }
					    if (metricMap.getFoundAfter() != null) {
					    	findAfterRowIndex = findAfterBeforeTerm(wb, metricMap.getFoundAfter(), metricMap.getLabelZ());
					    	if (findAfterRowIndex != -1) {
					    		startSearchRow = findAfterRowIndex;
					    	}
					    }
    					int labelRowIndex = findLabelRow(wb, metricMap.getLabelText(), metricMap.getLabelZ(), startSearchRow, metricMap.getLabelY());

    					if (labelRowIndex > -1) {

    						int valueRowIndex = metricMap.getValueX() + labelRowIndex - metricMap.getLabelX();

    						int valueColIndex = metricMap.getValueY();
        					if (metricMap.isValuesRight()) {
        						valueColIndex = getLastCol(wb, metricMap.getValueZ());
        					}
    						String valueText = smartXLSHelper.getValue(wb, valueRowIndex, valueColIndex);

    						if (metricMap.getFoundAfter() != null) {
    							if (findAfterRowIndex == -1 || findAfterRowIndex > labelRowIndex) {
    								continue;
    							}
    						}
    						if (metricMap.getFoundBefore() != null) {
    							if (findBeforeRowIndex == -1 || findBeforeRowIndex < labelRowIndex) {
    								continue;
    							}
    						}
    						
    						metricMap.setValueText(valueText);
    						Boolean negate = isMetricMapNegate(metricMap);
    						metricMap.setNegate(negate);

    						metricMap.setLabelX(labelRowIndex);
    						metricMap.setValueX(valueRowIndex);
    						metricMap.setValueY(valueColIndex);
    						metricMap.merge();
    						cellCount++;

    						// set total value in metric_value table
    						refresh |= refreshMetricValue(company, metricMap.getMetric(), year, quarter);
    					}
    				} catch (Exception e) {
    					logger.error("", e);
    				}
    			}

    		} else {
    			logger.info("no existing metric maps found for  {}Q{}, trying previous quarter", year, quarter);

    			List<Period> olderPeriods = Period.findOlderPeriodsByCompany(company, year, quarter);
    			for(Period periodItem : olderPeriods)
    			{
    				int previousQuarterNumber = periodItem.getQuarter();
    				int previousQuarterYear = periodItem.getYear();
    				metricMapList = MetricMap.findMetricMaps(company, previousQuarterYear, previousQuarterNumber);
    				if (metricMapList != null && metricMapList.size() > 0){

    					logger.info("found {} existing metric maps for {}Q{}, copying to this quarter", new Object[] {
    							metricMapList.size(), previousQuarterYear, previousQuarterNumber });

    					ArrayList<MetricMap> newMetricMapList = new ArrayList<MetricMap>();
    					for (MetricMap metricMap : metricMapList) {

    						// search for label in expected cell or below
    						int findAfterRowIndex = -1;
    					    int findBeforeRowIndex = -1;
    					    
    					    int startSearchRow = metricMap.getLabelX();
    					    if (metricMap.getFoundBefore() != null) {
    					    	findBeforeRowIndex = findAfterBeforeTerm(wb, metricMap.getFoundBefore(), metricMap.getLabelZ());
    					    	startSearchRow = 0;
    					    }
    					    if (metricMap.getFoundAfter() != null) {
    					    	findAfterRowIndex = findAfterBeforeTerm(wb, metricMap.getFoundAfter(), metricMap.getLabelZ());
    					    	if (findAfterRowIndex != -1) {
    					    		startSearchRow = findAfterRowIndex;
    					    	}
    					    }
    						int labelRowIndex = findLabelRow(wb, metricMap.getLabelText(), metricMap.getLabelZ(), startSearchRow, metricMap.getLabelY());
    						if (labelRowIndex > -1) {

    							int valueRowIndex = metricMap.getValueX() + labelRowIndex - metricMap.getLabelX();
    							int valueColIndex = metricMap.getValueY();
            					if (metricMap.isValuesRight()) {
            						valueColIndex = getLastCol(wb, metricMap.getValueZ());
            					}
    							String valueText = smartXLSHelper.getValue(wb, valueRowIndex, valueColIndex);

    							if (metricMap.getFoundAfter() != null) {
    								if (findAfterRowIndex == -1 || findAfterRowIndex > labelRowIndex) {
    									continue;
    								}
    							}
    							if (metricMap.getFoundBefore() != null) {
    								if (findBeforeRowIndex == -1 || findBeforeRowIndex < labelRowIndex) {
    									continue;
    								}
    							}
    							
    							// only save if label matches old label and we found
    							// non-null value
    							MetricMap newMap = new MetricMap();
    							newMap.setCompany(metricMap.getCompany());
    							newMap.setLabelText(metricMap.getLabelText());
    							newMap.setLabelX(labelRowIndex);
    							newMap.setLabelY(metricMap.getLabelY());
    							newMap.setLabelZ(metricMap.getLabelZ());

    							newMap.setValueX(valueRowIndex);
    							newMap.setValueY(valueColIndex);
    							newMap.setValueZ(metricMap.getValueZ());

    							newMap.setFoundAfter(metricMap.getFoundAfter());
    							newMap.setFoundBefore(metricMap.getFoundBefore());
    							newMap.setValuesRight(metricMap.isValuesRight());
    							
    							newMap.setMetric(metricMap.getMetric());
    							newMap.setQuarter(quarter);
    							newMap.setYear(year);
    							newMap.setValueText(valueText);
    							newMap.setNegate(metricMap.isNegate());
    							newMap.persist();
    							newMetricMapList.add(newMap);


    							cellCount++;
    							// set total value in metric_value table

    							refresh |= refreshMetricValue(company, newMap.getMetric(), year, quarter);
    						}
    					}
    					metricMapList = newMetricMapList;
    					break;
    				}
    			}
    		}
    	} catch (Exception e) {

    	}
    	
    	if (refresh) {
    		ReportDataCache.removeAndUpdateValueFacts(company);
    	}

    	return cellCount;
    }

    public List<MetricValue> mapHistoric(String filename, InputStream is, Company company, List<Metric> metrics) {

	List<MetricValue> metricValues = new ArrayList<MetricValue>();

	WorkBook wb = new WorkBook();

	try {
		ExcelHelper.readWorkBook(wb, is);

	    wb.setSheet(0);
	    RangeArea firstAuditCell = wb.find(0, 0, "AUDIT", false);
	    if (firstAuditCell == null) {
		return null;
	    }

	    LinkedHashMap<Integer, String> dateColumns = new LinkedHashMap<Integer, String>();
	    // period map, key is 2012Q4, etc

	    // temporary FY start month based on AUDIT columns, could
	    // CHANGE!
	    int fiscalYearStart = company.getFiscalYearStart();
	    int year = 0;
	    PeriodType previousPeriodType = null;

	    int lastCol = wb.getLastColForRow(firstAuditCell.getRow1());

	    for (int i = 0; i < lastCol + 1; i++) {

		String cellText = smartXLSHelper.getValue(wb, firstAuditCell.getRow1(), i).toLowerCase();
		if (cellText != null && (cellText.contains("audit") || cellText.contains("interim"))) {

		    String endDate = wb.getFormattedText(firstAuditCell.getRow1() + 1, i);

		    dateColumns.put(i, endDate);

		    Period period = Period.findPeriod(company, endDate);

		    if (period != null) {
			// purge old metric values for this period
			purgeMetricValues(period);
			period.remove();
		    }

		    // figure out year, quarter

		    PeriodType periodType = PeriodType.INTERIM;
		    if (cellText != null && cellText.contains("audit")) {
			int fyeMonth = Integer.parseInt(endDate.split("/")[0]);
			fiscalYearStart = fyeMonth + 1;
			if (fiscalYearStart > 12)
			    fiscalYearStart = 1;
			periodType = PeriodType.AUDIT;
		    }

		    try {
			// create new period
			int quarter = DateHelper.getQuarter(fiscalYearStart, endDate);

			int thisYear = DateHelper.getYear(fiscalYearStart, endDate);
			if (PeriodType.AUDIT.equals(periodType) && PeriodType.AUDIT.equals(previousPeriodType)
				&& thisYear == year) {
			    quarter = 5; // stub year!

			}

			year = thisYear;

			// delete STUB year for now
			// TODO: handle STUB year, but to do this we'll
			// need
			// to remodel metric values
			period = Period.findPeriod(company, year, quarter);
			if (period != null) {
			    // purge old metric values for this period
			    purgeMetricValues(period);
			    period.remove();
			}

			period = new Period();
			period.setCompany(company);
			period.setEndDate(endDate);
			period.setYear(year);
			period.setQuarter(quarter);
			period.setPeriodType(periodType);
			period.persist();

		    } catch (ParseException e) {
			logger.error("", e);
		    }
		    previousPeriodType = periodType;
		}

	    }

	    // loop through metrics
	    for (Metric metric : metrics) {

		logger.info("METRIC: {} TYPE: {}", metric.getName(), metric.getType());

		int searchColumn = 0;
		int startSearchRow = 0;
		int categoryRow = -1;
		List<Integer> metricRow = null;
		List<Integer> metricRow2 = null;

		if (metric.getParent() != null) {

		    categoryRow = smartXLSHelper.findRowIndex(wb, metric.getParent().getName(), searchColumn,
			    startSearchRow);
		    if (categoryRow > -1) {
			startSearchRow = categoryRow;
		    }
		}
		try {
		    metricRow = smartXLSHelper
			    .findRowIndices(wb, metric.getName(), searchColumn, startSearchRow, false); // just
													// the
													// first
													// row
		} catch (Exception e) {
		    logger.error("", e);
		}

		// if can't find by name, try account code
		if (metricRow == null || metricRow.size() == 0) {
		    try {
			metricRow = smartXLSHelper.findRowIndices(wb, metric.getFullAccountCode(), searchColumn,
				startSearchRow, true);
		    } catch (Exception e) {
			logger.error("", e);
		    }
		}

		if (metricRow != null && metricRow.size() > 0) {

		    Set<Integer> columns = dateColumns.keySet();
		    for (int column : columns) {

			String date = dateColumns.get(column);

			Double value = null;
			String textValue = "";
			for (Integer row : metricRow) {
			    String valueString = null;
			    try {

				valueString = smartXLSHelper.getValue(wb, row, column);
				if (valueString != null && !valueString.equals("")) {
				    if (metric.getType() == MetricType.TEXT) {
					if (textValue.length() > 0) {
					    textValue += ",";
					}
					textValue += valueString;

					// regular numeric metric
				    } else {
					valueString = valueString.replaceAll(" ", "");
					if (valueString.length() > 0) {
					    double d = NumberHelper.getDouble(valueString);
					    if (value == null) {
						value = new Double(0.0);
					    }
					    value += d;
					}
				    }
				}

			    } catch (NumberFormatException e) {
				logger.warn("unable to map " + metric.getName() + " for " + date + " value is ["
					+ valueString + "]");

			    } catch (Exception e) {
				logger.warn("unable to map " + metric.getName() + " for " + date + " value is ["
					+ valueString + "]");
			    }
			}

			Period period = Period.findPeriod(company, date);

			if (period == null) {
			    logger.error("invalid period: " + date);
			} else {
			    if ((textValue != null && textValue.length() > 0) || value != null) {
				MetricValue metricValue = new MetricValue();
				metricValue.setMetric(metric);
				metricValue.setYear(period.getYear());
				metricValue.setQuarter(period.getQuarter());
				metricValue.setCompany(company);
				metricValues.add(metricValue);

				if (metric.getType() == MetricType.TEXT) {
				    metricValue.setTextValue(textValue);
				} else {
				    metricValue.setAmount(value);
				}
			    }
			}
		    }
		}

	    }
	} catch (Exception e) {
	    logger.error("", e);
	}

	return metricValues;
    }

    private void purgeMetricValues(Period period) {
        MetricValue.deleteMetricValues(period.getQuarter(), period.getYear(), period.getCompany(), false, true);
    }

    @Transactional
    // returns true if the report data cache (and value facts) need to be cached (recalculated)
    public boolean refreshMetricValue(Company company, Metric metric, int year, int quarter) {
    	// save the actual value for this quarter right away(to avoid need to
    	// "apply")

    	// first delete old one if there is one already
    	// TODO: enforce uniqueness constraint and simplify this
    	MetricValue oldMetricValue = MetricValue.findMetricValue(quarter, year, company.getId(), metric.getId());

    	List<MetricMap> metricMaps = MetricMap.findMetricMaps(company, metric, year, quarter);
    	if (metricMaps == null || metricMaps.isEmpty()) {
    		if (oldMetricValue !=null)
    		{
    			oldMetricValue.remove();
    			return true;
    			//ReportDataCache.removeAndUpdateValueFacts(company);
    		}
    		return false;
    	}

    	// sum up value
    	Double value = getSum(metricMaps);
    	// get existing value, if any
    	MetricValue metricValue;
    	if (oldMetricValue !=null)
    	{
    		metricValue = oldMetricValue;
    	}
    	else
    	{
    		metricValue = new MetricValue();
    		metricValue.setCompany(company);
    		metricValue.setMetric(metric);
    		metricValue.setQuarter(quarter);
    		metricValue.setYear(year);
    	}
    	metricValue.setAmount(value);
    	metricValue.merge();

    	//ReportDataCache.removeAndUpdateValueFacts(company);
    	return true;
    }

    Double getSum(List<MetricMap> metricMaps) {
	Double value = 0.0;
	for (MetricMap mm : metricMaps) {
	    if (mm.getValueText() != null) {
		Double valueIncrement = NumberHelper.getDouble(mm.getValueText());
		if (mm.isNegate()) {
		    valueIncrement = -valueIncrement;
		}
		value += valueIncrement;

	    }
	}

	return value;
    }

    @Transactional
    public boolean remapMetric(MappedCell[] cells) {
		Metric metric = Metric.findMetric(cells[0].getMetricId());
		Company company = Company.findCompany(cells[0].getCompanyId());
		int year = cells[0].getYear();
		int quarter = cells[0].getQuarter();
		new MetricMap().deleteOlderMetricMaps(company, metric, year, quarter);
		
	    createPeriodIfNotExist(company, year, quarter);	
		final List<Period> periods = Period.findAfterPeriodsByCompany(company, year, quarter);
		// save the actual value for this and further quarters right away
		boolean successRemap = remapOlderPeriods(company, periods, cells, metric);
		return successRemap;
    }
    
    private void createPeriodIfNotExist(Company company, int year, int quarter)
    {
    	Period period = Period.findPeriod(company, year, quarter);
    	if (period == null) {
    	    period = new Period();
    	    period.setCompany(company);
    	    String endDate = DateHelper.getQuarterEndDate(year, quarter, company.getFiscalYearStart());
    	    period.setEndDate(endDate);
    	    period.setQuarter(quarter);
    	    period.setYear(year);
    	    period.persist();
    	}
    }

    private boolean remapOlderPeriods(Company company, List<Period> periods, MappedCell[] cells, Metric metric) {
	// save the actual value for this and further quarters right away(to
	// avoid need to "apply")
	// first delete old one if there is one already
	// TODO: enforce uniqueness constraint and simplify this
	final DocumentDataService docDataService = SpringApplicationContext.getDocumentDataService();
	final DocumentType docType = DocumentType.findDocumentType(DocumentType.DOCUMENT_TYPE_FINANCIAL_STATEMENT_ID);
	Assert.notNull(docType);

	boolean refresh = false;
	boolean successMap = true;
	for (MappedCell cell : cells) {
	    for (Period period : periods) {
		// if there is mapped cell and its text values are
		// empty then don't add it back in.
		if (!"".equals(cell.getLabelText()) && !"".equals(cell.getValueText())) {

		    final List<Document> statementDocs = Document.findDocument(company, docType, period.getYear(),
			    period.getQuarter());
		    if ((statementDocs != null) && (statementDocs.size() > 0)) {
			try {
			    if (statementDocs.size() > 1) {
				logger.warn("more than 1 statement found for company {} in {}", company,
					period.getYear() + "Q" + period.getQuarter());
			    }
			    final Document doc = statementDocs.get(0);
			    final String filename = doc.getFileName();
			    final InputStream is = docDataService.getDocumentData(company.getId(), doc.getId());
			    WorkBook wb = new WorkBook();
			    boolean readExcel = ExcelHelper.readWorkBook(wb, is);
			    if (!readExcel) {
			    	logger.error("Invalid excel file: " + filename);
					continue;
			    }

			    MetricMap metricMap = new MetricMap();
			    metricMap.setCompany(company);
			    metricMap.setMetric(metric);
			    metricMap.setQuarter(period.getQuarter());
			    metricMap.setYear(period.getYear());
			    metricMap.setLabelText(cell.getLabelText());
			    metricMap.setLabelX(cell.getLabelX());
			    metricMap.setLabelY(cell.getLabelY());
			    metricMap.setLabelZ(cell.getLabelZ());
			    metricMap.setValueX(cell.getValueX());
			    metricMap.setValueY(cell.getValueY());
			    metricMap.setValueZ(cell.getValueZ());
			    metricMap.setNegate(cell.isNegate());
			    metricMap.setValuesRight(cell.isValuesRight());
			    if (cell.getFoundAfter() != "") {
			    	metricMap.setFoundAfter(cell.getFoundAfter());
			    }
			    if (cell.getFoundBefore() != "") {
			    	metricMap.setFoundBefore(cell.getFoundBefore());
			    }

			    // search for label in expected cell or below
			    int findAfterRowIndex = -1;
			    int findBeforeRowIndex = -1;
			    
			    int startSearchRow = metricMap.getLabelX();
			    if (metricMap.getFoundBefore() != null) {
			    	findBeforeRowIndex = findAfterBeforeTerm(wb, metricMap.getFoundBefore(), metricMap.getLabelZ());
			    	startSearchRow = 0;
			    }
			    if (metricMap.getFoundAfter() != null) {
			    	findAfterRowIndex = findAfterBeforeTerm(wb, metricMap.getFoundAfter(), metricMap.getLabelZ());
			    	if (findAfterRowIndex != -1) {
			    		startSearchRow = findAfterRowIndex;
			    	}
			    }
			    int labelRowIndex = findLabelRow(wb, metricMap.getLabelText(), metricMap.getLabelZ(), startSearchRow, metricMap.getLabelY());

			    if (labelRowIndex > -1) {
					int valueRowIndex = metricMap.getValueX() + labelRowIndex - metricMap.getLabelX();
					int valueColIndex = metricMap.getValueY();
					if (metricMap.isValuesRight()) {
						valueColIndex = getLastCol(wb, metricMap.getValueZ());
					}
					String valueText = smartXLSHelper.getValue(wb, valueRowIndex, valueColIndex);
					
					if (metricMap.getFoundAfter() != null) {
						if (findAfterRowIndex == -1 || findAfterRowIndex > labelRowIndex) {
							successMap = false;
							continue;
						}
					}
					if (metricMap.getFoundBefore() != null) {
						if (findBeforeRowIndex == -1 || findBeforeRowIndex < labelRowIndex) {
							successMap = false;
							continue;
						}
					}
	
					metricMap.setValueText(valueText);
					metricMap.setLabelX(labelRowIndex);
					metricMap.setValueX(valueRowIndex);
					metricMap.setValueY(valueColIndex);
					metricMap.merge();
			    } else {
			    	successMap = false;
			    }

			    wb.dispose();

			} catch (Exception e) {
			    logger.error("", e);
			}
		    }
		}
		refresh |= refreshMetricValue(company, metric, period.getYear(), period.getQuarter());
	    }
	}
	
	if (refresh) {
		ReportDataCache.removeAndUpdateValueFacts(company);
	}
	
	return successMap;
    }

    /**
     * finds row id of label
     * 
     * @param wb
     * @param evaluator
     * @param labelText
     * @param rowIndex
     * @param columnIndex
     * @param sheetIndex
     * @return label row number or -1 if not found
     */

    private int findLabelRow(WorkBook wb, String labelText, int sheetIndex, int rowIndex, int columnIndex) {
		int offset = 0;
	
		if (labelText == null || labelText == "") {
		    return -1;
		}
	
		int dollarSignIndex = labelText.indexOf("$");
		String currentLabelText = (dollarSignIndex > 0) ? labelText.substring(0, dollarSignIndex): labelText;
	
		try {
		    wb.setSheet(sheetIndex);
		    RangeArea ra = wb.find(rowIndex, columnIndex, currentLabelText, true);
		    if (ra != null) {
			return ra.getRow2();
		    }
		    else{
		    	//hot fix for bug with '*' in labelText
		    	ra = wb.find(rowIndex, columnIndex, currentLabelText, false);
		    	if (ra != null)
		    	{
		    		int resultRow = ra.getRow2();
		    		String findLabelText = wb.getText(resultRow, columnIndex);
		    		if (labelText.equals(findLabelText))
		    		{
		    			return resultRow;
		    		}
		    	}
		    }
		} catch (Exception e) {
		}
	
		logger.warn("could not find " + labelText + " expected row " + rowIndex + ", column " + columnIndex + " tried "
			+ offset + " rows");
		return -1;
    }
    
    private int findAfterBeforeTerm(WorkBook wb, String term, int sheetIndex) {
    	try {
			wb.setSheet(sheetIndex);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
    	
    	final int columnSearchRange = wb.getLastCol();
    	int resultRowIndex = -1;
    	for (int i = 0; i <= columnSearchRange; i++) {
    		resultRowIndex = findLabelRow(wb, term, sheetIndex, 0, i);
    		if (resultRowIndex != -1) {
    			break;
    		}
    	}
    	return resultRowIndex;
    }

    public List<Footnote> mapHistoricFootnotes(String filename, InputStream is, Company company) {

	ArrayList<Footnote> footnotes = new ArrayList<Footnote>();

	WorkBook wb = new WorkBook();

	int columnIndex = 1;
	try {
		ExcelHelper.readWorkBook(wb, is);

	    for (int sheetIndex = 0; sheetIndex < wb.getNumSheets(); sheetIndex++) {
		// for (int sheetIndex = 0; sheetIndex < wb.getNumberOfSheets();
		// sheetIndex++) {

		ReportPage reportPage = null;

		wb.setSheet(sheetIndex);

		String sheetName = wb.getSheetName(sheetIndex);

		if ("Stmt Position".equalsIgnoreCase(sheetName)) {
		    reportPage = ReportPage.POSITION;
		} else if ("Stmt Activities".equalsIgnoreCase(sheetName)) {
		    reportPage = ReportPage.ACTIVITY;
		} else if ("Sum".equalsIgnoreCase(sheetName)) {
		    reportPage = ReportPage.SUMMARY;
		}

		if (reportPage != null) {
		    logger.info("checking sheet {} for footnotes", sheetName);

		    int rowIndex = smartXLSHelper.findRowIndex(wb, "Notes:", columnIndex);
		    logger.info("rowIndex: " + rowIndex);

		    if (rowIndex > -1) {
			for (int irow = rowIndex + 1; irow <= wb.getLastRow(); irow++) {
			    String contents = smartXLSHelper.getValue(wb, irow, columnIndex);

			    if (contents.length() > 2 && contents.charAt(0) == '(' && contents.charAt(2) == ')') {
				logger.info("found footnote: " + contents);
				String indicator = contents.substring(0, 3);

				String footnoteLabelRaw = smartXLSHelper.findCellContentEndingWith(wb, indicator,
					columnIndex);

				logger.info("footnote label raw: " + footnoteLabelRaw);

				if (footnoteLabelRaw != null && footnoteLabelRaw.length() > 3) {

				    String footnoteLabel = footnoteLabelRaw.substring(0, footnoteLabelRaw.indexOf("("))
					    .trim();
				    Footnote footnote = new Footnote();
				    footnote.setReportPage(reportPage);
				    footnote.setComment(contents.substring(3).trim());
				    if (footnote.getComment().length() > 500) {
					footnote.setComment(footnote.getComment().substring(0, 500));
				    }
				    footnote.setCompany(company);
				    footnote.setCreated(new Date());
				    footnote.setLabel(footnoteLabel.trim());
				    footnotes.add(footnote);
				}
			    }
			}
		    }
		}
	    }

	} catch (Exception e) {
	    logger.error("", e);
	}
	return footnotes;
    }
    
    private Boolean isMetricMapNegate(MetricMap metricMap)
    {
    	int year = metricMap.getYear();
    	int quarter = metricMap.getQuarter();    	
    	Metric metric = metricMap.getMetric();
		Company company = metricMap.getCompany();
		List<MetricMap> metricMaps = MetricMap.findMetricMapsBefore(company, metric, year, quarter);
    	Boolean negate = metricMaps.size() > 0;
		for(MetricMap foundMetricMap : metricMaps)
		{
	    	if (!foundMetricMap.isNegate())
	    	{
	    		negate = false;
	    		break;
	    	}
		}
    	return negate;
    }
    
    private int getLastCol(WorkBook wb, int sheetIndex) throws Exception {
    	wb.setSheet(sheetIndex);
    	int lastColLib = wb.getLastCol();
    	int lastRowLib = wb.getLastRow();
    	int lastCol = lastColLib;
    	loops:
    	for (int i = lastColLib; i >= 0; i--) {
    		for (int j = 0; j <= lastRowLib; j++) {
    			String cellValue = smartXLSHelper.getValue(wb, j, i);
    			if (cellValue != null && cellValue != "") {
    				lastCol = i;
    				break loops;
    			}
    		}
    	}
    	return lastCol;
    }
}
