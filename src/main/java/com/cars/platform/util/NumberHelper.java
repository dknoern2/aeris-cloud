package com.cars.platform.util;

public class NumberHelper {

    public static Double getDouble(String s) {

	Double result = 0.0;
	try {
	    s = s.replaceAll(",", "");
	    s = s.replaceAll("\\$", "");
	    s = s.replaceAll(" ", "");
	    if (s.length() > 0)
		result = Double.parseDouble(s);
	} catch (NumberFormatException e) {
	}
	return result;
    }
}
