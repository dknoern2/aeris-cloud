package com.cars.platform.util;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import com.cars.platform.domain.Person;

@Component
public class LoginListener implements ApplicationListener<InteractiveAuthenticationSuccessEvent> {

	@Autowired
	private HttpSession httpSession;

    @Override
    public void onApplicationEvent(InteractiveAuthenticationSuccessEvent event)
    {
        //UserDetails userDetails = (UserDetails) event.getAuthentication().getPrincipal();
        Person person = UserHelper.getCurrentUser();
        httpSession.setAttribute("person", person);
    }
}
