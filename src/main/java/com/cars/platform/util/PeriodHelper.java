package com.cars.platform.util;

import java.util.Collection;

import com.cars.platform.domain.Period;

/**
 * Helper class that exists only so the contained static method can be executed in unit tests with 
 * AnnotationDrivenStaticEntityMockingControl enabled.
 * @author kurtl
 *
 */
public class PeriodHelper {
	
    public static Period getMostRecent(Collection<Period> periods) {
	if (periods == null) {
	    throw new IllegalArgumentException("The periods argument is required");
	}
	Period mostRecentAuditPeriod = null;
	for (Period period : periods) {
	    if (period.getQuarter() < Period.LAST_QUARTER) {
		continue;
	    }
	    if ((mostRecentAuditPeriod == null)
		    || (mostRecentAuditPeriod.getYear() < period.getYear())
		    || ((mostRecentAuditPeriod.getYear() == period.getYear()) && (mostRecentAuditPeriod.getQuarter() < period
			    .getQuarter()))) {
		mostRecentAuditPeriod = period;
	    }
	}

	return mostRecentAuditPeriod;
    }
    
}
