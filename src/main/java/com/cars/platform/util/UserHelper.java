package com.cars.platform.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.cars.platform.domain.Person;

public class UserHelper {

    public static Person getCurrentUser() {

	Person person = null;

	Object principal = SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	UserDetails userDetails = null;
	if (principal instanceof UserDetails) {
	    userDetails = (UserDetails) principal;
	    String username = userDetails.getUsername();
	    person = Person.findPeopleByUsernameEquals(username);
	}

	return person;
    }

}
