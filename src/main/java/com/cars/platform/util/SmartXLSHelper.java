package com.cars.platform.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import B.WB;

import com.smartxls.RangeArea;
import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;

public class SmartXLSHelper {
	Logger log = Logger.getLogger("SmartXLSHelper");

	DecimalFormat decimalFormat = new DecimalFormat("#,##");
	SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
	NumberFormat percentFormat = NumberFormat.getPercentInstance();

	public SmartXLSHelper() {
		percentFormat.setMinimumFractionDigits(0);
		percentFormat.setMaximumFractionDigits(0);
	}

	public int findRowIndex(WorkBook wb, String text, int columnIndex) {
		return findRowIndex(wb, text, columnIndex, 0);
	}

	public int findRowIndex(WorkBook wb, String text, int columnIndex, int startSearchRow) {
		int rowIndex = -1;
		List<Integer> rowIndices = findRowIndices(wb, text, columnIndex, startSearchRow, false);
		if (rowIndices != null && rowIndices.size() > 0) {
			rowIndex = rowIndices.get(0);
		}
		return rowIndex;
	}

	public List<Integer> findRowIndices(WorkBook wb, String text, int columnIndex) {
		return findRowIndices(wb, text, columnIndex, 0, true);
	}

	public List<Integer> findRowIndices(WorkBook wb, String text, int columnIndex, int startSearchRow, boolean all) {

		ArrayList<Integer> rowIndex = new ArrayList<Integer>();

		if (startSearchRow > wb.getLastRow()) {
			// System.out.println("out of bound");
			return rowIndex;
		}

		for (int row = startSearchRow; row <= wb.getLastRow(); row++) {
			try {
				short cellType = wb.getType(row, columnIndex);
				if (cellType == wb.TypeEmpty) {
					// skip
				} else if (cellType == wb.TypeNumber) {
					try {
						double number = Double.parseDouble(text);
						double contents = wb.getNumber(row, columnIndex);

						if (Math.abs(contents - number) < .001) {
							rowIndex.add(row);
							if (!all)
								return rowIndex;
						}
					} catch (Exception ignore) {
					}
				} else if (cellType == wb.TypeText) {
					if (text.equals(wb.getText(row, columnIndex))) {
						rowIndex.add(row);
						if (!all)
							return rowIndex;
					}
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return rowIndex;
	}

	public void setValue(WorkBook wb, int rowIndex, int columnIndex, Integer value) {
		try {
			wb.setNumber(rowIndex, columnIndex, value);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void setValue(WorkBook wb, int rowIndex, int columnIndex, String value) {
		try {
			wb.setText(rowIndex, columnIndex, value);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void setValue(WorkBook wb, int rowIndex, int columnIndex, double value) {
		try {
			wb.setNumber(rowIndex, columnIndex, value);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String getValue(WorkBook wb, int rowIndex, int columnIndex) {
		String value = "";
		try {
			if (wb.getType(rowIndex, columnIndex) != WorkBook.TypeEmpty) {
				value = wb.getFormattedText(rowIndex, columnIndex);
				if (wb.getType(rowIndex, columnIndex) == WorkBook.TypeNumber || wb.getType(rowIndex, columnIndex) == -1/* formula */) {
					value = value.replace("(", "-");
					value = value.replace(")", "");
					if (value.lastIndexOf("-") == value.length() - 1) {
						value = "0";
					}
				}
			}
		} catch (Exception e) {
		}
		return value;
	}

	// TODO
	// not user what to do here as smartXLS doe not have a cell object
	// public void getCell(WorkBook wb, int sheetIndex, int rowIndex, int
	// columnINdex){
	//
	// }
	//
	// public void getCell(WorkBook wb, int rowIndex,int columnIndex){
	//
	// }
	//
	// public String evaluateCell(RangeArea cell) {
	// return null;
	// }

	private String formatNumericCellValue(double d, String dataFormatString) {

		String value = "";

		if (dataFormatString.contains("%")) {
			value = percentFormat.format(d);
		} else if ("@".equals(dataFormatString)) {
			// probably just an integer cod, etc,percent commas.
			value = Double.toString(d);
			if (value.endsWith(".0")) {
				value = value.substring(0, value.length() - 2);
			}
		} else {
			value = decimalFormat.format(d);
		}

		return value;
	}

	public String getValue(WorkBook wb, int rowIndex, int columnIndex, int sheetIndex) {
		String value = null;
		try {
			wb.setSheet(sheetIndex);
			value = getValue(wb, rowIndex, columnIndex);
		} catch (Exception e) {
		}
		return value;

	}

	public String findCellContentEndingWith(WorkBook wb, String indicator, int columnIndex) {
		String content = null;
		try {
			short cellType;
			for (int row = 0; row < wb.getLastRow(); row++) {
				if (wb.getType(row, columnIndex) == WorkBook.TypeText) {
					String text = wb.getFormattedText(row, columnIndex);
					text = text.trim();
					if (text.endsWith(indicator)) {
						content = text;
						break;
					}
				}
			}
		} catch (Exception e) {
		}
		return content;
	}

	public int getSheetByName(WorkBook wb, String sheetName) {
		try {
			if (wb != null) {
				for (int i = wb.getNumSheets(); i > -1; --i) {
					if (sheetName.equals(wb.getSheetName(i))) {
						return i;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public void boldCellPrefix(WorkBook wb, int row, int col, int length) throws Exception {
		wb.setActiveCell(row, col);
		wb.setTextSelection(0, length-1);
		RangeStyle rangeStyle = wb.getRangeStyle();
		rangeStyle.setFontBold(true);
		wb.setRangeStyle(rangeStyle);
	}
}