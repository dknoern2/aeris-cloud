package com.cars.platform.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class PropertiesHelper {
	
	private static final Logger logger = LoggerFactory
			.getLogger(PropertiesHelper.class);
	
	private static String ENVIRONMENT_KEY = "Environment";

	@Autowired
	ServletContext servletContext;
	
	Properties properties;
	
	String environment;
	
	@PostConstruct
	private void init() {
		
//		Map<String, String> environments = System.getenv();
//		for (String envVariable : environments.keySet()) {
//			System.out.println("variable: " + envVariable + " value: " + environments.get(envVariable));
//		}
//
//		Properties sysProperties = System.getProperties();
//		for (Object property : sysProperties.keySet()) {
//			System.out.println("property: " + property.toString() + " value: " + sysProperties.get(property).toString());
//		}

		// in AWS create an 'Environment Property' which creates a System Property (not an environment variable)
		environment = System.getenv(ENVIRONMENT_KEY);
		if (null == environment) {
			environment = System.getProperty(ENVIRONMENT_KEY);
			if (null == environment) {
				logger.error("environment variable/system property  '" + ENVIRONMENT_KEY + "' is not set... using dev.properties");
				environment = "dev";
			}
		}
		System.out.println("Environment is: " + environment);
		String fileName = "/WEB-INF/config/" + environment + ".properties";
		FileInputStream is = null;
    	
    	String pathname = servletContext.getRealPath(fileName);
		try {			
			is = new FileInputStream(pathname);

		} catch (FileNotFoundException e) {
			logger.error("Can't find properties file named: " + fileName + ". Is the environment variable or system property 'Environment' set properly?");
			System.exit(1);
		}
		
		properties = new Properties();
		try {
			properties.load(is);
		} catch (IOException e) {
			logger.error("Can't load properties from the file named: " + fileName);
			e.printStackTrace();
		}
		
		setAppliationTimeZone();
	}
	
	private void setAppliationTimeZone() {
		final String timeZone = "America/New_York";
		TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
		System.out.println("Application time zone is set to: " + timeZone);
	}
	
	public Properties getProperties() {
		return properties;
	}

	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	public String getProperty(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue);
	}
	
	public boolean getBooleanProperty(String key) {
		String value = properties.getProperty(key);
		if (null == value) {
			return false;
		}
		return Boolean.parseBoolean(value);
	}
	
	public boolean isProduction() {
		return environment.equals("prod");
	}
	
	public String getCarsDomain() {
		return properties.getProperty("carsDomain");
	}

	public String getPlatformUrl() {
		return properties.getProperty("platformUrl");
	}
	
	public String getMarketingUrl() {
		return properties.getProperty("marketingUrl");
	}


}
