package com.cars.platform.util;

import java.util.Date;
import java.util.List;

import com.cars.platform.domain.Access;
import com.cars.platform.domain.Company;
import com.cars.platform.domain.Person;
import com.cars.platform.domain.ShareFinancials;
import com.cars.platform.domain.Subscription;
import com.cars.platform.domain.SubscriptionType;

public class AuthorizationHelper {

	public final static int ACCESS_TYPE_ANY=0;
	public final static int ACCESS_TYPE_FINANCIALS=1;
	public final static int ACCESS_TYPE_LIBRARY=2;
	public final static int ACCESS_TYPE_PEERGROUPS=4;
	
	
	public static boolean isAuthorized(Person person, Company company) {
		return isAuthorized(person, company, 0);

	}
	
	public static boolean isAuthorized(Person person, Company company, int accessType) {
		List<Subscription> activeSubscriptions = null;
		
		if (null != person && null != person.getCompany()) {
			activeSubscriptions = Subscription.findAllActiveSubscriptionsForSubscriber(person.getCompany());
		}

		return isAuthorized(person, company, accessType, activeSubscriptions);
	}

	
	public static boolean isAuthorized(Person person, Company company, int accessType, List<Subscription> activeSubscriptions) {
		if (person != null && person.isEnabled()) {
			if (person.getCompany().isCARS()) {
				if (null != company && company.isCARS()) {
					return true;
				}
				if (person.isAdmin()) {
					return true;
				}
				else { // Analysts can only see the CDFIs that are assigned to them
					return company.getAnalysts().contains(person);
				}
			}
			
			if (person.getCompany().isCDFI()) {
			    return person.getCompany().getId().equals(company.getId());
			}
			
			// at this point person.company == investor
			
			if (accessType==ACCESS_TYPE_ANY) {
				return true;
			}
			
			if (company != null && accessType==ACCESS_TYPE_FINANCIALS && company.getShareFinancials() == ShareFinancials.NONE) {
				return false;
			}
			
			if (company != null && accessType==ACCESS_TYPE_PEERGROUPS && !company.isPeerGroupAllowed()) {
				return false;
			}
			
			for (Subscription subscription : activeSubscriptions) {
				// with All subscriptions the CDFI doesn't need to be added to the list of access
				// (that is added to the garage) to gain access to Financials or peer groups
				if (subscription.getSubscriptionType().equals(SubscriptionType.ALL)) {
					if (accessType==ACCESS_TYPE_FINANCIALS && subscription.isFinancials() && company.getShareFinancials() == ShareFinancials.ALL) {
						return true;
					}
					if (accessType==ACCESS_TYPE_PEERGROUPS && subscription.isPeerGroups()) {
						return true;
					}
					// Library access can't be part of an ALL subscription
				}
				for (Access access : subscription.getCdfis()) {
					if (access.getCompany().equals(company)) {
						// leaving this check in for FileViewServlet and LibraryExportServlet
						if (accessType==ACCESS_TYPE_ANY) {
						    return true;
						}
// existing code I'm to chicken to get rid of. Nobody uses ACCESS_TYPE_RATINGS
//						if (accessType==ACCESS_TYPE_RATINGS && subscription.isRatings()) {
//							if (subscription.getSubscriptionType().equals(SubscriptionType.SELECT)) {
//								if (access.isActive()) {
//									return true;
//								}
//							}
//							else {
//							    return true;
//							}
//						}
						if (accessType==ACCESS_TYPE_FINANCIALS && subscription.getSubscriptionType().equals(SubscriptionType.SINGLE) && subscription.isFinancials() && company.getShareFinancials() != ShareFinancials.NONE) {
							return true;
						}
						if (accessType==ACCESS_TYPE_PEERGROUPS && subscription.isPeerGroups()) {
							return true;
						}
						if (accessType==ACCESS_TYPE_LIBRARY && subscription.isLibrary()) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}