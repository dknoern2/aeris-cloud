package com.cars.platform.util;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RedirectFilter implements Filter {
	boolean firstTime = true;
	boolean redirect = false;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest req = (HttpServletRequest) request;
	    HttpServletResponse res = (HttpServletResponse) response;
	    
	    if (firstTime) {
			PropertiesHelper propertiesHelper = SpringApplicationContext.getPropertiesHelper();
			redirect = propertiesHelper.getBooleanProperty("redirectHttp");
//		    Enumeration headerNames = req.getHeaderNames();
//		    while (headerNames.hasMoreElements()) {
//		    	String headerName = (String)headerNames.nextElement();
//		    	String headerValue = req.getHeader(headerName);
//		    	System.out.println(headerName + ": " + headerValue);
//		    }
//		    
//		    System.out.println(req.getContextPath());
//		    System.out.println(req.getLocalAddr());
//		    System.out.println(req.getLocalName());
//		    System.out.println(req.getLocalPort());
//		    System.out.println(req.getRemoteAddr());
//		    System.out.println(req.getRemoteHost());
//		    System.out.println(req.getRequestURI());
//		    System.out.println(req.getPathInfo());
//		    System.out.println(req.getPathTranslated());
//		    
//		    System.out.println("-------------------");
		    firstTime = false;
	    }
		
		if (!redirect) {
		    chain.doFilter(request, response);
		    return;
		}
		
	    String protocol = req.getHeader("X-Forwarded-Proto");
	    if (null != protocol && protocol.equals("http")) {
		    String host = req.getHeader("x-forwarded-host");
	    	String requestURI = req.getRequestURI();
    		String redirectUrl = "https://" + host + requestURI;
    		System.out.println("redirecting to: " + redirectUrl);
//		    System.out.println(req.getContextPath());
//		    System.out.println(req.getLocalAddr());
//		    System.out.println(req.getLocalName());
//		    System.out.println(req.getLocalPort());
//		    System.out.println(req.getRemoteAddr());
//		    System.out.println(req.getRemoteHost());
//		    System.out.println(req.getRequestURI());
//		    System.out.println(req.getPathInfo());
//		    System.out.println(req.getPathTranslated());
//		    System.out.println("-------------------");
		    res.sendRedirect(redirectUrl);
    		return;
	    }
	    chain.doFilter(req, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
