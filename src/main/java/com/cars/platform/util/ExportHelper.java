package com.cars.platform.util;

import com.cars.platform.viewmodels.report.Category;
import com.cars.platform.viewmodels.report.Equation;
import static com.cars.platform.service.ReportService.*;


public class ExportHelper {
	private static final String LIQUIDITY_CATEGORY = "Liquidity";
	private static final String OUTPUT_DATA_CATEGORY = "Output Data";
	private static final String OUTCOME_DATA_CATEGORY = "Outcome Data";
	private static final String PORTFOLIO_QUALITY_CATEGORY = "Portfolio Quality";
	private static final String MANAGEMENT_CATEGORY = "Management";
	private static final String EARNINGS_CATEGORY = "Earnings";
	
	private static final String LEVERAGE_EQUATION = "Leverage (Total Debt/Net Assets)";
	
	public static boolean showCH(String sheetName, Category category, Equation equation) {
		if (sheetName.equals(SUMMARY_TAB)) {
			if (equation.getName().equals(LEVERAGE_EQUATION)) {
				return false;
			}
			switch(category.getName()) {
			case LIQUIDITY_CATEGORY:
			case OUTPUT_DATA_CATEGORY:
			case OUTCOME_DATA_CATEGORY:
				return false;
			}
		} else if (sheetName.equals(ADDITIONAL_TAB)) {
			switch(category.getName()) {
			case PORTFOLIO_QUALITY_CATEGORY:
			case MANAGEMENT_CATEGORY:
			case EARNINGS_CATEGORY:
			case LIQUIDITY_CATEGORY:
				return false;
			}
		}
		return true;
	}
}
