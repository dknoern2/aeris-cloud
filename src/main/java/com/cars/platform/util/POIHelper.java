package com.cars.platform.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

public class POIHelper {

	Logger log = Logger.getLogger("POIHelper");
		
	DecimalFormat decimalFormat = new DecimalFormat("#,###");
	SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
	NumberFormat percentFormat = NumberFormat.getPercentInstance();
	
	public POIHelper(){
		percentFormat.setMinimumFractionDigits(0);
		percentFormat.setMaximumFractionDigits(0);		
	}
		
	public int findRowIndex(HSSFSheet sheet, String text, int columnIndex) {
		return findRowIndex(sheet,text,columnIndex,0);
	}	
	
	public int findRowIndex(HSSFSheet sheet, String text, int columnIndex,int startSearchRow) {
		int rowIndex=-1;
		List<Integer> rowIndices = findRowIndices(sheet, text, columnIndex,startSearchRow,false);
		if(rowIndices!=null&&rowIndices.size()>0){
			rowIndex = rowIndices.get(0);
		}
		return rowIndex;
	}		
		
	public List<Integer> findRowIndices(HSSFSheet sheet, String text, int columnIndex) {
		return findRowIndices(sheet, text, columnIndex,0,true);
	}
	
	public List<Integer> findRowIndices(HSSFSheet sheet, String text, int columnIndex,int startSearchRow,boolean all) {
		
		ArrayList<Integer> rowIndex = new ArrayList<Integer>();
				
		if(startSearchRow>sheet.getLastRowNum())return rowIndex;
		
		for (int i = startSearchRow; i <= sheet.getLastRowNum(); i++) {
			HSSFRow row = sheet.getRow(i);
			if (row != null) {
				HSSFCell cell = row.getCell(columnIndex);
				if (cell == null) {
					// skip
				} else if (cell != null
						&& cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					try {
						double number = Double.parseDouble(text);
						double contents = cell.getNumericCellValue();
						if (Math.abs(contents - number) < .001) {
							rowIndex.add(i);
							if(!all)return rowIndex;
						}
					} catch (Exception ignore) {
					}
				} else if (cell != null
						&& cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					if (text.equals(cell.getStringCellValue())) {
						rowIndex.add(i);
						if(!all)return rowIndex;
					}
				}
			}
		}

		return rowIndex;
	}

	public void setValue(HSSFSheet sheet, int rowIndex, int columnIndex,
			Integer value) {
		HSSFRow row = sheet.getRow(rowIndex);
		HSSFCell cell = row.getCell(columnIndex);
		cell.setCellValue(value);
	}

	public void setValue(HSSFSheet sheet, int rowIndex, int columnIndex,
			String value) {
		HSSFRow row = sheet.getRow(rowIndex);
		HSSFCell cell = row.getCell(columnIndex);
		if(cell==null){
			System.out.println("ERROR: can't find cell for row " + rowIndex+", column " + columnIndex);
		}
		cell.setCellValue(value);
	}

	public void setValue(HSSFSheet sheet, int rowIndex, int columnIndex,
			double value) {
		HSSFRow row = sheet.getRow(rowIndex);
		if(row!=null){
		
		
		HSSFCell cell = row.getCell(columnIndex);
		if(cell!=null){
		    cell.setCellValue(value);
		}else{
			log.info("can't set value for cell " +rowIndex +","+columnIndex +": null cell");
		}
		}else{
			log.info("can't set value for cell " +rowIndex +","+columnIndex +": null row");
		}
	}

	public String getValue(HSSFSheet sheet, int rowIndex, int columnIndex) {
		String value = "UNKNOWN";
		HSSFCell cell = getCell(sheet, rowIndex, columnIndex);

		if (cell != null) {
			value = cell.toString();
		}
		return value;

	}

	public HSSFCell getCell(HSSFWorkbook wb, int sheetIndex, int rowIndex, int columnIndex) {
		
		HSSFCell cell = null;
		
		
		HSSFSheet sheet = wb.getSheetAt(sheetIndex);
		
		if(sheet!=null){
		cell = getCell(sheet,rowIndex,columnIndex);
		}
		
		return cell;
	}	
	
	
	
	public HSSFCell getCell(HSSFSheet sheet, int rowIndex, int columnIndex) {
		HSSFCell cell = null;
		if (sheet != null) {
			HSSFRow row = sheet.getRow(rowIndex);
			if (row != null) {
				cell = row.getCell(columnIndex);
			}
		}
		return cell;

	}
		
	public String evaluateCell(HSSFCell cell, FormulaEvaluator evaluator) {
		
		String value = "";
		if (cell != null) {
			
			String dataFormatString = cell.getCellStyle().getDataFormatString();

			// first try to evaluate formula
			try{
			CellValue cellValue = evaluator.evaluate(cell);

			if (cellValue != null) {
				switch (cellValue.getCellType()) {
				case Cell.CELL_TYPE_BOOLEAN:
					value = "" + cellValue.getBooleanValue();
					break;
				case Cell.CELL_TYPE_NUMERIC:
		
					if (dataFormatString.contains("M/D/YYYY")||dataFormatString.contains("m/d/yy")) {	
					    value = sdf.format(cell.getDateCellValue());
					}else{
						value = formatNumericCellValue(cellValue.getNumberValue(),dataFormatString);
					}								
					
					break;
				case Cell.CELL_TYPE_STRING:
					value = cellValue.getStringValue();
					break;
				case Cell.CELL_TYPE_BLANK:
					break;
				case Cell.CELL_TYPE_ERROR:
					break;

				// CELL_TYPE_FORMULA will never happen
				case Cell.CELL_TYPE_FORMULA:
					break;
				}
			}
			}catch(RuntimeException e){
			
				// could be an external reference, try and get "cached" value
				if(e.getMessage()!=null&& e.getMessage().contains("Could not resolve external workbook name")){
				
					    switch (cell.getCellType()) {
					    case Cell.CELL_TYPE_BOOLEAN:
							value = "" + cell.getBooleanCellValue();
							break;
					    case Cell.CELL_TYPE_NUMERIC:
					
						    double d = cell.getNumericCellValue();						
						     value =formatNumericCellValue(d,dataFormatString);
						    
						    break;
					    case Cell.CELL_TYPE_STRING:
							value = cell.getStringCellValue();
							break;
						case Cell.CELL_TYPE_BLANK:
							break;
						case Cell.CELL_TYPE_ERROR:
							break;
						// CELL_TYPE_FORMULA will never happen
						case Cell.CELL_TYPE_FORMULA:
							break;
						}						        					    
				}else{
					throw e;
				}
			}				
		}
		return value;
	}
	
	private String formatNumericCellValue(double d, String dataFormatString) {

		String value = "";

		if (dataFormatString.contains("%")) {
			value = percentFormat.format(d);
		} else if ("@".equals(dataFormatString)) {
			// probably just an integer code, etc, percent commas.
			value = Double.toString(d);
			if (value.endsWith(".0")) {
				value = value.substring(0, value.length() - 2);
			}
		} else {
			value = decimalFormat.format(d);
		}

		return value;

	}

	public String getValue(HSSFWorkbook wb, int rowIndex, int columnIndex,
			int sheetIndex) {
		String value = null;
		HSSFSheet sheet = wb.getSheetAt(sheetIndex);
		if(sheet!=null){
			value = getValue(sheet,rowIndex,columnIndex);
		}
		return value;
	}

	public String findCellContentEndingWith(HSSFSheet sheet, String indicator, int columnIndex) {
		
		String cellContent = null;
		for (int i = 0; i <= sheet.getLastRowNum(); i++) {
			HSSFRow row = sheet.getRow(i);
			if (row != null) {
				HSSFCell theCell = row.getCell(columnIndex);
				if (theCell != null 
						&& theCell.getCellType() == HSSFCell.CELL_TYPE_STRING) {	
					String text = theCell.getStringCellValue();
					text = text.trim();
					if(text.endsWith(indicator)){
						cellContent = text;
						break;
					}
				}								
			}
		}
		
		return cellContent;

	}
}
