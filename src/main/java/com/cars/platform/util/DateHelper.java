package com.cars.platform.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.cars.platform.domain.Period;

public class DateHelper {

	private static SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");

	public static int DeltaQuarters(int year1, int quarter1, int year2, int quarter2){
		int delta = (year1 - year2)*4 + quarter1 - quarter2;
		return delta;
	}

	public static String getCompactFormattedDate(Date date) {
		return sdf.format(date);
	}

	public static Date parseCompactFormattedDate(String date) throws ParseException {
		return sdf.parse(date);
	}

	public static String getQuarterEndDate(int year, int quarter, int fiscalMonthStart) {
		int fiscalMonth = fiscalMonthStart - 1; // calendar API is 0 based

		int day;
		int monthsToAdd = quarter * 3 - 1;

		Calendar cal = GregorianCalendar.getInstance();
		cal.clear();

		if (fiscalMonthStart != 1)
			cal.set(Calendar.YEAR, year - 1);
		else
			cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, fiscalMonth);
		// add 3 months per quarter
		cal.add(Calendar.MONTH, monthsToAdd);

		// figure out the last day in the month and set it.
		day = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, day);

		return sdf.format(cal.getTime());

	}

	public static String getQuarterStartDate(int year, int quarter, int fiscalMonthEnd) {
		int fiscalMonth = fiscalMonthEnd - 1; // calendar API is 0 based

		int day = 1;
		int monthsToAdd = quarter * 3 - 3;

		Calendar cal = GregorianCalendar.getInstance();
		cal.clear();

		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, fiscalMonth);
		// add 3 months per quarter
		cal.add(Calendar.MONTH, monthsToAdd);

		cal.set(Calendar.DAY_OF_MONTH, day);

		return sdf.format(cal.getTime());
	}

	public static int getCurrentYear() {
		Calendar cal = GregorianCalendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		return year;
	}

	public static int getCurrentYear(int fiscalMonthStart) {
		Calendar cal = GregorianCalendar.getInstance();
		return getYear(fiscalMonthStart, cal);
	}

	private static int getYear(int fiscalMonthStart, Calendar cal) {
		int fiscalYear = 0;

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;

		if (fiscalMonthStart == 1 || month < fiscalMonthStart)
			fiscalYear = year;
		else
			fiscalYear = year + 1;

		return fiscalYear;
	}

	public static int getCurrentQuarter(int fiscalMonthStart) {
		Calendar cal = GregorianCalendar.getInstance();
		return getQuarter(fiscalMonthStart, cal);
	}

	private static int getQuarter(int fiscalMonthStart, Calendar cal) {
		int month = cal.get(Calendar.MONTH) + 1;
		if (month < fiscalMonthStart)
			month += 12;
		int quarter = Math.abs((month - fiscalMonthStart) / 3) + 1;
		return quarter;
	}

	public static Calendar getQuarterStart(int fiscalMonthStart, int year, int quarter) {

		if (quarter == 5) {
			quarter = 1;
			year = year + 1;
		}
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.YEAR, year);

		int fyadjust = 1;
		if (fiscalMonthStart != 1)
			fyadjust = 13;
		// / jan==0
		cal.set(Calendar.MONTH, fiscalMonthStart + (quarter - 1) * 3 - fyadjust);

		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		return cal;
	}

	public static int getQuarter(int fiscalMonthStart, String dateString) throws ParseException {

		Date date = sdf.parse(dateString);
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);

		return getQuarter(fiscalMonthStart, cal);
	}

	public static int getYear(int fiscalMonthStart, String dateString) throws ParseException {

		Date date = sdf.parse(dateString);
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);

		return getYear(fiscalMonthStart, cal);
	}

	public static String getTimePassed(Date date) {
		long elapsedSeconds = (System.currentTimeMillis() - date.getTime()) / 1000;
		long elapsedMinutes = (System.currentTimeMillis() - date.getTime()) / 1000 / 60;
		long elapsedHours = (System.currentTimeMillis() - date.getTime()) / 1000 / 60 / 60;
		long elapsedDays = (System.currentTimeMillis() - date.getTime()) / 1000 / 60 / 60 / 24;

		if (elapsedDays == 1)
			return elapsedDays + " day ago";
		else if (elapsedDays > 1)
			return elapsedDays + " days ago";
		else if (elapsedHours == 1)
			return elapsedHours + " hour ago";
		else if (elapsedHours > 1)
			return elapsedHours + " hours ago";
		else if (elapsedMinutes == 1)
			return elapsedMinutes + " minute ago";
		else if (elapsedMinutes > 1)
			return elapsedMinutes + " minutes ago";
		else if (elapsedSeconds == 1)
			return elapsedSeconds + " second ago";
		else
			return elapsedSeconds + " seconds ago";
	}

	/*
	 * Given a date string (3/31/2012) and the fiscal year start month (1-12)
	 * Compute the date string for the previous year end (12/31/2011)
	 */
	public static String getPriorYearEndString(String period, int fiscalMonthStart) throws ParseException {
		int year = getYear(fiscalMonthStart, period) - 1;
		return getQuarterEndDate(year, 4, fiscalMonthStart);
	}

	public static int getDaysUntilDue(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return getDaysUntilDue(calendar);
	}

	
	public static int getDaysUntilDue(Calendar dueDate) {
		// this doesn't work for N days around daylight savings time
		return (int) ((dueDate.getTimeInMillis() - System.currentTimeMillis()) / (1000 * 60 * 60 * 24));
	}

	// get "due in 4 days" or "3 days overdue"
	public static String getDueInString(int days) {
		String s;
		if (days < -1) {
			s = "was due " + Math.abs(days) + " days ago";
		} else if (days == -1) {
			s = "was due yesterday";
		} else if (days == 0) {
			s = "is due today";
		} else if (days == 1) {
			s = "is due tomorrow";
		} else {
			s = "is due in " + days + " days";
		}
		return s;
	}

	public static int getMonthsBetween(String curDateString, String prevDateString) throws ParseException {
		Date curDate = parseCompactFormattedDate(curDateString);
		Date prevDate = parseCompactFormattedDate(prevDateString);
		if (curDate.before(prevDate)) {
			throw new IllegalArgumentException("Current date before previous date: " + curDate + " < " + prevDate);
		}
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		calendar1.setTime(curDate);
		calendar2.setTime(prevDate);

		return (calendar1.get(Calendar.YEAR) * 12 + calendar1.get(Calendar.MONTH))
				- (calendar2.get(Calendar.YEAR) * 12 + calendar2.get(Calendar.MONTH));

	}
	
	
	public static String getFiscalDateString(int year, int quarter) {
		if  (quarter >= Period.LAST_QUARTER) {
			return "FYE " + year;
		}
		else {
			return "Q" + quarter + " " + year;
		}
	}
}
