package com.cars.platform.util;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.web.context.support.XmlWebApplicationContext;


//copied from: // http://jawspeak.com/2010/11/28/spring-slow-autowiring-by-type-getbeannamesfortype-fix-10x-speed-boost-3600ms-to/
//also see https://jira.springsource.org/browse/SPR-6870. Unfortunately upgrading to newer version of spring was not compatible with the version of ROO we are using
//see http://forum.spring.io/forum/spring-projects/roo/737072-is-1-2-4-of-roo-compatible-with-3-2-5-release-of-spring-framework


/**
 * Takes advantage of a spring hook to create a caching Bean Factory. Profiling identified the
 * high amount of reflection in spring 2.5.6 as a very large proportion of CPU time per request.
 * This class metadata should not change, so we cache it.
 */
public class CachingWebApplicationContext extends XmlWebApplicationContext {
	 
    @Override
    protected DefaultListableBeanFactory createBeanFactory() {
        return new CachingByTypeBeanFactory();
    }
}
