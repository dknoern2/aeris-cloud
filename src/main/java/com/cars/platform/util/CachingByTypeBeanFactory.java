package com.cars.platform.util;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;


//http://stackoverflow.com/questions/10949942/spring-injection-via-javax-inject-provider-much-too-slow


// copied from: // http://jawspeak.com/2010/11/28/spring-slow-autowiring-by-type-getbeannamesfortype-fix-10x-speed-boost-3600ms-to/
// also see https://jira.springsource.org/browse/SPR-6870. Unfortunately upgrading to newer version of spring was not compatible with the version of ROO we are using
// see http://forum.spring.io/forum/spring-projects/roo/737072-is-1-2-4-of-roo-compatible-with-3-2-5-release-of-spring-framework
/**
 * Caches certain calls to get bean names for type. Profiling showed that significant cpu time was spent on
 * reflection because this was not cached. It does not change, so should be safe to cache.
 */
public class CachingByTypeBeanFactory extends DefaultListableBeanFactory {

/*	@Override
	public void ignoreDependencyInterface(Class<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void ignoreDependencyType(Class<?> arg0) {
		// TODO Auto-generated method stub

	}
*/


    private static Logger log = Logger.getLogger(CachingByTypeBeanFactory.class);
    ConcurrentHashMap<TypeKey, String[]> cachedBeanNamesForType = new ConcurrentHashMap<TypeKey, String[]>();

    @Override
    public String[] getBeanNamesForType(Class type) {
        return getBeanNamesForType(type, true, true);
    }

    @Override
    public String[] getBeanNamesForType(Class type, boolean includeNonSingletons, boolean allowEagerInit) {
        TypeKey typeKey = new TypeKey(type, includeNonSingletons, allowEagerInit);
        if (cachedBeanNamesForType.containsKey(typeKey)) {
            log.debug("will retrieve from cache: " + typeKey);
            return cachedBeanNamesForType.get(typeKey);
        }
        String[] value = super.getBeanNamesForType(type, includeNonSingletons, allowEagerInit);
        if (log.isDebugEnabled()) {
            log.debug("will add to cache: " + typeKey + " " + Arrays.asList(value));
        }
        cachedBeanNamesForType.putIfAbsent(typeKey, value);
        return value;
    }

    // This is the input parameters, which we memoize.
    // We conservatively cache based on the possible parameters passed in. Assuming that state does not change within the
    // super.getBeanamesForType() call between subsequent requests.
    static class TypeKey {
        Class type;
        boolean includeNonSingletons;
        boolean allowEagerInit;

        TypeKey(Class type, boolean includeNonSingletons, boolean allowEagerInit) {
            this.type = type;
            this.includeNonSingletons = includeNonSingletons;
            this.allowEagerInit = allowEagerInit;
        }

        @Override
        public String toString() {
            return "TypeKey{" + type + " " + includeNonSingletons + " " + allowEagerInit + "}";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TypeKey typeKey = (TypeKey) o;

            if (allowEagerInit != typeKey.allowEagerInit) return false;
            if (includeNonSingletons != typeKey.includeNonSingletons) return false;
            if (type != null ? !type.equals(typeKey.type) : typeKey.type != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = type != null ? type.hashCode() : 0;
            result = 31 * result + (includeNonSingletons ? 1 : 0);
            result = 31 * result + (allowEagerInit ? 1 : 0);
            return result;
        }
    }
}
