package com.cars.platform.util;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Document;
import com.cars.platform.domain.DocumentType;

public class LogoHelper {

	public static Document getCorporateLogo(Company company) {

		Document logo = null;

		try {

			DocumentType corporateLogoDocumentType = DocumentType
					.findDocumentTypesByNameEquals("Corporate Logo")
					.getSingleResult();

			logo = Document.findDocument(company, corporateLogoDocumentType, 0,
					0).get(0);

		} catch (Exception ignore) {
		}

		return logo;
	}
}
