package com.cars.platform.viewmodels;

public class MessagePOJO {
	
	public MessagePOJO(){
		
	}
	
	private String text;
	private String subject;
	private String sentDate;
	private String senderFullName;
	private String recipientFullName;
	private String senderCompanyName;
	private long senderCompanyId;
	private String recipientCompanyName;
	private long recipientCompanyId;
	private long recipientId;
	private long senderId;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSentDate() {
		return sentDate;
	}
	public void setSentDate(String sentDate) {
		this.sentDate = sentDate;
	}
	public String getSenderFullName() {
		return senderFullName;
	}
	public void setSenderFullName(String senderFullName) {
		this.senderFullName = senderFullName;
	}
	public String getRecipientFullName() {
		return recipientFullName;
	}
	public void setRecipientFullName(String recipientFullName) {
		this.recipientFullName = recipientFullName;
	}
	public String getSenderCompanyName() {
		return senderCompanyName;
	}
	public void setSenderCompanyName(String senderCompanyName) {
		this.senderCompanyName = senderCompanyName;
	}
	public long getSenderCompanyId() {
		return senderCompanyId;
	}
	public void setSenderCompanyId(long senderCompanyId) {
		this.senderCompanyId = senderCompanyId;
	}
	public String getRecipientCompanyName() {
		return recipientCompanyName;
	}
	public void setRecipientCompanyName(String recipientCompanyName) {
		this.recipientCompanyName = recipientCompanyName;
	}
	public long getRecipientCompanyId() {
		return recipientCompanyId;
	}
	public void setRecipientCompanyId(long recipientCompanyId) {
		this.recipientCompanyId = recipientCompanyId;
	}
	
	public long getRecipientId() {
		return recipientId;
	}
	public void setRecipientId(long recipientId) {
		this.recipientId = recipientId;
	}
	public long getSenderId() {
		return senderId;
	}
	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}

	
	

}
