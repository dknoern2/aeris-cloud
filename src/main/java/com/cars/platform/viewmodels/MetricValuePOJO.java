package com.cars.platform.viewmodels;

import com.cars.platform.domain.MetricType;

public class MetricValuePOJO {

	public MetricValuePOJO(){
		
	}
	private Double amount;
	private long metricId;
	private String textValue;
	private String name;
	private MetricType type;

	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public long getMetricId() {
		return metricId;
	}
	public void setMetricId(long metricId) {
		this.metricId = metricId;
	}
	public String getTextValue() {
		return textValue;
	}
	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MetricType getType() {
		return this.type;
	}
	public void setType(MetricType type) {
		this.type = type;
	}

}
