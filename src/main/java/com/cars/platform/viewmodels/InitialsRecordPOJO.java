package com.cars.platform.viewmodels;

import java.util.Date;


import com.cars.platform.domain.InitialsRecord;

public class InitialsRecordPOJO {
	
	public InitialsRecordPOJO(InitialsRecord initialsRecord){
		id = initialsRecord.getId();
		reviewId = initialsRecord.getReview().getId();
		documentTypeId = initialsRecord.getDocumentType().getId();
		companyId = initialsRecord.getCompany().getId();
		initials = initialsRecord.getInitials();
		dateStamp = initialsRecord.getDateStamp();
	}
	
	public Long getId(){
		return id;
	}
	public Long getReviewId(){
		return reviewId;
	}
	public Long getDocumentTypeId(){
		return documentTypeId;
	}
	public Long getCompanyId(){
		return companyId;
	}
	public String getInitials(){
		return initials;
	}
	public Date getDateStamp(){
		return dateStamp;
	}
	
	private Long id;
	private Long reviewId;
	private Long documentTypeId;
	private Long companyId;
	private String initials;
	private Date dateStamp;
}
