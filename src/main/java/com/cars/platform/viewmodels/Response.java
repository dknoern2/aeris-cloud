package com.cars.platform.viewmodels;

public class Response {

	private String redirect;

	public Response(String redirect) {
		
		this.redirect = redirect;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

}
