package com.cars.platform.viewmodels;

import java.util.Date;

import com.cars.platform.domain.Note;

public class NotePOJO {
	
	public NotePOJO(Note note){
		id = note.getId();
		reviewId = note.getReview().getId();
		documentTypeId = note.getDocumentType().getId();
		companyId = note.getCompany().getId();
		text = note.getText();
		dateStamp = note.getDateStamp();
		personId = note.getPerson().getId();
	}
	
	public Long getId(){
		return id;
	}
	public Long getReviewId(){
		return reviewId;
	}
	public Long getDocumentTypeId(){
		return documentTypeId;
	}
	public Long getCompanyId(){
		return companyId;
	}
	public String getText(){
		return text;
	}
	public Date getDateStamp(){
		return dateStamp;
	}
	
	private Long id;
	private Long reviewId;
	private Long documentTypeId;
	private Long companyId;
	private String text;
	private Date dateStamp;
	private Long personId;
}
