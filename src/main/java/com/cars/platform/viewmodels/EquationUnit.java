package com.cars.platform.viewmodels;

public class EquationUnit {

	public EquationUnit(){
		
	}
	
	public EquationUnit(String textValue, String numericValue, String type){
		this.textValue = textValue;
		this.numericValue = numericValue;
		this.type = type;
	}
	
	private String textValue;
	private String numericValue;
	private String type;
	public String getTextValue() {
		return textValue;
	}
	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}
	public String getNumericValue() {
		return numericValue;
	}
	public void setNumericValue(String numericValue) {
		this.numericValue = numericValue;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
