package com.cars.platform.viewmodels;

public class SpreadsheetCell {

	private int x;
	private int y;
	private int z;
	private String value;
	
	public SpreadsheetCell(int x, int y, int z,String value) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
		this.value = value;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getZ() {
		return z;
	}
	public void setZ(int z) {
		this.z = z;
	}		
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
