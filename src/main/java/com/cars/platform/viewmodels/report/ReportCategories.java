package com.cars.platform.viewmodels.report;

import java.util.List;

import com.cars.platform.domain.ReportPage;

/** 
 * A wrapper for the list of viewmodel Category instances. Used in the scenario where we need to show/manage the
 * list of all equations for all report categories
 * @author kurtl
 *
 */
public class ReportCategories {
	
	private String name;
	private List<Category> categories;
	
	public ReportCategories(ReportPage reportPage) {
		this.name = reportPage.toString();
	}
	
	
	public String getName() {
		return name;
	}
	
	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
