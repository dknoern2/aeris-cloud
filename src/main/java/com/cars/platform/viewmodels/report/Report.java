package com.cars.platform.viewmodels.report;

import java.util.List;

import com.cars.platform.domain.ReportPage;

public class Report {
	
	private String name;
	private List<String> columns;
	private List<Category> categories;
	
	public Report(ReportPage reportPage) {
		this.name = reportPage.toString();
	}
	
	
	public String getName() {
		return name;
	}
	
	public List<String> getColumns() {
		return columns;
	}

	public List<Category> getCategories() {
		return categories;
	}


	public void setColumns(List<String> columns) {
		this.columns = columns;
	}


	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
