package com.cars.platform.viewmodels.report;

import java.util.List;

import com.cars.platform.domain.ReportCategory;

public class Category {

	private Long id;
	private int code;
	private String name;
	private List<Category> subCategories;
	private List<Equation> datas;
	
	public Category(ReportCategory reportCategory) {
		this.id = reportCategory.getId();
		this.code = reportCategory.getRank();
		this.name = reportCategory.getName();
	}
	
	public Long getId() {
		return id;
	}
	public int getCode() {
		return code;
	}
	public String getName() {
		return name;
	}

	public List<Category> getSubCategories() {
		return subCategories;
	}

	public List<Equation> getDatas() {
		return datas;
	}

	public void setSubCategories(List<Category> subCategories) {
		this.subCategories = subCategories;
	}

	public void setDatas(List<Equation> datas) {
		this.datas = datas;
	}

}
