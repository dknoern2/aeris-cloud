package com.cars.platform.viewmodels.investor;

import java.util.Date;

import com.cars.platform.domain.Document;
import com.cars.platform.util.DateHelper;

import flexjson.JSONSerializer;

public class Report implements Comparable<Report> {
	

	private Document document;
	boolean downloaded;
	
	
	public Report(Document document) {
		this.document = document;
	}
	
	public String getType() {
		return document.getDocumentType().getName();
	}
	public long getDocumentId() {
		return document.getId();
	}
	
	public String getFiscalDate() {
		return DateHelper.getFiscalDateString(document.getFiscalYear(), document.getFiscalQuarter());
	}
	
	public Date getApprovalDate() {
		return document.getApprovedDate();
	}
	
	public String getDocumentDate() {
		return DateHelper.getQuarterEndDate(document.getFiscalYear(),
				document.getFiscalQuarter(), document.getCompany().getFiscalYearStart());
	}
	
	public boolean isDownloaded() {
		return downloaded;
	}
	public void setDownloaded(boolean downloaded) {
		this.downloaded = downloaded;
	}

	@Override
	public int compareTo(Report o) {
		if (null == o) {
			return 1;
		}
		
		return this.getApprovalDate().compareTo(o.getApprovalDate());
	}
	
	@Override
	public String toString() {
	    JSONSerializer serializer = new JSONSerializer();
	    return serializer.serialize( this );
	}

}
