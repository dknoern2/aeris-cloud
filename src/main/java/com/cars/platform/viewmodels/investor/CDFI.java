package com.cars.platform.viewmodels.investor;

import java.util.Set;
import java.util.TreeSet;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.LendingType;
import com.cars.platform.util.DateHelper;

import flexjson.JSONSerializer;

public class CDFI implements Comparable<CDFI> {
	
	private Company cdfi;
	private boolean peerGroupAccess;
	private boolean financialsAccess;
	private boolean libraryAccess;
	private Set<Report> reports;
	
	public CDFI(Company cdfi) {
		this.cdfi = cdfi;
		reports = new TreeSet<Report>();
	}
	
	public long getId() {
		return cdfi.getId();
	}
	public String getName() {
		return cdfi.getName();
	}
	public boolean isRated() {
		return cdfi.isRated();
	}
	
	public boolean isPeerGroupAccess() {
		return peerGroupAccess;
	}
	public boolean isFinancialsAccess() {
		return financialsAccess;
	}
	public boolean isLibraryAccess() {
		return libraryAccess;
	}
	public Set<Report> getReports() {
		return reports;
	}
	public void setPeerGroupAccess(boolean peerGroupAccess) {
		this.peerGroupAccess = peerGroupAccess;
	}
	public void setFinancialsAccess(boolean financialsAccess) {
		this.financialsAccess = financialsAccess;
	}
	public void setLibraryAccess(boolean libraryAccess) {
		this.libraryAccess = libraryAccess;
	}


	@Override
	public int compareTo(CDFI o) {
		if (null == o) {
			return 1;
		}
		return this.getName().compareTo(o.getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdfi == null) ? 0 : cdfi.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CDFI other = (CDFI) obj;
		if (cdfi == null) {
			if (other.cdfi != null)
				return false;
		} else if (!cdfi.getId().equals(other.cdfi.getId()))
			return false;
		return true;
	}
	
	public String getReportColumnText() {
        boolean first = true;
        String ans = "";
        for (Report report : this.reports) {
            if (!first) ans += "<br/>";
            first = false;
            ans += report.getType();
        }
        return ans;
    }
	
	public String getDateColumnText() {
        boolean first = true;
        String ans = "";
        for (Report report : this.reports) {
            if (!first) ans += "<br/>";
            first = false;
            ans += report.getDocumentDate();
        }
        return ans;
    }
	
	
	@Override
	public String toString() {
	    JSONSerializer serializer = new JSONSerializer();
	    return serializer.serialize( this );
	}
}
