package com.cars.platform.viewmodels;

public class PersonPOJO {

	public PersonPOJO(){
		
	}
	
	private String firstName;
	private String lastName;
	private String fullName;
	private long id;
	private long companyId;
	private String companyName;
	private String lastSentDate;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getLastSentDate() {
		return lastSentDate;
	}
	public void setLastSentDate(String lastSentDate) {
		this.lastSentDate = lastSentDate;
	}
	
}
