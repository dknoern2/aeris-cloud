package com.cars.platform.viewmodels;

import java.util.ArrayList;
import java.util.List;

public class CDFISearch {

	public List<String> getStates() {
		return states;
	}
	public void setStates(List<String> states) {
		this.states = states;
	}
	public List<String> getAreaServed() {
		return areaServed;
	}
	public void setAreaServed(List<String> areaServed) {
		this.areaServed = areaServed;
	}
	public List<String> getLendingType() {
		return lendingType;
	}
	public void setLendingType(List<String> lendingType) {
		this.lendingType = lendingType;
	}
	private List<String> states = new ArrayList<String>();
	private List<String> areaServed= new ArrayList<String>();
	private List<String> lendingType= new ArrayList<String>();
	private Long cdfi = 0L;
	
	public Long getCdfi() {
		return cdfi;
	}
	public void setCdfi(Long cdfi) {
		this.cdfi = cdfi;
	}
	public CDFISearch(){
		states.add("All");
		areaServed.add("All");
		lendingType.add("All");
	}
	
	
}
