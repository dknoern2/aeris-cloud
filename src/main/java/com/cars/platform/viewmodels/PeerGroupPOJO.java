package com.cars.platform.viewmodels;

import java.util.List;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.PeerGroup;
import com.cars.platform.domain.PeerGroupFilter;
import com.cars.platform.util.DateHelper;

public class PeerGroupPOJO {
	
	private Long id;
	private String name;
	private Long companyId; // null == global
	private Long cdfiId;    // not null indicates which cdfi is being compared against
	private PeerGroupFilter[] filters; // lists don't serialize out of the box
	private PeerPOJO[] peers;
	private String updated;
	private boolean transitory; // has not been saved by the user
	private Long originalId; // the peer group that a transitory one is modifying, can be null (0)
	
	public PeerGroupPOJO() {		
	}
	
	public PeerGroupPOJO(PeerGroup peerGroup) {
		this.id = peerGroup.getId();
		this.name = peerGroup.getName();
		Company company = peerGroup.getCompany();
		if (null != company) {
			this.companyId = company.getId();
		}
		List<PeerGroupFilter> filters = peerGroup.getFilters();
		if (null != filters) {
		    this.filters = filters.toArray(new PeerGroupFilter[filters.size()]);
		}
		
		List<Company> pgPeers = peerGroup.getPeers();
		this.peers = new PeerPOJO[peerGroup.getPeers().size()];
		for (int i=0; i< pgPeers.size(); i++) {
			this.peers[i] = new PeerPOJO(pgPeers.get(i));
		}
		
		if (peerGroup.getUpdated() != null)
			this.updated = DateHelper.getCompactFormattedDate(peerGroup.getUpdated());
		
		this.transitory = peerGroup.isTransitory();
		if (null != peerGroup.getOriginal()) {
			this.originalId = peerGroup.getOriginal().getId();
		}
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Long getCompanyId() {
		return companyId;
	}
	
	public Long getCdfiId() {
		return cdfiId;
	}

	public PeerGroupFilter[] getFilters() {
		return filters;
	}

	public PeerPOJO[] getPeers() {
		return peers;
	}

	public String getUpdated() {
		return updated;
	}
	
	public boolean isTransitory() {
		return transitory;
	}

	public Long getOriginalId() {
		return originalId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public void setCdfiId(Long cdfiId) {
		this.cdfiId = cdfiId;
	}

	public void setFilters(PeerGroupFilter[] filters) {
		this.filters = filters;
	}

	public void setPeers(PeerPOJO[] peers) {
		this.peers = peers;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public void setTransitory(boolean transitory) {
		this.transitory = transitory;
	}

	public void setOriginalId(Long originalId) {
		this.originalId = originalId;
	}
}
