package com.cars.platform.viewmodels.graph;

import com.cars.platform.domain.GraphType;

public class PieChart extends Chart {

	@Override
	public GraphType getType() {
		return GraphType.PIE;
	}

}
