package com.cars.platform.viewmodels.graph;

import com.cars.platform.domain.GraphType;

public class BarChart extends Chart {

	@Override
	public GraphType getType() {
		return GraphType.COLUMN;
	}

}
