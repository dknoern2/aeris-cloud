package com.cars.platform.viewmodels.graph;

import java.util.List;

import com.cars.platform.domain.BreakdownTable;
import com.cars.platform.domain.GraphType;

public class Table extends Graph {
	
	private Row totals;
	
	public Table() {
	}

	@Override
	public GraphType getType() {
		return GraphType.TABLE;
	}

	public Row getTotals() {
		return totals;
	}

	public void setTotals(Row totals) {
		this.totals = totals;
	}

}
