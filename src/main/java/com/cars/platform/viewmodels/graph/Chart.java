package com.cars.platform.viewmodels.graph;

import com.cars.platform.domain.IEquationFormat;
import com.cars.platform.domain.UnitType;

public abstract class Chart extends Graph implements IEquationFormat {
	
	private UnitType unitType;
	private int decimalPlaces;

	public UnitType getUnitType() {
		return unitType;
	}

	public void setUnitType(UnitType unitType) {
		this.unitType = unitType;
	}

	public Integer getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(Integer decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

}
