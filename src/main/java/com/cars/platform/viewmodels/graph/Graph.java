package com.cars.platform.viewmodels.graph;

import java.util.List;

import com.cars.platform.domain.GraphType;

public abstract class Graph {
	
	protected String title;
	
	protected String date;
	
	protected Row columnHeaders;
	
	protected List<Row> datas;
	
	private String notes;
	
	public Graph() {
	}

	public String getTitle() {
		return title;
	}

	public String getDate() {
		return date;
	}

	public Row getColumnHeaders() {
		return columnHeaders;
	}

	public List<Row> getDatas() {
		return datas;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setColumnHeaders(Row columnHeaders) {
		this.columnHeaders = columnHeaders;
	}

	public void setDatas(List<Row> datas) {
		this.datas = datas;
	}
	
	abstract public GraphType getType();
	
	public String getNotes() {
		return this.notes;
	}
	
	public void setNotes(String notes) {
		this.notes = notes;
	}

}
