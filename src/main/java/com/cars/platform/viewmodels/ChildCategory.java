package com.cars.platform.viewmodels;
import java.util.ArrayList;
import java.util.List;

import com.cars.platform.domain.MetricValue;



public class ChildCategory {
	
	private String name;
	private List<MetricValuePOJO> metricValues;
	
	public ChildCategory(){
		
		metricValues = new ArrayList<MetricValuePOJO>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<MetricValuePOJO> getMetricValues() {
		return metricValues;
	}

	public void setMetricValues(List<MetricValuePOJO> metricValues) {
		this.metricValues = metricValues;
	}

	

}
