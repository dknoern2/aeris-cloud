package com.cars.platform.viewmodels;

import java.util.List;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.LendingType;
import com.cars.platform.domain.SectoralFocus;
import com.cars.platform.domain.ShareFinancials;
import com.cars.platform.domain.State;

public class CdfiPOJO {
	private Long id;
	private String name;
	private String address;
    private String city;
    private String state;
    private String zip;
    private String phone;
    private List<State> areaServed;
    private String description;
    private List<LendingType> lendingType;
    private List<SectoralFocus> sectoralFocus;
    private String rating;
    private boolean isRated;
    private ShareFinancials shareFinancials;
    private boolean peerGroupAllowed;
    private Double totalAssets;
    private Long subscriptionId = 0L;
    
    public CdfiPOJO(Company company) {
    	this.id = company.getId();
    	this.name = company.getName();
    	this.address = company.getAddress();
    	this.city = company.getCity();
    	this.state = company.getState();
    	this.zip = company.getZipString();
    	this.phone = company.getPhone();
    	this.areaServed = company.getAreaServed();
    	this.description = company.getDescription();
    	this.lendingType = company.getLendingType();
    	this.sectoralFocus = company.getSectoralFocus();
    	if (company.isRated()) {
    		this.rating = company.getRating();
    	} else {
    		this.rating = "";
    	}
    	this.isRated = company.isRated();
    	this.shareFinancials = company.getShareFinancials();
    	this.peerGroupAllowed = company.isPeerGroupAllowed();
    	this.totalAssets = company.getTotalAssets();
    }
    
    public Long getId() {
    	return id;
    }
    
    public Long getSubscriptionId() {
    	return this.subscriptionId;
    }
    
    public void setSubscriptionId(Long id) {
    	this.subscriptionId = id;
    }
    
	public String getName() {
		return name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getState() {
		return state;
	}
	
	public String getZip() {
		return zip;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public String getDescription() {
		return description;
	}
	
	public List<State> getAreaServed() {
		return areaServed;
	}
	
	public List<LendingType> getLendingType() {
		return lendingType;
	}
	
	public String getLendingTypeString() {
		StringBuffer sb = new StringBuffer();
		String delim = "";
		for (LendingType type : this.lendingType) {
		    sb.append(delim).append(type.getName());
		    delim = "<br/>";
		}
		return sb.toString();
	}
	
	public String getAreasServedString() {
		StringBuffer sb = new StringBuffer();
		String delim = "";
		for (State type : this.areaServed) {
		    sb.append(delim).append(type.getStateCode());
		    delim = ", ";
		}
		return sb.toString();
	}

	public String getRating() {
		return rating;
	}
	
	public String getRated() {
		if (this.isRated) return "Rated";
		return "Non-Rated";
	}

	public List<SectoralFocus> getSectoralFocus() {
		return sectoralFocus;
	}

	public ShareFinancials getShareFinancials() {
		return shareFinancials;
	}

	public boolean isPeerGroupAllowed() {
		return peerGroupAllowed;
	}

	public Double getTotalAssets() {
		return totalAssets;
	}
}
