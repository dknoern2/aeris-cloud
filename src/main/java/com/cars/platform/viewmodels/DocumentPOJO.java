package com.cars.platform.viewmodels;

import com.cars.platform.domain.Document;

public class DocumentPOJO {
	private Long id;
	private String fileName;	
	private boolean hasAccess = false;
    private boolean canToggleAccess = true;
    private Long categoryId;
    private Long folderId;
    
    public DocumentPOJO() {

	}
    
    public DocumentPOJO(Document doc) {
    	this.id = doc.getId();
    	String name = "";
    	if (doc.getFiscalYear() > 0)
    		name += String.format("%d ", doc.getFiscalYear());
    	if (doc.getFiscalQuarter() > 0)
    		name += String.format("Q%d ", doc.getFiscalQuarter());
    	
    	name += doc.getFileName();
    	this.fileName = name;
    }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public boolean isHasAccess() {
		return hasAccess;
	}
	public void setHasAccess(boolean hasAccess) {
		this.hasAccess = hasAccess;
	}
	public boolean isCanToggleAccess() {
		return canToggleAccess;
	}
	public void setCanToggleAccess(boolean canToggleAccess) {
		this.canToggleAccess = canToggleAccess;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}
	public Long getFolderId() {
		return folderId;
	}
}
