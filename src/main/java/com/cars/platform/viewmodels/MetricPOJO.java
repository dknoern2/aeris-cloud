package com.cars.platform.viewmodels;

import com.cars.platform.domain.MetricType;

public class MetricPOJO {

	
	public MetricPOJO(){
		
	}
	
	private String accountCode;
	private long grandParentId;
	private long id;
	private MetricType type;
	private String name;
	private long parentId;
	private long rank;
	private long companyId;
	private long breakdownId;
	
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public long getGrandParentId() {
		return grandParentId;
	}
	public void setGrandParentId(long grandParentId) {
		this.grandParentId = grandParentId;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public MetricType getType() {
		return type;
	}
	public void setType(MetricType type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getParentId() {
		return parentId;
	}
	public void setParentId(long parentId) {
		this.parentId = parentId;
	}
	public long getRank(){
	    return rank;
	}
	public void setRank(long rank){
	    this.rank = rank;
	}
	public void setCompanyId(long companyId){
	    this.companyId = companyId;	    
	}
	
	public long getCompanyId(){
	    return companyId;
	}
	public long getBreakdownId() {
		return breakdownId;
	}
	public void setBreakdownId(long breakdownId) {
		this.breakdownId = breakdownId;
	}
	
	
	
	

	
	

	
}
