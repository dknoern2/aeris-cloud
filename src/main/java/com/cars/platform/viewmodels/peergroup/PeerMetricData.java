package com.cars.platform.viewmodels.peergroup;

import java.util.ArrayList;
import java.util.List;

public class PeerMetricData {
	private String cdfiName;
	private List<String> values;
	
	
	public PeerMetricData(String cdfiName) {
		super();
		this.cdfiName = cdfiName;
		this.values = new ArrayList<String>();
	}
	
	public String getCdfiName() {
		return cdfiName;
	}
	public List<String> getValues() {
		return values;
	}
}
