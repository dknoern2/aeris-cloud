package com.cars.platform.viewmodels.peergroup;

import com.cars.platform.domain.Equation;

public class PeerMetricBase {

	protected Long equationId;

	protected String metricName;

	public PeerMetricBase() {
		super();
	}
	
	public PeerMetricBase(Equation equation) {
		super();
		this.metricName = equation.getName();
		this.equationId = equation.getId();
	}
	

	public String getMetricName() {
		return metricName;
	}
	
	public Long getEquationId() {
		return equationId;
	}
}