package com.cars.platform.viewmodels.peergroup;

import java.util.Date;

public class Rating {
	
	private String cdfiName;
	private Date date;
	private String impact;
	private String strength;
	
	public Rating(String cdfiName, Date date, String impact, String strength) {
		super();
		this.cdfiName = cdfiName;
		this.date = date;
		this.impact = impact;
		this.strength = strength;
	}
	
	public String getCdfiName() {
		return cdfiName;
	}
	public Date getDate() {
		return date;
	}
	public String getImpact() {
		return impact;
	}
	public String getStrength() {
		return strength;
	}

}
