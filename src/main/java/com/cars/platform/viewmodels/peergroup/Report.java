package com.cars.platform.viewmodels.peergroup;

import java.util.List;

import com.cars.platform.domain.PeerGroupFilter;
import com.cars.platform.viewmodels.graph.PieChart;

public class Report {

	private CDFI cdfi;
	private List<PeerGroupFilter> filters;
	private List<String> dates;
	private List<String> peerNames;
	private List<PieChart> ratings;
	private List<PeerMetricBase> peerMetrics; // in the offline case this will be a PeerMetric
	
	
	
	public CDFI getCdfi() {
		return cdfi;
	}
	public List<PeerGroupFilter> getFilters() {
		return filters;
	}
	public List<String> getDates() {
		return dates;
	}
	public List<String> getPeerNames() {
		return peerNames;
	}
	public List<PieChart> getRatings() {
		return ratings;
	}
	public List<PeerMetricBase> getPeerMetrics() {
		return peerMetrics;
	}
	
	public void setCdfi(CDFI cdfi) {
		this.cdfi = cdfi;
	}
	public void setFilters(List<PeerGroupFilter> filters) {
		this.filters = filters;
	}
	public void setDates(List<String> dates) {
		this.dates = dates;
	}
	public void setPeerNames(List<String> peerNames) {
		this.peerNames = peerNames;
	}
	public void setRatings(List<PieChart> ratings) {
		this.ratings = ratings;
	}
	public void setPeerMetrics(List<PeerMetricBase> peerMetrics) {
		this.peerMetrics = peerMetrics;
	}

	


}
