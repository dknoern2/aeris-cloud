package com.cars.platform.viewmodels.peergroup;

import java.util.List;

import com.cars.platform.domain.Company;
import com.cars.platform.domain.Review;
import com.cars.platform.domain.ReviewType;
import com.cars.platform.util.DateHelper;

/**
 * POJO (JSON) representation of the CDFI being compared in a peer group report
 * @author kurtl
 *
 */
public class CDFI {
	private Company cdfi;
	private List<CdfiCategory> categories;
	
	public CDFI(Company cdfi) {
		this.cdfi = cdfi;
	}

	public String getName() {
		return cdfi.getName();
	}

	public String getLogoUrl() {
		return cdfi.getLogo();
	}

	public String getWebSite() {
		return cdfi.getUrl();
	}

	public List<CdfiCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<CdfiCategory> categories) {
		this.categories = categories;
	}
	
	public String getRating() {
		Review fullreview = Review.findMostRecentReviewByType(cdfi, ReviewType.ANNUAL_RATING);
		Review annualreview = Review.findMostRecentReviewByType(cdfi, ReviewType.ANNUAL);
		
		String full="";
		String annual="";	
		if (fullreview != null)
			full = fullreview.getImpactPerformance() + fullreview.getFinancialStrength() + " " + DateHelper.getCompactFormattedDate(fullreview.getDateOfAnalysis());
		if (annualreview != null)
			annual = annualreview.getImpactPerformance() +"/" + annualreview.getFinancialStrength() + " " + DateHelper.getCompactFormattedDate(annualreview.getDateOfAnalysis());
		return full + "<br/>" + annual;
		
	}

}
