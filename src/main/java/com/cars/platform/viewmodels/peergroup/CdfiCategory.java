package com.cars.platform.viewmodels.peergroup;

import java.util.List;

public class CdfiCategory {
	private String name;
	private List<CdfiMetric> metrics;
	
	public String getName() {
		return name;
	}
	public List<CdfiMetric> getMetrics() {
		return metrics;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMetrics(List<CdfiMetric> metrics) {
		this.metrics = metrics;
	}

}
