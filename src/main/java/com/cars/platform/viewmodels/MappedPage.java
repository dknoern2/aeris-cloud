package com.cars.platform.viewmodels;

import java.util.List;

public class MappedPage {	
	public List<MappedRow> getPage() {
		return page;
	}

	public void setPage(List<MappedRow> page) {
		this.page = page;
	}

	List<MappedRow> page;
}
