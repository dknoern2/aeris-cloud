--- this is not final data for peer groups... but something so that reports show up


-- delete all RCEs for the PeerGroup report
delete from report_category_equation where id in (
	select rceid from (
		select rce.id as rceid from report_category_equation rce, report_category rc where rc.report_page = 6 and rce.report_category = rc.id
	) as x
);

-- delete all the peer group report_categories
delete from report_category where report_page = 6;

-- select * from report_category where report_page = 6 


-- add the new report_category rows
insert into report_category (name, rank, report_page, version)
values
('Capitalization', 0, 6, 0),
('Asset Quality', 1, 6, 0),
('Earnings', 2, 6, 0),
('Liquidity', 3, 6, 0);

-- Capitalization equations
insert into report_category_equation (rank, version, report_category, equation)
values
(
	0, 
	0,
	(select id from report_category where name = 'Capitalization' and report_page = 6),
	(select id from equation where name =  'Total Assets' and company is null)
),
(
	1, 
	0,
	(select id from report_category where name = 'Capitalization' and report_page = 6),
	(select id from equation where name =  'Unrestricted Net Assets/Total Assets' and company is null and formula_category != 5)
),
(
	2, 
	0,
	(select id from report_category where name = 'Capitalization' and report_page = 6),
	(select id from equation where name =  'Total Debt/Total Assets' and company is null)
),
(
	3, 
	0,
	(select id from report_category where name = 'Capitalization' and report_page = 6),
	(select id from equation where name =  "(Unrestricted Net Assets + Loan Loss Reserves)/Gross Loans Receivable" and company is null)
);


-- Asset Quality equations
insert into report_category_equation (rank, version, report_category, equation)
values
(
	0, 
	0,
	(select id from report_category where name = 'Asset Quality' and report_page = 6),
	(select id from equation where name =  "Delinquency (> 90 Days)/Outstandings" and company is null)
);

-- Earnings equations
insert into report_category_equation (rank, version, report_category, equation)
values
(
	0, 
	0,
	(select id from report_category where name = 'Earnings' and report_page = 6),
	(select id from equation where name =  "Self-Sufficiency (Earned Revenue/Expenses)" and company is null)
);

-- Liquidity equations
insert into report_category_equation (rank, version, report_category, equation)
values
(
	0, 
	0,
	(select id from report_category where name = 'Liquidity' and report_page = 6),
	(select id from equation where name =  "Current Ratio (Current Assets/Current Liabilities)" and company is null)
),
(
	1, 
	0,
	(select id from report_category where name = 'Liquidity' and report_page = 6),
	(select id from equation where name =  "Deployment" and company is null)
);


