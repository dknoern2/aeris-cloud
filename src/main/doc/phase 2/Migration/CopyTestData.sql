
---------------------------
-- copy data needed from test 'platform' instance into a separate, temporary migration schema 
---------------------------

CREATE SCHEMA `migration`;

---------------------
-- Breakdown stuff
---------------------

CREATE TABLE migration.metric_breakdown LIKE platform.metric_breakdown;
CREATE TABLE migration.metric_breakdown_item LIKE platform.metric_breakdown_item;
CREATE TABLE migration.breakdown_graph_columns LIKE platform.breakdown_graph_columns;
CREATE TABLE migration.breakdown_graph LIKE platform.breakdown_graph;
CREATE TABLE migration.breakdown_column LIKE platform.breakdown_column;

CREATE TABLE migration.metric_category LIKE platform.metric_category;
CREATE TABLE migration.metric LIKE platform.metric;


INSERT INTO migration.metric_breakdown SELECT * FROM platform.metric_breakdown;
INSERT INTO migration.metric_breakdown_item SELECT * FROM platform.metric_breakdown_item;
INSERT INTO migration.breakdown_column SELECT * FROM platform.breakdown_column;
INSERT INTO migration.breakdown_graph SELECT * FROM platform.breakdown_graph;
INSERT INTO migration.breakdown_graph_columns SELECT * FROM platform.breakdown_graph_columns;

INSERT INTO migration.metric_category
SELECT mc.* FROM platform.metric_category mc, platform.metric_category mcp where mc.parent = mcp.id and mcp.account_code = 50;

INSERT INTO migration.metric SELECT * from platform.metric where breakdown is not null;

---------------------
-- Peer Group Report
---------------------
CREATE TABLE migration.report_category LIKE platform.report_category;
INSERT INTO migration.report_category SELECT * FROM platform.report_category where report_page = 6;


CREATE TABLE migration.report_category_equation LIKE platform.report_category_equation;
INSERT INTO migration.report_category_equation
SELECT rce.* FROM platform.report_category_equation rce, platform.report_category rc
where rce.report_category = rc.id and rc.report_page = 6;

-- TODO this will not account for any CDFI overrides, could simply copy all equations as Paste script only adds new ones
CREATE TABLE migration.equation LIKE platform.equation;
INSERT INTO migration.equation
SELECT e.* FROM platform.equation e, platform.report_category_equation rce, platform.report_category rc
where e.id = rce.equation and rce.report_category = rc.id and rc.report_page = 6;

CREATE TABLE migration.peer_group LIKE platform.peer_group;
CREATE TABLE migration.peer_group_filter LIKE platform.peer_group_filter;
CREATE TABLE migration.peer_group_peers LIKE platform.peer_group_peers;

INSERT INTO migration.peer_group SELECT * FROM platform.peer_group where transitory = 0 and company is null;
INSERT INTO migration.peer_group_peers
SELECT pgp.* FROM platform.peer_group_peers pgp, platform.peer_group pg
where pgp.peer_group = pg.id and pg.transitory = 0 and pg.company is null;
INSERT INTO migration.peer_group_filter
SELECT pgf.* FROM platform.peer_group_filter pgf, platform.peer_group pg
where pgf.peer_group = pg.id and pg.transitory = 0 and pg.company is null;


-----------------------
-- CDFI Company attributes
-----------------------
CREATE TABLE migration.company LIKE platform.company;
INSERT INTO migration.company select * from platform.company where company_type = 0;
CREATE TABLE migration.company_sectoral_focus LIKE platform.company_sectoral_focus;
INSERT INTO migration.company_sectoral_focus select * from platform.company_sectoral_focus;



