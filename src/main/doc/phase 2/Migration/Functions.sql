----------
--- Create a single subscription for financials and/or ratings
----------

DELIMITER $$
CREATE DEFINER=CURRENT_USER FUNCTION `create_single_subscription`(p_owner bigint(20), p_cdfi bigint(20), p_date datetime, p_ratings bit(1), p_financials bit(1)) RETURNS int(11)
BEGIN

declare subscription_id bigint(20);
declare subscription_access_id bigint(20);
declare cdfi_access_id bigint(20);

INSERT INTO subscription
(
`version`,
`expiration_date`,
`subscription_type`,
`owner`,
`financials`,
`library`,
`checked`,
`ratings`,
`opinions`,
`peer_groups`,
`annual_reviews`,
`show_peer_metrics`)
VALUES
(1, p_date, 2, p_owner, p_financials, 0, 0, p_ratings, 0, 0, p_ratings, 0);

set subscription_id = last_insert_id();

INSERT INTO access
(`version`,
`company`)
VALUES
(1, p_owner);

set subscription_access_id = last_insert_id();

INSERT INTO access
(`version`,
`company`)
VALUES
(1, p_cdfi);

set cdfi_access_id = last_insert_id();

INSERT INTO subscription_subscribers
(`subscription`,
`subscribers`)
VALUES
(subscription_id, subscription_access_id);

INSERT INTO subscription_cdfis
(`subscription`,
`cdfis`)
VALUES
(subscription_id, cdfi_access_id);




RETURN 1;
END$$
DELIMITER ;




----------
--- Delete a subscription_cdfis row and its associated Access table
----------

DELIMITER $$
CREATE DEFINER=CURRENT_USER FUNCTION `delete_subscription_cdfis`(p_id bigint(20)) RETURNS int(11)
BEGIN

DELETE subscription_cdfis, access
FROM subscription_cdfis INNER JOIN access ON subscription_cdfis.cdfis = access.id
WHERE subscription_cdfis.subscription = p_id;

RETURN ROW_COUNT();
END$$
DELIMITER ;





----------
--- Delete a subscription_subscribers row and its associated Access table
----------

DELIMITER $$
CREATE DEFINER=CURRENT_USER FUNCTION `delete_subscription_subscribers`(p_id bigint(20)) RETURNS int(11)
BEGIN

DELETE subscription_subscribers, access
FROM subscription_subscribers INNER JOIN access ON subscription_subscribers.subscribers = access.id
WHERE subscription_subscribers.subscription = p_id;

RETURN ROW_COUNT();
END$$
DELIMITER ;





----------
--- Delete a Subscription entry
----------

DELIMITER $$
CREATE DEFINER=CURRENT_USER FUNCTION `delete_subscription`(p_id bigint(20)) RETURNS int(11)
BEGIN

declare delete_count int(11);

set delete_count = delete_subscription_subscribers(p_id);
set delete_count = delete_count + delete_subscription_cdfis(p_id);

/*
have to delete the subscription separately
delete from subscription where id = p_id;

set delete_count = delete_count + row_count();
*/

RETURN delete_count;
END$$
DELIMITER ;




----------
--- Migrate graph_formulas
----------

DELIMITER $$
CREATE DEFINER=CURRENT_USER FUNCTION `migrate_graph_equations`(p_id bigint(20)) RETURNS int(11)
BEGIN


insert into graph_equation (version, rank, graph, equation)
select 0, @curRow := @curRow + 1 as row_number, g.id, gf.formulas
from graph_formulas gf, graph g 
JOIN    (SELECT @curRow := 0) r
where gf.graph = g.id and g.id = p_id;
/*
insert into graph_equation (version, rank, graph, equation)
select 0, @curRow := @curRow + 1 as row_number, p_id, gf.formulas
JOIN  (SELECT @curRow := 0) r
from graph_formulas gf where gf.id = p_id
*/

RETURN ROW_COUNT();
END$$
DELIMITER ;

