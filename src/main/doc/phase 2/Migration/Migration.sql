

-- Migration of relationship between equations and categories into a separate table
-- TODO remove equation.formula_category, equation.code, and drop table formula_category
-- note that code has been renamed to rank which could really start at zero in increase. We maintain the legacy code values
-- as a matter of convenience here.
-- We're only migrating Additional and Summary pages here... A graph can use any formula

--  1. This creates the override relationship between equations by looking at rows with the same code
update equation e1, equation e2
set e1.override = e2.id where e1.code = e2.code and e1.company is not null and e2.company is null;

-- 2. FormulaCategory --> ReportCategory
INSERT INTO report_category (id, name, rank, report_page, version, parent)  
SELECT id, name, code, report_page, version, parent
  FROM formula_category fc
 WHERE fc.report_page = 2 or fc.report_page = 3;

-- 3. ReportCategoryEquation, skip creating relationships for the overrides set in step 1
INSERT INTO report_category_equation (rank, version, equation, report_category)
 SELECT e.code, 0, e.id, e.formula_category
  FROM equation e, formula_category fc
 WHERE e.formula_category = fc.id and (fc.report_page = 2 or fc.report_page = 3) and e.override is null;



----------
-- Update of new peerGroupAllowed and shareFinancial booleans
----------
UPDATE company
SET
`peer_group_allowed` = 1,
`share_financials` = 1
WHERE company_type = 0;


-------
-- New CDFI attribute Sectoral Focus (actually labeled Impact Area)
---------

INSERT INTO sectoral_focus
(`id`, `name`,`version`)
VALUES
(1, "Small Business/Co-op Growth", 1),
(2, "Environment/Energy/TOD", 1),
(3, "Sustainable Agriculture/Fisheries", 1),
(4, "Healthcare", 1),
(5, "Food Access/Healthy Food", 1),
(6, "Affordable Housing", 1),
(7, "Job Creation", 1),
(8, "Racial & Ethnic Minorities", 1),
(9, "Education", 1),
(10, "Women", 1);

-- Changes to Lending type

UPDATE lending_type
SET
`name` = "Housing – Development"
WHERE `name` = "Housing";

INSERT INTO lending_type
(`name`,`version`)
VALUES
("Housing – Home Financing", 1),
("Commercial Real Estate ", 1);

------------------
-- Subscription Migration
------------------

-- TODO set Access dates to null except for SELECT

-- THREE_PACK is going away, create single subscriptions instead
CALL migrate_threepack();

-- Financials move from REPORTING (on CDFI) to SINGLE (on Subscriber)
call migrate_financials();

-- clean up the libraray and rating flags for Select and Reporting
-- 4 = SELECT
update subscription set
	financials = 0,
	library = 0,
	ratings = 1
where subscription_type = 4;

-- 3 = REPORTING
update subscription set
	financials = 0,
	library = 1,
	ratings = 0
where subscription_type = 3;


-- late breaking development,  annual_reviews was added as a separate field. This goes with all Ratings subscription initially
-- although annual_reviews can now be controlled separately
update subscription set
	annual_reviews = 1
where ratings = 1;

-- Access should have dates only if it is a CDFI tied to a SELECT subscription
update access a, subscription_cdfis sc, subscription s
set a.effective_date = null, a.expiration_date = null
where sc.subscription = s.id and s.subscription_type != 4 and sc.cdfis = a.id;

/* need to decide whether or not to do something about these. the owner is of the wrong type if these return any rows
select s.*, c.name from subscription s, company c
where s.owner = c.id and c.company_type = 0 and (s.subscription_type = 0 or s.subscription_type = 2)

select s.*, c.name from subscription s, company c
where s.owner = c.id and c.company_type = 2 and (s.subscription_type = 3 or s.subscription_type = 4)
*/


-----------------------
--- DocumentType.reportType, plus create the Opinion DocumentType
-----------------------

-- 0 == NONE
update document_type set report_type = 0;

-- 1 == FULL
update document_type dt, document_type_category dtc set dt.report_type = 1
where dt.document_type_category = dtc.id and dtc.name = 'CARS - Report';

-- 3 == ANNUAL
update document_type dt, document_type_category dtc set dt.report_type = 3
where dt.document_type_category = dtc.id and dtc.name = 'CARS - Report' and dt.name = "Annual Review";

-- 2 == OPINION (new)
insert into document_type (frequency, name, version, annual, full, quarterly, document_type_format, one_time, report_type, document_type_category)
 SELECT 0, 'Opinion', 0, 0, 0, 0, 0, 0, 2, dtc.id
  FROM document_type_category dtc
 WHERE dtc.name = 'CARS - Report';


--------------------
-- CARS --> AERIS name change
--------------------

update document_type_category set name = "AERIS - Internal" where name = "CARS - Internal";
update document_type_category set name = "AERIS - Report" where name = "CARS - Report";
update document_type_category set name = "AERIS - Resources" where name = "CARS - Resources";


------------------------
-- graph_formulas --> graph_equations (add ordering (rank) of formulas on graphs)
------------------------


select migrate_graph_equations(id) from graph;

-- TODO drop table graph_formulas


------------------------
-- Breakdown metric (Additional Graph) template
-- uses ID of 1000 to 'ensure' no collisions with existing rows
------------------------

INSERT INTO document_type
(`id`,
`name`,
`version`,
`annual`,
`full`,
`quarterly`,
`document_type_category`,
`document_type_format`,
`one_time`,
`report_type`)
VALUES
(1000, "Additional Graph Metrics", 0, 1, 0, 0, 1, 1, 0, 0);

insert into document (file_name, fiscal_quarter, fiscal_year, all_analysts, all_subscribers, document_type)
values ("AdditionalGraphTemplate.xlsx", 0, 0, 0, 0, 1000);




------------------------------------------
-- Well known Root Metric Category of 50 for the Additional (Breakdown) Graphs
------------------------------------------

insert into metric_category (account_code, name, version)
values(50, "Breakdowns", 0);


--------------------
-- CARS --> AERIS name change
--------------------

update document_type_category set name = "AERIS - Internal" where name = "CARS - Internal";
update document_type_category set name = "AERIS - Report" where name = "CARS - Report";
update document_type_category set name = "AERIS - Resources" where name = "CARS - Resources";

update company set name = "AERIS" where name = "CARS";

---------- misc SQL ----

/*

select override from equation

select e1.code, e1.id, e1.name, e1.company, e2.id, e2.name, e2.company from Equation e1, equation e2 where e1.code = e2.code and e1.company is not null and e2.company is null


select distinct fc.report_page from equation e, formula_category fc where e.formula_category = fc.id and (fc.report_page != 2 and fc.report_page != 3)

select * from report_category
select * from report_category_equation


select * from formula_category

-- this shows the number of CDFIs that are overriding a specific global equation.
-- This 'override' should be modeled as a direct relationship between the equations themselves, allowing PeerGroup reports
-- to show the same overiding as Summary and Additional reports
select count(rank) as total, rank from report_category_equation group by rank order by total desc
-- the worst offender. total = 43 just like graphs
select * from Equation where code = "26000715" order by company

-- something like this to migrate shared codes to overrides design
select e1.id, e1.name, e1.company, e2.id, e2.name, e2.company from Equation e1, equation e2 where e1.code = e2.code and e1.company is not null and e2.company is null


---------- graph stuff --------------
-- redundant graph definitions due to not having equation overrides
select g.id, g.title, g.company from graph_formulas gf, graph g, equation e1, equation e2
where gf.formulas = e1.id and e1.code = e2.code and e1.company is not null and e2.company is null
and g.id = gf.graph
order by g.id

-- top guy is code 06000 total = 43
select count(code) as total, code from graph group by code order by total desc

-- all the equations for all 43 graphs ordered by company
select g.id, g.title, g.company, e1.id, e1.code, e1.name, e1.company
from equation e1, graph g, graph_formulas gf
where g.code = 06000 and gf.graph = g.id and gf.formulas = e1.id
order by g.company, e1.code

-- same as above but showing by each equation if there is a CDFI override
select g.id as graphID, g.title, g.company as graphCompany, e1.id, e1.code, e1.name as equationName, e1.company as equationCompany, e2.id overridenId, e2.company
from graph g
join graph_formulas gf on g.id = gf.graph
join equation e1 on gf.formulas = e1.id
left join equation e2 on e1.code = e2.code and e2.company is null
where g.code = 06000
order by g.company, e1.code



select distinct company from graph where code = 06000
*/

