Process for database migration
   -1. Backup old production platform database
	0. Create dump of production database (MySQL Workbench --> Data Export)
	1. Create migration schema from test database (CopyTestData.sql)
	2. Create dump of migration schema from step 1 (MySQL Workbench --> Data Export)
	3. Drop platform schema for the new production database (AERIS in Elastic Beanstalk)
	4. Import prod dump from step 0 (MySQL Workbench --> Data Import)
	5. Import migration dump from step 1 (MySQL Workbench --> Data Import)
	6. Restart app server (this updates the platform schema)
	7. Run migration sql (set platform as default schema)
		a. Functions.sql
		b. Procs.sql
		c. Migration.sql
		d. PasteTestData.sql
	8. Load Value Facts (ManagePlatform --> Raw Data)
