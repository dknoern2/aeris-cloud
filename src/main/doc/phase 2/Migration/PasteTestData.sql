/***************
-- Breakdown stuff
****************/

INSERT INTO platform.metric_breakdown SELECT * FROM migration.metric_breakdown;
INSERT INTO platform.metric_breakdown_item SELECT * FROM migration.metric_breakdown_item;
INSERT INTO platform.breakdown_column SELECT * FROM migration.breakdown_column;
INSERT INTO platform.breakdown_graph SELECT * FROM migration.breakdown_graph;
INSERT INTO platform.breakdown_graph_columns SELECT * FROM migration.breakdown_graph_columns;

-- metric_category

CREATE TABLE `migration`.`metric_category_ids` (
  `old_id` BIGINT(20) NOT NULL,
  `new_id` BIGINT(20) NOT NULL);


DELIMITER $$
CREATE DEFINER=CURRENT_USER FUNCTION `migration`.`insert_metric_category`(p_old_id bigint(20), p_account_code varchar(255), p_name varchar(255), p_parent bigint(20)) RETURNS int(11)
BEGIN

declare new_id bigint(20);

INSERT INTO platform.metric_category
(`account_code`,
`name`,
`version`,
`parent`)
VALUES
(p_account_code, p_name, 0, p_parent);

set new_id = last_insert_id();

INSERT INTO `migration`.`metric_category_ids`
(`old_id`, `new_id`)
VALUES
(p_old_id, new_id);


RETURN 1;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=CURRENT_USER PROCEDURE `migration`.`migrate_metric_categories`()
BEGIN

declare new_parent bigint(20);
-- set new_parent = (select new_id from `migration`.`metric_category_breakdown_id`);
set new_parent = (select id from platform.metric_category where parent is null and account_code = 50);

select migration.insert_metric_category(id, account_code, name, new_parent) from migration.metric_category;

END$$
DELIMITER ;

call migration.migrate_metric_categories;

-- metric

INSERT INTO platform.metric
(`account_code`,
`name`,
`version`,
`parent`,
`company`,
`type`,
`rank`,
`breakdown`)
(select m.account_code, m.name, m.version, mcid.new_id, m.company, m.type, m.rank, m.breakdown
from migration.metric m, migration.metric_category_ids mcid where m.parent = mcid.old_id);


---------------------
-- Peer Group Report
---------------------
INSERT INTO platform.report_category SELECT * FROM migration.report_category;
-- insert only new equations
INSERT INTO platform.equation
SELECT m.*
FROM migration.equation m
LEFT JOIN platform.equation p
ON m.id = p.id
WHERE p.id IS NULL;

INSERT INTO platform.report_category_equation SELECT * FROM migration.report_category_equation;

INSERT INTO platform.peer_group SELECT * FROM migration.peer_group;
INSERT INTO platform.peer_group_peers SELECT * FROM migration.peer_group_peers;
INSERT INTO platform.peer_group_filter SELECT * FROM migration.peer_group_filter;

---------------------
-- CDFI company attributes
---------------------
UPDATE platform.company p, migration.company m
   SET p.ein = m.ein,
       p.mission = m.mission,
       p.peer_group_allowed = m.peer_group_allowed,
       p.share_financials = m.share_financials
WHERE p.id = m.id;
INSERT INTO platform.company_sectoral_focus select * from migration.company_sectoral_focus;




