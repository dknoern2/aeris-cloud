
function PeerGrourReportViewModel(options) {	
	var store;
	var dataSource;
	var columns;
	var fields;
	var pojo=null;
	
	var showAllYears=$("#showAllYearsCheckbox").is(':checked');
	var showInterim=$("#showInterimCheckbox").is(':checked');
	var showIncomplete=$("#showIncompleteYearsCheckbox").is(':checked');
	
	var metrics = new Array();
	var accordion;
	var tabpanel;
	var local = this;
	local.stores = new Array();
	local.options = options;
	local.tabpanels = new Array();
	Highcharts.setOptions({
	     colors: ['#C09A4D', '#088096', '#9EB579', '#0D2E3D', '#CBE2A8', '#484848', '#9A9998', '#BCBBB9']
	});
	
	$("#impactDialog").dialog({
		autoOpen: false,
		resizable : false,
		height : 600,
		width:600,
		modal : true,
	});
	
	$("#financialDialog").dialog({
		autoOpen: false,
		resizable : false,
		height : 600,
		width:600,
		modal : true,
	});
	
	if (local.options.globalEditBtn != "undefined") {
		$(local.options.globalEditBtn).click(function() {
			getReport();
		});	
	}
	
	$("#showAllYearsCheckbox").click(function() {	    
		showAllYears = $("#showAllYearsCheckbox").is(':checked');
		if (showAllYears) {
			$("#showIncompleteYearsCheckbox").attr('disabled', true);
			$("#showIncompleteYearsCheckbox").attr('checked', true);
			showIncomplete = true;
		}
		else {
			$("#showIncompleteYearsCheckbox").removeAttr('disabled');
			$("#showIncompleteYearsCheckbox").attr('checked', false);
			showIncomplete = false;
		}
		loadPeerGroup();
	});	
	
	$("#showInterimCheckbox").click(function() {	    
		showInterim=$("#showInterimCheckbox").is(':checked');
		loadPeerGroup();
	});	
	
	$("#showIncompleteYearsCheckbox").click(function() {
		showIncomplete=$("#showIncompleteYearsCheckbox").is(':checked');
		loadPeerGroup();
	});
	
	$("#export").click(function(e) {	
		var active = accordion.getActive();
		var exportRequest = {};
		var equationIds = new Array();
		for (var i=0; i<active.length; i++) {
			var id = $("#divContainer"+active[i]).prev()[0].id;
			equationIds.push(id);
		}
		exportRequest.equationIds = equationIds;
		exportRequest.allYears = showAllYears;
		exportRequest.showInterim = showInterim;
		exportRequest.showIncomplete = showIncomplete;

		var url = options.exportUrl + '&' + decodeURIComponent($.param(exportRequest));
		window.location = url;
		e.preventDefault();
	});
	
	$("#savePeerGroup").click(function() {
		if (pojo == null) return;
		pojo.transitory = false;
		pojo.name = $("#peergroupName").val();
		$.ajax({
			url :  local.options.peergroupServicesUrl + "/" + pojo.id,
			type : "put",
			async : true,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(pojo),
			success : function(data) {
				var id = data;
				window.location = local.options.peergroupsUrl;
			},
			error : function() {
				alert("Error: Could not save peer group.")
			}
		});
	});
	
	loadPeerGroup();
		
	function loadPeerGroup() {
		
		$.ajax({
			url : local.options.peergroupServicesUrl + "/" + local.options.id,
			dataType : 'json',
			async : false,
			cache: false,
			success : function(data) {
				pojo = data;
				$("#peergroupTitle").text(pojo.name);
				$("#peergroupName").val(pojo.name);
			}
		});
		$.ajax({
			url : options.peergroupServicesUrl + "/" + options.id + options.reportDataUrl,
			cache : false,
			data: {
				showIncompleteYears: showIncomplete
			},
			dataType : 'json',
			async : true,
			success : function(data) {
				local.reportData = data;
				$('#pageContainer').show();
				loadData(data);				
			},
			error : function(request, status, error) {
				alert("Error: Could not get report.");
			}
		});
	}
	
	function loadData(data) {
		setReportInfo(data);
		createDataSetForPieCharts(data)
		if (!options.isGlobal)
			createDataSetforPerformanceMetrics(data);
	    drawChart();
	    accordion = $("#analysisTables").togglepanels({"active" : -1, ontoggle: getPeerMetric });
		$(".ui-icon-triangle-1-e").addClass("custom-ui-state-hover");
	   
	}
	
	// Fix for IE - nextSibling should be used instead of nextElementSibling
	function nextElementSibling(el) {
	    if (el.nextElementSibling) return el.nextElementSibling;
	    do { el = el.nextSibling } while (el && el.nodeType !== 1);
	    return el;
	}
	
	function getPeerMetric(arg) {
		if (arg.className.indexOf("ui-accordion-header-active") < 0) { 
			var equationId = arg.id;
			var containerId = nextElementSibling(arg).id.substr(12);
			$.ajax({
				url : options.peergroupServicesUrl + "/" + options.id + "/peerMetric/" + equationId,
				cache : false,
				dataType : 'json',
				data : {
					showIncomplete: showIncomplete
				},
				async : true,
				success : function(data) {
					createDataSetforPeerAnalysis(data, containerId);		
				},
				error : function(request, status, error) {
					alert("Error: Could not get report.");
				}
			});
		}
	}
	
	function showEditMode() {
    	$("#editMode-popup").editMode('show', { 
			isGlobalEdit : true,
        	data : dataSource,
        	readonlyStore: store,
        	columns: columns,
        	fields: fields,
        	companyId: "",
        	globalEquationsUrl: local.options.globalEquationsUrl,
        	overrideEquationUrl: local.options.overrideEquationUrl,
        	sortEquationsUrl: local.options.sortEquationsUrl,
			onSortComplete: function(pageIndex) {
				loadPeerGroup();
			}
    	 });
    }
	
	function getReport(url) {
    	$.ajax({
			url : local.options.getReportUrl,
			cache : false,
			dataType : 'json',
			type: 'get',
			async : true,
			global: true,
			success : function(data) {					
				renderEditableDialog(data);
			},
			error : function(request, status, error) {
				globalDataSource = [];
				alert(error.message);
			}
		});
    }
	
	function renderEditableDialog(data) {
	    fields = new Array();
	    columns = new Array();
	    columns.add({ 
			   text : "",
			   sortable : false,
			   width: 251,
			   locked : true,
			   dataIndex : "company"
		   });
		fields.push("company");
		fields.push("rowInfo");
				 
		var renderer = new TableProvider();
		var result = renderer.render(data, fields, null, null);	
		dataSource = result.dataSource;
	    store = Ext.create('Ext.data.ArrayStore', {
	       	fields: fields,
	       	data: dataSource
	    });
		showEditMode();
	}
	
	function setReportInfo(data) {
		$('#peersCount').html(data.peerNames.length);
		if (data.cdfi != null) {
			$("#cdfiRating").text(data.cdfi.name + " - ").append('<label style="float:right; width:auto;">'+data.cdfi.rating+'</label>');
		}
		var peers ="";
		for (var i=0; i<data.peerNames.length; i++) {
			peers += "- " + data.peerNames[i] + ".\n";
		}
		$('#peersList').attr('title', peers);
		
		$("#criterias").html("");
		var criterias = new Array();
		var filters = new Array();
		if (data.filters.length > 0) {
			for (var i=0; i<data.filters.length; i++) {
				var append=true;
				if (data.filters[i].filter == "TOTAL_ASSETS") {
					if (data.filters[i].values[0] !== undefined && data.filters[i].values[1] !== undefined) 
						criterias.push("$" + data.filters[i].values[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " to " + "$" + data.filters[i].values[1].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
					else
						append=false;
					
				} else { 
					for (var j=0; j<data.filters[i].values.length; j++) {
							criterias.push(data.filters[i].values[j]);
					}
				}
				if (append) {
				var c = criterias.join(", ");
					$("#criterias").append('<i style="display:block;">' + data.filters[i].displayName + ': ' + '<span style="font-style:normal">' + c  + '</span></i>');
				}
				criterias = [];
			}
		} else {
			$("#criterias").append('<span style="display:block;">None</span>')
		}
	}
	
	function createDataSetForPieCharts(data) {
		$("#pie-charts").html("");
		local.pieCharts = new Array();
	    var dataSource = new Array();
	    var x=0;
		for (var i=0; i<data.ratings.length; i++) {
			for (var j=0; j<data.ratings[i].datas.length; j++) {				
				dataSource[x] = new Array();
				dataSource[x].push(data.ratings[i].datas[j].label);					
				for (var l=0; l<data.ratings[i].datas[j].values.length; l++) {
					dataSource[x].push(data.ratings[i].datas[j].values[l]["amount"]);
				}
				x++;						
			}

			local.pieCharts.push({name: data.ratings[i].metricName, values: dataSource, decimalPlaces: data.ratings[i].decimalPlaces, unitType: data.ratings[i].unitType });
			dataSource = new Array();
			x=0;
		}
									 
	}
	
	var firstPeriodColumn=0;
	var indexToFilter = new Array();
	function filterYears() {
		firstPeriodColumn=0;
		indexToFilter = [];
		
		if (!showAllYears) {	
		    var fyeCount =0;
			for(i=local.reportData.dates.length-1;i>=0;i--){
				if(local.reportData.dates[i] != null && (local.reportData.dates[i].startsWith('FYE') || local.reportData.dates[i].startsWith('Rating'))){
					fyeCount++;
					if(fyeCount==5)break
				}
			}
			firstPeriodColumn=i;
		}
		for (var i=0; i < local.reportData.dates.length; i++) {
			if (i<firstPeriodColumn) {
				indexToFilter.push(i);
				continue;
			}
			if (!showInterim && !local.reportData.dates[i].startsWith('FYE') && !local.reportData.dates[i].startsWith('Rating')){
				indexToFilter.push(i);
				continue;
			}
			
		}
	}
	
	
	function createDataSetforPerformanceMetrics(data) {
		$("#grid-metrics").html("");
		$("#gridMetricTitle").html(data.cdfi.name + " Performance Metrics");
		$("#gridMetricTitle").show();
		var fields = new Array();
		var columns = new Array();
		columns.add({ 
			   id: 'firstColumn',
			  // text : data.cdfi.name + " Performance Metrics",
			   sortable : false,
			   width:350,
			   locked: true,
			   align: 'left',
			   dataIndex : "metric"
		   });
		
		fields.add("metric");
		
		filterYears();
		
		for (i=0; i < data.dates.length; i++) {
			if ($.inArray(i, indexToFilter) == -1) {
				var field = "year" + i;
					columns.add({ 
					   id: 'otherColumns' + i,
					   text : data.dates[i],
					   sortable : false,
					   flex:1,
					   align: 'right',
					   dataIndex : field
					});
				fields.add(field);
			}
		}
		
		fields.add("raw");
		
		var dataPerformanceMetrics = new Array();

		var x = 0;
		var y = 0;
		
		for (i=0; i< data.cdfi.categories.length; i++) {
			var category = data.cdfi.categories[i];
			dataPerformanceMetrics[x] = new Array(category.metrics.length + 1);
			dataPerformanceMetrics[x++][0] = "<b>" + category.name + "</b>";											
			
			for (j=0; j<category.metrics.length; j++) {				
				dataPerformanceMetrics[x] = new Array(category.metrics.length + 1);			
				dataPerformanceMetrics[x][0] = "<span style=\"padding:15px\"></span>" + category.metrics[j].metric;
				y++;							
				
				for (z=0; z<category.metrics[j].values.length; z++) {
					if ($.inArray(z, indexToFilter) == -1) {
						dataPerformanceMetrics[x][y++] = category.metrics[j].values[z];						
					}
				}
				dataPerformanceMetrics[x][y] = category.metrics[j].metric
				x++;
				y = 0;
			}			
		}
		
		var store = Ext.create('Ext.data.ArrayStore', {
	        fields: fields,
	        data: dataPerformanceMetrics
	    }); 	
		
		Ext.create('Ext.grid.Panel', {
            store : store,
            columnLines : true,
            rowLines:false,
            selType : 'rowmodel',
            columns : columns,
            height : 200,
            width : $(window).width() - 40,
            renderTo: 'grid-metrics',
            viewConfig: {
                stripeRows: true
            },
            listeners: {
            	selectionchange: function(grid, selected, eOpts) {
            		var index = 0;
            		var active = accordion.getActive();
            		for (var i=0; i<metrics.length; i++) {
            			if (selected[0].data.raw == metrics[i].name)
            				index = metrics[i].index;
            		}
            		
            		for (var i=0; i<active.length; i++) {
            			accordion.toggle($("#divContainer"+active).prev("h4"));
            		}
            		
            		var divToExpand = $("#divContainer"+index).prev("h4");
            		accordion.toggle(divToExpand);
            		
            		$('html, body').animate({
            	        scrollTop: $("#divContainer" + index).offset().top - 50
            	    }, 1000);
            	}
            }
        });	
	}
	
	function createDataSetforPeerAnalysis(data, containerId) {	
		local.data = data;

		var selectedCDFIData = new Array();
		var dataSourceTable = new Array();
		var dataSourceLinechart = new Array();
		
		filterYears();
		var x=0;
		var y=0;
	    if (data != null) {	   	
	    	for(var propt in data){	
	    		var d = data[propt];
	    		if (propt == "table" || propt == "lineChart") {	    		
		    		var type = d.type;	
					var x=0;
					var y=0;
					var property = type == "TABLE" ? "value" : "amount";						
					for (var j=0; j<d.datas.length; j++) {				
						if (type == "TABLE") {
							dataSourceTable[x] = new Array();
							dataSourceTable[x].push(d.datas[j].label);					
							for (var l=0; l<d.datas[j].values.length; l++) {
								if ($.inArray(l, indexToFilter) == -1)
									dataSourceTable[x].push(d.datas[j].values[l][property]);
							}
							x++;						
							if (d.totals != null) {						
								dataSourceTable[x] = new Array();
								dataSourceTable[x].push(d[i].totals.label);
								for (var j=0; j<d[i].totals.values.length; j++) {
									if ($.inArray(j, indexToFilter) == -1)
										dataSourceTable[x].push(d[i].totals.values[j].value);
								}
							}
							
						} else {
							dataSourceLinechart[y] = new Array();
							dataSourceLinechart[y] = { name: d.datas[j].label };	
							dataSourceLinechart[y].data = new Array();
							for (var l=0; l<d.datas[j].values.length; l++) {
								if ($.inArray(l, indexToFilter) == -1)
									dataSourceLinechart[y].data.push(d.datas[j].values[l][property]);
							}
							y++;							
						}
					}
					
					var headers = new Array();
					if (d.columnHeaders != null) {				
						for (var j=0; j<d.columnHeaders.values.length; j++) {
							if ($.inArray(j, indexToFilter) == -1)
								headers.push(d.columnHeaders.values[j].value);
						}				
					}	    	   		    		
	    		} 
	    	}
	    	
	    	local.pairCharTable = {table : {name: data.metricName, values: dataSourceTable, headers: headers, type: "TABLE"},
	              chart : {name: data.metricName, values: dataSourceLinechart, headers: headers, decimalPlaces: data["lineChart"].decimalPlaces, unitType: data["lineChart"].unitType, type: "LINE" }};
	    	
	    	dataSourceTable = new Array();
	    	dataSourceLinechart = new Array();
			x=0;
			y=0;
			
			drawPairChartTable(containerId);
	    }
	}
	
	function drawPairChartTable(containerId) {
		var table = local.pairCharTable.table;
		var columns = new Array();
		var fields = new Array();
		columns.push({ 
			   text : table.name,
			   sortable : false,
			   width:350,
			   locked: true,
			   align : 'left',
			   dataIndex : "col0",
		   });
		fields.push("col0");
		for (i=0; i<table.headers.length; i++) {
			columns.push({ 					   
					   text : table.headers[i],
					   sortable : false,
					   flex:1,
					   align: 'right',
					   dataIndex : "col"+(i+1)
				   });
				fields.push("col"+ (i+1));
		}
		
		var store = Ext.create('Ext.data.ArrayStore', {
			fields: fields,
			data: table.values
		}); 	
		var width = $(window).width() - 100;
		
		$('#table' + containerId).html("");
		var grid = Ext.create('Ext.grid.Panel', {
	        store : store,
	        columnLines : true,
	        rowLines:false,
	        selType : 'rowmodel',
	        columns : columns,
	        height : 300,
	        renderTo: 'table' + containerId,
	        viewConfig: {
	            stripeRows: true,
	        },
	        layout: {
	            type: 'hbox'
	        }
	    });
		local.stores.push({id:containerId, grid:grid});
		   
		var chart = local.pairCharTable.chart;  		
	    var decimalPlaces = chart.decimalPlaces;
	    var tooltip = {valueDecimals:decimalPlaces};
	    if (chart.unitType == "DOLLAR")
	    	tooltip.valuePrefix = "$";
	    else if (chart.unitType == "PERCENTAGE") {
	    	tooltip.formatter = function() {
	    		var s = '<span>' + this.x + '</span><br/>';	
			    return  s + '<span style="color:' + this.series.color+ '">'+this.series.name+'</span>: <b>'+Highcharts.numberFormat(this.y*100, decimalPlaces) +' %'+'</b><br/>';
			}
		}
		
		var isNegative = false;
		var max = 10;
		var maxValueAddDecimals = 10;
		for (var i = 0; i < chart.values.length; i++) {
			for (var j = 0; j < chart.values[i].data.length; j++) {
				var value = chart.values[i].data[j];
				if (value < 0) {
					isNegative = true;
				}
				if ((value * 100) > max) {
					max = value * 100;
				}
			}
		}
		
	    $('#chart' + containerId).html("");
		$('#chart' + containerId).highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: "line", 
	            width: width
	        },
	        title: {
	            text: null
	        },
	        tooltip: tooltip,
	        xAxis: {
	        	categories: chart.headers
	        },
	        yAxis: {
	        	stackLabels: {
	                enabled: true,
	                format: "{total:,." + chart.decimalPlaces + "f}",
	                style: {
	                    fontWeight: 'bold',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                }
	            },
	            labels: {
	            	formatter:function() {
	            		if (chart.unitType == "PERCENTAGE") {
							if (max <= maxValueAddDecimals) {
								return (Math.round(Number(this.value * 100) * 100) / 100) + "%";
							} else {
								return (Math.round(Number(this.value * 100))) + "%";
							}
	            		} else if (chart.unitType == "DOLLAR") {
	            			return "$" + this.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); ;
	            		}
	            		return this.value;
	            	}
	            },
	            title: ""
	        },
	        series: chart.values,
	        credits : {
	            enabled : false
	        }
	    });
		
		var ch = $('#chart' + containerId).highcharts();
		if (!isNegative) {
			ch.yAxis[0].update({ min: 0 });
		}
		
		for (var i=0; i<local.tabpanels.length;i++) {
	 		if (local.tabpanels[i].id == containerId) {
	 			local.tabpanels[i].tabpanel.doLayout();
			}
	 	}
	}

	function drawChart() {	
		// draw pie charts
		var tablewidth = Math.ceil($(window).width() / 2) - 60;
				
		for (var i=0; i<local.pieCharts.length; i++) {
			var chart = local.pieCharts[i];
			var decimalPlaces = chart.decimalPlaces;
		    var tooltip = {valueDecimals:decimalPlaces};
		    if (chart.unitType == "DOLLAR")
		    	tooltip.valuePrefix = "$";
			else if (chart.unitType == "PERCENTAGE") {
			    tooltip.formatter = function() {
			    	var s = '<span>' + this.x + '</span><br/>';	
					return  s + '<span style="color:' + this.series.color+ '">'+this.series.name+'</span>: <b>'+Highcharts.numberFormat(this.y*100, decimalPlaces) +' %'+'</b><br/>';
				}
			}
			
			var ser = [];
			//if (i == 0) {
			//	ser[0] = chart.values[2];
			//	ser[1] = chart.values[1];
			//	ser[2] = chart.values[0];
			//	ser[3] = chart.values[3];
			//} else {
				ser = chart.values.slice();
			//}
		    
			var div = $('<div style="float:left; width:50%; height:400px;"></div>');
			div.attr("id", "pie" + i);
			$("#pie-charts").append(div);
			var pieTitle = i==0  ? 'Distribution Current Impact Performance Ratings' : 'Distribution of Current FSP Ratings';
			$(div).highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		        },
		        title: {
		            text: '<a href="#" id="pieTitle'+i+'">'+pieTitle+'</a>',
		            useHTML: true
		        },
		        tooltip: tooltip,
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
		                }, 
		                showInLegend: true
		            }
		        },		        
		        series: [{
		            type: 'pie',
		            name: 'Value',
		            data: ser
		        }],
		        credits : {
		            enabled : false
		        }
		    });
		}
		
		$('#pieTitle0').click(function(e){
			e.preventDefault();
		    $("#impactDialog").dialog("open");
		});
		
		$('#pieTitle1').click(function(e){
			e.preventDefault();
		    $("#financialDialog").dialog("open");
		});
		
		$('#analysisTables').html("");
		metrics = new Array();
		// draw tables + charts		
		for (var j=0; j<local.reportData.peerMetrics.length; j++) {
			var peerMetric = local.reportData.peerMetrics[j];

		    
			var title = $("<h4 id="+ peerMetric.equationId + ">" + peerMetric.metricName + "</h4>");
			metrics.push({name:peerMetric.metricName, index: j});
			var divContainer = $('<div></div>');
			divContainer.attr("id", "divContainer"+j);
			$("#analysisTables").append(title);
			$("#analysisTables").append(divContainer);
			
			var div = $('<div><!--empty--></div>');
			div.attr("id", "table"+j);
			$("#divContainer" + j).append(div);
				
			
			var div = $('<div><!--empty--></div>');
			div.attr("id", "chart"+j);
			$("#divContainer" + j).append(div);	
			
			tabpanel = Ext.widget('tabpanel', {
	       		renderTo: "divContainer" + j,
	       	 	activeTab: 0,
	       	    plain: true,
	       	   // width: width,
	       	    deferredRender:false,
	       	 	items: [{
		       	 	contentEl:"chart"+j, 
		   	     	title: 'Chart'
	      	  	},{
		      	  	contentEl:"table"+j, 
	       	     	title: 'Table'	            	
	       	 	}],
	       	 	listeners: {
	       	 		tabchange: function( tabPanel, newCard, oldCard, eOpts ) {
	       	 			if (newCard.title == "Table") {
	       	 				var index = parseInt(tabPanel.renderTo.substr(12));
			       	 		for (var i=0; i<local.stores.length;i++) {
			       	 			if (local.stores[i].id == index)
			       	 				local.stores[i].grid.doLayout();
			       	 		}	       	 			
			       	 	}
	       	 		}
	       	 	}
			});
			
			local.tabpanels.add({id: j, tabpanel: tabpanel});
		}
	}
}

$.fn.togglepanels =  function(opts){
    var acc ;

    // Default options
    opts = opts || {
        "active":   0
    };
    
    toggle = function(target) {
        if(opts.ontoggle !== undefined) {
            if(typeof(opts.ontoggle) !== 'function') {
                console.log("opts.ontoggle is not a function");
            }
            else if(opts.ontoggle(target)===false)
                return;
        }
        $(target)
        .toggleClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
        .find("> .ui-icon").toggleClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
        .next().slideToggle();
    };
    
    acc = this.each(function(){
        $(this).addClass("ui-accordion ui-accordion-icons ui-widget ui-helper-reset")
                  .find("h4")
                  .addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom")
                  .hover(function() { $(this).toggleClass("ui-state-hover"); })
                  .prepend('<span class="ui-icon ui-icon-triangle-1-e"></span>')
                  .click(function() {
                      toggle(this);
                    return false;
                  })
                  .next()
                    .addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom")
                    .hide();
    });
    $.each($(this).find("h4"), function(i) {
            if(opts.active !== undefined) {
                if(typeof(opts.active) === 'object') {
                    if(opts.active.indexOf(i) !== -1)
                        toggle(this);
                }
                else if(typeof(opts.active) === 'number') {
                    if(opts.active === i)
                        toggle(this);
                }
            }
    });
    
    this.getActive = function() {
        var isActive = [];
        $.each($(this).find("h4"), function(i) {
            if($(this).hasClass("ui-state-active"))
                isActive.push(i);
        });
        return isActive;
    };
    
    this.toggle = function(target) {
    	toggle(target[0]);
    }
    
    return acc;
  };


var TableProvider = (function() { 
	var ctor = function TableProvider() {}
	
	var traverseCategories = function(dataSource, x, y, n, subCategories, fieldss, obj, showFootnote, footnotes, pageName) {
		if (subCategories == null) return x;
		for (var i=0; i< subCategories.length; i++)	{
			var fn = "";
			if (showFootnote)
				fn = getFootnote(footnotes, subCategories[i].name, obj, pageName);
			
			dataSource[x] = new Array(n);
			dataSource[x][0] = "<span style=\"padding:15px\"></span><b>" + subCategories[i].name + "</b>" + fn;
			dataSource[x++][fieldss.length-1] = { isCategory : true, id : subCategories[i].id };
			for (var j=0; j<subCategories[i].datas.length; j++) {
				if (showFootnote) 
					fn = getFootnote(footnotes, subCategories[i].datas[j].name, obj, pageName);
					
				dataSource[x] = new Array(n);
				var title = "";
				if (subCategories[i].datas[j].definition && subCategories[i].datas[j].definition != "") {
					title = "title=\"" + subCategories[i].datas[j].definition + "\"";
				}
				dataSource[x][0] = "<span style=\"padding:15px\"></span><span " + title + " style=\"width:200px; display: inline-block;\">" + subCategories[i].datas[j].name + fn + "</span>";
				y++;							
				if (subCategories[i].datas[j].values != null) {
					for (var z=0; z<subCategories[i].datas[j].values.length; z++)
						if ($.inArray(z, obj.indexToFilter) == -1)
							dataSource[x][y++] = subCategories[i].datas[j].values[z];
					dataSource[x][y++] = {name: subCategories[i].datas[j].name, formula: subCategories[i].datas[j].formula, isCategory : false, id : subCategories[i].datas[j].id, 
							isGlobal: subCategories[i].datas[j].global, isOverride: subCategories[i].datas[j].override, decimalPlaces: subCategories[i].datas[j].decimalPlaces, 
							unitType: subCategories[i].datas[j].unitType, isSmallerBetter: subCategories[i].datas[j].isSmallBetter, definition: subCategories[i].datas[j].definition };
				}
				else {
					dataSource[x][fieldss.length-1] = {
							name: subCategories[i].datas[j].name, formula: subCategories[i].datas[j].formula,
							isCategory : false, 
							id : subCategories[i].datas[j].id,
							isGlobal: subCategories[i].datas[j].global,
							isOverride: subCategories[i].datas[j].override,
							decimalPlaces: subCategories[i].datas[j].decimalPlaces, 
							unitType: subCategories[i].datas[j].unitType,
							isSmallerBetter: subCategories[i].datas[j].isSmallBetter,
							definition: subCategories[i].datas[j].definition
						};				
				}
				x++;
				y = 0;		
			}
			dataSource[x++] = new Array(n);
			traverseCategories(dataSource, x, y, n, subCategories[i].subCategories, fieldss, obj, showFootnote, footnotes, pageName);	
		}
		return x;
	}
	
	var getFootnote = function(footnotes, label, obj, pageName) {
		if (footnotes !== null && footnotes !== 'undefined') {
			var fn = "";
			for(var m=0; m < footnotes.length; m++){
				
				if(footnotes[m]['reportPage']==pageName && label.trim() == footnotes[m]['label'].trim()){							
		   			var footnoteKey = getFootnoteKey(footnotes[m]['comment'], obj.renderedFootnotes);						    	
		   			if(footnoteKey.length <= 0){
					    footnoteKey = obj.footnoteIndicators[obj.maxFootnoteIndicator];
					    obj.renderedFootnotes[obj.footnoteIndicators[obj.maxFootnoteIndicator]] = footnotes[m]['comment'];
					    obj.maxFootnoteIndicator++;
					}							
		   			fn += " <sup>"+footnoteKey+"</sup>" 
				}					
			}
		}
		return fn;
	}
	
	// look for an already-used footnote comment
	// this will allow the same text to appear just once for multiple items even if it is used more than once.
	var getFootnoteKey = function(comment,renderedFootnotes){
	    var foundKey="";			
        for (var key in renderedFootnotes) {
            if(comment==renderedFootnotes[key]){
            	foundKey=key;
            }
        }
		return foundKey;
	}
 
	ctor.prototype.render = function(data, fieldss, param, footnotes, pageName) {
    	var showFootnote = footnotes != null;
		var obj = param;
    	
		var dataSource = new Array();
		var x=0;
		var y=0;
		if (data == null || data.length == 0){
			dataSource[x] = new Array(fieldss.length);
		}
		else {
			for (var i=0; i<data.categories.length; i++) {		
				var fn="";
				if (showFootnote)	
					fn = getFootnote(footnotes, data.categories[i].name, obj, pageName);	
				
				dataSource[x] = new Array(fieldss.length);
				dataSource[x][0] = "<b>" + data.categories[i].name + "</b>" + fn;
				dataSource[x++][fieldss.length-1] = {isCategory : true, id : data.categories[i].id};
				for (var j=0; j<data.categories[i].datas.length; j++) {
					if (showFootnote)
						fn = getFootnote(footnotes, data.categories[i].datas[j].name, obj, pageName);
				
					dataSource[x] = new Array(fieldss.length);
					var title = "";
					if (data.categories[i].datas[j].definition && data.categories[i].datas[j].definition != "") {
						title = "title=\"" + data.categories[i].datas[j].definition + "\"";
					}
					dataSource[x][0] = "<span style=\"padding:15px\"></span><span " + title + " style=\"width:200px; display: inline-block;\">" + data.categories[i].datas[j].name + fn + "</span>";
					y++;							
					
					if (data.categories[i].datas[j].values != null) {
						for (var z=0; z<data.categories[i].datas[j].values.length; z++) {
							if ($.inArray(z, obj.indexToFilter) == -1)
								dataSource[x][y++] = data.categories[i].datas[j].values[z];
						}
						dataSource[x][y++] = {name: data.categories[i].datas[j].name, formula: data.categories[i].datas[j].formula, isCategory : false, id : data.categories[i].datas[j].id, 
								isGlobal : data.categories[i].datas[j].global, isOverride : data.categories[i].datas[j].override,
								decimalPlaces: data.categories[i].datas[j].decimalPlaces, 
								unitType: data.categories[i].datas[j].unitType, isSmallerBetter: data.categories[i].datas[j].isSmallBetter, definition: data.categories[i].datas[j].definition};					
					}
					else {
						dataSource[x][fieldss.length-1] = {name: data.categories[i].datas[j].name, formula: data.categories[i].datas[j].formula, isCategory : false, id : data.categories[i].datas[j].id,
								isGlobal : data.categories[i].datas[j].global, isOverride : data.categories[i].datas[j].override, decimalPlaces: data.categories[i].datas[j].decimalPlaces, 
								unitType: data.categories[i].datas[j].unitType, isSmallerBetter: data.categories[i].datas[j].isSmallBetter, definition: data.categories[i].datas[j].definition };
					}
					
					x++;
					y = 0;
				}
				dataSource[x++] = new Array(fieldss.length);
				
				x = traverseCategories(dataSource, x, y, fieldss.length, data.categories[i].subCategories, fieldss, obj, showFootnote, footnotes, pageName);
			}	
		}
		
		return {dataSource:dataSource, obj:obj};
	}   
	return ctor;
})();

var GraphProvider = (function() { 
	var ctor = function GraphProvider() {}
	var stores = new Array();
	ctor.prototype.render = function(data, tabIndex, container, isSubscriber) {
		for (var i=0; i<data.length; i++) {
			var type = data[i].type;
		
			var dataSource = new Array();		
			var x=0;
			var y=0;
			var property = type == "TABLE" ? "value" : "amount";						
			for (var j=0; j<data[i].datas.length; j++) {
				dataSource[x] = new Array();
				if (type == "TABLE" || type == "PIE") {
					dataSource[x][y++] = data[i].datas[j].label;					
					for (var l=0; l<data[i].datas[j].values.length; l++) {
						dataSource[x][y++] = data[i].datas[j].values[l][property];
					}
					x++;
					y=0;
				} else {
					dataSource[x] = { name: data[i].datas[j].label };	
					dataSource[x].data = new Array();
					for (var l=0; l<data[i].datas[j].values.length; l++) {
						dataSource[x].data[y++] = data[i].datas[j].values[l][property];
					}
					x++;
					y=0;
				}
			}
			if (type == "TABLE" && data[i].totals != null) {						
				dataSource[x] = new Array();
				dataSource[x][y++] = data[i].totals.label;
				for (var j=0; j<data[i].totals.values.length; j++) {
					dataSource[x][y++] = data[i].totals.values[j].value;
				}
			}
			
			var fields = new Array();				
			var columns = new Array();
			columns.push({ 
				id : "firtColumn" + tabIndex + i,
				//text : data[i].columnHeaders.label,						
		   		sortable : false,
		   		align : 'left',
		   		locked:true,
		   		width: isSubscriber ? 175 : 350,
           		//flex: 1,
           		
				dataIndex : data[i].columnHeaders.label
			});
			fields.push(data[i].columnHeaders.label);
			for (var j=0; j<data[i].columnHeaders.values.length; j++) {
				columns.push({ 
					id : 'otherColumns-'+tabIndex+i + '-' + j,
					text : data[i].columnHeaders.values[j].value,						
		   	   		sortable : false,
		   	   		align : 'right',
           	  		width: 90,
           	     	autoWidth: true,
           		  	flex: 1,
			   		dataIndex : data[i].columnHeaders.values[j].value
				});
				fields.push(data[i].columnHeaders.values[j].value);
			}
										
			var list;
			if (isSubscriber) 
				list = $('<li class="perfgraph"></li>');
			else
				list = $('<li class="perfgraph" style="margin-top:50px;"></li>');
			list.attr("id", "list"+tabIndex+i);
			var title = $("<h4>" + data[i].title + "</h4>")
			var divContainer = $('<div style="width:100%; overflow:hidden; "></div>');
			divContainer.attr("id", "divContainer"+tabIndex+i);
			divContainer.append(title);
			list.append(divContainer);

			$(container).append(list);
				
			var div = $('<div></div>');			
			div.attr("id", "graph"+tabIndex+i);
			$("#divContainer"+tabIndex+ i).append(div);
			if (data[i].notes != null) {
				var note = $('<span style="width:100%; float:left;">* ' + data[i].notes + '</span>');
				$("#divContainer"+tabIndex+ i).append(note);
				if (type != "TABLE")
					$(note).css("border-top", "2px solid #eee");
			}
			if (type == "TABLE") {
				var store = Ext.create('Ext.data.ArrayStore', {
		    		   	fields: fields,
		       			data: dataSource
		    		}); 	
				var width = isSubscriber ? 720 : $(window).width() - 100;
				var grid=Ext.create('Ext.grid.Panel', { 
	       			store : store,
	       	 		columnLines : true,
	       	 		rowLines:false,
	        		selType : 'rowmodel',
	        		columns : columns,
	        		width:width,
	        		renderTo : 'graph'+tabIndex+i,
	        		viewConfig: {
	        			stripeRows: true
	       	 		},
	        		layout: {
	           		 	type: 'hbox'
	        		}
	    		});
				if (isSubscriber) {
					//grid.maxHeight= 200;
				} else {
					grid.minHeight= 60;
					grid.maxHeight= 200;
				}
				
				stores.push({id:i, grid:grid});
			}
				
			
			if (type == "PIE" || type == "COLUMN" || type == "LINE") {					
			   // $(div).width($(window).width() - 180);
			    var decimalPlaces = data[i].decimalPlaces;
			    var tooltip = {valueDecimals:decimalPlaces};
			    if (data[i].unitType == "DOLLAR")
			    	tooltip.valuePrefix = "$";
			    else if (data[i].unitType == "PERCENTAGE")
			    {
			    	tooltip.formatter = function() {
			    		var s = '<span>' + this.x + '</span><br/>';
			    	
			    		return  s + '<span style="color:' + this.series.color+ '">'+this.series.name+'</span>: <b>'+Highcharts.numberFormat(this.y*100, decimalPlaces) +' %'+'</b><br/>';
			    	}
			    }
			    var series = [{name: "Value", data: dataSource}];
			    if (type == "COLUMN" || type == "LINE") {
			    	series = dataSource;
			    }
			    drawChart(div, type.toLowerCase(), tooltip, fields.slice(1,fields.length), series, data[i].decimalPlaces, data[i].unitType);
			}
		}
	}
	
	ctor.prototype.refresh = function(index) {
		for (var i=0; i<stores.length;i++) {
			if (stores[i].id == index)
				stores[i].grid.doLayout();
		}
	}
	
	function drawChart(div, type, tooltip, categories, series, decimalPlaces, unitType) {
		$(div).highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: type, 
	           // spacingLeft: 80
	        },
	        title: {
	            text: null
	        },
	        tooltip: tooltip,
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: true,
	                    color: '#000000',
	                    connectorColor: '#000000',
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
	                }, 
	                showInLegend: true
	            },
	            column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        format: "{point.y:,." + decimalPlaces + "f}",
                        crop:false,
                        overflow :"none",
                    },
                    minPointLength: 0
                }, 
                line: {
                	tooltip: {
                		//pointFormat: '{series.name}: <b>{point.y}</b>'
                	}
                }
	        },
	        xAxis: {
	        	categories: categories
	        },
	        yAxis: { 
	        	stackLabels: {
                    enabled: true,
                    format: "{total:,." + decimalPlaces + "f}",
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                },
                labels: {
                	formatter:function() {
                		if (unitType == "PERCENTAGE") {
                			return (Math.round(Number(this.value * 100))) + "%";
                		} else if (unitType == "DOLLAR") {
                			return "$" + this.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); ;
                		}
                		return this.value;
                	}
                },
                title: ""
	        },
	        series: series,
	        credits : {
	            enabled : false
	        }
	    });
	}
	return ctor;
})();

$(function() {
	var $loading = $('#loading').hide();
    	$(document)
    	  .ajaxStart(function () {
    	    $loading.show();
    	  })
    	  .ajaxStop(function () {
    	    $loading.hide();
    	  })
    	  .ajaxError(function () {
    		$loading.hide();
    	  });
})
