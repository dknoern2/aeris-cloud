	$.fn.togglepanels =  function(opts){
            var acc ;

            // Default options
            opts = opts || {
                "active":   0
            };
            
            toggle = function(target) {
                if(opts.ontoggle !== undefined) {
                    if(typeof(opts.ontoggle) !== 'function') {
                        console.log("opts.ontoggle is not a function");
                    }
                    else if(opts.ontoggle(target)===false)
                        return;
                }
                $(target)
                .toggleClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
                .find("> .ui-icon").toggleClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
                .next().slideToggle();
            };
            
            acc = this.each(function(){
                $(this).addClass("ui-accordion ui-accordion-icons ui-widget ui-helper-reset")
                          .find("h4")
                          .addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom")
                          .hover(function() { $(this).toggleClass("ui-state-hover"); })
                          .prepend('<span class="ui-icon ui-icon-triangle-1-e"></span>')
                          .click(function() {
                              toggle(this);
                            return false;
                          })
                          .next()
                            .addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom")
                            .hide();
            });
            $.each($(this).find("h4"), function(i) {
                    if(opts.active !== undefined) {
                        if(typeof(opts.active) === 'object') {
                            if(opts.active.indexOf(i) !== -1)
                                toggle(this);
                        }
                        else if(typeof(opts.active) === 'number') {
                            if(opts.active === i)
                                toggle(this);
                        }
                    }
            });
            
            this.getActive = function() {
                var isActive = [];
                $.each($(this).find("h4"), function(i) {
                    if($(this).hasClass("ui-state-active"))
                        isActive.push(i);
                });
                return isActive;
            };
            
            this.collapse = function(target) {
            	toggle(target);
            }
            
            return acc;
          };
