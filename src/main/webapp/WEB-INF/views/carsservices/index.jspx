<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<div xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:spring="http://www.springframework.org/tags" xmlns:util="urn:jsptagdir:/WEB-INF/tags/util" version="2.0">
  <jsp:directive.page contentType="text/html;charset=UTF-8"/>
  <jsp:output omit-xml-declaration="yes"/>
  
  <script>
  $(function() {
    $( "#accordion" ).accordion({heightStyle:"content"});
  });
  </script>

<!-- 
<p>Aeris strives to provide you with the most transparent and comprehensive 
ratings reports and financial data available</p>
-->


<div id="accordion">
  <h3>Ratings Services</h3>  
  <div>
  
     <p><b>More Value. More Options.</b></p>
   
     <p>Get more from your relationship with Aeris from these services below. Please contact us for 
     details and to get started.</p>

     <p><b>Subscription Info</b></p>
     
     <p>Aeris Subscriptions include in-depth reporting for each investor-selected CDFI including:</p>

     <ul>
     <li><b>Ratings Report</b>: A comprehensive, 50-60 page CARS&#8482; analysis report with narrative discussion 
     to support each rating and dozens of informative tables and graphs that make it easy for investors 
     to find critical data.</li>
     <li><b>Annual Reviews</b>: An annual review report, which is issued for each of the two years 
     following the rating, provides updated financial information and narrative that highlight 
     significant changes in the CDFI’s impact and financial performance.</li>
     
     
     </ul>
     
     <p>
     <table>
         <tr><th>Subscription Type</th><th>What's Included</th><th>Price</th></tr>
         <tr><td>Annual</td><td>Ratings and reports for all CDFIs issued prior to and during the 12-month
         subscription period</td><td>$15,000<sup>*</sup></td></tr>
         <tr><td>3-pak</td><td>Three years of ratings and reports for any three CDFIs selected over a 12-month period</td><td>$7,000</td></tr>
         <tr><td>Single Purchase</td><td>Three years of ratings and reports for one CDFI</td><td>$2,500</td></tr>
     
     </table>
     </p>
     
     <p><sup>*</sup> Prices vary for access to reports on more than 20 CDFIs.</p>
 
     <p>In addition, investors may request that Aeris rate a specific CDFI outside the ratings schedule. 
     For more information on special pricing, please contact <a
			href="mailto:info@AerisInsight.com?Subject=User%20Feedback">info@AerisInsight.com</a>.</p>
     
     
  </div>
  <h3>Quarterly Financial Statements</h3>
  <div>

     <p>Aeris now offers investors and funders access to standardized quarterly financial statements and 
     analysis on CDFIs in your portfolio. We collect the data, normalize it, and notify you when it is 
     available for download. Save time and money while strengthening your monitoring activities of CDFI 
     loan fund investments. The Aeris Data Platform provides 
     </p>
     
     <ul>
 
       <li>Centralized access to standardized quarterly CDFI financials similar to the 
       FDIC’s call report system for banks.</li>
       <li>Presentation of CDFI loan fund financials using the template you are used 
       to in CARS&#8482; Ratings Reports.</li>
       <li>Data certified by both CARS Inc and the CDFI loan fund</li>
       <li>Access to Aeris standard summary ratios and graphs.</li>
       <li>Data export (including supplemental tables and graphs) to an Excel file for downloading.</li>
    </ul>
 
     <p>Contact <a
			href="mailto:info@AerisInsight.com?Subject=User%20Feedback">info@AerisInsight.com</a> for pricing.
</p>


  </div>
  <h3>CDFI Document Library Access</h3>
  <div>
     <p>CDFIs can now maintain a centralized reporting library on the Aeris Data Platform, 
     providing access to monitoring and due diligence documents for their investors and funders.
     </p>
 
     <p>Aeris notifies the CDFI when quarterly or annual documents are due and notifies you when 
     those documents are ready for download.
     </p>
 
     <p>It’s simple and timely for you and your CDFI partners.</p>
 
     <p>Contact us for more information.</p>
  </div>
  <h3>Custom Analyses</h3>
  <div>
    
    <p>Since 2004, Aeris has provided due diligence and ratings for investors on the impact 
    performance and financial strength of community development financial institutions (CDFIs). 
    As the only ratings agency with expertise in CDFI risk assessment and impact performance 
    analysis, Aeris is uniquely positioned to underwrite risk for the growing number of new 
    investment programs targeting CDFIs. 
    </p>
 
    <p>Aeris now offers a range of services for investors 
    that complement our core ratings product. Our expanded services support a 
    variety of investor needs and investment strategies. 
    </p>
   
   <ul>
 
      <li>Assessment of CDFI creditworthiness</li>
      <li>Customized risk ratings and analyses tied to client-defined benchmarks</li>
      <li>Customized impact performance analyses for both regulated and unregulated 
      institutions tied to client-defined criteria</li>
      <li>Risk assessments of a CDFI’s loan portfolio quality, underwriting, monitoring,
       and management capacity and practices</li>
      <li>Analysis and assessment of CDFI capital strength and liquidity related to specific 
      debt issuances</li>
      <li>Comparative analysis of CDFI performance against CARS-rated peer institutions based 
      on client-defined financial indicators</li>
      <li>Customized financial trend analysis based on the Aeris database and tied to client-defined 
      benchmarks</li>
      <li>Training and Webinars on underwriting CDFI loan funds</li>

    </ul>

  </div>
</div>
 








</div>
